const path = require('path')
const webpack = require('webpack')

const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const FileManagerPlugin = require('filemanager-webpack-plugin')
// const CompressionPlugin = require("compression-webpack-plugin")

const isProd = JSON.stringify(process.env.NODE_ENV)=='"production"'

//Generate Dependency Graph for bundle js
// const Visualizer = require('webpack-visualizer-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
// const dependencyGraphPlugin = [
//         new Visualizer({
//             filename: './BundleVisualizer.html'
//         }),
//         new BundleAnalyzerPlugin({
//             analyzerMode: 'static',
//             reportFilename: './BundleAnalyzer.html',
//             openAnalyzer: false
//         })
//     ]

const webapckPlugin = isProd?[
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }),
    new webpack.optimize.AggressiveMergingPlugin({}),
    new webpack.optimize.OccurrenceOrderPlugin(true),
    new UglifyJsPlugin({
        test: /\.js($|\?)/i,
        sourceMap: false,
        cache: true,
        parallel: true
    }),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    //Generate bundle in gzip format
    // new CompressionPlugin({
    //     asset: "[path].gz[query]",
    //     algorithm: "gzip",
    //     test: /\.js$|\.css$|\.html$/,
    //     threshold: 10240,
    //     minRatio: 0.8
    // }),
    new FileManagerPlugin({
        onStart : [
            {
                delete: [
                    "./dist/prod/"
                ]
            },
            {
                mkdir: [
                    "./dist/prod/"
                ]
            }
        ],
        onEnd: [
            {
                copy: [
                    // cid.js cannot be uglified for IE version <=7
                    { source: "./src/main/proxy/cid.js", destination: "./dist/prod/js/cid.js" },
                    { source: "./cidImageViewer.html", destination: "./dist/prod/cidImageViewer.html" },
                ]
            }
        ]
    })
]:[
    new webpack.DefinePlugin({'process.env.NODE_ENV': JSON.stringify('development')}),
    new FileManagerPlugin({
        onStart : [
            {
                delete: [
                    "./dist/dev/"
                ]
            },
            {
                mkdir: [
                    "./dist/dev/"
                ]
            }
        ],
        onEnd: [
            {
                copy: [
                    { source: "./cidImageViewer.html", destination: "./dist/dev/cidImageViewer.html" },
                ]
            }
        ]
    })
]

var config = {
    entry: {
        'cidImageViewer': './index.js',
        'cidviewerproxy': './src/main/proxy/CidImageViewerProxy.js',
        'cid': './src/main/proxy/cid.js'
	},
    output: {
        path: path.resolve(__dirname, (isProd?'dist/prod':'dist/dev')),
        filename: './js/[name].js',
        libraryTarget: 'var',
        library: ["[name]"]
    },
    devServer: {
		contentBase: './',
		hot: true,
        inline: false, // Set to false to avoid browser auto reload(live reloading) due to any script changed
        port: 7778
    },
    module: {
        rules: [
            {
                enforce: 'pre', // require for showing error after executing $>webpack
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                options: {
                    failOnError: true
                }
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['env', 'react']
                }
            },
            {
                test: /\.(jpe?g|png|gif|svg|cur)$/i,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: 'assets/images/[name].[ext]'
                }
            }
        ]
    },
    resolve: {
        extensions: ['.jsx', '.js']
    },
    devtool: isProd?false:'inline-source-map',
    node: {
        fs: 'empty'
    },
    plugins: [  
        // ...dependencyGraphPlugin,
        ...webapckPlugin
    ]
}
module.exports = config
