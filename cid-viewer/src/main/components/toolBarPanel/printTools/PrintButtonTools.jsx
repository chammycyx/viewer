//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu';

import PrintButton from '@components/toolBarPanel/fileTools/PrintButton'
import { PRINT_MODE } from '@actions/components/toolBarPanel/Action'

import SvgIcon from '@material-ui/core/SvgIcon'
import ListItemIcon from '@material-ui/core/ListItemIcon'

import printArrow from '@assets/images/printArrow.png'
import Divider from '@material-ui/core/Divider'

export const styles: Function = (theme: *): * => ({
    menuItem: {
        fontSize: theme.toolBarPanel.toolBarBtn.printButtonTools.fontSize,
        fontFamily: theme.typography.fontFamily,
        height:"5px",
        width:"120px"
    }
})

type PrintButtonToolsType = CidComponentPropsType & {
    isEnabled: boolean,
    onClick: Function
}

class PrintButtonTools extends Component<PrintButtonToolsType> {
    state: *
    setState: Function
    onPrintButtonClick: Function
    handleClose: Function
    printViewingArea: Function
    printSelectedImages: Function

    constructor(props: *) {
        super(props)

        this.onPrintButtonClick = this.onPrintButtonClick.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.printViewingArea = this.printViewingArea.bind(this)
        this.printSelectedImages = this.printSelectedImages.bind(this)

        this.state = {
            anchorEl : null
        }

    }


    onPrintButtonClick(e: *){
        this.setState({ anchorEl: e.currentTarget });
    }

    printViewingArea(){
        this.props.onClick(PRINT_MODE.PRINT_VIEWING_AREA)
        this.setState({ anchorEl: null })
    }

    printSelectedImages(){
        this.props.onClick(PRINT_MODE.PRINT_SELECTED_IMAGES)
        this.setState({ anchorEl: null })
    }

    handleClose(){
        this.setState({ anchorEl: null })
    }

    render(): Node {
        const classes = this.props.classes
        const { anchorEl } = this.state
        const open = Boolean(anchorEl)
        return (
            <div>
                <PrintButton aria-label="Print" aria-owns={'long-menu'} aria-haspopup="true" toolTipTitle={'Print images'} onClick={this.onPrintButtonClick} isEnabled={this.props.isEnabled} isSelected={false}/>
                <Menu id="long-menu" style={{marginTop:"30px"}} anchorEl={anchorEl} open={open} onClose={this.handleClose}>
                    <MenuItem className={classes.menuItem} onClick={this.printViewingArea}>
                        <ListItemIcon>
                            <SvgIcon style={{position:'relative', left: -17, top: 3}}>
                                <image width={38} height={15} href={printArrow}/>
                            </SvgIcon>
                        </ListItemIcon>
                        <span style={{position:"relative",left:"-30px"}}>Print viewing area</span>
                    </MenuItem>
                    <Divider/>
                    <MenuItem className={classes.menuItem} onClick={this.printSelectedImages}>
                        <ListItemIcon>
                            <SvgIcon style={{position:'relative', left: -17, top: 3}}>
                                <image width={38} height={15} href={printArrow}/>
                            </SvgIcon>
                        </ListItemIcon>
                        <span style={{position:"relative",left:"-30px"}}>Print selected images</span>
                    </MenuItem>
                </Menu>
                {/* <Menu id="long-menu" style={{marginTop:"30px"}} anchorEl={anchorEl} open={open} onClose={this.handleClose}>
                    <MenuItem className={classes.menuItem} onClick={this.printViewingArea}>
                        <ListItemIcon>
                            <SvgIcon style={{width:"20px", height:"20px"}}>
                                <image style={{width:"20px", height:"20px"}} href='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAICAYAAADA+m62AAAABHNCSVQICAgIfAhkiAAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L/GEFAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAW0lEQVQoU53PMQrAIAyF4bR7zhWnYC6tZ9IlrY9GSu1Q+kMwwofg5mf0of06Z7VWzNJ4MSqlODNjxn5vwkCqinliwEA5Z++9e2ttwYAi4mYGFAVOKeH+/9fvER1KRoYR/98QgAAAAABJRU5ErkJggg=='/>
                            </SvgIcon>
                        </ListItemIcon>
                        <span>Print viewing area</span>
                    </MenuItem>
                    <MenuItem className={classes.menuItem} onClick={this.printSelectedImages}>
                        <ListItemIcon>
                            <SvgIcon style={{position:"relative",left:"-15px",top:"4px"}}>
                                <image width={35} href={printArrow}/>
                            </SvgIcon>
                        </ListItemIcon>
                        <span style={{position:"relative",left:"-30px"}}>Print selected images</span>
                    </MenuItem>
                </Menu> */}
            </div>
        )
    }
}

export default cidComponent(styles)(PrintButtonTools)