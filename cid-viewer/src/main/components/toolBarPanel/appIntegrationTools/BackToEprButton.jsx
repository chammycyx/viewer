//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnBackTo from '@assets/images/btnBackTo.png'
import btnBackTo_over from '@assets/images/btnBackTo_over.png'

const BackToEprButton = cidToolBarButton(btnBackTo, btnBackTo_over, btnBackTo, 'BackToEprButton')
export default BackToEprButton