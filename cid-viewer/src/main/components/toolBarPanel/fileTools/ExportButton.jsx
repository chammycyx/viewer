//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnExport from '@assets/images/btnExport.png'
import btnExport_over from '@assets/images/btnExport_over.png'
import btnExport_disable from '@assets/images/btnExport_disable.png'

const ExportButton = cidToolBarButton(btnExport, btnExport_over, btnExport_disable, 'ExportButton')
export default ExportButton