// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import download from 'downloadjs'

// import utils
import { getReactCreateRef } from '@utils/ValidationByPassUtil'

// import material-ui Dialog
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import cidNestMuiThemeProvider from '@components/base/CidNestMuiThemeProvider'
import { cidThemeForDialog } from '@styles/CidTheme'

type ExportImageDialogPropsType = CidComponentPropsType & {
    exportImageDialogData: *,
    exportConfig: *,
    selectedSeriesId: string,
    selectedImageIds: Array<string>,
    setVisible: Function,
    setDownload: Function,
    onExport: Function
}

type StateType = {
    isAgreed: boolean,
    isPasswordValid: boolean
}

export const styles: Function = function(theme: *): * {
	const DISC_DIALOG_WIDTH = '570px'
    const DISC_DIALOG_HEIGHT = '440px'
    const DL_DIALOG_WIDTH = '370px'
	const DL_DIALOG_HEIGHT = '110px'

	return {
        discContent: {
            fontSize: "14px",
            borderStyle: "solid",
            borderWidth: "1px",
            padding: "10px"
        },
        textField: {
            margin: "10px"
        },
        instruction: {
            fontSize: "12px"
        },
        pointerCursor: {
            cursor:"pointer"
        },
        discDialog: {
                
        },
        discPaper: {
            width: DISC_DIALOG_WIDTH,
            minWidth: DISC_DIALOG_WIDTH,
            maxWidth: DISC_DIALOG_WIDTH,
            height: DISC_DIALOG_HEIGHT,
            minHeight: DISC_DIALOG_HEIGHT,
            maxHeight: DISC_DIALOG_HEIGHT
        },
        dlDialog: {
                
        },
        dlPaper: {
            width: DL_DIALOG_WIDTH,
            minWidth: DL_DIALOG_WIDTH,
            maxWidth: DL_DIALOG_WIDTH,
            height: DL_DIALOG_HEIGHT,
            minHeight: DL_DIALOG_HEIGHT,
            maxHeight: DL_DIALOG_HEIGHT
        }
    }
}

class ExportImageDialog extends Component<ExportImageDialogPropsType, StateType> {
    passwordTextField: *
    password2TextField: *
    handleOK: Function
    handleClose: Function
    handleChecked: Function
    handlePassword: Function
    handleDownload: Function

    constructor(props: *) {
        super(props)
        this.passwordTextField = getReactCreateRef()
        this.password2TextField = getReactCreateRef()
        this.handleOK = this.handleOK.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.handleChecked = this.handleChecked.bind(this)
        this.handlePassword = this.handlePassword.bind(this)
        this.handleDownload = this.handleDownload.bind(this)
        this.state = {
            isAgreed: false,
            isPasswordValid: false
        }
    }

    handleClose() {
        this.setState({
            isAgreed: false,
            isPasswordValid: false
        })
        this.props.setVisible(false)
        this.props.setDownload(false, null)
    }

    handleOK() {
        this.props.onExport(this.props.selectedSeriesId, this.props.selectedImageIds, this.passwordTextField.current.value)
    }

    handleChecked(event: *) {
        let targetState = event.target.value
        this.setState({ 
            [targetState]: event.target.checked 
        })
    }

    handlePassword() {
        let password1 = this.passwordTextField.current.value
        let password2 = this.password2TextField.current.value
        let isPassword1Valid = (password1 != null && password1.length > 0)
        let isPassword2Valid = (password2 != null && password1.length > 0)
        if (isPassword1Valid && isPassword2Valid) {
            this.setState({ 
                isPasswordValid: password1 == password2
            })
        }
    }

    handleDownload() {
        let fileBase64 = this.props.exportImageDialogData.exportImageBase64
        download(fileBase64, this.props.exportConfig.imageExportFileName)
        this.handleClose()
    }

    render(): Node {
        const classes = this.props.classes

        return (
            <div>
                <Dialog 
                    open={this.props.exportImageDialogData.isVisible}
                    classes={{root: classes.discDialog, paper: classes.discPaper}}>
                    <DialogTitle 
                        id="export-dialog-title">
                        {this.props.exportImageDialogData.title}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText 
                            className={classes.discContent}>
                            <span dangerouslySetInnerHTML={{__html:this.props.exportConfig.imageExportDisclaimer}}></span>
                        </DialogContentText>
                        <FormGroup>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={this.state.isAgreed}
                                        onChange={this.handleChecked}
                                        value="isAgreed"
                                    />
                                }
                                label={this.props.exportImageDialogData.agreementLabel}
                            />
                        </FormGroup>
                        <DialogContentText 
                            className={classes.instruction}>
                            {this.props.exportImageDialogData.passwordQueryLabel}
                        </DialogContentText>
                        <TextField 
                            className={classes.textField}
                            margin="normal" 
                            id="passwordTextField"
                            inputRef={this.passwordTextField}
                            type="password" 
                            label={this.props.exportImageDialogData.passwordLabel} 
                            onChange={this.handlePassword}
                            disabled={!this.state.isAgreed}
                        />
                        <TextField 
                            className={classes.textField}
                            margin="normal" 
                            id="password2TextField"
                            inputRef={this.password2TextField}
                            type="password" 
                            label={this.props.exportImageDialogData.password2Label} 
                            onChange={this.handlePassword}
                            disabled={!this.state.isAgreed}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button 
                            onClick={this.handleOK} 
                            color="primary" 
                            disabled={!this.state.isPasswordValid||!this.state.isAgreed}>
                            OK
                        </Button>
                        <Button 
                            onClick={this.handleClose} 
                            color="primary">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
                {/* <Dialog 
                    open={this.props.exportImageDialogData.isRequesting}>
                    <DialogContent>
                        <div>
                            <b>{this.props.exportImageDialogData.requestingMsg}</b>
                        </div>
                    </DialogContent>
                </Dialog> */}
                <Dialog 
                    open={this.props.exportImageDialogData.isDownload}
                    classes={{root: classes.dlDialog, paper: classes.dlPaper}}>
                    <DialogContent>
                        <div>
                            <b>Please click <a className={classes.pointerCursor} onClick={this.handleDownload}><u>HERE</u></a> to download the image file.</b>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button 
                            onClick={this.handleClose} 
                            color="primary">
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default cidComponent(styles)(cidNestMuiThemeProvider(cidThemeForDialog)(ExportImageDialog))