//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnPrint from '@assets/images/btnPrint.png'
import btnPrint_over from '@assets/images/btnPrint_over.png'
import btnPrint_disable from '@assets/images/btnPrint_disable.png'

const PrintButton = cidToolBarButton(btnPrint, btnPrint_over, btnPrint_disable, 'PrintButton')
export default PrintButton