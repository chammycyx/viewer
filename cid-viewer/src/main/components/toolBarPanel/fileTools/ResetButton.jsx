//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnReset from '@assets/images/btnReset.png'
import btnReset_over from '@assets/images/btnReset_over.png'
import btnReset_disable from '@assets/images/btnReset_disable.png'

const ResetButton = cidToolBarButton(btnReset, btnReset_over, btnReset_disable, 'ResetButton')
export default ResetButton