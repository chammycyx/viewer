import cidToolBarButton from '@components/base/CidToolBarButton'

import btnDeselect from '@assets/images/btnDeselect.png'
import btnDeselect_over from '@assets/images/btnDeselect_over.png'
import btnDeselect_disable from '@assets/images/btnDeselect_disable.png'

const SelectorButton = cidToolBarButton(btnDeselect, btnDeselect_over, btnDeselect_disable, 'SelectorButton')
export default SelectorButton