// @flow
import { connect, type MapStateToProps, type MapDispatchToProps } from 'react-redux'
import ToolBarPanel from './ToolBarPanel'
import { backToEpr, resetImages, setTool, printImage, onColorAdjustmentButtonClick } from '@actions/components/toolBarPanel/Action'
import  { onViewModeChange, type ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import { rotateImage, flipImage } from '@actions/components/toolBarPanel/transformationTools/Action'
import { setVisible as setColorAdjustmentPropsBoxVisible, changeColorAdjustment } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'
import { setVisible as setExportImageDialogVisible , setDownload , requestExportImages } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'
import { play as playSlideShow, pause as pauseSlideShow, stop as stopSlideShow, setPrev as setSlideShowPrev, setNext as setSlideShowNext } from '@actions/components/toolBarPanel/slideShowTools/Action'
import { setFps } from '@actions/components/toolBarPanel/slideShowTools/fpsSelector/Action'

const mapStateToProps = (state: *): MapStateToProps => {
    return {
        viewMode: state.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode,
        canvasGrid: state.components.imageViewingPanel.canvasGrid.rootState,
        colorAdjustmentPropsBox: state.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState,
        exportImageDialogData: state.components.toolBarPanel.fileTools.dialogs.exportImageDialog.rootState,
        exportConfig: state.data.config.application.rootState.export,
        slideShow: state.components.toolBarPanel.slideShowTools.rootState,
        fps: state.components.toolBarPanel.slideShowTools.fpsSelector.rootState.fps,
        btnState: state.components.toolBarPanel.rootState.btnState,
        appWidth: state.components.rootState.appWidth
    }
}

const mapDispatchToProps = (dispatch: *): MapDispatchToProps => ({
    onViewModeChange: (viewMode: ViewModeType) => {
        dispatch(onViewModeChange(viewMode))
    },
    backToEpr: () => {
        dispatch(backToEpr())
    },
    onRotateButtonClick: (rotateDirection: string) => {
        dispatch(rotateImage(rotateDirection))
    },
    onFlipButtonClick: (flipDirection: string) => {
        dispatch(flipImage(flipDirection))
    },
    setColorAdjustmentPropsBoxVisible: (visible: boolean) => {
        dispatch(setColorAdjustmentPropsBoxVisible(visible))
    },
    onColorAdjustmentButtonClick: () => {
        dispatch(onColorAdjustmentButtonClick())
    },
    onColorAdjustmentChange: (brightness: number, contrast: number, hue: number, saturation: number, invert: boolean, contrastToolIdx: number) => {
        dispatch(changeColorAdjustment(brightness, contrast, hue, saturation, invert, contrastToolIdx))
    },
    setExportImageDialogVisible: (isVisible: boolean) => {
        dispatch(setExportImageDialogVisible(isVisible))
    },
    setDownload: (isDownload: boolean, downloadString: string) => {
        dispatch(setDownload(isDownload, downloadString))
    },
    onExport: (selectedSeriesId: string, selectedImageIds: Array<string>, password: string) => {
        dispatch(requestExportImages(selectedSeriesId, selectedImageIds, password))
    },
	onReset: (selectedSeriesId: string, selectedImageIds: Array<string>) => {
        dispatch(resetImages(selectedSeriesId, selectedImageIds))
    },
    setTool: (tool: string)=>{
        dispatch(setTool(tool))
    },
    playSlideShow: (fps: number)=>{
        dispatch(playSlideShow(fps))
    },
    pauseSlideShow: ()=>{
        dispatch(pauseSlideShow())
    },
    stopSlideShow: ()=>{
        dispatch(stopSlideShow())
    },
    setSlideShowPrev: ()=>{
        dispatch(setSlideShowPrev())
    },
    setSlideShowNext: ()=>{
        dispatch(setSlideShowNext())
    },
    setFps: (fps: number)=>{
        dispatch(setFps(fps))
    },
    onPrintButtonClick: (printMode: string)=>{
        dispatch(printImage(printMode))
    }
})

export default connect(
    mapStateToProps, // comment it for testing its absent behavior
    mapDispatchToProps
)(ToolBarPanel)