//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnZoomOut from '@assets/images/btnZoomOut.png'
import btnZoomOut_over from '@assets/images/btnZoomOut_over.png'
import btnZoomOut_disable from '@assets/images/btnZoomOut_disable.png'

const ZoomOutButton = cidToolBarButton(btnZoomOut, btnZoomOut_over, btnZoomOut_disable, 'ZoomOutButton')
export default ZoomOutButton