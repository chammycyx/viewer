//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnZoomIn from '@assets/images/btnZoomIn.png'
import btnZoomIn_over from '@assets/images/btnZoomIn_over.png'
import btnZoomIn_disable from '@assets/images/btnZoomIn_disable.png'

const ZoomInButton = cidToolBarButton(btnZoomIn, btnZoomIn_over, btnZoomIn_disable, 'ZoomInButton')
export default ZoomInButton