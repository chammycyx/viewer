//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnContrast from '@assets/images/btnContrast.png'
import btnContrast_over from '@assets/images/btnContrast_over.png'
import btnContrast_disable from '@assets/images/btnContrast_disable.png'

const ColorAdjustmentButton = cidToolBarButton(btnContrast, btnContrast_over, btnContrast_disable, 'ColorAdjustmentButton')
export default ColorAdjustmentButton