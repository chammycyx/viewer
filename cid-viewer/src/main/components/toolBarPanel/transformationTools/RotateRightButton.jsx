//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnRotateRight from '@assets/images/btnRotateRight.png'
import btnRotateRight_over from '@assets/images/btnRotateRight_over.png'
import btnRotateRight_disable from '@assets/images/btnRotateRight_disable.png'

const RotateRightButton = cidToolBarButton(btnRotateRight, btnRotateRight_over, btnRotateRight_disable, 'RotateRightButton')
export default RotateRightButton