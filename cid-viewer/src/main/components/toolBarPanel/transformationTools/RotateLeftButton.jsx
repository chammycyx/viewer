//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnRotateLeft from '@assets/images/btnRotateLeft.png'
import btnRotateLeft_over from '@assets/images/btnRotateLeft_over.png'
import btnRotateLeft_disable from '@assets/images/btnRotateLeft_disable.png'

const RotateLeftButton = cidToolBarButton(btnRotateLeft, btnRotateLeft_over, btnRotateLeft_disable, 'RotateLeftButton')
export default RotateLeftButton