// @flow
import React, { Component } from 'react'
import type { Node } from 'react'

import { getDocument } from '@utils/ValidationByPassUtil'
import Slider from '@material-ui/lab/Slider'
import Tooltip from '@material-ui/core/Tooltip'
import Button from '@material-ui/core/Button'
import { CanvasProfileInitState } from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'
import cidPropsBox from '@components/base/CidPropsBox'
import type { PropsBoxPropsType } from '@components/base/CidPropsBox'
import cidNestMuiThemeProvider from '@components/base/CidNestMuiThemeProvider'
import { cidThemeForDialog } from '@styles/CidTheme'
import {ContrastPanelTool} from '@constants/ComponentConstants'

export const PIXEL_PER_VALUE: number = 1.194

type ColorAdjustmentPropsBoxPropsType = PropsBoxPropsType & {
    brightness: number,
    contrast: number,
    hue: number,
    saturation: number,
    invert: boolean,
    setVisible: Function,
    onChange: Function,
    onDragMouseDown: Function,
    propsBoxRef: *
}

type StateType = {
    brightnessChangeable: boolean,
    contrastChangeable: boolean,
    hueChangeable: boolean,
    saturationChangeable: boolean
}

export const styles: Function = (theme: *): * => ({
    title: {
        width: '100%',
        color: '#FCDA3B',
        cursor: 'default'
    },
    content: {
        background: '#FFFFFF',
        margin: 10,
        padding: 10
    },
    subTitle: {
        width: '100%',
        cursor: 'default'
    },
    slider: {
        width: '90%',
        paddingLeft: 2,
        paddingRight: 2
    },
    bottomNavigation: {
        height: 30
    },
    button: {
        textTransform: 'none',
        display: 'block',
        fontWeight: 'bold'
    },
    resetButton: {
        float: 'left',
        marginLeft: 10
    },
    closeButton: {
        float: 'right',
        marginRight: 10
    },
	brightnessTooltip: {
        margin: function(props: *): string {
            const movementValue: number = props.brightness * PIXEL_PER_VALUE
            return '-4px '+ (-movementValue).toString() + 'px -4px ' + movementValue.toString() + 'px'
        }
    },
	contrastTooltip: {
        margin: function(props: *): string {
            const movementValue: number = props.contrast * PIXEL_PER_VALUE
            return '-4px '+ (-movementValue).toString() + 'px -4px ' + movementValue.toString() + 'px'
        }
    },
	hueTooltip: {
        margin: function(props: *): string {
            const pixelPerValue: number = PIXEL_PER_VALUE / 1.8  //maximum value of hue = 180
            const movementValue: number = props.hue * pixelPerValue
            return '-4px '+ (-movementValue).toString() + 'px -4px ' + movementValue.toString() + 'px'
        }
    },
	saturationTooltip: {
        margin: function(props: *): string {
            const movementValue: number = props.saturation * PIXEL_PER_VALUE
            return '-4px '+ (-movementValue).toString() + 'px -4px ' + movementValue.toString() + 'px'
        }
    }    
})

class ColorAdjustmentPropsBox extends Component<ColorAdjustmentPropsBoxPropsType, StateType> {
    onChange: Function
    onBrightnessChange: Function
    onContrastChange: Function
    onHueChange: Function
    onSaturationChange: Function
    onInvertChange: Function
    onResetClick: Function
    onCloseButtonClick: Function
    onBrightnessClose: Function
    onContrastClose: Function
    onHueClose: Function
    onSaturationClose: Function
    onBrightnessHover: Function
    onContrastHover: Function
    onHueHover: Function
    onSaturationHover: Function

    constructor(props: *) {
        super(props)
        this.onChange = this.onChange.bind(this)
        this.onBrightnessChange = this.onBrightnessChange.bind(this)
        this.onContrastChange = this.onContrastChange.bind(this)
        this.onHueChange = this.onHueChange.bind(this)
        this.onSaturationChange = this.onSaturationChange.bind(this)
        this.onInvertChange = this.onInvertChange.bind(this)
        this.onResetClick = this.onResetClick.bind(this)
        this.onCloseButtonClick = this.onCloseButtonClick.bind(this)
        this.onBrightnessClose = this.onBrightnessClose.bind(this)
        this.onContrastClose = this.onContrastClose.bind(this)
        this.onHueClose = this.onHueClose.bind(this)
        this.onSaturationClose = this.onSaturationClose.bind(this)
        this.onBrightnessHover = this.onBrightnessHover.bind(this)
        this.onContrastHover = this.onContrastHover.bind(this)
        this.onHueHover = this.onHueHover.bind(this)
        this.onSaturationHover = this.onSaturationHover.bind(this)
        this.state = {
            brightnessChangeable: true,
            contrastChangeable: true,
            hueChangeable: true,
            saturationChangeable: true
        }
    }

    componentDidUpdate(prevProps: *) {
        let that = this

        if(!prevProps.visible && this.props.visible && getDocument().onclick==null) {
            getDocument().onclick = function(e: *) {
                // if the clicked native DOM node does not belong to this props box, close this props box
                if(!that.props.propsBoxRef.current.contains(e.target)) {
                    that.props.setVisible(false)
                }
            }
        } else if(!this.props.visible) {
            getDocument().onclick = null
        }
    }

    onChange(argIdx: number, value: *) {
        let params = [
            this.props.brightness,
            this.props.contrast,
            this.props.hue,
            this.props.saturation,
            this.props.invert,
            argIdx
        ]

        params[argIdx] = value
        this.props.onChange(...params)
    }

    onBrightnessChange(e: *, value: number) {
        if(this.state.brightnessChangeable) this.onChange(ContrastPanelTool.BRIGHTNESS, value)
    }

    onContrastChange(e: *, value: number) {
        if(this.state.contrastChangeable) this.onChange(ContrastPanelTool.CONTRAST, value)
    }

    onHueChange(e: *, value: number) {
        if(this.state.hueChangeable) this.onChange(ContrastPanelTool.HUE, value)
    }

    onSaturationChange(e: *, value: number) {
        if(this.state.saturationChangeable) this.onChange(ContrastPanelTool.SATURATION, value)
    }

    onBrightnessClose(e: *) {
        this.setState({
            brightnessChangeable: false
        })
    }

    onContrastClose(e: *) {
        this.setState({
            contrastChangeable: false
        })
    }

    onHueClose(e: *) {
        this.setState({
            hueChangeable: false
        })
    }

    onSaturationClose(e: *) {
        this.setState({
            saturationChangeable: false
        })
    }

    onBrightnessHover(e: *) {
        this.setState({
            brightnessChangeable: true
        })
    }

    onContrastHover(e: *) {
        this.setState({
            contrastChangeable: true
        })
    }

    onHueHover(e: *) {
        this.setState({
            hueChangeable: true
        })
    }

    onSaturationHover(e: *) {
        this.setState({
            saturationChangeable: true
        })
    }

    onInvertChange() {
        this.onChange(ContrastPanelTool.INVERT, !this.props.invert)
    }

    onResetClick() {
        let params = [
            CanvasProfileInitState.brightness,
            CanvasProfileInitState.contrast,
            CanvasProfileInitState.hue,
            CanvasProfileInitState.saturation,
            CanvasProfileInitState.invert,
            ContrastPanelTool.RESET
        ]

        this.props.onChange(...params)
    }

    onCloseButtonClick() {
        this.props.setVisible(false)
    }

    render(): Node {
        const classes = this.props.classes
        return (
            <div>
                <div onMouseDown={this.props.onDragMouseDown}>
                    <span className={classes.title}>Window Level and Contrast</span>
                </div>
                <div className={classes.content}>
                    <span className={classes.subTitle}>Brightness</span>
                    <Tooltip classes={{tooltipPlacementTop: classes.brightnessTooltip}} title={this.props.brightness} placement="top" onClose={this.onBrightnessClose} onOpen={this.onBrightnessHover}>
                        <Slider className={classes.slider} max={100} min={-100} step={1} value={this.props.brightness} onChange={this.onBrightnessChange}/>
                    </Tooltip>
                    <span className={classes.subTitle}>Contrast</span>
                    <Tooltip classes={{tooltipPlacementTop: classes.contrastTooltip}} title={this.props.contrast} placement="top" onClose={this.onContrastClose} onOpen={this.onContrastHover}>
                        <Slider className={classes.slider} max={100} min={-100} step={1} value={this.props.contrast} onChange={this.onContrastChange}/>
                    </Tooltip>
                    <span className={classes.subTitle}>Hue</span>
                    <Tooltip classes={{tooltipPlacementTop: classes.hueTooltip}} title={this.props.hue} placement="top" onClose={this.onHueClose} onOpen={this.onHueHover}>
                        <Slider className={classes.slider} max={180} min={-180} step={1} value={this.props.hue} onChange={this.onHueChange}/>
                    </Tooltip>
                    <span className={classes.subTitle}>Saturation</span>
                    <Tooltip classes={{tooltipPlacementTop: classes.saturationTooltip}} title={this.props.saturation} placement="top" onClose={this.onSaturationClose} onOpen={this.onSaturationHover}>
                        <Slider className={classes.slider} max={100} min={-100} step={1} value={this.props.saturation} onChange={this.onSaturationChange}/>
                    </Tooltip>
                    <input type="checkbox" checked={this.props.invert} onChange={this.onInvertChange}/><span>Invert</span><br/>
                </div>
                <div className={classes.bottomNavigation}>
                    <Button variant="outlined" size="small" className={classes.button + ' ' + classes.resetButton} onClick={this.onResetClick}><span>Reset</span></Button>
                    <Button variant="outlined" size="small" className={classes.button + ' ' + classes.closeButton} onClick={this.onCloseButtonClick}><span>Close</span></Button>
                </div>
            </div>
        )
    }
}

export default cidPropsBox(styles)(cidNestMuiThemeProvider(cidThemeForDialog)(ColorAdjustmentPropsBox))