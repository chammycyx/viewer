//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnFlipV from '@assets/images/btnFlipV.png'
import btnFlipV_over from '@assets/images/btnFlipV_over.png'
import btnFlipV_disable from '@assets/images/btnFlipV_disable.png'

const FlipVerticalButton = cidToolBarButton(btnFlipV, btnFlipV_over, btnFlipV_disable, 'FlipVerticalButton')
export default FlipVerticalButton