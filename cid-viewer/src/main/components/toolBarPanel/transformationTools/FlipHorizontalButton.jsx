//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnFlipH from '@assets/images/btnFlipH.png'
import btnFlipH_over from '@assets/images/btnFlipH_over.png'
import btnFlipH_disable from '@assets/images/btnFlipH_disable.png'

const FlipHorizontalButton = cidToolBarButton(btnFlipH, btnFlipH_over, btnFlipH_disable, 'FlipHorizontalButton')
export default FlipHorizontalButton