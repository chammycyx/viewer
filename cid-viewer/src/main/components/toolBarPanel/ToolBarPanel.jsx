// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import Separator from './Separator'
import SelectorButton from './selectorTools/SelectorButton'
import ZoomInButton from './zoomTools/ZoomInButton'
import ZoomOutButton from './zoomTools/ZoomOutButton'

import RotateLeftButton from './transformationTools/RotateLeftButton'
import RotateRightButton from './transformationTools/RotateRightButton'
import FlipVerticalButton from './transformationTools/FlipVerticalButton'
import FlipHorizontalButton from './transformationTools/FlipHorizontalButton'
import ColorAdjustmentButton from './transformationTools/ColorAdjustmentButton'
import ColorAdjustmentPropsBox from './transformationTools/propsBox/ColorAdjustmentPropsBox'

import PrevButton from './slideShowTools/PrevButton'
import NextButton from './slideShowTools/NextButton'
import PauseButton from './slideShowTools/PauseButton'
import StopButton from './slideShowTools/StopButton'
import PlayButton from './slideShowTools/PlayButton'
import FpsSelector from './slideShowTools/FpsSelector'

import ResetButton from './fileTools/ResetButton'
import ExportButton from './fileTools/ExportButton'
import ExportImageDialog from '@components/toolBarPanel/fileTools/dialogs/exportImageDialog/ExportImageDialog'
import PrintButtonTools from '@components/toolBarPanel/printTools/PrintButtonTools'

import ViewModeDropDownList from './viewModeTools/ViewModeDropDownList'
import BackToEprButton from './appIntegrationTools/BackToEprButton'

import type { ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import { RotateDirections, FlipDirections } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { getInitColorAdjustmentValues } from '@src/main/utils/ColorAdjustmentUtil'
import {ImageTool} from '@constants/ComponentConstants'

type ToolBarPanelPropsType = CidComponentPropsType & {
    backToEpr: Function,
    viewMode: ViewModeType,
    onViewModeChange: Function,
    onRotateButtonClick: Function,
    onFlipButtonClick: Function,
    setColorAdjustmentPropsBoxVisible: Function,
    onColorAdjustmentButtonClick: Function,
    onColorAdjustmentChange: Function,
    colorAdjustmentPropsBox: *,
    setExportImageDialogVisible: Function,
    setDownload: Function,
    onExport: Function,
    setTool: Function,
    exportImageDialogData: *,
    exportConfig: *,
    canvasGrid: *,
    slideShow: *,
    playSlideShow: Function,
    pauseSlideShow: Function,
    stopSlideShow: Function,
    setSlideShowPrev: Function,
    setSlideShowNext: Function,
    fps: number,
    setFps: Function,
    onReset: Function,
    onPrintButtonClick: Function,
    btnState: *,
    appWidth: number
}

export const styles: Function = (theme: *): * => ({
    outerDiv: {
        width: function(props: *): number {
            return props.appWidth
        },
        height: theme.toolBarPanel.height,
        maxHeight: theme.toolBarPanel.height, // for inner scroll bar
        overflow: 'auto'
    },
    div: {
        height: theme.toolBarPanel.height,
        width: 960
    }
})

class ToolBarPanel extends Component<ToolBarPanelPropsType> {
    onRotateLeftButtonClick: Function
    onRotateRightButtonClick: Function
    onFlipVerticalButtonClick: Function
    onFlipHorizonalButtonClick: Function
    onExportButtonClick: Function
    onZoomInButtonClick: Function
    onZoomOutButtonClick: Function
    onSelectorButtonClick: Function
    onPrevButtonClick: Function
    onNextButtonClick: Function
    onPauseButtonClick: Function
    onStopButtonClick: Function
    onPlayButtonClick: Function
	onResetButtonClick: Function
    colorAdjustmentButton: *
    outerDiv: *
    
    constructor(props: *) {
        super(props)
        this.onRotateLeftButtonClick = this.onRotateLeftButtonClick.bind(this)
        this.onRotateRightButtonClick = this.onRotateRightButtonClick.bind(this)
        this.onFlipVerticalButtonClick = this.onFlipVerticalButtonClick.bind(this)
        this.onFlipHorizonalButtonClick = this.onFlipHorizonalButtonClick.bind(this)
        this.onExportButtonClick = this.onExportButtonClick.bind(this)
        this.onZoomInButtonClick = this.onZoomInButtonClick.bind(this)
        this.onZoomOutButtonClick = this.onZoomOutButtonClick.bind(this)
        this.onSelectorButtonClick = this.onSelectorButtonClick.bind(this)
        this.onPrevButtonClick = this.onPrevButtonClick.bind(this)
        this.onNextButtonClick = this.onNextButtonClick.bind(this)
        this.onPauseButtonClick = this.onPauseButtonClick.bind(this)
        this.onStopButtonClick = this.onStopButtonClick.bind(this)
        this.onPlayButtonClick = this.onPlayButtonClick.bind(this)
        this.onResetButtonClick = this.onResetButtonClick.bind(this)
    }

    onRotateLeftButtonClick() {
        this.props.onRotateButtonClick(RotateDirections.LEFT)
    }

    onRotateRightButtonClick() {
        this.props.onRotateButtonClick(RotateDirections.RIGHT)
    }

    onFlipVerticalButtonClick() {
        this.props.onFlipButtonClick(FlipDirections.VERTICAL)
    }

    onFlipHorizonalButtonClick() {
        this.props.onFlipButtonClick(FlipDirections.HORIZONTAL)
    }

    onExportButtonClick() {
        this.props.setExportImageDialogVisible(true)
    }

    onSelectorButtonClick() {
        this.props.setTool(ImageTool.SELECT)
    }

    onZoomInButtonClick() {
        this.props.setTool(ImageTool.ZOOM_IN)
    }

    onZoomOutButtonClick() {
        this.props.setTool(ImageTool.ZOOM_OUT)
    }

    onPrevButtonClick() {
        this.props.setSlideShowPrev()
    }

    onNextButtonClick() {
        this.props.setSlideShowNext()
    }

    onPauseButtonClick() {
        this.props.pauseSlideShow()
    }

    onStopButtonClick() {
        this.props.stopSlideShow()
    }

    onPlayButtonClick() {
        let numFps = this.props.fps
        this.props.playSlideShow(numFps)
    }

    onResetButtonClick() {
        this.props.onReset(this.props.canvasGrid.selectedSeriesId, this.props.canvasGrid.selectedImageIds)
    }
    
    render(): Node {
        const classes = this.props.classes
        const btnState = this.props.btnState
        const colorAdjustmentValues = getInitColorAdjustmentValues(this.props.canvasGrid.selectedImageIds, this.props.canvasGrid.canvasProfiles)

        return (
            <div className={classes.outerDiv}>
                <div className={classes.div}>
                    <SelectorButton toolTipTitle={'Deselect'} onClick={this.onSelectorButtonClick} isEnabled={btnState.deselectBtnState} isSelected={false}/>
                    <Separator />
                    <ZoomInButton toolTipTitle={'Zoom In'} onClick={this.onZoomInButtonClick} isEnabled={btnState.zoomInBtnState} isSelected={false}/>
                    <ZoomOutButton toolTipTitle={'Zoom Out'} onClick={this.onZoomOutButtonClick} isEnabled={btnState.zoomOutBtnState} isSelected={false}/>
                    <Separator />
                    <RotateLeftButton toolTipTitle={'Rotate Left'} onClick={this.onRotateLeftButtonClick} isEnabled={btnState.rotateLeftBtnState} isSelected={false}/>
                    <RotateRightButton toolTipTitle={'Rotate Right'} onClick={this.onRotateRightButtonClick} isEnabled={btnState.rotateRightBtnState} isSelected={false}/>
                    <FlipVerticalButton toolTipTitle={'Flip Vertical'} onClick={this.onFlipVerticalButtonClick} isEnabled={btnState.flipVerticalBtnState} isSelected={false}/>
                    <FlipHorizontalButton toolTipTitle={'Flip Horizontal'} onClick={this.onFlipHorizonalButtonClick} isEnabled={btnState.flipHorizonalBtnState} isSelected={false}/>
                    <ColorAdjustmentButton toolTipTitle={'Contrast'} onClick={this.props.onColorAdjustmentButtonClick} isEnabled={btnState.contrastBtnState} isSelected={false}/>
                    <ColorAdjustmentPropsBox visible={this.props.colorAdjustmentPropsBox.visible} opacity={0.9} resetPositionWhenReopen={true}
                        x={'0px'}
                        y={(this.props.appWidth - this.props.theme.toolBarPanel.propsBox.width - 20) + 'px'}
                        brightness={colorAdjustmentValues.brightness}
                        contrast={colorAdjustmentValues.contrast}
                        hue={colorAdjustmentValues.hue}
                        saturation={colorAdjustmentValues.saturation}
                        invert={colorAdjustmentValues.invert}
                        setVisible={this.props.setColorAdjustmentPropsBoxVisible}
                        onChange={this.props.onColorAdjustmentChange}/>
                    <Separator />
                    <PrevButton toolTipTitle={'Previous'} onClick={this.onPrevButtonClick} isEnabled={btnState.previousBtnState} isSelected={false}/>
                    <NextButton toolTipTitle={'Next'} onClick={this.onNextButtonClick} isEnabled={btnState.nextBtnState} isSelected={false}/>
                    <PauseButton toolTipTitle={'Pause'} onClick={this.onPauseButtonClick} isEnabled={btnState.pauseBtnState} isSelected={false}/>
                    <StopButton toolTipTitle={'Stop'} onClick={this.onStopButtonClick} isEnabled={btnState.stopBtnState} isSelected={false}/>
                    <PlayButton toolTipTitle={'Play'} onClick={this.onPlayButtonClick} isEnabled={btnState.playBtnState} isSelected={false}/>
                    <FpsSelector
                        toolTipTitle={'Frame per second'}
                        fps={this.props.fps}
                        setFps={this.props.setFps}
                        isEnabled={btnState.fpsBtnState}/>
                    <Separator />
                    <ResetButton toolTipTitle={'Reset'} onClick={this.onResetButtonClick} isEnabled={btnState.resetBtnState} isSelected={false}/>
                    <ExportButton toolTipTitle={'Export images'} onClick={this.onExportButtonClick} isEnabled={btnState.exportBtnState} isSelected={false}/>
                    <ExportImageDialog 
                        exportImageDialogData={this.props.exportImageDialogData}
                        exportConfig={this.props.exportConfig}
                        selectedSeriesId={this.props.canvasGrid.selectedSeriesId}
                        selectedImageIds={this.props.canvasGrid.selectedImageIds}
                        setVisible={this.props.setExportImageDialogVisible}
                        setDownload={this.props.setDownload}
                        onExport={this.props.onExport}/>
                    <PrintButtonTools onClick={this.props.onPrintButtonClick} isEnabled={btnState.printBtnState}/>
                    <Separator />
                    <ViewModeDropDownList toolTipTitle={'Changing ViewMode'} viewMode={this.props.viewMode} onChange={this.props.onViewModeChange} isEnabled={btnState.viewModeBtnState}/>
                    <Separator />
                    <BackToEprButton toolTipTitle={'Back to EPR'} onClick={this.props.backToEpr} isEnabled={btnState.backToEPRBtnState} isSelected={false}/>
                </div>
            </div>
        )
    }
}

export default cidComponent(styles)(ToolBarPanel)