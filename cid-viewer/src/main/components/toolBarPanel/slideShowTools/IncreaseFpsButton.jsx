//@flow
import cidSpecialButton from '../../base/CidSpecialButton'
import FpsArrowUp from '../../../../assets/images/FpsArrowUp.png'
import FpsArrowUp_over from '../../../../assets/images/FpsArrowUp_over.png'
import FpsArrowUp_disabled from '../../../../assets/images/FpsArrowUp_disabled.png'

const IncreaseFpsButton = cidSpecialButton(FpsArrowUp, FpsArrowUp_over, FpsArrowUp_disabled, 'IncreaseFpsButton', [true])
export default IncreaseFpsButton