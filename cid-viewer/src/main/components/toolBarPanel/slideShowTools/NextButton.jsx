//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnNext from '@assets/images/btnNext.png'
import btnNext_over from '@assets/images/btnNext_over.png'
import btnNext_disable from '@assets/images/btnNext_disable.png'

const NextButton = cidToolBarButton(btnNext, btnNext_over, btnNext_disable, 'NextButton')
export default NextButton