//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnPrevious from '@assets/images/btnPrevious.png'
import btnPrevious_over from '@assets/images/btnPrevious_over.png'
import btnPrevious_disable from '@assets/images/btnPrevious_disable.png'

const PrevButton = cidToolBarButton(btnPrevious, btnPrevious_over, btnPrevious_disable, 'PrevButton')
export default PrevButton