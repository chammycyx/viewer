//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnPause from '@assets/images/btnPause.png'
import btnPause_over from '@assets/images/btnPause_over.png'
import btnPause_disable from '@assets/images/btnPause_disable.png'

const PauseButton = cidToolBarButton(btnPause, btnPause_over, btnPause_disable, 'PauseButton')
export default PauseButton