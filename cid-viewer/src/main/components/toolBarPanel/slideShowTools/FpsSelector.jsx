//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

// import components
import IncreaseFpsButton from './IncreaseFpsButton'
import DecreaseFpsButton from './DecreaseFpsButton'

// import material-ui
import Tooltip from '@material-ui/core/Tooltip'

//import utils
import { getReactCreateRef } from '@utils/ValidationByPassUtil'
import logger from '@utils/Logger'

type FpsSelectorPropsType = CidComponentPropsType & {
    toolTipTitle: string,
    fps: number,
    setFps: Function,
    isEnabled: boolean
}

export const styles: Function = (theme: *): * => ({
    mainDiv: {
        float: 'left',
        padding: '4px 0'
    },
    buttonDiv: {
        float: 'left'
    },
    label: {
        float: 'left',
        margin: '2px 0'
    },
    textInput: {
        float:'left', 
        margin: '2px 0',
        textAlign: 'center',
        width: 30
    },
    buttonStyle: {
        padding: 0,
        float:'top'
    }
})

class FpsSelector extends Component<FpsSelectorPropsType> {
    fpsTextField: *
    increaseFps: Function
    decreaseFps: Function
    verifyFps: Function
    onIncreaseButtonClick: Function
    onDecreaseButtonClick: Function
    handleFpsChange: Function
    onFpsKeyDown: Function

    constructor(props: *) {
        super(props)
        this.fpsTextField = getReactCreateRef()
        this.increaseFps = this.increaseFps.bind(this)
        this.decreaseFps = this.decreaseFps.bind(this)
        this.verifyFps = this.verifyFps.bind(this)
        this.onIncreaseButtonClick = this.onIncreaseButtonClick.bind(this)
        this.onDecreaseButtonClick = this.onDecreaseButtonClick.bind(this)
        this.handleFpsChange = this.handleFpsChange.bind(this)
        this.onFpsKeyDown = this.onFpsKeyDown.bind(this)
    }


    increaseFps(oldFps: number): * {
        let numFps = oldFps
        numFps = numFps * 5
        numFps = Math.floor(numFps)
        numFps++
        numFps = Math.max(numFps, 1)
        numFps = Math.min(numFps, 50)
        numFps = (numFps/5).toFixed(1)

        this.props.setFps(numFps)
        return numFps
    }

    decreaseFps(oldFps: number): * {
        let numFps = oldFps
        numFps = numFps * 5
        numFps = Math.ceil(numFps)
        numFps--
        numFps = Math.max(numFps, 1)
        numFps = Math.min(numFps, 50)
        numFps = (numFps/5).toFixed(1)

        this.props.setFps(numFps)
        return numFps
    }

    verifyFps() {
        let regexNum = /^[0-9]\d*(\.\d+)?$/

        let target = this.fpsTextField.current
        let numFps = 0.2

        if(target.value != null && target.value != '' && target.value.match(regexNum)) {
            numFps = target.value
        }

        numFps = numFps * 5
        numFps = Math.round(numFps)
        numFps = Math.max(numFps, 1)
        numFps = Math.min(numFps, 50)
        numFps = (numFps/5).toFixed(1)

        target.value = numFps
    }

    onIncreaseButtonClick() {
        this.increaseFps(this.props.fps)
    }

    onDecreaseButtonClick() {
        this.decreaseFps(this.props.fps)
    }

    handleFpsChange(event: *) {
        this.verifyFps()
        this.props.setFps(event.target.value)
    }

    onFpsKeyDown(event: *): * {
        const target = event.target
        const keyCode = event.which
        switch(keyCode) {
            case 38:
                target.value = this.increaseFps(target.value)
                break
            case 40:
                target.value = this.decreaseFps(target.value)
                break
            case 13:
                target.blur()
                break
            default :
        }
        if (event.shiftKey || !((keyCode >= 33 && keyCode <= 57)  
            //Numbers, Arrow keys, Home, End, PageUp, PageDown, Delete
                || (keyCode >= 96 && keyCode <= 105) //Numpads
                || keyCode == 110 //NumPadDecimal
                || keyCode == 190 //Decimal 
                || keyCode == 8) //Backspace
        ) event.preventDefault()
    }

    componentDidMount() {
        this.fpsTextField.current.value = this.props.fps
    }
    
    componentDidUpdate() {
        this.fpsTextField.current.value = this.props.fps
    }
    
    render(): Node {
        const classes = this.props.classes
        const buttonWidth = 20
        const buttonHeight = 11

        let fpsButtons = <div className={classes.buttonDiv}>

                            <div className={classes.buttonStyle}>
                                <IncreaseFpsButton width={buttonWidth} height={buttonHeight} onClick={this.onIncreaseButtonClick} isEnabled={this.props.isEnabled}/>
                            </div>

                            <div className={classes.buttonStyle}>
                                <DecreaseFpsButton width={buttonWidth} height={buttonHeight} onClick={this.onDecreaseButtonClick} isEnabled={this.props.isEnabled}/>
                            </div>
                            
                        </div>

        if(this.props.toolTipTitle != undefined) {
            fpsButtons = <Tooltip title={this.props.toolTipTitle}>
                            {fpsButtons}
                        </Tooltip>
        }

        return (
            <div className={classes.mainDiv}>
                <span className={classes.label}>fps: </span>
                <input 
                    type="text" 
                    ref={this.fpsTextField}
                    className={classes.textInput}
                    onBlur={this.handleFpsChange}
                    onKeyDown={this.onFpsKeyDown}
                    disabled={!this.props.isEnabled}/>
                
                {fpsButtons}
            </div>
        )
    }
}

export default cidComponent(styles)(FpsSelector)