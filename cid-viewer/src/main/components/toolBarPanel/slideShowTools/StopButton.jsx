//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnStop from '@assets/images/btnStop.png'
import btnStop_over from '@assets/images/btnStop_over.png'
import btnStop_disable from '@assets/images/btnStop_disable.png'

const StopButton = cidToolBarButton(btnStop, btnStop_over, btnStop_disable, 'StopButton')
export default StopButton