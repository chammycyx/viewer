//@flow
import cidToolBarButton from '@components/base/CidToolBarButton'

import btnPlay from '@assets/images/btnPlay.png'
import btnPlay_over from '@assets/images/btnPlay_over.png'
import btnPlay_disable from '@assets/images/btnPlay_disable.png'

const PlayButton = cidToolBarButton(btnPlay, btnPlay_over, btnPlay_disable, 'PlayButton')
export default PlayButton