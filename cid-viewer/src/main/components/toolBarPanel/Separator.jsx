//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import seperator from '@assets/images/Seperator.png'

type SeparatorPropsType = CidComponentPropsType & {
}

export const styles: Function = (theme: *): * => ({
    separator: {
        height: theme.toolBarPanel.toolBarBtn.height,
        float: 'left',
        overflow: 'hidden'
    }, 
    img: {
        height: theme.toolBarPanel.toolBarBtn.height
    }
})

class Separator extends Component<SeparatorPropsType> {
    render(): Node {
        const classes = this.props.classes

        return (
            <div className={classes.separator}>
                <img src={seperator} className={classes.img}/>
            </div>
        )
    }
}

export default cidComponent(styles)(Separator)