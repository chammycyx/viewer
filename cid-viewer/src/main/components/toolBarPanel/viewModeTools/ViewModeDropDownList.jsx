//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import SvgIcon from '@material-ui/core/SvgIcon'

import viewMode_1_1 from '@assets/images/viewMode_1_1.png'
import viewMode_1_2 from '@assets/images/viewMode_1_2.png'
import viewMode_2_2 from '@assets/images/viewMode_2_2.png'
import viewMode_2_3 from '@assets/images/viewMode_2_3.png'
import viewMode_3_3 from '@assets/images/viewMode_3_3.png'
import viewMode_4_4 from '@assets/images/viewMode_4_4.png'
import viewMode_5_5 from '@assets/images/viewMode_5_5.png'

import { VIEW_MODES } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import type { ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import Tooltip from '@material-ui/core/Tooltip'

import ArrowDropDown from '@material-ui/core/internal/svg-icons/ArrowDropDown'

//viewModeIcon
const viewModeIconMap = {
    '1x1': viewMode_1_1,
    '1x2': viewMode_1_2,
    '2x2': viewMode_2_2,
    '2x3': viewMode_2_3,
    '3x3': viewMode_3_3,
    '4x4': viewMode_4_4,
    '5x5': viewMode_5_5
}

const viewModeIcon = (image: string, style: *, width: number = 24, height: number = 24): Node => (
    <SvgIcon>
        <image width={width} height={height} href={image} style={style}/>
    </SvgIcon>
)
//viewModeIcon

//arrowDrowDownHover
const arrowDrowDownHoverStyles: Function = (theme: *): * => ({
    arrowDrowDownHover: {
        top: 'calc(50% - 12px)',
        right: '0px',
        color: '#ffc000',
        position: 'absolute',
        pointerEvents: 'none'
    }
})

type ArrowDropDownHoverPropsType = CidComponentPropsType & {

}

class ArrowDropDownHover extends Component<ArrowDropDownHoverPropsType> {
    render(): Node {
        const classes = this.props.classes
        return (
                <ArrowDropDown className={classes.arrowDrowDownHover}/>
        )
    }
}

const CidCompArrowDropDownHover = cidComponent(arrowDrowDownHoverStyles)(ArrowDropDownHover)
//arrowDrowDownHover

//ViewModeDropDownList
type StateType = {
    showTooltip: boolean,
    isSelectOpened: boolean,
    hover: boolean
}

export const styles: Function = (theme: *): * => ({
    outerDiv: {
        float: 'left',
        width: 150,
        height: 40,
        position: 'relative'
    },
    maskSelectedMenuItemDiv: {
        position: 'absolute', // absolute to the nearest parent with relative position
        width: 'auto', 
        height: 50, 
        marginTop: -50, 
        backgroundColor: 'black', 
        pointerEvents: 'none',
        overflow: 'hidden'
    },
    maskSelectedMenuItem: {
        height: 40,
    },
    select: {
        width: 150,
        height: 40,
        marginTop: -4
    },
    listItemSpan: {
        width: 60,
        textAlign: 'center',
        fontSize: '12pt'
    }
})

type ViewModeDropDownListPropsType = CidComponentPropsType & {
    onChange: Function,
    viewMode: ViewModeType,
    toolTipTitle: string,
    isEnabled: boolean
}

class ViewModeDropDownList extends Component<ViewModeDropDownListPropsType, StateType> {
    onChange: Function
    isDisable: Function
    getIconStyle: Function 
    hideTooltip: Function
    showTooltip: Function
    isSelectOpened: boolean
    onOpen: Function
    onClose: Function
    onMouseEnter: Function
    onMouseLeave: Function  

    constructor(props: *) {
        super(props)

        this.state = {
            showTooltip : false,
            isSelectOpened: false,
            hover: false
        }

        this.onChange = this.onChange.bind(this)
        this.isDisable = this.isDisable.bind(this)
        this.getIconStyle = this.getIconStyle.bind(this)
        this.onOpen = this.onOpen.bind(this)
        this.onClose = this.onClose.bind(this)

        this.onMouseEnter = this.onMouseEnter.bind(this)
        this.onMouseLeave = this.onMouseLeave.bind(this)
    }

    onMouseEnter(event: *){
        this.setState({
            hover: true
        })
        this.showTooltip(event)
    }

    onMouseLeave(event: *){
        this.setState({
            hover: false,
        })
        this.hideTooltip(event)
    }

    showTooltip(event: *){
        if(!this.state.isSelectOpened) {
            this.setState({
                showTooltip : true
            })
        }
    }

    hideTooltip(event: *) {
        this.setState({
            showTooltip : false
        })
    }

    onChange(event: *, key: number, payload: *) {
        const viewModeArr = [VIEW_MODES.ONE_X_ONE, VIEW_MODES.ONE_X_TWO, VIEW_MODES.TWO_X_TWO, VIEW_MODES.TWO_X_THREE, VIEW_MODES.THREE_X_THREE, VIEW_MODES.FOUR_X_FOUR, VIEW_MODES.FIVE_X_FIVE]

        const selectedValue = event.target.value||payload
        let selectedViewMode = null
        for(let i=0;i<viewModeArr.length;i++){
            const viewMode = viewModeArr[i]
            if(viewMode.text == selectedValue){
                selectedViewMode = viewMode
                break;
            }
        }
        this.props.onChange(selectedViewMode)
    }

    onOpen(event: *) {
        this.setState({
            showTooltip: false,
            isSelectOpened: true
        })
    }

    onClose(event: *) {
        this.setState({
            showTooltip: false,
            isSelectOpened: false
        })
    }

    isDisable(viewMode: ViewModeType): boolean{
        if(this.props.viewMode.text == viewMode.text){
            return true
        }
        return false
    }

    getIconStyle(viewMode: ViewModeType): *{
        if(this.props.viewMode.text == viewMode.text){
            return {opacity:0.5}
        }
        return {}
    }

    render(): Node {
        const classes = this.props.classes

        return (
            <div className={classes.outerDiv}>
                <Tooltip title={this.props.toolTipTitle} open={this.state.showTooltip}>
                <Select
                    onMouseEnter={this.onMouseEnter}
                    onMouseLeave={this.onMouseLeave}
                    onClick={this.onMouseLeave}
                    onChange={this.onChange}
                    value={this.props.viewMode.text}
                    className={classes.select}
                    disableUnderline={true}
                    disabled={!this.props.isEnabled}
                    open={this.state.isSelectOpened}
                    onOpen={this.onOpen}
                    onClose={this.onClose}
                    IconComponent={this.state.hover ? CidCompArrowDropDownHover : ArrowDropDown}
                >
                    <MenuItem value={VIEW_MODES.ONE_X_ONE.text} disabled={this.isDisable(VIEW_MODES.ONE_X_ONE)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_1_1, this.getIconStyle(VIEW_MODES.ONE_X_ONE))}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.ONE_X_ONE.text}</span>
                    </MenuItem>
                    <MenuItem value={VIEW_MODES.ONE_X_TWO.text} disabled={this.isDisable(VIEW_MODES.ONE_X_TWO)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_1_2, this.getIconStyle(VIEW_MODES.ONE_X_TWO))}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.ONE_X_TWO.text}</span>
                    </MenuItem>
                    <MenuItem value={VIEW_MODES.TWO_X_TWO.text} disabled={this.isDisable(VIEW_MODES.TWO_X_TWO)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_2_2, this.getIconStyle(VIEW_MODES.TWO_X_TWO))}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.TWO_X_TWO.text}</span>
                    </MenuItem>
                    <MenuItem value={VIEW_MODES.TWO_X_THREE.text} disabled={this.isDisable(VIEW_MODES.TWO_X_THREE)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_2_3, this.getIconStyle(VIEW_MODES.TWO_X_THREE), 24, 25)}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.TWO_X_THREE.text}</span>
                    </MenuItem>
                    <MenuItem value={VIEW_MODES.THREE_X_THREE.text} disabled={this.isDisable(VIEW_MODES.THREE_X_THREE)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_3_3, this.getIconStyle(VIEW_MODES.THREE_X_THREE))}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.THREE_X_THREE.text}</span>
                    </MenuItem>
                    <MenuItem value={VIEW_MODES.FOUR_X_FOUR.text} disabled={this.isDisable(VIEW_MODES.FOUR_X_FOUR)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_4_4, this.getIconStyle(VIEW_MODES.FOUR_X_FOUR))}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.FOUR_X_FOUR.text}</span>
                    </MenuItem>
                    <MenuItem value={VIEW_MODES.FIVE_X_FIVE.text} disabled={this.isDisable(VIEW_MODES.FIVE_X_FIVE)}>
                        <ListItemIcon>
                            {viewModeIcon(viewMode_5_5, this.getIconStyle(VIEW_MODES.FIVE_X_FIVE))}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{VIEW_MODES.FIVE_X_FIVE.text}</span>
                    </MenuItem>
                </Select>
                </Tooltip>
                <div className={classes.maskSelectedMenuItemDiv}>
                    <MenuItem disabled={!this.props.isEnabled} className={classes.maskSelectedMenuItem}>
                        <ListItemIcon>
                            {viewModeIcon(viewModeIconMap[this.props.viewMode.text], {}, 24, (this.props.viewMode.text==VIEW_MODES.TWO_X_THREE.text)?25:24)}
                        </ListItemIcon>
                        <span className={classes.listItemSpan}>{this.props.viewMode.text}</span>
                    </MenuItem>
                </div>
            </div>
        )
    }
}

export default cidComponent(styles)(ViewModeDropDownList)
//ViewModeDropDownList