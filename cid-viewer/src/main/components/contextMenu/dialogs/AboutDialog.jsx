//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'

import cidNestMuiThemeProvider from '@components/base/CidNestMuiThemeProvider'
import { cidThemeForDialog } from '@styles/CidTheme'
import { getVersion, getConfigVersion, getServiceVersion } from '@utils/VersionUtil'

type AboutDialogPropsType = CidComponentPropsType & {
	isOpen: boolean,
	onClose: Function,
	applicationVersion: string,
	cidWsContextPath: string,
	requestSys: string,
	userId: string,
	hospCode: string
}

export const styles: Function = function(theme: *): * {
	const DIALOG_WIDTH = '570px'
	const DIALOG_HEIGHT = '340px'

	return {
		aboutTitle: {
			padding: "24px 24px 20px",
			fontSize: "24px",
			fontFamily: theme.typography.fontFamily
		},
		aboutContent: {
			fontFamily: theme.typography.fontFamily,
			fontSize: "14px",
			borderStyle: "solid",
			borderWidth: "1px",
			width: "100%",
			lineHeight: "1.5em",
			color: "rgba(0, 0, 0, 0.54)",
			paddingTop: "10px",
			paddingLeft: "10px"
		},
		aboutButton: {
			display: 'block',
			textAlign: 'center'
		},
		dialog: {

		},
		dialogContent: {
			overflow: 'hidden'
		},
		infoLabel: {
			boxSizing: 'border-box',
			verticalAlign: 'text-top',
			display: 'inline-block',
			width: '25%',
		},
		infoValue: {
			boxSizing: 'border-box',
			verticalAlign: 'text-top',
			display: 'inline-block',
			width: '75%',
			textOverflow: 'ellipsis'
		},
		paper: {
			width: DIALOG_WIDTH,
			minWidth: DIALOG_WIDTH,
			maxWidth: DIALOG_WIDTH,
			height: DIALOG_HEIGHT,
			minHeight: DIALOG_HEIGHT,
			maxHeight: DIALOG_HEIGHT
		}
	}
}

class AboutDialog extends Component<AboutDialogPropsType> {
	
	render(): Node {
		const classes = this.props.classes

		return (
			<div>
				<Dialog
					open={this.props.isOpen}
					classes={{root: classes.dialog, paper: classes.paper}}>
					<DialogTitle disableTypography={true} className={classes.aboutTitle}>
						<span>About CID Viewer</span>
					</DialogTitle>
					<DialogContent className={classes.dialogContent}>
						<div className={classes.aboutContent}>
							<div>
								<span><b>{"<Application Information>"}</b></span><br/>
							</div>	

							<div>
								<div className={classes.infoLabel}>
									<span>{"Version (Build)"}</span>
								</div>	
								<div className={classes.infoValue}>
									<span>{": "+getVersion()}</span><br/>
								</div>
							</div>

							<div>
								<div className={classes.infoLabel}>
									<span>{"Version (Config)"}</span>
								</div>	
								<div className={classes.infoValue}>
									<span>{": "+getConfigVersion(this.props.applicationVersion)}</span><br/>
								</div>
							</div>	

							<div>
								<div className={classes.infoLabel}>
									<span>{"Version (Service)"}</span>
								</div>	
								<div className={classes.infoValue}>
									<span>{": "+getServiceVersion(this.props.cidWsContextPath)}</span><br/>
								</div>
							</div>	
							<br/>
							<div>
								<span><b>{"<Session Information>"}</b></span><br/>
							</div>	

							<div>
								<div className={classes.infoLabel}>
									<span>{"System"}</span>
								</div>	
								<div className={classes.infoValue}>
									<span>{": "+this.props.requestSys}</span><br/>
								</div>
							</div>

							<div>
								<div className={classes.infoLabel}>
									<span>{"User ID"}</span>
								</div>	
								<div className={classes.infoValue}>
									<span>{": "+this.props.userId}</span><br/>
								</div>
							</div>

							<div>
								<div className={classes.infoLabel}>
									<span>{"HOSP. Code"}</span>
								</div>	
								<div className={classes.infoValue}>
									<span>{": "+this.props.hospCode}</span><br/>
								</div>
							</div>
						</div>
					</DialogContent>
					<DialogActions className={classes.aboutButton}>
						<Button 
							onClick={this.props.onClose} 
							disableRipple = {false}
							focusRipple = {true}
							autoFocus
							color="primary">
							OK
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		)
	}
}

export default cidComponent(styles)(cidNestMuiThemeProvider(cidThemeForDialog)(AboutDialog))