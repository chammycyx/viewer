//@flow
import { connect } from 'react-redux'
import AboutDialog from './AboutDialog'
import { setIsOpen as setIsAboutDialogOpen } from '@actions/components/contextMenu/aboutDialog/Action'

const mapStateToProps = (state: *): * => {
    return { 
        isOpen: state.components.contextMenu.aboutDialog.rootState.isOpen,
        applicationVersion: state.data.config.application.rootState.viewerControl.applicationVersion,
        cidWsContextPath: state.data.config.application.rootState.webServiceIcw.cidWsContextPath,
        requestSys: state.data.common.rootState.requestSys,
        userId: state.data.common.rootState.userId,
        hospCode: state.data.common.rootState.hospCode
    }
}

const mapDispatchToProps = (dispatch: *): * => ({
    onClose: () => {
        dispatch(setIsAboutDialogOpen(false))
    }
})

export default connect(
    mapStateToProps, // comment it for testing its absent behavior
    mapDispatchToProps
)(AboutDialog)