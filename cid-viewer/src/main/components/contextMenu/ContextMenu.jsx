//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import {getVersion} from '@utils/VersionUtil'

import { hideMenu } from 'react-contextmenu/modules/actions'


type ContextMenuPropsType = CidComponentPropsType & {
	menuItems: Array<*>,
	onAboutDialogOpen: Function
}
export const styles: Function = (theme: *): * => ({
	menuList: {
		boxShadow: '0px 1px 5px 0px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 3px 1px -2px rgba(0,0,0,0.12)',
		borderRadius: '4px',
		backgroundColor: theme.palette.background.paper,
		zIndex: '1600'
	}
})

class ContextMenu extends Component<ContextMenuPropsType> {
	menuItems: Array<*>
	onAboutButtonClick: Function

	constructor(props: *) {
		super(props)
		this.onAboutButtonClick = this.onAboutButtonClick.bind(this)
    }

	onAboutButtonClick(menuItem: *): Function {
		return function() {
			menuItem.onOpen()
			hideMenu()
		}
	}

	render(): Node {
		const classes = this.props.classes

		let menuItems = [{
            title: 'About CID Viewer ('+getVersion()+')...',
            onOpen: this.props.onAboutDialogOpen,
		}]

		return (
			<div>
				<MenuList className={classes.menuList}>
					<MenuItem onClick={this.onAboutButtonClick(menuItems[0])}>{menuItems[0].title}</MenuItem>
				</MenuList>
			</div>
		)
	}
}
export default cidComponent(styles)(ContextMenu)