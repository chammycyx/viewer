//@flow
import { connect } from 'react-redux'
import ContextMenu from './ContextMenu'
import { setIsOpen as setIsAboutDialogOpen } from '@actions/components/contextMenu/aboutDialog/Action'

const mapStateToProps = (state: *): * => {
    return { 
    }
}

const mapDispatchToProps = (dispatch: *): * => ({
    onAboutDialogOpen: () => {
        dispatch(setIsAboutDialogOpen(true))
    }
})

export default connect(
    mapStateToProps, // comment it for testing its absent behavior
    mapDispatchToProps
)(ContextMenu)