// Creator HOC

//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import IconButton from '@material-ui/core/IconButton'
import {ImageTool} from '@constants/ComponentConstants'
import Tooltip from '@material-ui/core/Tooltip'
import SvgIcon from '@material-ui/core/SvgIcon'

type CidToolBarButtonPropsType = CidComponentPropsType & {
    isEnabled: boolean,
    onClick: Function,
    toolTipTitle: string
}

type StateType = {
    hover: boolean
}

export const styles: Function = (theme: *): * => ({
    div: {
        float: 'left',
        width: theme.toolBarPanel.toolBarBtn.width,
        height: theme.toolBarPanel.toolBarBtn.height,
        background: theme.toolBarPanel.toolBarBtn.color.bg,
    },
    button: {
        width: theme.toolBarPanel.toolBarBtn.width,
        height: theme.toolBarPanel.toolBarBtn.height,
        borderRadius: 0
    },
    svgIcon: {
        width: theme.toolBarPanel.toolBarBtn.width,
        height: theme.toolBarPanel.toolBarBtn.height
    }
})

export function noHoc(iconNormal: *, iconOver: *, iconDisabled: *, displayName: string,
    tool: string = ImageTool.NO_TOOL, PropBox: * = null, propBoxAttrObj: * = null): * {
    class CidToolBarButton extends Component<CidToolBarButtonPropsType, StateType> {
        onClick: Function
        onMouseEnter: Function
        onMouseLeave: Function  
        
        constructor(props: *) {
            super(props)
            this.onClick = this.onClick.bind(this)
            this.state = {
                hover: false
            }
            this.onMouseEnter = this.onMouseEnter.bind(this)
            this.onMouseLeave = this.onMouseLeave.bind(this)
        }

        onClick(e: *) {
            if(this.props.isEnabled) {
                this.props.onClick(e)
            }
        }

        onMouseEnter(){
            this.setState({
                hover: true
            })
        }
        
        onMouseLeave(){
            this.setState({
                hover: false
            })
        }

        render(): Node {
            const classes = this.props.classes
            let imageSrc = iconNormal
            const width = this.props.theme.toolBarPanel.toolBarBtn.width
            const height = this.props.theme.toolBarPanel.toolBarBtn.height
            const viewBox = '0 0 ' + width + ' ' + height

            if(this.props.isEnabled != undefined && !this.props.isEnabled && iconDisabled != null)
                imageSrc = iconDisabled

            let iconButtonComp = <IconButton 
                                    onClick={this.onClick} 
                                    onMouseEnter={this.onMouseEnter} 
                                    onMouseLeave={this.onMouseLeave} 
                                    className={classes.button} 
                                    disabled={!this.props.isEnabled}>
                                    <SvgIcon viewBox={viewBox} className={classes.svgIcon}>
                                        <image x={0} y={4} width={width} height={height} href={(this.state.hover && this.props.isEnabled) ? iconOver : imageSrc} />
                                    </SvgIcon>
                                </IconButton>

            if(this.props.toolTipTitle != undefined) {
                iconButtonComp = <Tooltip title={this.props.toolTipTitle} disableFocusListener={true}>
                                    <div>
                                        {iconButtonComp}
                                    </div>
                                </Tooltip>
            }

            return (
                <div className={classes.div}>
                    {iconButtonComp}
                </div>
            )
        }
    }

    CidToolBarButton.displayName = displayName

    return CidToolBarButton
}

export function applyHoc(noHocComp: *): * {
    return cidComponent(styles)(noHocComp)
}

export default function cidToolBarButton(IconCompNormal: *, IconCompOver: *, IconCompDisabled: *, displayName: string,
    tool: string = ImageTool.NO_TOOL, PropBox: * = null, propBoxAttrObj: * = null): * {
    return applyHoc(noHoc(IconCompNormal, IconCompOver, IconCompDisabled, displayName,
        tool, PropBox, propBoxAttrObj))
}