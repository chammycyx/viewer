// Wrapper HOC

// @flow
import React, { Component, type ComponentType } from 'react'
import type { Node } from 'react'

import { getReactCreateRef, getDocument } from '@utils/ValidationByPassUtil'
import Paper from '@material-ui/core/Paper'
import createDisplayName from '@utils/DisplayNameUtil'

import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

export type PropsBoxPropsType = CidComponentPropsType & {
    visible: boolean,
    opacity: number,
    resetPositionWhenReopen: boolean,
    x: number,
    y: number
}

export const styles: Function = function(wrappedCompStyles?: Function): * {
    return function(theme: *): * {
        const stylesObj = {
            outerDiv: {
                position: 'absolute',
                zIndex: 9,
                width: theme.toolBarPanel.propsBox.width,
                visibility: function(props: *): string {
                    return props.visible ? 'visible' : 'hidden'
                }
            },
            paper: {
                width: '100%',
                height: '100%',
                padding: 10,
                background: '#808080',
                color: '#000000',
                opacity: function(props: *): number {
                    return props.opacity
                }
            }
        }

        if(wrappedCompStyles!=undefined) {
            const wrappedCompStylesObj = wrappedCompStyles(theme)
            return Object.assign(stylesObj, wrappedCompStylesObj)
        } else {
            return stylesObj
        }
    }
}

export function noHoc(WrappedComponent: ComponentType<*>): ComponentType<*> {
    class CidPropsBox extends Component<*> {
        static defaultProps: *

        propsBoxRef: *
        pos1: number
        pos2: number
        pos3: number
        pos4: number
        onDragMouseDown: Function
        elementDrag: Function

        constructor(props: *) {
            super(props)
            this.propsBoxRef = getReactCreateRef()
            this.pos1 = this.pos2 = this.pos3 = this.pos4 = 0

            this.onDragMouseDown = this.onDragMouseDown.bind(this)
            this.elementDrag = this.elementDrag.bind(this)
        }

        onDragMouseDown(e: *) {
            e = e || window.event
            e.preventDefault()
            // get the mouse cursor position at startup:
            this.pos3 = e.clientX
            this.pos4 = e.clientY
            getDocument().onmouseup = this.closeDragElement
            // call a function whenever the cursor moves:
            getDocument().onmousemove = this.elementDrag
        }

        elementDrag(e: *) {
            e = e || window.event
            e.preventDefault()
            // calculate the new cursor position:
            this.pos1 = this.pos3 - e.clientX
            this.pos2 = this.pos4 - e.clientY
            this.pos3 = e.clientX
            this.pos4 = e.clientY
            // set the element's new position:
            this.propsBoxRef.current.style.top = (this.propsBoxRef.current.offsetTop - this.pos2) + "px"
            this.propsBoxRef.current.style.left = (this.propsBoxRef.current.offsetLeft - this.pos1) + "px"
        }

        closeDragElement() {
            /* stop moving when mouse button is released:*/
            getDocument().onmouseup = null
            getDocument().onmousemove = null
        }

        resetPosition() {
            this.propsBoxRef.current.style.top = this.props.x
            this.propsBoxRef.current.style.left = this.props.y
        }

        componentDidMount() {
            this.resetPosition()
        }

        componentDidUpdate(prevProps: *) {
            // The only case to not to reset position is the dialog being changed when it keeps opened
            if(this.props.resetPositionWhenReopen && !(prevProps.visible && this.props.visible)) {
                this.resetPosition()
            }
        }

        render(): Node {
            const classes = this.props.classes
            return (
                <div ref={this.propsBoxRef} className={classes.outerDiv}>
                    <Paper className={classes.paper}>
                        <WrappedComponent onDragMouseDown={this.onDragMouseDown} propsBoxRef={this.propsBoxRef} {...this.props}/>
                    </Paper>
                </div>
            )
        }
    }

    CidPropsBox.defaultProps = {
        visible: false,
        opacity: 1,
        resetPositionWhenReopen: false,
        x: 0,
        y: 0
    }

    CidPropsBox.displayName = createDisplayName('CidPropsBox', WrappedComponent)

    return CidPropsBox
}

export function applyHoc(wrappedCompStyles?: Function): Function {
    return function(NoHocComp: ComponentType<*>): ComponentType<*> {
        return cidComponent(styles(wrappedCompStyles))(NoHocComp)
    }
}

export default function cidPropsBox(wrappedCompStyles?: Function): Function {
    return function(WrappedComponent: ComponentType<*>): ComponentType<*> {
        return applyHoc(wrappedCompStyles)(noHoc(WrappedComponent))
    }
}