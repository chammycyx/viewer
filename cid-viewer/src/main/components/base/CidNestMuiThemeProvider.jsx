// Wrapper HOC

//@flow
import React, { Component, type Node, type ComponentType } from 'react'
import createDisplayName from '@utils/DisplayNameUtil'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'

type CidNestMuiThemeProviderPropsType = {
    
}

export default function cidNestMuiThemeProvider(nestTheme: *): Function {
    return function(WrappedComp: ComponentType<*>): ComponentType<*> {
        class CidNestMuiThemeProvider extends Component<CidNestMuiThemeProviderPropsType> {
            constructor(props: *) {
                super(props)
            }

            render(): Node{
                return(
                    <MuiThemeProvider theme={createMuiTheme(nestTheme)}>
                        <WrappedComp {...this.props}/>
                    </MuiThemeProvider>
                )
            }
        }

        CidNestMuiThemeProvider.displayName = createDisplayName('CidNestMuiThemeProvider', WrappedComp)

        return CidNestMuiThemeProvider
    }
}