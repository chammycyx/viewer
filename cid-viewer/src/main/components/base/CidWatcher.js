//@flow

import React, {Component, type Node} from 'react'
import { connect } from 'react-redux'
import createDisplayName from '../../utils/DisplayNameUtil'



/**
 * 
 * Watcher HOC
 * 
 * @deprecated Use loggerForComponent instead
 * 
 * @param {any} WrappedComponent 
 * @returns 
 */
export function cidWatcher(watcherInfo: *): Function {
    return function(WrappedComponent: *): * {
        type WatcherPropsType = {
            dispatcher: Function
        }

        class Watcher extends React.Component<WatcherPropsType> {
            shouldComponentUpdate(nextProps: *): boolean{
                let dispatcher: * = this.props.dispatcher
                watcherInfo.callback(dispatcher, this.props, nextProps)
                return false
            }

            render(): Node {
            return (
                ""
            )
            }
        }
        
        const watcherStateProps = watcherInfo.watchState
        
        const watcherActionProps = (dispatch: *): * => ({
                dispatcher: (action: *)=>{
                    dispatch(action)
                }
            })
        
        let ConnectedWatcher = connect(
            watcherStateProps,
            watcherActionProps
        )(Watcher)


        type CidWatcherPropsType = {
        }

        class CidWatcher extends Component<CidWatcherPropsType> {
            constructor(props: *) {
                super(props)
            }

            render(): Node{
                return( 
                    <div>
                        <ConnectedWatcher/>
                        <WrappedComponent {...this.props}/>
                    </div>
                )
            }
        }

        CidWatcher.propTypes = {
        }

        CidWatcher.displayName = createDisplayName('CidWatcher', WrappedComponent)

        return CidWatcher
    }
}
