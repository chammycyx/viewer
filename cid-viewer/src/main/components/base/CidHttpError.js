//@flow
import CidError from './CidError'

export default class CidHttpError extends CidError {
    exceptionMsg: string
    status: number
    isTimeout: boolean

    constructor(className: string, functionName: string, url: string, err: *, data?: * = '') {
        let exceptionMsg: string = ''
        let status: number = -1
        let statusText: string = ''
        let isTimeout: boolean = false

        if(err.request){
            let httpRequest = err.request
            exceptionMsg = httpRequest.getResponseHeader("Exception-Message")
            status = httpRequest.status
            statusText = httpRequest.statusText
        }
        if(err.code == 'ECONNABORTED'){
            isTimeout = true
        }
        
        let msg = 'Context Path: ' + url + '\n'
        + 'Status & Text: ' + status + ' ' + statusText + '\n'
        + 'Exception message: ' + exceptionMsg + '\n'
        + 'Data: ' + JSON.stringify(data) + '\n'
        + 'Error Content: ' + JSON.stringify(err) + err

        super(className, functionName, msg)

        this.exceptionMsg = exceptionMsg
        this.status = status
        this.isTimeout = isTimeout
    }
}