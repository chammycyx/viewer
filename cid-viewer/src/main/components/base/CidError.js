//@flow
export default class CidError extends Error {
    classFuncPath: Array<string>
    plainMsg: string
    
    constructor(className: string, functionName: string, plainMsg: string) {
        let classFunc = className+'.'+functionName
        let msgWithClassFuncPath = classFunc+'\n'+plainMsg

        super(msgWithClassFuncPath)

        this.classFuncPath = [classFunc]
        this.plainMsg = plainMsg
    }

    // append(className: string, functionName: string) {
    //     this.classFuncPath.unshift(className+'.'+functionName)

    //     const classFuncPathLength = this.classFuncPath.length
    //     let classFuncStr = ''

    //     for(let i=0; i<classFuncPathLength; i++) {
    //         classFuncStr += this.classFuncPath[i]+'\n'
    //     }

    //     super.message = classFuncStr+this.msg
    // }
}