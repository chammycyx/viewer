//Wrapper HOC

//@flow
import React, { Component, type Node, type ComponentType } from 'react'
import { loggerForComponent, type LoggerPropsType } from './LoggerForComponent'
import createDisplayName from '@utils/DisplayNameUtil'

import withTheme from '@material-ui/core/styles/withTheme'

import injectSheet from 'react-jss'

type CidComponentOwnPropsType = {
    
}

type MuiComponentPropsType = {
    theme: *
}

type JssComponentPropsType = {
    classes: *
}

export default function cidComponent(styles?: Function): Function {
    return function(WrappedComponent: ComponentType<*>): ComponentType<*> {
        let WrappedCompWithHoc = WrappedComponent

        // Logger
        WrappedCompWithHoc = loggerForComponent(WrappedCompWithHoc) //displayName = LoggerForComponent(XXXXXX)

        // MUI withTheme
        WrappedCompWithHoc = withTheme()(WrappedCompWithHoc) //displayName = WithTheme(XXXXXX)

        // JSS injectSheet
        if(styles!=undefined)
            WrappedCompWithHoc = injectSheet(styles)(WrappedCompWithHoc) //displayName = Jss(XXXXXX)

        const cidCompDisplayName = createDisplayName('CidComponent', WrappedCompWithHoc)

        class CidComponent extends Component<CidComponentOwnPropsType> {
            constructor(props: *) {
                super(props)
            }

            render(): Node{
                return(
                    <div data-react-class={cidCompDisplayName}>
                        <WrappedCompWithHoc {...this.props}/>
                    </div>
                )
            }
        }

        CidComponent.displayName = cidCompDisplayName

        return CidComponent
    }
}

// Consolidate all CID HOC props type
export type CidComponentPropsType = LoggerPropsType & CidComponentOwnPropsType & MuiComponentPropsType & JssComponentPropsType