// Creator HOC

//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import Tooltip from '@material-ui/core/Tooltip'

type CidSpecialButtonPropsType = CidComponentPropsType & {
    onClick: Function,
    onMouseEnter: Function,
    onMouseLeave: Function,
    width: number,
    height: number,
    isEnabled: boolean,
    toolTipTitle: string
}

type StateType = {
    hover: boolean
}

export const styles: Function = (theme: *): * => ({
    div: {
        
        width: function(props: *): number {
            return props.width
        },
        height: function(props: *): number {
            return props.height
        },
        padding: 0,
        position: 'relative'
    },
    button: {
        width: function(props: *): number {
            return props.width
        },
        height: function(props: *): number {
            return props.height
        },
        padding: 0,
        border: 'none',
        cursor: function(props: *): string {
            return props.isEnabled? 'pointer' : 'default'
        },
        backgroundColor: 'transparent'
    }
})

export function noHoc(iconNormal: string, iconOver: string, iconDisabled: string, displayName: string, onClickParams: Array<*> = []): * {
    class CidSpecialButton extends Component<CidSpecialButtonPropsType, StateType> {
        static defaultProps: *
        onClick: Function
        onMouseEnter: Function
        onMouseLeave: Function        

        constructor(props: *) {
            super(props)
            this.onClick = this.onClick.bind(this)
            this.state = {
                hover: false
            }
            this.onMouseEnter = this.onMouseEnter.bind(this);
            this.onMouseLeave = this.onMouseLeave.bind(this);
        }
    
        onClick() {
            if(this.props.isEnabled)
                this.props.onClick(...onClickParams)
        }
        
        onMouseEnter(){
            this.setState({
                hover: true
            })
        }
        
        onMouseLeave(){
            this.setState({
                hover: false
            })
        }

        render(): Node {
            const classes = this.props.classes
            let imageSrc = iconNormal
            let buttonTag

            if(iconDisabled != '' && !this.props.isEnabled)
                imageSrc = iconDisabled

            buttonTag = <button 
                                onClick={this.onClick} 
                                onMouseEnter={this.onMouseEnter} 
                                onMouseLeave={this.onMouseLeave} 
                                className={classes.button}
                                disabled={!this.props.isEnabled}>
                                <img 
                                    src={(this.state.hover && this.props.isEnabled) ? iconOver : imageSrc}
                                />
                            </button>

            if(this.props.toolTipTitle != undefined) {
                buttonTag = <Tooltip title={this.props.toolTipTitle} placement='right' disableFocusListener={true}>
                                {buttonTag}
                            </Tooltip>
            }

            return (
                <div className={classes.div}>
                    {buttonTag}
                </div>
            )
        }
    }

    CidSpecialButton.defaultProps = {
        isEnabled: true
    }

    CidSpecialButton.displayName = displayName

    return CidSpecialButton
}

export function applyHoc(noHocComp: *): * {
    return cidComponent(styles)(noHocComp)
}

export default function cidSpecialButton(iconNormal: string, iconOver: string, iconDisabled: string, displayName: string, onClickParams: Array<*> = []): * {
    return applyHoc(noHoc(iconNormal, iconOver, iconDisabled, displayName, onClickParams))
}