export const noHoc = jest.requireActual('../CidPropsBox.jsx').noHoc
export const applyHoc = jest.requireActual('../CidPropsBox.jsx').applyHoc
export const styles = jest.requireActual('../CidPropsBox.jsx').styles

export default function cidPropsBox(wrappedCompStyles?,
    mockOnDragMouseDown? = jest.fn(), mockResetPosition? = jest.fn()) {
    return function(WrappedComponent) {
        let CidPropsBox = noHoc(WrappedComponent)
        CidPropsBox.prototype.onDragMouseDown = mockOnDragMouseDown
        CidPropsBox.prototype.resetPosition = mockResetPosition
        return applyHoc(wrappedCompStyles)(CidPropsBox)
    }
}