//@flow
import { connect, type MapStateToProps, type MapDispatchToProps} from 'react-redux'
import cidComponent from '@components/base/CidComponent'
import ImageViewingPanel from './ImageViewingPanel'
import {onImgMoved, onCanvasClick, setImageZoom, setComponentState} from '@actions/components/imageViewingPanel/canvasGrid/Action'
import WatcherInfo from '../../model/vo/WatcherInfo'
import type {ViewModeType} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

const mapStateToProps = (state: *): MapStateToProps => {
    return {
        canvasGridData: state.components.imageViewingPanel.canvasGrid.rootState
    }
}

const mapDispatchToProps = (dispatch: *): MapDispatchToProps => ({
    onImgMoved: (seriesId: string, imageId: string, xCoor: number, yCoor: number) => {
        dispatch(onImgMoved(seriesId, imageId, xCoor, yCoor))
    },
    onCanvasClick: (imageId: string, button: string) => {
        dispatch(onCanvasClick(imageId, button))
    },
    onImageZoom: (seriesId: string, imageId: string, zoomMode: string, zoomX: number, zoomY: number, pannedCoordinateRule: Function) => {
        dispatch(setImageZoom(seriesId, imageId, zoomMode, zoomX, zoomY, pannedCoordinateRule))
    },
    updateComponentState: (componentState: *) => {
        dispatch(setComponentState(componentState))
    }
})

let connector = connect(
    mapStateToProps,
    mapDispatchToProps
)

let connectedComponent = connector(cidComponent()(ImageViewingPanel))

export default connectedComponent
