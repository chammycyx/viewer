//@flow

import React, { Component, type Node } from 'react'
import { fabric } from 'fabric'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import {DateParser, DATE_FORMAT} from '@utils/DateParser'
import {getReactCreateRef} from '@utils/ValidationByPassUtil'

import {isNullOrEmptyString} from '@utils/StringUtil'

import openHandCursor from '@src/assets/images/cursor/openhand.cur'
import closedHandCursor from '@src/assets/images/cursor/closedhand.cur'
import zoomInCursor from '@src/assets/images/cursor/zoomIn.cur'
import zoomOutCursor from '@src/assets/images/cursor/zoomOut.cur'
import {ImageTool, ImageSelection, BRIGHTNESS_ADJUSTMENT_FACTOR, RELOCATE_IMAGE_IN_CANVAS_FACTOR} from '@constants/ComponentConstants'

import Tooltip from '@material-ui/core/Tooltip'

import { glErrorToString, checkIfErrorOccurred, getWebGLContext } from '@utils/WebGlUtil'

import CidError from '@components/base/CidError'

type CanvasPropsType = CidComponentPropsType & {
    width: number,
    height: number,
    canvasProfile: *,
    seriesIndexId: string,
    imgIdexId: string,
    caseNo: string,
    displayDateTime: string,
    onImgMoved: Function,
    onCanvasClick: Function,
    fontType: string, 
    fontSize: number,
    fontColor: string,
    bgColor: string,
    headerVisibility: boolean,
    headerTimeText: string,
    imageTool: string,
    onImageZoom: Function,
    updateComponentState: Function
}

const FABRIC_IMAGE_ID: string = 'FABRIC_IMAGE_ID'

export const styles: Function = (theme: *): * => ({
    containerDiv: {
        padding: 0,
        position: 'relative'
    },
    headerContainerDiv: {
        position: 'absolute', 
        top: 0, 
        left: 0, 
        zIndex: 5, 
        width: '100%'
    },
    headerDiv: {
        backgroundColor: function(props: *): string {
			return props.bgColor
		}, 
        padding: theme.imageViewingPanel.canvasGrid.headerPaddingTop + 'px ' + theme.imageViewingPanel.canvasGrid.headerPaddingLeft + 'px', 
        whiteSpace: 'nowrap', 
        overflow: 'hidden', 
        textOverflow: 'ellipsis',
        fontStyle: 'italic', 
        fontSize: function(props: *): number {
            return props.fontSize
        }, 
        fontFamily: function(props: *): string {
            return props.fontType
        },
        color: function(props: *): string {
            return props.fontColor
        }
    },
    canvasContainerDiv: {
        position: 'absolute', 
        top: 0, 
        left: 0
    }
})

class Canvas extends Component<CanvasPropsType> {
    canvas: *
    fabricCanvas: *
    fabricImg: *
    init: Function
    createFaricCanvas: Function
    createFabricImg: Function
    renderFabricImg: Function   
    renderFabricCanvas: Function
    renderFabricImgAttributes: Function
    privateMembers: PrivateMembers

    //Introduce below variables to fix the late back case caused by Image.Onload, 
    canvasProfile: *
    imageTool: string
    imgIdexId: string

    canvasHeader: *

    constructor(props: *) {
        super(props)
        this.canvas = getReactCreateRef()
        this.canvasHeader = getReactCreateRef();
        this.init = this.init.bind(this)
        this.createFaricCanvas = this.createFaricCanvas.bind(this)
        this.createFabricImg = this.createFabricImg.bind(this)
        this.renderFabricCanvas = this.renderFabricCanvas.bind(this)
        this.renderFabricImg = this.renderFabricImg.bind(this)
        this.renderFabricImgAttributes = this.renderFabricImgAttributes.bind(this)
        this.privateMembers = new PrivateMembers()
    }

    init(){
        this.fabricCanvas = this.createFaricCanvas()
        this.fabricImg = this.createFabricImg()
    }

    createFaricCanvas(): * {
        let canvas = new fabric.Canvas(this.canvas.current, {
            preserveObjectStacking: true
        })
        canvas.selection = false

        let that = this
        
        canvas.on('object:modified', function(evt: *) { 
            let modifiedObject = evt.target;
            if(modifiedObject.id==FABRIC_IMAGE_ID){
                //dispatch action to update the xcoor and ycoor
                if(!isNullOrEmptyString(that.props.seriesIndexId) && !isNullOrEmptyString(that.props.imgIdexId)){
                    that.props.onImgMoved(that.props.seriesIndexId, that.props.imgIdexId, modifiedObject.get('left'), modifiedObject.get('top'))
                }
            }
        })
        
        canvas.on('mouse:move', function(evt: *) { 
            if(that.fabricImg != undefined) {
                let pointer = canvas.getPointer(evt.e)
                let pointerInCanvas: boolean = pointer.x > 0 
                    && pointer.y > that.canvasHeader.current.offsetHeight
                    && pointer.x < canvas.width 
                    && pointer.y < canvas.height
            
                if(pointerInCanvas){
                    that.fabricImg.lockMovementX = that.fabricImg.lockMovementY = false
                }else{
                    that.fabricImg.lockMovementX = that.fabricImg.lockMovementY = true
                }
            }
        })

        canvas.on('mouse:down', function(evt: *) { 
            if(evt.e.shiftKey){
                that.props.onCanvasClick(that.props.imgIdexId,ImageSelection.SHIFT)
            }else if(evt.e.ctrlKey){
                that.props.onCanvasClick(that.props.imgIdexId,ImageSelection.CTRL)
            }else{
                that.props.onCanvasClick(that.props.imgIdexId,'')
            }
        })

        return canvas
    }

    createFabricImg(): * {
        let that = this
        let fabricImg = new fabric.Image(null,{
            id:FABRIC_IMAGE_ID, 
            hasControls: false,
            hasBorders: false
        })
        fabricImg.on('mousedown', function(opt: *) {
            let evt = opt.e
            let imageTool = that.props.imageTool

            that.props.onImageZoom(that.props.seriesIndexId, that.props.imgIdexId, imageTool, evt.offsetX, evt.offsetY, that.privateMembers.calculatePannedCoordinate)
        })

        //add mouse wheel listener
        fabricImg.on('mousewheel', function(opt: *) {
            let evt = opt.e
            if(evt.ctrlKey===false){
                let delta = opt.e.deltaY
                let zoomMode = null
                if(delta>0){
                    zoomMode = ImageTool.ZOOM_OUT
                }else{
                    zoomMode = ImageTool.ZOOM_IN
                }
                that.props.onImageZoom(that.props.seriesIndexId, that.props.imgIdexId, zoomMode, evt.offsetX, evt.offsetY, that.privateMembers.calculatePannedCoordinate)
            }
        })

        return fabricImg
    }

    shouldComponentUpdate(nextProps: *): boolean {
        // To prevent extra rendering when state change but only affect content of FabricJS canvases, so handle Fabric related logic in here and return false to bypass render()
        //fabricCanvas and fabricImg would be initialized after componentDidMount.
        //So that fabric image and header would not be rendered at the Mounting cycle. They should be availble to render after the Mounting cycle.
        //API forceUpdate() is called in componentDidMount in order to make the fabric image and header effective
        if(this.fabricCanvas!=null && this.fabricImg!=null){
            this.renderFabricCanvas(nextProps)
            if(nextProps.canvasProfile==null){
                //1. remove image and header from canvas
                this.fabricImg.opacity = 0
                this.fabricCanvas.remove(this.fabricImg)
                //this.fabricHeader.text = ''
                //For some case(random bug) that the header text can't remove
            }else{
                this.fabricImg.opacity = 1
                this.renderFabricImg(nextProps)
            }
        }

        if(this.props.width!=nextProps.width || this.props.height!=nextProps.height
            || this.props.imgIdexId != nextProps.imgIdexId
        ) 
            return true
        else
            return false
    }

    componentDidUpdate(prevProps: *, prevState: *, snapshot: *) {
        this.props.updateComponentState({imageHeaderWidth: this.canvasHeader.current.offsetWidth, imageHeaderHeight: this.canvasHeader.current.offsetHeight})
    }

    componentDidMount(){
        this.init()
        //Fabric image and header are available to render after they are initialized.
        
        //Trigger forceUpdate() to render the fabric image and header
        // this.forceUpdate()
        //Repalce this.forceUpdate() by this.setState({}) so that shouldComponentUpdate() won't be bypassed but still force update
        this.setState({})

    }

    componentWillUnmount(){
        this.fabricImg.off()
        this.fabricCanvas.off()
        this.fabricCanvas.dispose()
    }

    render(): Node {
        const classes = this.props.classes
        let canvasHeaderInfo = ''
        let canvasHeaderComp
        
        if(this.props.headerVisibility) {
            if(this.props.imgIdexId != undefined && this.props.imgIdexId != '') {
                if(this.props.caseNo != undefined && this.props.caseNo != '') {
                    canvasHeaderInfo = this.props.caseNo
                }
                if(this.props.displayDateTime != undefined
                    && this.props.displayDateTime != '') {
                    canvasHeaderInfo += this.props.headerTimeText 
                                    + DateParser.parse(this.props.displayDateTime, DATE_FORMAT.FULL_YYYYMMDDTHHMMSS_PLAIN,DATE_FORMAT.LONG_DDMMMYYYYTHHMMSS);
                }
                canvasHeaderComp = <div className={classes.headerDiv}>
                                        <Tooltip title={canvasHeaderInfo}>
                                            <span>{canvasHeaderInfo}</span>
                                        </Tooltip>
                                    </div>
            }
        }

        return (
            <div className={classes.containerDiv}>
                <div className={classes.headerContainerDiv} ref={this.canvasHeader}>
                    {canvasHeaderComp}
                </div>    
            {/* <div style={{zIndex:5, border: this.props.isSelected?'1px solid #00D2FF':'1px solid #7C7C7C'}}> */}
                <div className={classes.canvasContainerDiv}>
                    <canvas ref={this.canvas}/>
                </div>
            </div>
        )
    }

    renderFabricCanvas(nextProps: *){
        this.fabricCanvas.setWidth(nextProps.width)
        this.fabricCanvas.setHeight(nextProps.height)
    }

    renderFabricImg(nextProps: *) {
        let that = this
        this.canvasProfile = nextProps.canvasProfile
        this.imageTool = nextProps.imageTool
        this.imgIdexId = nextProps.imgIdexId

        const imgUri = this.canvasProfile.imageBase64
        if(this.fabricImg._originalElement==undefined || this.fabricImg._originalElement==null || this.fabricImg._originalElement.currentSrc != imgUri) {
            fabric.util.loadImage(imgUri, function(img: *): * {
                if (that.canvasProfile == null) {
                    return 
                }

                //check the imageId if it is the latest value to avoid dummy onload caused by late back cases
                if(that.imgIdexId==nextProps.imgIdexId){
                    that.fabricImg.setElement(img)

                    that.fabricImg.opacity = 1
                    that.fabricImg.cacheKey = that.props.imgIdexId
                    that.renderFabricImgAttributes(that.canvasProfile, that.imageTool)
                }
            })
        } else {
            this.renderFabricImgAttributes(this.canvasProfile, this.imageTool)
        }
    }

    renderFabricImgAttributes(canvasProfile: *, imageTool: string){
        //ensure only one fabric image in canvas
        if(!this.fabricCanvas.contains(this.fabricImg)){
            this.fabricCanvas.add(this.fabricImg)
        }

        //set z-index
        this.fabricCanvas.sendToBack(this.fabricImg)

        //render cursor icon
        if(([ImageTool.ZOOM_IN, ImageTool.ZOOM_OUT]).indexOf(imageTool)>-1){
            this.fabricImg.selectable = false
            this.fabricCanvas.discardActiveObject()
            if(ImageTool.ZOOM_IN==imageTool){
                this.fabricImg.hoverCursor = 'url('+zoomInCursor+'),auto'
            }
            if(ImageTool.ZOOM_OUT==imageTool){
                this.fabricImg.hoverCursor = 'url('+zoomOutCursor+'),auto'
            }
        }else{
            this.fabricImg.selectable = true
            this.fabricImg.hoverCursor = 'url('+openHandCursor+'),auto'
            this.fabricImg.moveCursor = 'url('+closedHandCursor+'),auto'
        }

        //set oringin X and Y to center and rotate angle
        this.fabricImg.originX = canvasProfile.imageOriginX
        this.fabricImg.originY = canvasProfile.imageOriginY
        this.fabricImg.rotate(canvasProfile.rotateAngle)
        // After rotating img, must call setCoords() to update control area. Otherwise control area will remain the one before rotating

        //set flip
        this.fabricImg.flipX = canvasProfile.flipHorizontal
        this.fabricImg.flipY = canvasProfile.flipVertical

        //set image coordinate
        this.fabricImg.left = canvasProfile.imageXCoor
        this.fabricImg.top = canvasProfile.imageYCoor

        //set image scale/auto fit
        this.fabricImg.scaleX = canvasProfile.imageScaleX
        this.fabricImg.scaleY = canvasProfile.imageScaleY

        // set color adjustment
        if(canvasProfile.invert) {
            this.fabricImg.filters[0] = new fabric.Image.filters.Invert()
        } else if(this.fabricImg.filters[0] != null) {
            delete this.fabricImg.filters[0]
        }

        if(this.fabricImg.filters[1] == null) {
            let brightnessFilter = new fabric.Image.filters.Brightness({
                brightness: (canvasProfile.brightness / 100) * BRIGHTNESS_ADJUSTMENT_FACTOR
            })
            this.fabricImg.filters[1] = brightnessFilter
        } else {
            this.fabricImg.filters[1]['brightness'] = (canvasProfile.brightness / 100) * BRIGHTNESS_ADJUSTMENT_FACTOR
        }

        if(this.fabricImg.filters[2] == null) {
            let contrastFilter = new fabric.Image.filters.Contrast({
                contrast: canvasProfile.contrast / 100
            })
            this.fabricImg.filters[2] = contrastFilter
        } else {
            this.fabricImg.filters[2]['contrast'] = canvasProfile.contrast / 100
        }

        if(this.fabricImg.filters[3] == null) {
            let saturationFilter = new fabric.Image.filters.Saturation({
                saturation: canvasProfile.saturation / 100
            })
            this.fabricImg.filters[3] = saturationFilter
        } else {
            this.fabricImg.filters[3]['saturation'] = canvasProfile.saturation / 100
        }

        if(this.fabricImg.filters[4] == null) {
            let hueFilter = new fabric.Image.filters.HueRotation({
                rotation: canvasProfile.hue / 180
            })
            this.fabricImg.filters[4] = hueFilter
        } else {
            this.fabricImg.filters[4]['rotation'] = canvasProfile.hue / 180
        }

        const gl = getWebGLContext()
        this.fabricImg.applyFilters()
        // This trunk is business logic and should be placed in controller layer
        // However, it is hard to move business logic from Canvas to a controller since the logic is tightly depends on the related actual DOM element
        const glError = gl ? gl.getError() : null
        if(gl && glError && checkIfErrorOccurred(gl, glError)) {
            const errorStr = 'Web GL Error: ' + glErrorToString(gl, glError)
            throw new CidError('src.main.components.imageViewingPanel.canvasGrid.canvas.Canvas', 'renderFabricImgAttributes()', errorStr)
        }

        this.fabricImg.setCoords()

        //Render Canvas
        this.fabricCanvas.renderAll()
    }

}

class PrivateMembers{
    calculatePannedCoordinate: Function
    
    constructor(){
        this.calculatePannedCoordinate = this.calculatePannedCoordinate.bind(this)
    }

    calculatePannedCoordinate(imageWidth: number, imageHeight: number, imageLeft: number, imageTop: number, zoomX: number, zoomY: number, currentScaleXFactor: number, currentScaleYFactor: number, nextScaleXFactor: number, nextScaleYFactor: number): {xCoor: number, yCoor: number}{
        //calculate panned Left and panned Top
        let relativeX = this.toRelativeX(imageWidth*currentScaleXFactor, imageLeft, zoomX)
        let relativeY = this.toRelativeY(imageHeight*currentScaleYFactor, imageTop, zoomY)
        let scaledRelativeX = this.toScaledRelativeX(relativeX, (imageWidth*currentScaleXFactor), (imageWidth*(nextScaleXFactor)))
        let scaledRelativeY = this.toScaledRelativeY(relativeY, (imageHeight*currentScaleYFactor), (imageHeight*(nextScaleYFactor)))
        let absoluteX = this.toAbsoluteX(imageWidth*currentScaleXFactor, imageLeft, relativeX)
        let absoluteY = this.toAbsoluteY(imageHeight*currentScaleYFactor, imageTop, relativeY)
        let scaledAbsoluteX = this.toAbsoluteX(imageWidth*(nextScaleXFactor), imageLeft, scaledRelativeX)
        let scaledAbsoluteY = this.toAbsoluteY(imageHeight*(nextScaleYFactor), imageTop, scaledRelativeY)

        let pannedLeft = imageLeft + absoluteX - scaledAbsoluteX
        let pannedTop = imageTop + absoluteY - scaledAbsoluteY

        return {xCoor: pannedLeft, yCoor: pannedTop}
    }

    toRelativeX(imgWidth: number, imgLeft: number, absoluteX: number, imgOriginX: string = 'center'): number{
        if(imgOriginX=='center'){
            return (imgWidth/2)+(absoluteX-imgLeft)
        }
        return 0
    }

    toScaledRelativeX(relativeX: number, width: number, scaledWidth: number): number{
        return relativeX * scaledWidth / width
    }

    toAbsoluteX(imgWidth: number, imgLeft: number, relativeX: number): number{
        return relativeX - (imgWidth/2) + imgLeft
    }

    toRelativeY(imgHeight: number, imgTop: number, absoluteY: number, imgOriginY: string = 'center'): number{
        if(imgOriginY=='center'){
            return (imgHeight/2)+(absoluteY-imgTop)
        }
        return 0
    }

    toScaledRelativeY(relativeY: number, height: number, scaledHeight: number): number{
        return relativeY * scaledHeight / height
    }

    toAbsoluteY(imgHeight: number, imgTop: number, relativeY: number): number{
        return relativeY - (imgHeight/2) + imgTop
    }
}

export default cidComponent(styles)(Canvas)