//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import Canvas from './canvas/Canvas'

import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import type {ViewModeType} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import type {StateType} from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'

type CanvasGridPropsType = CidComponentPropsType & {
    onImgMoved: Function,
    onCanvasClick: Function,
    canvasGridData: StateType,
    onImageZoom: Function,
    updateComponentState: Function
}

export const styles: Function = (theme: *): * => ({
    div: {
        backgroundColor: theme.imageViewingPanel.canvasGrid.bg,
        width: function(props: *): number {
            return props.canvasGridData.width
        },
        height: function(props: *): number {
            return props.canvasGridData.height
        }
    }
})

class CanvasGrid extends Component <CanvasGridPropsType> {
    constructor(props: *) {
        super(props)
    }

    getGridListTileStyle(isSelected: boolean, rowWidth: number, rowHeight: number): *{
        if(rowHeight==0 && rowWidth==0){
            return {}
        }
        return {
            border: isSelected?'1px solid #00D2FF':'1px solid #7C7C7C', 
            boxSizing: 'border-box'
        }
    }

    render(): Node {
        const classes = this.props.classes

        // //panel info
        let panelHeight = this.props.canvasGridData.height
        let panelWidth = this.props.canvasGridData.width
        
        // //grid info for view mode
        let viewMode: ViewModeType = this.props.canvasGridData.viewMode
        let noOfRow: number = Number(viewMode.text.split('x')[0])
        let noOfCol: number = Number(viewMode.text.split('x')[1])
        // let rowHeight: number = panelHeight/noOfRow
        // let rowWidth: number = panelWidth/noOfCol
        let rowHeight: number = this.props.canvasGridData.canvasHeight
        let rowWidth: number = this.props.canvasGridData.canvasWidth
        let noOfGrid: number = viewMode.noOfImage

        //shown img info
        let canvasGridData: StateType = this.props.canvasGridData
        let showImageIds: Array<string> = canvasGridData.showImageIds
        let caseNo: string = canvasGridData.caseNo
        let displayDateTime: Array<string> = canvasGridData.displayDateTime
        let selectedImageIds: Array<string> = canvasGridData.selectedImageIds
        let selectedSeriesId: string = canvasGridData.selectedSeriesId

        //Canvas header configration from appConfig.json
        let headerTimeText: string = canvasGridData.headerTimeText
        let imageTool = this.props.canvasGridData.imageTool

        //pre-define Grid Tile components
        let reversedShowImageIds: Array<string> = []
            reversedShowImageIds = reversedShowImageIds.concat(showImageIds)
            reversedShowImageIds.reverse()
        let noOfGridTile = []
        for(let i=0;i<noOfGrid;i++){
            let canvasProfile: * = null
            let toShowImgIndexId: string
            let fontType: string
            let fontSize: number
            let fontColor: string
            let bgColor: string
            let headerVisibility: boolean

            if(reversedShowImageIds.length>0){
                toShowImgIndexId = reversedShowImageIds.pop()
                canvasProfile = canvasGridData.canvasProfiles.byId[toShowImgIndexId]
                fontType = canvasProfile.fontType
                fontSize = canvasProfile.fontSize
                fontColor = canvasProfile.fontColor
                bgColor = canvasProfile.bgColor
                headerVisibility = canvasProfile.headerVisibility
            }

            noOfGridTile.push(
                // <GridTile key={'GridTile-'+i} style={{boxSizing: 'border-box'}}>
                <GridListTile key={'GridTile-'+i} style={this.getGridListTileStyle(selectedImageIds.indexOf(toShowImgIndexId)>-1, rowWidth, rowHeight)}>
                    {/* <Canvas height={rowHeight} width={rowWidth} imgIdexId={tempImgIdexId} canvasProfile={canvasProfile} caseNo={caseNo} studyDateTime={studyDateTime} isSelected={(selectedImageIds.indexOf(tempImgIdexId)>-1)} style={{border: (selectedImageIds.indexOf(tempImgIdexId)>-1)?'1px solid #00D2FF':'1px solid #7C7C7C', boxSizing: 'border-box'}}/> */}
                    {/* <Canvas height={rowHeight} width={rowWidth} imgIdexId={tempImgIdexId} canvasProfile={canvasProfile} caseNo={caseNo} studyDateTime={studyDateTime} isSelected={(selectedImageIds.indexOf(tempImgIdexId)>-1)} /> */}
                    <Canvas height={rowHeight} width={rowWidth} seriesIndexId={selectedSeriesId}
                        isSelected={selectedImageIds.indexOf(toShowImgIndexId)>-1} imgIdexId={toShowImgIndexId}
                        canvasProfile={canvasProfile} caseNo={caseNo} displayDateTime={displayDateTime[i]}
                        onImgMoved={this.props.onImgMoved} onCanvasClick={this.props.onCanvasClick}
                        fontType={fontType} fontSize={fontSize} fontColor={fontColor} bgColor={bgColor}
                        headerVisibility={headerVisibility}
                        headerTimeText={headerTimeText} imageTool={imageTool} onImageZoom={this.props.onImageZoom}
                        updateComponentState={this.props.updateComponentState}/>
                </GridListTile>
            )
        }

        return (
            <div className={classes.div}>
                <GridList rows={noOfRow} cols={noOfCol} cellHeight={rowHeight} spacing={0}>
                    {noOfGridTile}
                </GridList>
            </div>
        )
    }
}

export default cidComponent(styles)(CanvasGrid)