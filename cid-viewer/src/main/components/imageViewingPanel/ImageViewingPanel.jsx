//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import CanvasGrid from './canvasGrid/CanvasGrid'
import {type StateType} from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'

type ImageViewingPanelPropsType = CidComponentPropsType & {
    onImgMoved: Function,
    onCanvasClick: Function,
    canvasGridData: StateType,
    onImageZoom: Function,
    updateComponentState: Function
}

class ImageViewingPanel extends Component<ImageViewingPanelPropsType> {

    constructor(props: *) {
        super(props)
    }


    render(): Node {
        return (
            <div>
                <CanvasGrid canvasGridData={this.props.canvasGridData}
                    onImgMoved={this.props.onImgMoved}
                    onCanvasClick={this.props.onCanvasClick}
                    onImageZoom={this.props.onImageZoom}
                    updateComponentState={this.props.updateComponentState}
                    />
            </div>
        );
    }
}

export default cidComponent()(ImageViewingPanel)