// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'

import cidNestMuiThemeProvider from '@components/base/CidNestMuiThemeProvider'
import { cidThemeForDialog } from '@styles/CidTheme'

import icoAlert from '@assets/icons/ico_alert.jpg'

type MsgDialogPropsType = CidComponentPropsType & {
    msgDialogData: *,
    setMsgDialog: Function
}

export const styles: Function = function(theme: *): * {
    const DIALOG_WIDTH = theme.msgDialog.width
    const DIALOG_HEIGHT = theme.msgDialog.height
    const DIALOG_PADDING_LEFT = theme.msgDialog.paddingLeft
    const DIALOG_BORDER_WIDTH = theme.msgDialog.borderWidth

    return {
        dialog: {

        },
        paper: {
            width: DIALOG_WIDTH,
            minWidth: DIALOG_WIDTH,
            maxWidth: DIALOG_WIDTH,
            height: DIALOG_HEIGHT,
            minHeight: DIALOG_HEIGHT,
            maxHeight: DIALOG_HEIGHT,
            padding: '0px 0px 0px '+DIALOG_PADDING_LEFT+'px',
            border: DIALOG_BORDER_WIDTH+'px solid red',
            fontSize: theme.msgDialog.fontSize
        },
        
        dialogTitle: {
            paddingTop: '1em',
            paddingBottom: '0.5em'
        },

        dialogContent: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            paddingBottom: '0em',
            overflowY: 'hidden'
        },

        dialogContentMessage: {
            flex: '1 1 25%',
            minHeight: '4.5em',
            overflowY: 'auto',
            border: '1px solid #bdbdbd',
            borderRadius: '2px',
            padding: '0em 0.3em'
        },

        additionalContent: {
            flex: '1 1 75%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            paddingBottom: '0em',
            overflowY: 'hidden'
        },

        additionalContentTitle: {
            paddingTop: '0.3em',
            paddingBottom: '0.1em',
            fontSize: '1.1em'
        },

        additionalContentMessage: {
            minHeight: '7.5em',
            overflowY: 'auto',
            border: '1px solid #bdbdbd',
            borderRadius: '2px',
            padding: '0em 0.3em'
        },

        dialogActions: {
            paddingTop: '0em',
            paddingBottom: '0em',
            minHeight: '2em'
        },

        dialogIcon: {
            position: 'absolute', 
            top: theme.msgDialog.icon.top, 
            left: theme.msgDialog.icon.left
        }
    }
}

class MsgDialog extends Component<MsgDialogPropsType> {
    handleClose: Function

    constructor(props: *) {
        super(props)
        this.handleClose = this.handleClose.bind(this)
    }

    handleClose(){
        this.props.setMsgDialog(false, null, '', null, null)
    }

    render(): Node {
        
        const classes = this.props.classes

        // main content for dialog messages
        let content = []
        if(this.props.msgDialogData.content!=null) {
            const contentSentenceList = this.props.msgDialogData.content.split("\n")
            content.push(contentSentenceList[0])
            for(let i=1; i<contentSentenceList.length; i++) {
                content.push(<br key={'DialogContentTextContentBr-'+i}/>)
                content.push(contentSentenceList[i])
            }
        }

        // additional content for dialog messages
        let additionalContent = []
        // title for additional content
        additionalContent.push(
            <DialogContentText 
                key={'DialogContentTextAddiContent0'}
                classes={{root: classes.additionalContentTitle}}>
                    <b>{"Additional information"}</b>
            </DialogContentText>
        )

        // content for additional content
        additionalContent.push(
            <DialogContentText 
                key={'DialogContentTextAddiContent1'}
                classes={{root: classes.additionalContentMessage}}>
                    {this.props.msgDialogData.additionalContent}
            </DialogContentText>
        )

        return (
            <Dialog
                open={this.props.msgDialogData.isVisible}
                classes={{root: classes.dialog, paper: classes.paper}}>
                    <img src={icoAlert} className={classes.dialogIcon}/>
                    <DialogTitle classes={{root: classes.dialogTitle}}>
                            {this.props.msgDialogData.title}
                    </DialogTitle>
                    <DialogContent classes={{root: classes.dialogContent}}>
                        <DialogContentText classes={{root: classes.dialogContentMessage}}>
                            {content}
                        </DialogContentText>
                        <div className={classes.additionalContent}>
                            {additionalContent}
                        </div>
                    </DialogContent>
                    <DialogActions classes={{action: classes.dialogActions}}>
                        <Button onClick={this.handleClose} color="primary">
                        OK
                        </Button>
                    </DialogActions>
            </Dialog>
        )
    }
}

export default cidComponent(styles)(cidNestMuiThemeProvider(cidThemeForDialog)(MsgDialog))