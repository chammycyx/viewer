// @flow
import { connect, type MapStateToProps, type MapDispatchToProps } from 'react-redux'
import MsgDialog from './MsgDialog'
import {setMsgDialog} from '@actions/components/msgDialog/Action'

const mapStateToProps = (state: *): MapStateToProps => {
    return {
        msgDialogData: state.components.msgDialog.rootState
    }
}

const mapDispatchToProps = (dispatch: *): MapDispatchToProps => ({
    setMsgDialog: (isVisible: boolean, msgCode: string, msgTitle: string, msgContent: string, additionalMsg: string) => {
        dispatch(setMsgDialog(isVisible, msgCode, msgTitle, msgContent, additionalMsg))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(MsgDialog)