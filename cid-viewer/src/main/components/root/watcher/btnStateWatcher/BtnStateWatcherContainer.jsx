// @flow
import { connect, type MapStateToProps, type MapDispatchToProps } from 'react-redux'
import BtnStateWatcher from './BtnStateWatcher'
import {numOfSelectImageChanged} from '@actions/components/root/watcher/btnStateWatcher/Action'

const mapStateToProps = (state: *): MapStateToProps => {
    return {
        selectedImageIds: state.components.imageViewingPanel.canvasGrid.rootState.selectedImageIds,
        slideShowMode: state.components.toolBarPanel.slideShowTools.rootState.isModeOn,
        isPlayingslideShow: state.components.toolBarPanel.slideShowTools.rootState.isPlaying
    }
}

const mapDispatchToProps = (dispatch: *): MapDispatchToProps => ({
    dispatchNumOfSelectImageChanged: (numOfSelectImage: number, slideShowMode: boolean, isPlayingslideShow: boolean) => {
        dispatch(numOfSelectImageChanged(numOfSelectImage, slideShowMode, isPlayingslideShow))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(BtnStateWatcher)