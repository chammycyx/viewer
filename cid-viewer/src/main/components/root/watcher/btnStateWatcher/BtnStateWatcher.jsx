// @flow
import { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

type BtnStateWatcherPropsType = CidComponentPropsType & {
    selectedImageIds: Array<string>,
    slideShowMode: boolean,
    isPlayingslideShow: boolean,
    dispatchNumOfSelectImageChanged: Function
}

export const styles: Function = (theme: *): * => ({

})


class BtnStateWatcher extends Component<BtnStateWatcherPropsType> {

    constructor(props: *) {
        super(props)
    }

    componentDidUpdate(prevProps: *){
        if(prevProps.selectedImageIds.length != this.props.selectedImageIds.length)
            this.props.dispatchNumOfSelectImageChanged(this.props.selectedImageIds.length, this.props.slideShowMode, this.props.isPlayingslideShow)
    }

    render(): Node {
        return null
    }
}

export default cidComponent(styles)(BtnStateWatcher)