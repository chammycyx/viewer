// @flow
import { connect, type MapStateToProps, type MapDispatchToProps } from 'react-redux'
import AppMask from './AppMask'

const mapStateToProps = (state: *): MapStateToProps => {
    return {
        appMaskData: state.components.appMask.rootState
    }
}

const mapDispatchToProps = (dispatch: *): MapDispatchToProps => ({
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AppMask)