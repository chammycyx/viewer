// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import Divider from '@material-ui/core/Divider'
import { ContextMenu as ContextMenuWrapper, ContextMenuTrigger } from 'react-contextmenu'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

import ContextMenuContainer from '@components/contextMenu/ContextMenuContainer'
import ToolBarPanelContainer from '@components/toolBarPanel/ToolBarPanelContainer'
import PictorialIndexPanelContainer from '@components/pictorialIndexPanel/PictorialIndexPanelContainer'
import ImageViewingPanelContainer from '@components/imageViewingPanel/ImageViewingPanelContainer'
import MsgDialogContainer from '@components/msgDialog/MsgDialogContainer'
import AboutDialogContainer from '@components/contextMenu/dialogs/AboutDialogContainer'
import AppMaskContainer from '@components/root/appMask/AppMaskContainer'
import BtnStateWatcherContainer from '@components/root/watcher/btnStateWatcher/BtnStateWatcherContainer'

type RootAppPropsType = CidComponentPropsType & {
    onRetrieveStudy: Function,
    appWidth: number,
    appHeight: number
}

export const styles: Function = (theme: *): * => ({
	rootApp: {
        marginTop: 0,
        paddingTop: 0,
        left: 0,
        right: 0,
        width: function(props: *): string {
            return props.appWidth
        },
        height: function(props: *): string {
            return props.appHeight
        },
        overflow: 'hidden',
        background: theme.palette.background.default,
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.fontSize,
        color: theme.palette.text.primary
    },
    divider: {
        margin: '-1px 0px 0px',
        height: '1px',
        backgroundColor: 'rgba(255, 255, 255, 0.3)'
    },
    outerDiv: {
        overflow: 'auto',
        width: '100vw',
        height: '100vh'
    }
})

class RootApp extends Component<RootAppPropsType> {
    render(): Node {
        const classes = this.props.classes

        return (
            <div className={classes.outerDiv}>
                <BtnStateWatcherContainer />
                <ContextMenuTrigger id="cid-context-menu" holdToDisplay={-1}>
                <AppMaskContainer />
                    <div className={classes.rootApp}>
                            <div style={{float: 'top'}}>
                                <ToolBarPanelContainer/>
                            </div>
                            <Divider className={classes.divider}/>
                            <div style={{float: 'left'}}>
                                <PictorialIndexPanelContainer/>
                            </div>
                            <div style={{float: 'left'}}>
                                <ImageViewingPanelContainer/>
                            </div>
                    </div>
                    <MsgDialogContainer />
                    <AboutDialogContainer />
                    <ContextMenuWrapper id="cid-context-menu">
                        <ContextMenuContainer />
                    </ContextMenuWrapper>
                    
                </ContextMenuTrigger>
            </div>
        )
    }
}

export default cidComponent(styles)(RootApp)
