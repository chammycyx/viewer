// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import { DateParser, DATE_FORMAT } from '@utils/DateParser'
import Tooltip from '@material-ui/core/Tooltip'

type StudyInfoPropsType = CidComponentPropsType & {
    patName: string,
    patHkid: string,
    patSex: string,
    patDob: string,
    accessionNumber: string
}

export const styles: Function = (theme: *): * => ({
    studyInfo: {
        boxSizing: 'border-box',
        verticalAlign: 'text-top',
        fontWeight: 'bold'
    },
    wrapper: {
        width: '95%',
        height: theme.pictorialIndexPanel.studyInfo.wrapper.style.height,
        paddingTop: 15,
        paddingLeft: 0,
        whiteSpace: 'nowrap'
    },
    infoBase: {
        display: 'inline-block',
        whiteSpace: 'nowrap', // 'nowrap' or 'pre-line'
        wordWrap: 'break-word',
        overflow: 'hidden',
        fontSize: theme.pictorialIndexPanel.studyInfo.infoBase.fontSize
    },
    infoLabel: {
        width: '30%',
        textAlign: 'right'
    },
    infoValue: {
        width: '70%',
        paddingLeft: 10,
        textAlign: 'left',
        textOverflow: 'ellipsis'
    }
})

class StudyInfo extends Component<StudyInfoPropsType> {

    // Date patterns
    getDateInputPattern(): string {
        return DATE_FORMAT.FULL_YYYYMMDDTHHMMSSMS_PLAIN
    }

    getDateOutputPattern(): string {
        return DateParser.getDefaultPattern();
    }
    
    render(): Node {
        const classes = this.props.classes
        return( 
            <div className={classes.studyInfo+' '+classes.wrapper}>
                <div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoLabel}>
                        <span>Name:</span><br/>
                    </div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoValue}>
                        <Tooltip title={this.props.patName} placement='right' >
                            <span>{this.props.patName}</span>
                        </Tooltip>
                    </div>
                </div>
                <div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoLabel}>
                        <span>HKID:</span><br/>
                    </div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoValue}>
                        <span>{this.props.patHkid}</span><br/>
                    </div>
                </div>
                <div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoLabel}>
                        <span>Sex:</span><br/>
                    </div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoValue}>
                        <span>{this.props.patSex}</span><br/>
                    </div>
                </div>
                <div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoLabel}>
                        <span>DOB:</span><br/>
                    </div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoValue}>
                        <span>{DateParser.parse(this.props.patDob, this.getDateInputPattern(), this.getDateOutputPattern())}</span><br/>
                    </div>
                </div>
                <div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoLabel}>
                        <span>AccNum:</span>
                    </div>
                    <div className={classes.studyInfo+' '+classes.infoBase+' '+classes.infoValue}>
                        <span>{this.props.accessionNumber}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default cidComponent(styles)(StudyInfo)