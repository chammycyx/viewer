// @flow
import { connect, type MapStateToProps, type MapDispatchToProps } from 'react-redux'
import PictorialIndexPanel from './PictorialIndexPanel'
import { onThumbnailClick } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { onTogglePanelButtonClick } from '@actions/components/pictorialIndexPanel/Action'

const mapStateToProps = (state: *): MapStateToProps => {
    return {
        patName: state.data.study.patientDtl.rootState.name,
        patHkid: state.data.study.patientDtl.rootState.hkid,
        patSex: state.data.study.patientDtl.rootState.sex,
        patDob: state.data.study.patientDtl.rootState.dob,
        accessionNumber: state.data.study.studyDtl.rootState.accessionNo,
        seriesDtls: state.data.study.studyDtl.rootState.seriesDtls,
        selectedSeriesId: state.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId,
        selectedImageIds: state.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedImageIds,
        selectedFirstImageId: state.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId,
        viewMode: state.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode,
        isHidden: state.components.pictorialIndexPanel.rootState.isHidden,
        numOfColumn: state.components.pictorialIndexPanel.thumbnailGrid.rootState.numOfColumn,
        isShowSeriesHeading: state.components.pictorialIndexPanel.thumbnailGrid.rootState.isShowSeriesHeading,
        serieName: state.components.pictorialIndexPanel.thumbnailGrid.rootState.serieName,
        appHeight: state.components.rootState.appHeight
    }
}

const mapDispatchToProps = (dispatch: *): MapDispatchToProps => ({
    onShowHideButtonClick: (isHidden: boolean) => {
        dispatch(onTogglePanelButtonClick(isHidden))
    },
    onThumbnailClick: (selectedSeriesId: string, selectedFirstImageId: string) => {
        dispatch(onThumbnailClick(selectedSeriesId,selectedFirstImageId))
    }    
})

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PictorialIndexPanel)