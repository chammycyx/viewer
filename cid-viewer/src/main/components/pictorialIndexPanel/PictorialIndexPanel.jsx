// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import Divider from '@material-ui/core/Divider'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import StudyInfo from './studyInfo/StudyInfo'
import ThumbnailsGrid from './thumbnailGrid/ThumbnailGrid'
import HideButton from './hideButton/HideButton'
import ShowButton from './hideButton/ShowButton'
import { type ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

type PictorialIndexPanelPropsType = CidComponentPropsType & {
    patName: string,
    patHkid: string,
    patSex: string,
    patDob: string,
    accessionNumber: string,
    seriesDtls: {
        byId: *,
        allIds: []
    },
    selectedSeriesId: string,
    selectedImageIds: Array<string>,
    selectedFirstImageId: string,
    viewMode: ViewModeType,
    onShowHideButtonClick: Function,
    onThumbnailClick: Function,
    isHidden: boolean,
    numOfColumn: number,
    isShowSeriesHeading: boolean,
    serieName: string,
    appHeight: number
}

export const styles: Function = (theme: *): * => ({
    hideButtonDiv: {
        float: 'right',
        padding: 0
    },
    hideDiv: {
        display: function(props: *): string {
            return !props.isHidden ? 'none' : 'block'
        }
    },
    showDiv: {
        display: function(props: *): string {
            return props.isHidden ? 'none' : 'block'
        },
        width: theme.pictorialIndexPanel.width,
        height: function(props: *): string {
            return (props.appHeight - theme.toolBarPanel.height) + 'px'
        },
        overflow: 'auto'
    },
    divider: {
        margin: '-1px 0px 0px',
        height: '1px',
        backgroundColor: 'rgba(255, 255, 255, 0.3)'
    }
})

class PictorialIndexPanel extends Component<PictorialIndexPanelPropsType> {
    constructor(props: *) {
        super(props)
    }

    render(): Node {
        const classes = this.props.classes

        const showButtonWidth = this.props.theme.pictorialIndexPanel.showHideButton.showButton.width
        const showButtonHeight = this.props.theme.pictorialIndexPanel.showHideButton.showButton.height
        const hideButtonWidth = this.props.theme.pictorialIndexPanel.showHideButton.hideButton.width
        const hideButtonHeight = this.props.theme.pictorialIndexPanel.showHideButton.hideButton.height

        return (
            <div>
                <div className={classes.hideDiv}>
                    <ShowButton toolTipTitle='To open the panel of pictorial index' width={showButtonWidth} height={showButtonHeight} onClick={this.props.onShowHideButtonClick} isEnabled={true}/>
                </div>
                <div className={classes.showDiv}>
                    <div className={classes.hideButtonDiv}>
                        <HideButton width={hideButtonWidth} height={hideButtonHeight} onClick={this.props.onShowHideButtonClick} isEnabled={true}/>
                    </div>
                    <StudyInfo 
                        patName={this.props.patName}
                        patHkid={this.props.patHkid}
                        patSex={this.props.patSex}
                        patDob={this.props.patDob}
                        accessionNumber={this.props.accessionNumber}
                    />
                    <Divider className={classes.divider}/>
                    <ThumbnailsGrid
                        selectedImageIds={this.props.selectedImageIds}
                        selectedSeriesId={this.props.selectedSeriesId}
                        selectedFirstImageId={this.props.selectedFirstImageId}
                        seriesDtls={this.props.seriesDtls}
                        viewMode={this.props.viewMode}
                        numOfColumn={this.props.numOfColumn}
                        isShowSeriesHeading={this.props.isShowSeriesHeading}
                        onThumbnailClick={this.props.onThumbnailClick}
                        serieName={this.props.serieName}
                        appHeight={this.props.appHeight}
                        />
                </div>
            </div>
        )
    }
}

export default cidComponent(styles)(PictorialIndexPanel)