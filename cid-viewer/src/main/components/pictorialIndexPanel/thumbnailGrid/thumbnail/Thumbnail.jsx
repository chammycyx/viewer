// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'

type ThumbnailPropsType = CidComponentPropsType & {
    src: string,
    isSelected: boolean,
    seriesId: string,
    imageId: string,
    onClick: Function,
    cellHeight: number
}

export const styles: Function = (theme: *): * => ({
    img: {
        width: '100%',
        height: function(props: *): number {
            return props.cellHeight
        },
        borderWidth: theme.pictorialIndexPanel.thumbnailGrid.thumbnail.borderWidth,
        borderStyle: theme.pictorialIndexPanel.thumbnailGrid.thumbnail.borderStyle,
        borderColor: function(props: *): string {
            if(!props.isSelected)
                return theme.pictorialIndexPanel.thumbnailGrid.thumbnail.borderColor
            else
                return theme.pictorialIndexPanel.thumbnailGrid.thumbnail.borderColorSelected
        },
        boxSizing: 'border-box',
        cursor: 'pointer'
    }
})

class Thumbnail extends Component <ThumbnailPropsType> {
    onClick: Function

    constructor(props: *) {
        super(props)
        this.onClick = this.onClick.bind(this)
    }

    onClick() {
        this.props.onClick(this.props.seriesId, this.props.imageId)
    }

    render(): Node {
        const classes = this.props.classes

        return (
            <img src={this.props.src} className={classes.img} onClick={this.onClick}/>
        )
    }
}

export default cidComponent(styles)(Thumbnail)