//@flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import ListSubheader from '@material-ui/core/ListSubheader'
import Thumbnail from './thumbnail/Thumbnail'
import { type ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

type ThumbnailGridPropsType = CidComponentPropsType & {
    seriesDtls: {
        byId: *,
        allIds: []
    },
    onThumbnailClick: Function,
    selectedSeriesId: string,
    selectedImageIds: Array<string>,
    selectedFirstImageId: string,
    numOfColumn: number,
    isShowSeriesHeading: boolean,
    serieName: string,
    viewMode: ViewModeType
}

export function getCellHeight(pictorialIndexPanelWidth: number, numOfColumn: number, scrollBarWidth: number): number {
    const cellHeight = (pictorialIndexPanelWidth - scrollBarWidth)/numOfColumn
    return cellHeight
}

export const styles: Function = (theme: *): * => ({
    div: {
        overflowY: 'auto',
        width: theme.pictorialIndexPanel.width,
        maxHeight: function(props: *): * {
            return props.appHeight - theme.toolBarPanel.height
            - theme.pictorialIndexPanel.studyInfo.wrapper.style.height 
            - theme.pictorialIndexPanel.thumbnailGrid.subHeader.height
        },
        minHeight: function(props: *): * {
            const width = theme.pictorialIndexPanel.width
            const numOfColumn = props.numOfColumn
            const scrollBarWidth = theme.pictorialIndexPanel.thumbnailGrid.scrollBarWidth
            return getCellHeight(width, numOfColumn, scrollBarWidth)
        }
    },
    subheader: {
        height: theme.pictorialIndexPanel.thumbnailGrid.subHeader.height,
        lineHeight: theme.pictorialIndexPanel.thumbnailGrid.subHeader.lineHeight,
        paddingLeft: theme.pictorialIndexPanel.thumbnailGrid.subHeader.paddingLeft,
        fontSize: theme.pictorialIndexPanel.thumbnailGrid.subHeader.fontSize,
        fontFamily: theme.fontFamily,
        color: theme.pictorialIndexPanel.thumbnailGrid.subHeader.color
    },
    gridList: {
        margin: 0
    }
})

class ThumbnailGrid extends Component <ThumbnailGridPropsType> {
    onThumbnailClick: Function

    constructor(props: *) {
        super(props)
        this.onThumbnailClick = this.onThumbnailClick.bind(this)
    }

    onThumbnailClick(seriesId: string, imageId: string): * {
        this.props.onThumbnailClick(seriesId, imageId)
    }

    render(): Node {
        const classes = this.props.classes
        const that = this
        const width = this.props.theme.pictorialIndexPanel.width
        const numOfColumn = this.props.numOfColumn
        const scrollBarWidth = this.props.theme.pictorialIndexPanel.thumbnailGrid.scrollBarWidth
        const cellHeight = getCellHeight(width, numOfColumn, scrollBarWidth) //Since overflowY set as 'auto', scrollBarWidth should be included in this formula.
        const spacing = this.props.theme.pictorialIndexPanel.thumbnailGrid.spacing
        const serieName = this.props.serieName
        return (
            <div>
            {that.props.isShowSeriesHeading ? <ListSubheader className={classes.subheader}>{serieName}</ListSubheader> : ''}
                <div className={classes.div}>
                    <GridList cellHeight={cellHeight} cols={numOfColumn} spacing={spacing} className={classes.gridList}>
                        {
                            this.props.seriesDtls.allIds.map(function(seriesId: string): Node {
                                let node = []
                                node.push(that.props.seriesDtls.byId[seriesId].imageDtls.allHighestVerIds.map((imageId: string): Node => (
                                    <GridListTile key={imageId} >
                                        <Thumbnail isSelected={that.props.selectedSeriesId==seriesId && that.props.selectedImageIds.includes(imageId)}
                                            src={that.props.seriesDtls.byId[seriesId].imageDtls.byId[imageId].imageThumbnailBase64}
                                            onClick={that.onThumbnailClick}
                                            imageId={imageId}
                                            seriesId={seriesId}
                                            cellHeight={cellHeight}
                                            />
                                    </GridListTile>
                                )))
                                return node
                            })
                        }
                    </GridList>
                </div>
            </div>
        )
    }
}

export default cidComponent(styles)(ThumbnailGrid)