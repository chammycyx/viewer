// @flow
import cidSpecialButton from '../../base/CidSpecialButton'
import picArrow from '../../../../assets/images/picArrow.gif'
import picArrow_over from '../../../../assets/images/picArrow_over.gif'

const HideButton = cidSpecialButton(picArrow, picArrow_over, picArrow, 'HideButton', [true])
export default HideButton