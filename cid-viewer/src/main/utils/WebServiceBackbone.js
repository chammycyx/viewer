//@flow
import axios from 'axios'
import CidHttpError from '@components/base/CidHttpError'
import cidXhrAdapter from '@utils/axios/CidXhrAdapter'

export type WsRsltObjType = {
    response: string,
    data: *
}

async function getWsPromise(username: string, password: string, contextPath: string, endPoint: string, data: *, timeout: number, funcName?: string = 'getWsPromise()'): Promise<WsRsltObjType> {
    const options = {
        method: 'POST',
        headers: {
            'Cache-Control': 'no-cache',
			'Pragma': 'no-cache'
        },
        data,
        url: contextPath + endPoint,
        auth: {
            username,
            password
        },
        transformResponse: [(response: *): * => {
            return response // To prevent Axios parses JSON String to Object automactically
        }],
        adapter: cidXhrAdapter,
        timeout: timeout
    }

    return axios(options)
        .then(function (response: *): * {
            let rsltObj: WsRsltObjType = {
                response: response.data,
                data
            }

            return rsltObj
        })
        .catch(function (httpErr: *) {
            throw new CidHttpError('src.main.utils.WebServiceBackbone', funcName, contextPath+endPoint, httpErr, data)
        })
}

export async function callWs(username: string, password: string, contextPath: string, endPoint: string, data: *, timeout: number): Promise<WsRsltObjType> {
    try {
        let axiosResponse: WsRsltObjType = await getWsPromise(username, password, contextPath, endPoint, data, timeout, 'callWs()')
        return axiosResponse
    } catch(cidHttpError) {
        throw cidHttpError
    }
}

export async function callWsBatch(username: string, password: string, contextPath: string, endPoint: string, dataList: Array<*>, timeout: number): Promise<Array<WsRsltObjType>> {
    let promiseArr: Array<Promise<WsRsltObjType>> = []
    
    for(let i: number = 0; i < dataList.length; i++) {
        promiseArr.push(getWsPromise(username, password, contextPath, endPoint, dataList[i], timeout, 'callWsBatch()'))
    }

    let rsltObjList: Array<WsRsltObjType> = await Promise.all(promiseArr)
    return rsltObjList
}

export async function getJsonFile(url: string, timeout: number, successCallback?: Function, errorCallback?: Function, completeCallback?: Function): * {
    const options = {
        method: 'GET',
        url,
        headers: {
			'Cache-Control': 'no-cache',
            'Pragma': 'no-cache'
        },
        adapter: cidXhrAdapter,
        timeout: timeout
    }
    let axiosPromise = axios(options)
    let isAsyncFunction = (successCallback == undefined 
        && errorCallback == undefined
        && completeCallback == undefined)

    if(isAsyncFunction){
        let axiosResponse = await axiosPromise
        .catch(function (httpErr: *) {
            throw new CidHttpError('src.main.utils.WebServiceBackbone', 'getJsonFile()', url, httpErr)
        })
        return axiosResponse.data
    }else{
        if(successCallback != undefined) {
            axiosPromise = axiosPromise.then(function(axiosResponse: *) {
                if(successCallback != undefined) successCallback(axiosResponse.data) //For pass Flow checking
            })
        }

        if(errorCallback != undefined) {
            axiosPromise = axiosPromise.catch(function(error: *) {
                if(errorCallback != undefined) errorCallback(error) //For pass Flow checking
            })
        }

        if(completeCallback != undefined) {
            axiosPromise = axiosPromise.finally(function() {
                if(completeCallback != undefined) completeCallback() //For pass Flow checking
            })
        }
    }
}