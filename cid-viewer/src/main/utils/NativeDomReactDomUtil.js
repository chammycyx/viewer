//@flow

//Please be noted this class does NOT work in Production BUILD, thus this can only be used for development coding purpose

function findParentReactClassNamePathInternal(nativeNode: *, parentReactClassNamePath?: Array<string> = []) {
    // The original attr name is 'data-react-class' (please refer to CidComponent.jsx)
    // the attr name will be auto trans to 'reactClass'
    if(nativeNode != undefined && nativeNode != null) {
        if(nativeNode.dataset != undefined && nativeNode.dataset != null &&
            nativeNode.dataset['reactClass'] != undefined && nativeNode.dataset['reactClass'] != null) {
            parentReactClassNamePath.push(nativeNode.dataset['reactClass'])
        }

        const parentNode = nativeNode.parentNode

        if(parentNode != undefined && parentNode != null) {
            findParentReactClassNamePathInternal(parentNode, parentReactClassNamePath)
        }
    }
}

export function findParentReactClassNamePath(nativeNode: *): Array<string> {
    let parentReactClassNamePath: Array<string> = []
    findParentReactClassNamePathInternal(nativeNode, parentReactClassNamePath)
    return parentReactClassNamePath
}

export function findParentReactClassName(nativeNode: *): string | null {
    let parentReactClassNamePath: Array<string> = findParentReactClassNamePath(nativeNode)

    if(parentReactClassNamePath.length > 0)
        return parentReactClassNamePath[0]
    else
        return null
}

export function findWrappedCompReactClassName(reactClassName: string): string {
    const idxOfFirstOpenBrac = reactClassName.indexOf('(')
    const idxOfLastCloseBrac = reactClassName.lastIndexOf(')')

    if(idxOfFirstOpenBrac >= 0 && idxOfLastCloseBrac >= 0) {
        return findWrappedCompReactClassName(reactClassName.substring(idxOfFirstOpenBrac+1, idxOfLastCloseBrac))
    } else {
        return reactClassName
    }
}