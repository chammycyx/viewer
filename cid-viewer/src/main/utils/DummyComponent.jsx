// @flow

// To be wrapped by HOC for HOC Jest Unit Test
import React, { Component } from 'react'
import type { Node } from 'react'

class DummyComponent extends Component<*> {
    render(): Node {
        return (
            <div>[DummyComponent]</div>
        )
    }
}

export default DummyComponent