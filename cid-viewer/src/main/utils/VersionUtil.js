// @flow
// export const VERSION = require("../../../package.json").version
export const VERSION = require("../../../package.json").version
export const _appVersion = "0.0.0"

export function getVersion(): string {
    let returnedVer: string = ''
    if(VERSION){
        returnedVer = 'v'+VERSION
    }else{
        returnedVer =  _appVersion
    }
    return returnedVer
}

export function getConfigVersion(applicationVersion: string): string {
    let returnedVer: string = ''
    if(applicationVersion){
        returnedVer =  'v'+applicationVersion
    }else{
        returnedVer = 'Nil'
    }
    return returnedVer
}

export function getServiceVersion(cidWsContextPath: string): string {
    let returnedVer: string = ''
    if(cidWsContextPath){
        let versionPattern = new RegExp("[0-9]+\\_[0-9]+\\_[0-9]+\\_?[0-9]*")
        let versionString = cidWsContextPath.match(versionPattern)
        if (versionString) {
            returnedVer = "v"+ versionString[0].replace(new RegExp("\\_","g"),".");
        }
        else{
            returnedVer = "v3.0.0 (or before)"
        }
    }else{
        returnedVer = 'Nil'
    }

    return returnedVer
}