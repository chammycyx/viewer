// @flow

export function getUrlParameter(name: string): string {
    name = name.replace(/[\\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    let results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

// export function getPath(): string{
//     let href = window.location.href
//     let chunk = href.split('/')
//         chunk.splice(chunk.length-1, 1)
//     return chunk.join('/')
// }