//@flow

import JsonParser from '@utils/JsonParser'
import {setImage as setImageToData, setStudyDtl} from '@actions/data/study/studyDtl/Action'
import { setGrid, setImage as setImageToViewPanel, setSelectedImage as setCanvasSelectedImage, setImageScale, resetImages, rotateImage, flipImage, setColorAdjustment, setImagePosition, resizeImgViewingPanel } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import WebSInfo from '@domains/model/vo/WebSInfo'
import CidMagicUtil from '@utils/CidMagicUtil'
import {EncryptionConstants} from '@constants/EncyptionConstants'
import type {CaseProfileType, RepositionParaType} from '@domains/readers/CaseProfileReader'
import {setCaseProfile} from '@actions/data/common/Action'
import {setMsgCode} from '@actions/data/msgCode/Action'
import { setAppConfig } from '@actions/data/config/application/Action'
import { setPictorialIndexConfig, setSelectedFirstImage, setSelectedImageIds } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { setMessageDtl } from '@actions/data/study/messageDtl/Action'
import { setVisitDtl } from '@actions/data/study/visitDtl/Action'
import { setPatientDtl } from '@actions/data/study/patientDtl/Action'
import { setDownload } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'
import { stop as stopSlideShow, setMode as setSlideShowMode, setPlaying as setSlideShowPlaying, setSeqCanvas as setSlideShowSeqCanvas, setRefOffsets as setSlideShowRefOffsets, setTimerId as setSlideShowTimerId, setImage as setImageToSlideShow } from '@actions/components/toolBarPanel/slideShowTools/Action'
import { setMsgDialog } from '@actions/components/msgDialog/Action'
import { setAppMask as setApplicationMask , setProgressBar} from '@actions/components/root/appMask/Action'
import {IMAGE_BASE64_HEADERS} from '@utils/Base64Util'
import {getDimensions} from '@utils/ImageUtil'
import type {ViewModeType} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import { setAppSize } from '@actions/components/Action'
import { setVisible as setColorAdjustmentPropsBoxVisible, setColorAdjustmentConfig } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'
import { setIsHidden } from '@actions/components/pictorialIndexPanel/Action'
import type {ImgPropertyType} from '@domains/model/vo/ImgProperty'
import {CanvasProfileInitState} from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'
import { setViewMode } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action.js'
import { setTool as setImageTool } from '@actions/components/toolBarPanel/Action'
import { setBtnState } from '@actions/components/toolBarPanel/Action'

// import controllers
import { SlideShowController } from '@domains/controllers/SlideShowController'

import {sleep} from '@utils/CommonUtil'


class InnerClass{
    store: *

    constructor(store: *){
        this.store = store
    }

    getState(): *{
        return this.store.getState()
    }
}


class DataRepository{
    store: *
    innerClass: InnerClass

    setStore(store: *){
        this.store = store
        this.innerClass = new InnerClass(store)
    }

    /**
     * Check if image exists in data repository
     * 
     * @param {string} seriesIndexId series index id
     * @param {string} imgIndexId image index id
     * @returns {boolean} 
     * @memberof DataRepository
     */
    isImageExist(seriesIndexId: string, imgIndexId: string): boolean{
        let imgDtl = this.findImageDtl(seriesIndexId, imgIndexId)

        if(imgDtl.imageBase64==null || imgDtl.imageBase64=="" || imgDtl.imageBase64==undefined){
            return false
        }

        return true
    }
	
	findAllSeriesDtl(): *{
        try{
            const seriesDtls = this.innerClass.getState().data.study.studyDtl.rootState.seriesDtls
            return seriesDtls
        }catch(ex){
            return undefined
        }
    }

    findStudyDtl(): *{
        const studyDtlTree = JsonParser.getValue(this.innerClass.getState().data, 'studyDtl')
        return studyDtlTree
    }

    /**
     * Find SeriesDetail by series index id
     * 
     * @param {string} seriesIndexId series index id
     * @returns {*} 
     * @memberof DataRepository
     */
    findSeriesDtl(seriesIndexId: string): *{
        try{
            const seriesDtl = this.innerClass.getState().data.study.studyDtl.rootState.seriesDtls.byId[seriesIndexId]
            return seriesDtl
        }catch(ex){
            return undefined
    }
        // const seriesDtl = JsonParser.getValue(this.innerClass.getState(), seriesIndexId)
        // if(seriesDtl==null){
        //     return undefined 
        // }
    }

    /**
     * Find image detail by series index id and image index id
     * 
     * @param {string} seriesIndexId series index id
     * @param {string} imgIndexId image index id
     * @returns {*} 
     * @memberof DataRepository
     */
    findImageDtl(seriesIndexId: string, imgIndexId: string): *{
        let seriesDtl = this.findSeriesDtl(seriesIndexId)
        if(seriesDtl==null){
            return undefined 
        }

        let imgDtl = JsonParser.getValue(seriesDtl, imgIndexId)
        if(imgDtl==null){
            return undefined
        }

        return imgDtl
    }

    findAnnotationDtl(seriesIndexId: string, imgIndexId: string, annotationType: string): *{
        const imgDtl = this.findImageDtl(seriesIndexId, imgIndexId)

        const annotationAllIds: Array<string> = JsonParser.getValues(imgDtl.annotationDtls,'allIds')
        for(let i=0;i<annotationAllIds.length;i++){
            const annotationId = annotationAllIds[i]
            if(imgDtl.annotationDtls.byId[annotationId] && imgDtl.annotationDtls.byId[annotationId].annotationType == annotationType){
                return imgDtl.annotationDtls.byId[annotationId]
            }
        }
    }

    /**
     * Find image detail by series detail and image index id
     * 
     * @param {*} seriesDtl series detail
     * @param {string} imgIndexId image index id
     * @returns {*} 
     * @memberof DataRepository
     */
    findImageDtlBySeriesDtl(seriesDtl: *, imgIndexId: string): *{
        let imgDtl = JsonParser.getValue(seriesDtl.imageDtls.byId, imgIndexId)
        if(imgDtl==null){
            return undefined
        }
        return imgDtl
    }

    /**
     * Find image detail by series detail, image id and image version
     *
     * @param {*} seriesDtl
     * @param {string} imageId
     * @param {string} imageVersion
     * @returns {*}
     * @memberof DataRepository
     */
    findImageDtlByImageIdAndVersion(seriesDtl: *, imageId: string, imageVersion: string): * {
        let imgDtl = null

        seriesDtl.imageDtls.allIds.forEach((imageIndexId: string) => {
            let buffer = this.findImageDtlBySeriesDtl(seriesDtl, imageIndexId)
            
            if (buffer.imageVersion == imageVersion && buffer.imageID == imageId){
                imgDtl = buffer 
            }
            if(imgDtl) return
        })

        if(imgDtl==null){
            return undefined
        }

        return imgDtl
    }

    

    /**
     * Find the base parm of StudyService#APIs
     * 
     * @param {string} seriesIndexId 
     * @returns {*} 
     * @memberof DataRepository
     */
    // findStudyServiceBaseParm(): *{
    findStudyServiceBaseParm(seriesIndexId: string): *{
        let commonTree = JsonParser.getValue(this.innerClass.getState().data, 'common')
        let seriesDtlTree = this.findSeriesDtl(seriesIndexId)
        return { 
            patientKey: commonTree.rootState.patientKey,
            hospCode: commonTree.rootState.hospCode,
            caseNo: commonTree.rootState.caseNo,
            accessionNo: commonTree.rootState.accessionNo,
            seriesNo: seriesDtlTree.seriesNo,
            // imageSeqNo: commonTree.rootState.imageSeqNo,
            // versionNo: commonTree.rootState.versionNo,
            userId: commonTree.rootState.userId,
            workstationId: commonTree.rootState.workstationId,
            requestSys: commonTree.rootState.requestSys
         }

    }

    /**
     * Find the reposition para for window initialization
     * 
     * @returns {RepositionParaType} 
     * @memberof DataRepository
     */
    findRepositionParm(): RepositionParaType{
        return JsonParser.getValue(this.innerClass.getState().data.common.rootState, 'repositionPara')
    }

    /**
     * Find IAS web service auth info
     * 
     * @returns {WebSInfo} 
     * @memberof DataRepository
     */
    findIasWsAuthInfo(): WebSInfo{
        let configTree = JsonParser.getValue(this.innerClass.getState().data, 'config')
        let webServiceIasTree = configTree.application.rootState.webServiceIas
        return new WebSInfo(CidMagicUtil.unmagicByECB(webServiceIasTree.suid1, EncryptionConstants.IAS_KEY), CidMagicUtil.unmagicByECB(webServiceIasTree.suid2, EncryptionConstants.IAS_KEY), webServiceIasTree.cidWsContextPath)
    }

    /**
     * Find ICW web service auth info
     * 
     * @returns {WebSInfo} 
     * @memberof DataRepository
     */
    findIcwWsAuthInfo(): WebSInfo{
        let configTree = JsonParser.getValue(this.innerClass.getState().data, 'config')
        let webServiceIcwTree = configTree.application.rootState.webServiceIcw
        return new WebSInfo(CidMagicUtil.unmagicByECB(webServiceIcwTree.suid1, EncryptionConstants.ICW_KEY), CidMagicUtil.unmagicByECB(webServiceIcwTree.suid2, EncryptionConstants.ICW_KEY), webServiceIcwTree.cidWsContextPath)
    }

    /**
     * Find case profile
     * 
     * @returns {*} 
     * @memberof DataRepository
     */
    findCaseProfile(): *{
        let commonTree = JsonParser.getValue(this.innerClass.getState().data, 'common')
        return commonTree.rootState
    }

    /**
     * Find viewer control config
     * 
     * @returns {*}  
     * @memberof DataRepository
     */
    findViewerControlConfig(): *{
        let configTree = JsonParser.getValue(this.innerClass.getState().data, 'config')
        return configTree.application.rootState.viewerControl
    }

    findExportConfig(): *{
        let configTree = JsonParser.getValue(this.innerClass.getState().data, 'config')
        return configTree.application.rootState.export
    }

    findImageHeaderConfig(): *{
        let configTree = JsonParser.getValue(this.innerClass.getState().data, 'config')
        return configTree.application.rootState.imageHeader
    }

    /**
     * Construct exportImage data parameters for selected images 
     *
     * @param {string} selectedSeriesId
     * @param {Array<string>} selectedImageIds
     * @param {string} password
     * @returns {*}
     * @memberof DataRepository
     */
    async findExportImageParam(selectedSeriesId: string, selectedImageIds: Array<string>, password: string): * {
        let commonTree = JsonParser.getValue(this.innerClass.getState().data, 'common')
        let messageDtlTree = JsonParser.getValue(this.innerClass.getState().data, 'messageDtl')
        let patientDtlTree = JsonParser.getValue(this.innerClass.getState().data, 'patientDtl')
        let visitDtlTree = JsonParser.getValue(this.innerClass.getState().data, 'visitDtl')
        let studyDtlTree = JsonParser.getValue(this.innerClass.getState().data, 'studyDtl')
        let seriesDtlTree = this.findSeriesDtl(selectedSeriesId)

        let ha7Msg = {
            messageDtl: {
                transactionCode: messageDtlTree.rootState.transactionCode,
                serverHosp: messageDtlTree.rootState.serverHosp,
                transactionDtm: messageDtlTree.rootState.transactionDtm,
                transactionID: messageDtlTree.rootState.transactionID,
                sendingApplication: messageDtlTree.rootState.sendingApplication
            },
            patientDtl: {
                hkid: patientDtlTree.rootState.hkid,
                patKey: patientDtlTree.rootState.key,
                patName: patientDtlTree.rootState.name,
                patDOB: patientDtlTree.rootState.dob,
                patSex: patientDtlTree.rootState.sex,
                deathIndicator: patientDtlTree.rootState.deathIndicator
            },
            visitDtl: {
                visitHosp: visitDtlTree.rootState.visitHosp,
                visitSpec: visitDtlTree.rootState.visitSpec,
                visitWardClinic: visitDtlTree.rootState.visitWardClinic,
                payCode: visitDtlTree.rootState.payCode,
                caseNum: visitDtlTree.rootState.caseNum,
                adtDtm: visitDtlTree.rootState.adtDtm
            },
            studyDtl: {
                accessionNo: studyDtlTree.rootState.accessionNo,
                studyID: studyDtlTree.rootState.studyID,
                studyType: studyDtlTree.rootState.studyType,
                studyDtm: studyDtlTree.rootState.studyDtm,
                remark: studyDtlTree.rootState.remark,
                seriesDtls: {
                    seriesDtl: [
                        {
                            seriesNo: seriesDtlTree.seriesNo,
                            examType: seriesDtlTree.examType,
                            examDtm: seriesDtlTree.examDtm,
                            entityID: seriesDtlTree.entityID,
                            imageDtls: {
                                imageDtl: []
                            }
                        }
                    ]
                }
            }
        }

        for(let i = 0; i<selectedImageIds.length;i++){
            await sleep(0)

            let imageIndexId = selectedImageIds[i]
            let selectedImageDtlTree = this.findImageDtlBySeriesDtl(seriesDtlTree, imageIndexId)
            let imageDtlTree = this.findImageDtlByImageIdAndVersion(seriesDtlTree, selectedImageDtlTree.imageID, "1")
            let imageDtlHa7 = {
                imageFormat: imageDtlTree.imageFormat,
                imagePath: imageDtlTree.imagePath,
                imageFile: imageDtlTree.imageFile,
                imageID: imageDtlTree.imageID,
                imageVersion: imageDtlTree.imageVersion,
                imageSequence: imageDtlTree.imageSequence,
                imageKeyword: imageDtlTree.imageKeyword,
                imageHandling: imageDtlTree.imageHandling,
                imageType: imageDtlTree.imageType,
                imageStatus: imageDtlTree.imageStatus,
                annotationDtls: {}
            }

            ha7Msg.studyDtl.seriesDtls.seriesDtl[0].imageDtls.imageDtl.push(imageDtlHa7)
        }

        let exportImageParam = {
            ha7Msg: JSON.stringify(ha7Msg),
            password: password,
            userId: commonTree.rootState.userId,
            workstationId: commonTree.rootState.workstationId,
            requestSys: commonTree.rootState.requestSys
        }

        return exportImageParam
    }

    findThumbnailGrid(): * {
        let thumbnailGridTree = JsonParser.getValue(this.innerClass.getState().components, 'thumbnailGrid').rootState
        return thumbnailGridTree
    }

    findCanvasProfiles(): *{
        let canvasProfilesTree = JsonParser.getValue(this.innerClass.getState().components, 'canvasProfiles')
        return canvasProfilesTree
    }

    findCanvasProfile(seriesId: string, imageId: string): *{
        let canvasProfilesTree = JsonParser.getValue(this.innerClass.getState().components, 'canvasProfiles')
        return canvasProfilesTree.byId[imageId]
    }

    findCanvasGrid(): *{
        let canvasGridTree = JsonParser.getValue(this.innerClass.getState().components, 'canvasGrid')
        return canvasGridTree.rootState
    }

    async findImageDimensions(seriesIndexId: string, imgIndexIds: Array<string>): Promise<Array<{width: number, height: number}>>{
        let imageDimensionsPromiseArr = []
        for(let i=0; i<imgIndexIds.length; i++){
            const imgIndexId = imgIndexIds[i]

            const canvasProfile = this.findCanvasProfile(seriesIndexId, imgIndexId)
            if(canvasProfile!=null && canvasProfile.imageWidth>0 && canvasProfile.imageHeight>0){
                imageDimensionsPromiseArr.push(
                    new Promise (function (resolved: Function, rejected: Function) {
                          resolved({width: canvasProfile.imageWidth, height: canvasProfile.imageHeight})
                    })
                )
            }else{
                const imgDtl = this.findImageDtl(seriesIndexId, imgIndexId)
                const base64Uri = IMAGE_BASE64_HEADERS + imgDtl.imageBase64
                imageDimensionsPromiseArr.push(getDimensions(base64Uri))
            }
        }

        //wait for all images onloaded to get its width&height
        let imageDimensions: Array<{width: number, height: number}> = await Promise.all(imageDimensionsPromiseArr)

        return imageDimensions
    }

    /**
     * Find slide show tools
     * 
     * @returns {*} 
     * @memberof DataRepository
     */
    findSlideShowTools(): * {
        let slideShowToolsTree = JsonParser.getValue(this.innerClass.getState().components, 'slideShowTools')
        return slideShowToolsTree.rootState
    }

    /**
     * Find all image index ids(including version #1-3) by series Index Id
     * 
     * @param {string} seriesIdxId 
     * @returns {Array<string>} 
     * @memberof DataRepository
     */
    findAllImageIdxIds(seriesIdxId: string): Array<string>{
        const seriesDtl = this.findSeriesDtl(seriesIdxId)
        const allIds = seriesDtl.imageDtls.allIds
        return allIds
    }

    /**
     * Find all Highest Version Image Index Ids(including version #3 only) by series Index Id
     * 
     * @param {string} seriesIdxId 
     * @returns {Array<string>} 
     * @memberof DataRepository
     */
    findAllHighestVerImgIdxIds(seriesIdxId: string): Array<string>{
        const seriesDtl = this.findSeriesDtl(seriesIdxId)
        const allIds = seriesDtl.imageDtls.allHighestVerIds
        return allIds
    }

    /**
     * Find HA7 image Id by series Index Id and image Index Id
     * 
     * @param {string} seriesIdxId 
     * @param {string} imageIdxId 
     * @returns {string} 
     * @memberof DataRepository
     */
    findImageId(seriesIdxId: string, imageIdxId: string): string{
        const seriesDtl = this.findSeriesDtl(seriesIdxId)
        const imageId = seriesDtl.imageDtls.idsMapping[imageIdxId]
        return imageId
    }
	
	findImageIdByImageIdxId(imageIdxId: string): string{
        const seriesDtls = this.findAllSeriesDtl()
        let imageId: string = ""

        seriesDtls.allIds.forEach((seriesIndexId: string) =>{
            imageId = this.findImageId(seriesIndexId, imageIdxId)
            if(imageId != undefined) {
                return
            }
        })
        return imageId
    }

    /**
     * Find list of HA7 image Id by series Index Id and list of image Index Id
     * 
     * @param {string} seriesIdxId 
     * @param {Array<string>} imageIdxIds 
     * @returns {Array<string>} 
     * @memberof DataRepository
     */
    findImageIds(seriesIdxId: string, imageIdxIds: Array<string>): Array<string>{
        const seriesDtl = this.findSeriesDtl(seriesIdxId)
        let result = []
        imageIdxIds.forEach((element: string) => {
            const imageId = seriesDtl.imageDtls.idsMapping[element]
            result.push(imageId)
        })
        return result
    }
    
    findSelectedImageIds(): Array<string> {
        const selectedImageIds = JsonParser.getValue(this.innerClass.getState().components.imageViewingPanel.canvasGrid.rootState, 'selectedImageIds')
        return selectedImageIds
    }

    findColorAdjustmentMaxNumOfImage(): *{
        let maxNumOfImage = JsonParser.getValue(this.innerClass.getState().components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState, 'maxNumOfImage')
        return maxNumOfImage
    }

    findMsgCodeContent(msgCode: string): * {
        let msgCodeTree = JsonParser.getValue(this.innerClass.getState().data, 'msgCode')
        return msgCodeTree.rootState.errors[msgCode]
    }

    findAppDimension(): {appWidth: number, appHeight: number} {
        return {appWidth: this.innerClass.getState().components.rootState.appWidth, appHeight: this.innerClass.getState().components.rootState.appHeight}
    }

    findViewMode(): ViewModeType{
        return this.innerClass.getState().components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode
    }

    findWebServiceTimeoutConfig(): number {
        return this.innerClass.getState().data.config.application.rootState.webService.timeout
    }

    isPictorialIndexPanelShown(): boolean{
        return !this.innerClass.getState().components.pictorialIndexPanel.rootState.isHidden
    }

    /**
     * Save images to data repository
     * 
     * @param {string} seriesIndexId  series index id
     * @param {Array<string>} imgIndexIds image index ids
     * @param {Array<string>} imageBase64s images base64
     * @memberof DataRepository
     */
    saveImages(seriesIndexId: string, imgIndexIds: Array<string>, imageBase64s: Array<string>){
        this.store.dispatch(setImageToData(seriesIndexId, imgIndexIds, imageBase64s))
    }

    /**
     * Save images to data repository
     * 
     * @param {CaseProfileType} caseProfile case profile
     * @memberof DataRepository
     */
    saveCaseProfile(caseProfile: CaseProfileType){
        this.store.dispatch(setCaseProfile(caseProfile))
    }
    
    /**
     * Save message code to data repository
     * 
     * @param msgCode message code
     * @memberof DataRepository
     */
    saveMsgCode(msgCode: *){
        this.store.dispatch(setMsgCode(msgCode))
    }

    /**
     * Save application config to data repository
     * 
     * @param appConfig application config
     * @memberof DataRepository
     */
    saveAppConfig(appConfig: *){
        this.store.dispatch(setAppConfig(appConfig))
    }

    /**
     * Save Pictorial Index config to data repository
     * 
     * @param {number} numOfColumn number of column of Pictorial Index
     * @param {boolean} isShowSeriesHeading whether to show series heading
     * @param {string} serieName name of serie
     * @memberof DataRepository
     */
    savePictorialIndexConfig(numOfColumn: number, isShowSeriesHeading: boolean, serieName: string){
        this.store.dispatch(setPictorialIndexConfig(numOfColumn, isShowSeriesHeading, serieName))
    }

    /**
     * Save Image Header config to data repository
     * 
     * @param {string} fontType font type
     * @param {number} fontSize font size
     * @param {string} fontColor font color
     * @param {string} bgColor background color
     * @param {boolean} visibility visibility
     * @memberof DataRepository
     */
    saveImageHeaderConfig(fontType: string, fontSize: number, fontColor: string, bgColor: string, visibility: boolean){
        CanvasProfileInitState.fontType = fontType
        CanvasProfileInitState.fontSize = fontSize
        CanvasProfileInitState.fontColor = fontColor
        CanvasProfileInitState.bgColor = bgColor
        CanvasProfileInitState.headerVisibility = visibility
    }

    saveColorAdjustmentConfig(maxNumOfImage: number) {
        this.store.dispatch(setColorAdjustmentConfig(maxNumOfImage))
    }

    /**
     * Save Study to data repository
     * 
     * @param study study
     * @memberof DataRepository
     */
    saveStudy(study: *){
        this.store.dispatch(setMessageDtl(study.messageDtl))
        this.store.dispatch(setVisitDtl(study.visitDtl))
        this.store.dispatch(setPatientDtl(study.patientDtl))
        this.store.dispatch(setStudyDtl(study.studyDtl))
    }

    /**
     * Set images to the data source of viewing panel
     * 
     * @param {string} seriesIndexId series index id
     * @param {Array<string>} imgIndexIds image index id
     * @memberof DataRepository
     */
    async setImagesToViewingPanel(seriesIndexId: string, imgIndexIds: Array<string>, imgsProperties: Array<*>): Promise<void>{
        const commonTree = JsonParser.getValue(this.innerClass.getState().data, 'common')
        let imageBase64s = []

        for(let i=0; i<imgIndexIds.length; i++){
            const imgIndexId = imgIndexIds[i]
            const imgDtl = this.findImageDtl(seriesIndexId, imgIndexId)
            const base64Uri = IMAGE_BASE64_HEADERS + imgDtl.imageBase64
            imageBase64s.push(base64Uri)
            
        }
        const viewModeTree = JsonParser.getValue(this.innerClass.getState().components.toolBarPanel.viewModeTools, 'viewModeDropDownList')
        const viewMode = viewModeTree.rootState.viewMode

        this.store.dispatch(setImageToViewPanel(commonTree.rootState.caseNo, viewMode, seriesIndexId, imgIndexIds, imageBase64s, imgsProperties))
    }

    
    async setImageToSlideShow(seriesIndexId: string, imageIndexId4SlideShow: string, seqCanvas: number, imageProperties: *): Promise<void>{
        
        let that = this

        let imageIndexId = SlideShowController.getImageIndexIdWithoutSuffix(imageIndexId4SlideShow)

        let imgDtl = that.findImageDtl(seriesIndexId, imageIndexId)
        let imageBase64 = IMAGE_BASE64_HEADERS + imgDtl.imageBase64
        
        this.store.dispatch(setImageToSlideShow(imageIndexId4SlideShow, seqCanvas, imageBase64, imageProperties))
    }

    setSelectedFirstImageId(selectedSeriesId: string, selectedFirstImageId: string) {
        this.store.dispatch(setSelectedFirstImage(selectedSeriesId, selectedFirstImageId))
    }

    setSelectedImageIds(selectedImageIds: Array<string>) {
        this.store.dispatch(setSelectedImageIds(selectedImageIds))
    }

    setCanvasSelectedImageId(selectedImageIds: Array<string>) {
        this.store.dispatch(setCanvasSelectedImage(selectedImageIds))
    }

    /**
     * Set export image response to exportImageDialog
     *
     * @param {boolean} isDownload
     * @param {string} exportImageBase64
     * @memberof DataRepository
     */
    setExportImageDialogForDownload(isDownload: boolean, exportImageBase64: string) {
        this.store.dispatch(setDownload(isDownload, exportImageBase64))
    }

    setSlideShowToolsStop() {
        this.store.dispatch(stopSlideShow())
    }

    setSlideShowMode(isModeOn: boolean) {
        this.store.dispatch(setSlideShowMode(isModeOn))
    }

    setSlideShowPlaying(isPlaying: boolean) {
        this.store.dispatch(setSlideShowPlaying(isPlaying))
    }

    setSlideShowToolsSeqCanvas(seqCanvas: number) {
        this.store.dispatch(setSlideShowSeqCanvas(seqCanvas))
    }

    setSlideShowToolsRefOffsets(seqCanvas: number, width: number, height: number) {
        this.store.dispatch(setSlideShowRefOffsets(seqCanvas, width, height))
    }

    setSlideShowTimerId(timerId: *) {
        this.store.dispatch(setSlideShowTimerId(timerId))
    }

    // set image rotation/flipping
    setImageRotation(imageId: string, rotateAngle: number) {
        this.store.dispatch(rotateImage(imageId, rotateAngle))
    }

    setImageFlip(imageId: string, flipImageX: boolean, flipImageY: boolean) {
        this.store.dispatch(flipImage(imageId, flipImageX, flipImageY))
    }


    /**
     * Set message to message dialog
     * 
     * @param {number} msgCode message code 
     * @memberof DataRepository
     */
    setMsgDialogByCode(msgCode: string, additionalContent: ?string){
        let msgCodeTree = JsonParser.getValue(this.innerClass.getState().data, 'msgCode')
        this.store.dispatch(setMsgDialog(true, msgCode, msgCodeTree.rootState.errors[msgCode].title, msgCodeTree.rootState.errors[msgCode].content, additionalContent))
    }

    /**
     * Set message to message dialog
     * 
     * @param {number} msgCode message code 
     * @memberof DataRepository
     */
    setMsgDialogByContent(title: string, msgContent: string, additionalContent: ?string){
        this.store.dispatch(setMsgDialog(true, null, title, msgContent, additionalContent))
    }

    /**
     * 
     * 
     * @param {boolean} isMasked is Application masked
     * @memberof DataRepository
     */
    setAppMask(isMasked: boolean){
        this.store.dispatch(setApplicationMask(isMasked))
    }

    setProgressBar(isProgressing: boolean, description: string){
        this.store.dispatch(setProgressBar(isProgressing, description))
    }

    setImageScale(seriesId: string, imageId: string, scaleX: number, scaleY: number, pannedXCoor: number, pannedYCoor: number){
        this.store.dispatch(setImageScale(seriesId, imageId, scaleX, scaleY, pannedXCoor, pannedYCoor))
    }

    setGridsToViewingPanel(viewMode: ViewModeType, canvasWidth: number, canvasHeight: number){
        this.store.dispatch(setGrid(viewMode, canvasWidth, canvasHeight))
    }

    setResetImagesToViewingPanel(seriesIndexId: string, imgIndexIds: Array<string>, imgsProperties: Array<*>) {
        this.store.dispatch(resetImages(seriesIndexId, imgIndexIds, imgsProperties))
    }
    
    setAppSize(width: number, height: number) {
        this.store.dispatch(setAppSize(width, height))
    }
	
	setColorAdjustmentToViewingPanel(brightness: number, contrast: number, hue: number, saturation: number, invert: boolean) {
        this.store.dispatch(setColorAdjustment(brightness, contrast, hue, saturation, invert))
    }
	
    setColorAdjustmentPropsBoxVisible(visible: boolean) {
        this.store.dispatch(setColorAdjustmentPropsBoxVisible(visible))
    }

    // for Pictorial Index Panel
    setIsHiddenToPictorialIndexPanel(isHidden: boolean) {
        this.store.dispatch(setIsHidden(isHidden))
    }

    setImagePosition(seriesId: string, imageId: string, imageXCoor: number, imageYCoor: number){
        this.store.dispatch(setImagePosition(seriesId, imageId, imageXCoor, imageYCoor))
    }

    setViewingAreaDimension(viewingAreaWidth: number, viewingAreaHeight: number, canvasWidth: number, canvasHeight: number, imgsProperty: Array<ImgPropertyType>){
        this.store.dispatch(resizeImgViewingPanel(viewingAreaWidth, viewingAreaHeight, canvasWidth, canvasHeight, imgsProperty))
    }

    /**
     * Update status of View Mode state tree
     * 
     * @param {ViewModeType} viewMode 
     * @memberof DataRepository
     */
    setViewMode(viewMode: ViewModeType){
        this.store.dispatch(setViewMode(viewMode))
    }

    setImageTool(tool: string){
        this.store.dispatch(setImageTool(tool))
    }

    setToolbarBtnState(btnState: *){
        this.store.dispatch(setBtnState(btnState))
    }

// for debug purpose
    log4Debug(type: string, payload: *) {
        this.store.dispatch({
            type: type,
            payload
        })
    }

    getDispatcher(): *{
        return this.store.dispatch
    }

}

let dataRepository = new DataRepository()
export default dataRepository