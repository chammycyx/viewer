// @flow
/**
 * True if 2 json are equal to each other.
 * 
 * @param {*} value 
 * @param {*} other 
 * @returns boolean
 */
export function isEqual(value: *, other: *): boolean{
	
    let valueJsonStr = JSON.stringify(value)
    let otherJsonStr = JSON.stringify(other)

    return valueJsonStr==otherJsonStr
}