// @flow
import moment from 'moment'


export const DATE_FORMAT = {

    /**
     * Short date format
     *      i.e. 21-Dec-2010
     */
    SHORT_DDMMMYYYY: "DD-MMM-YYYY",
    
    /**
     * Short date format
     *      i.e. 2010-12-21
     *      
     */
    SHORT_YYYYMMDD: "YYYY-MM-DD",

    /**
     * Short date format
     *      i.e. 2010年12月21日
     *      
     */
    SHORT_YYYYMMDD_CN: "YYYY年MM月DD日",

    /**
     * Long date format
     *      i.e. 21-Dec-2010 23:15:06
     */
    LONG_DDMMMYYYYTHHMMSS: "DD-MMM-YYYY HH:mm:ss",

    /**
     * Long date format
     *      i.e. 2010-12-21 23:15:06
     */
    LONG_YYYYMMDDTHHMMSS: "YYYY-MM-DD HH:mm:ss",

    /**
     * Long date format
     *      i.e. 2010年12月21日 23時15分06秒
     */
    LONG_YYYYMMDDTHHMMSS_CN: "YYYY年MM月DD日 HH時mm分ss秒",

    /**
     * Full date format precised to millisecond
     *      i.e. 21-Dec-2010 23:15:06.000
     */
    FULL_DDMMMYYYYTHHMMSSMS: "DD-MMM-YYYY HH:mm:ss.SSS",

    /**
     * Full date format precised to millisecond
     *      i.e. 2010-12-21 23:15:06.000
     */
    FULL_YYYYMMDDTHHMMSSMS: "YYYY-MM-DD HH:mm:ss.SSS",

    /**
     * Full date format precised to millisecond
     *      i.e. 2010年12月21日 23時15分06秒000毫秒
     */
    FULL_YYYYMMDDTHHMMSSMS_CN: "YYYY年MM月DD日 HH時mm分ss秒SSS毫秒",

    /**
     * Full date format precised to millisecond
     *      i.e. 20101221231506.000
     */
    FULL_YYYYMMDDTHHMMSSMS_PLAIN: "YYYYMMDDHHmmss.SSS",

    /**
     * Full date format precised to millisecond
     *      i.e. 20101221231506
     */
    FULL_YYYYMMDDTHHMMSS_PLAIN: "YYYYMMDDHHmmss"

}

export class DateParser {

    /**
     * Get default date pattern
     * 
     * @static
     * @returns {string} 
     * @memberof DateParser
     */
    static getDefaultPattern(): string {
        return DATE_FORMAT.SHORT_DDMMMYYYY
    }

    /**
     * Parse dateStr to requested date format
     * 
     * @static
     * @param {string} date 
     * @param {string} inputFormat 
     * @param {string} [outputFormat=this.getDefaultPattern()] 
     * @returns {string} 
     * @memberof DateParser
     */
    static parse(date: string, inputFormat: string, outputFormat: string = this.getDefaultPattern()): string {

        let isMatch = this.isPatternMatch(date, inputFormat)

        if (date != null && date != "" && isMatch) {
            return moment(date, inputFormat).format(outputFormat)
        }

        return date
    }

    /**
     * Determine if input pattern match
     * 
     * @static
     * @param {string} date 
     * @param {string} inputFormat 
     * @returns {boolean} 
     * @memberof DateParser
     */
    static isPatternMatch(date: string, inputFormat: string): boolean {
        return inputFormat.length == date.length;
    }
}

