//@flow
import { CanvasProfileInitState } from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'

export function getInitColorAdjustmentValues(selectedImageIds: Array<string>, canvasProfiles: *): {brightness: number, contrast: number, hue: number, saturation: number, invert: boolean} {
    const defaultValueSet = {
        brightness: CanvasProfileInitState.brightness,
        contrast: CanvasProfileInitState.contrast,
        hue: CanvasProfileInitState.hue,
        saturation: CanvasProfileInitState.saturation,
        invert: CanvasProfileInitState.invert
    }

    if(selectedImageIds.length == 0) {
        return defaultValueSet
    } else if(selectedImageIds.length >= 1) {
        let valueSet = {
            brightness: canvasProfiles.byId[selectedImageIds[0]].brightness,
            contrast: canvasProfiles.byId[selectedImageIds[0]].contrast,
            hue: canvasProfiles.byId[selectedImageIds[0]].hue,
            saturation: canvasProfiles.byId[selectedImageIds[0]].saturation,
            invert: canvasProfiles.byId[selectedImageIds[0]].invert
        }

        if(selectedImageIds.length >= 2) {
            for(let i=1; i<selectedImageIds.length; i++) {
                if(valueSet.brightness != canvasProfiles.byId[selectedImageIds[i]].brightness
                    || valueSet.contrast != canvasProfiles.byId[selectedImageIds[i]].contrast
                    || valueSet.hue != canvasProfiles.byId[selectedImageIds[i]].hue
                    || valueSet.saturation != canvasProfiles.byId[selectedImageIds[i]].saturation
                    || valueSet.invert != canvasProfiles.byId[selectedImageIds[i]].invert
                ) {
                    return defaultValueSet
                }
            }
        }

        return valueSet
    }

    return defaultValueSet
}