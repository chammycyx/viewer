import shallow from 'enzyme/shallow'

export function shallowWrappedComponent(hocNode, wrappedCompDisplayName) {
    // Sw = ShallowWrapper
    let hocSw = shallow(hocNode)
    let innerCompNodes = hocSw.find(wrappedCompDisplayName)

    if(innerCompNodes.length == 0) {
        let nextHocNode = hocSw.findWhere(n => n.name().includes(wrappedCompDisplayName)).get(0)
        return shallowWrappedComponent(nextHocNode, wrappedCompDisplayName)
    }

    let innerCompSw = shallow(innerCompNodes.get(0))
    return innerCompSw
}

export function shallowWrappedComponentParent(hocNode, wrappedCompDisplayName) {
    // Sw = ShallowWrapper
    let hocSw = shallow(hocNode)
    let innerCompNodes = hocSw.find(wrappedCompDisplayName)

    if(innerCompNodes.length == 0) {
        let nextHocNode = hocSw.findWhere(n => n.name().includes(wrappedCompDisplayName)).get(0)
        return shallowWrappedComponentParent(nextHocNode, wrappedCompDisplayName)
    }

    return hocSw
}