//Json structure and State Tree Mapping
export const ViewerControlMap = {'viewerControl':'viewerControl'}
export const ImageHeaderMap = {'imageHeader':'imageHeader'}
export const ExportMap = {'export':'export'}
export const WebServiceIcwCidWsContextPathMap = {'webServiceIcw/cidWsContextPath':'webServiceIcw/cidWsContextPath'}
export const WebServiceIcwSuid1Map = {'webServiceIcw/suid1':'webServiceIcw/suid1'}
export const WebServiceIcwSuid2Map = {'webServiceIcw/suid2':'webServiceIcw/suid2'}
export const WebServiceIasCidWsContextPathMap = {'ImageServiceRS/RS_URL':'webServiceIas/cidWsContextPath'}
export const WebServiceIasSuid1Map = {'ImageServiceRS/RS_Username':'webServiceIas/suid1'}
export const WebServiceIasSuid2Map = {'ImageServiceRS/RS_Password':'webServiceIas/suid2'}
export const MessageDtlTransactionCodeMap = {'transactionCode':'transactionCode'}
export const MessageDtlServerHospMap = {'serverHosp':'serverHosp'}
export const MessageDtlTransactionDtmMap = {'transactionDtm':'transactionDtm'}
export const MessageDtlTransactionIDMap = {'transactionID':'transactionID'}
export const MessageDtlSendingApplicationMap = {'sendingApplication':'sendingApplication'}
export const PatientDtlHkidMap = {'hkid':'hkid'}
export const PatientDtlkeyMap = {'patKey':'key'}
export const PatientDtlNameMap = {'patName':'name'}
export const PatientDtlDobMap = {'patDOB':'dob'}
export const PatientDtlSexMap = {'patSex':'sex'}
export const PatientDtlDeathIndicatorMap = {'deathIndicator':'deathIndicator'}
export const VisitDtlVisitHospMap = {'visitHosp':'visitHosp'}
export const VisitDtlVisitSpecMap = {'visitSpec':'visitSpec'}
export const VisitDtlVisitWardClinicMap = {'visitWardClinic':'visitWardClinic'}
export const VisitDtlPayCodeMap = {'payCode':'payCode'}
export const VisitDtlCaseNumMap = {'caseNum':'caseNum'}
export const VisitDtlAdtDtmMap = {'adtDtm':'adtDtm'}
export const StudyDtlAccessionNoMap = {'accessionNo':'accessionNo'}
export const StudyDtlStudyIDMap = {'studyID':'studyID'}
export const StudyDtlStudyTypeMap = {'studyType':'studyType'}
export const StudyDtlStudyDtmMap = {'studyDtm':'studyDtm'}
export const StudyDtlRemarkMap = {'remark':'remark'}
export const WebServiceMap = {'webService':'webService'}

// export const ViewerControlMap = {'viewerControl':'data/config/application/viewerControl'}
// export const ImageHeaderMap = {'imageHeader':'data/config/application/imageHeader'}
// export const ExportMap = {'export':'data/config/application/export'}
// export const WebServiceIcwMap = {'webServiceIcw':'data/config/application/webServiceIcw'}
// export const WebServiceIasMap = {'webServiceIas':'data/config/application/webServiceIas'}
// export const MessageDtlMap = {'messageDtl':'messageDtl'}
// export const PatientDtlHkidMap = {'patientDtl/hkid':'patientDtl/hkid'}
// export const PatientDtlkeyMap = {'patientDtl/patKey':'patientDtl/patKey'}
// export const PatientDtlNameMap = {'patientDtl/patName':'patientDtl/patName'}
// export const PatientDtlDobMap = {'patientDtl/patDOB':'patientDtl/patDOB'}
// export const PatientDtlSexMap = {'patientDtl/patSex':'patientDtl/patSex'}
// export const PatientDtlDeathIndicatorMap = {'patientDtl/deathIndicator':'patientDtl/deathIndicator'}
// export const VisitDtlMap = {'visitDtl':'visitDtl'}
// export const StudyDtlAccessionNoMap = {'studyDtl/accessionNo':'studyDtl/accessionNo'}
// export const StudyDtlStudyIDMap = {'studyDtl/studyID':'studyDtl/studyID'}
// export const StudyDtlStudyTypeMap = {'studyDtl/studyType':'studyDtl/studyType'}
// export const StudyDtlStudyDtmMap = {'studyDtl/studyDtm':'studyDtl/studyDtm'}
// export const StudyDtlRemarkMap = {'studyDtl/remark':'studyDtl/remark'}

//Special handling - Json structure and State Tree Mapping for SeriesDtl
export const SeriesDtlMap = {'seriesDtls':'seriesDtls'}

//Full Set of Json structure and State Tree Mapping (except SeriesDtl)
export const StateTreeMap = Object.assign(
    ViewerControlMap,
    ImageHeaderMap,
    ExportMap,
    WebServiceIcwCidWsContextPathMap,
    WebServiceIcwSuid1Map,
    WebServiceIcwSuid2Map,
    WebServiceIasCidWsContextPathMap,
    WebServiceIasSuid1Map,
    WebServiceIasSuid2Map,
    MessageDtlTransactionCodeMap,
    MessageDtlServerHospMap,
    MessageDtlTransactionDtmMap,
    MessageDtlTransactionIDMap,
    MessageDtlSendingApplicationMap,
    PatientDtlHkidMap,
    PatientDtlkeyMap,
    PatientDtlNameMap,
    PatientDtlDobMap,
    PatientDtlSexMap,
    PatientDtlDeathIndicatorMap,
    VisitDtlVisitHospMap,
    VisitDtlVisitSpecMap,
    VisitDtlVisitWardClinicMap,
    VisitDtlPayCodeMap,
    VisitDtlCaseNumMap,
    VisitDtlAdtDtmMap,
    StudyDtlAccessionNoMap,
    StudyDtlStudyIDMap,
    StudyDtlStudyTypeMap,
    StudyDtlStudyDtmMap,
    StudyDtlRemarkMap,
    WebServiceMap
)