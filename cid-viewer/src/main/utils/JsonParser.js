// @flow

import ConfigSection from '../model/vo/ConfigSection'
import ConfigEntry from '../model/vo/ConfigEntry';
import JsonPath from 'jsonpath/jsonpath.min'

class JsonParser{

    /**
     * Turn JSON string into array of ConfigSection
     * 
     * @static
     * @param {String} JSON string 
     * @returns Array of ConfigSection
     * @memberof JsonParser
     */
    static getConfigSections(jsonString: string): Array<ConfigSection>{
        let json: {} = JSON.parse(jsonString)
        let sectionNames: Array<string> = Object.keys(json)
        let sections: Array<ConfigSection> = []  //ConfigSection
        sectionNames.forEach((sectionName: string) => {
            let configSection: ConfigSection = new ConfigSection()
                configSection.name = sectionName
            sections.push(configSection)

            let section: {} = json[sectionName]
            let entryKeys: Array<string> = Object.keys(section)
                entryKeys.forEach((entryKey: string) => {
                    let entryValue: string = section[entryKey]
                    configSection.addEntry(entryKey,entryValue)
                })

        })
        return sections
    }

    /**
     * Turn JSON string into array of ConfigEntry. Return all entries under different Sections
     * 
     * @static
     * @param {String} JSON string 
     * @returns Array of ConfigEntry
     * @memberof JsonParser
     */
    static getAllConfigEntries(jsonString: string): Array<ConfigEntry>{
        let entries: Array<ConfigEntry> = []

        let sections: Array<ConfigSection> = JsonParser.getConfigSections(jsonString)
        sections.forEach((section: ConfigSection)=>{
            entries = entries.concat(section.entries)
        })

        return entries
    }

    // /**
    //  * Turn JSON string into array of ConfigEntry. Return all level1 entries
    //  * 
    //  * @static
    //  * @param {String} JSON string 
    //  * @returns Array of ConfigEntry
    //  * @memberof JsonParser
    //  */
    // static getConfigEntries(jsonString){
    //     let json = JSON.parse(jsonString)
    //     let keys = Object.keys(json)
    //     let values = Object.values(json)

    //     let entries = new Array();
    //     for(let i=0;i<keys.length;i++){
    //         entries.push(new ConfigEntry(keys[i], values[i]))   
    //     }

    //     return entries
    // }

    /**
     * Find values by key from JSON string
     * 
     * @static
     * @param {Object} json
     * @param {String} key 
     * @returns Array<any>
     * @memberof JsonParser
     */
    static getValues(json: {}, key: string): any {
        let result: Array<any> = JsonPath.query(json, '$..' + key)
        return result
    }

    /**
     * Find value by key from JSON string
     * 
     * @static
     * @param {Object} json
     * @param {String} key 
     * @returns Value in String or Array<any>
     * @memberof JsonParser
     */
    static getValue(json: {}, key: string): any {
        return this.getValues(json, key)[0]
    }

    /**
     * Find values by key from JSON string
     * 
     * @static
     * @param {String} jsonString 
     * @param {String} key 
     * @returns Array<any>
     * @memberof JsonParser
     */
    static getValuesFromString(jsonString: string, key: string): any {
        let json: {} = JSON.parse(jsonString)
        return this.getValues(json, key)
    }

    /**
     * Find value by key from JSON string
     * 
     * @static
     * @param {String} jsonString 
     * @param {String} key 
     * @returns Value in String or Array<any>
     * @memberof JsonParser
     */
    static getValueFromString(jsonString: string, key: string): any {
        return this.getValuesFromString(jsonString, key)[0]
    }
}

export default JsonParser