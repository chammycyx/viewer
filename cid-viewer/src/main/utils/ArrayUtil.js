//@flow

/**
 * Null-safe check if the specified collection is empty.
 * Null and undefined returns true.
 * 
 * @export Function
 * @param {Array<*>} array the collection to check, may be null
 * @returns {boolean} true if empty or null
 */
export function isEmpty(array: Array<*>): boolean{
    if(array==null || typeof array == 'undefined' || array.length==0){
        return true
    }
    return false
}

export function symmetricDifference(a1: Array<*>, a2: Array<*>): Array<*> {
    return difference(a1, a2).concat(difference(a2, a1))
}

function difference(a1: Array<*>, a2: Array<*>): Array<*> {
    let a2Set = new Set(a2);
    return a1.filter(function(x: *): boolean { return !a2Set.has(x); })
}
    