import * as React from 'react'

/**
 * This function helps to bypass the validation from ESlint-FlowType/React
 * 
 * React.createRef() introduced in React v16.3. 
 * False alarm prompted as validation of ESlint-FlowType doesn't catch up with the changes of latest React.
 * 
 * @export
 * @returns 
 */
export function getReactCreateRef(){
    return React.createRef()
}

/**
 * This function helps to bypass the validation from ESlint-FlowType/React
 * 
 * Some parameters in "document" are always treated as undefined (e.g. onmouseup, onmousemove) even if they are not
 * 
 * @export
 * @returns 
 */
export function getDocument(){
    return document
}