// @flow
import {StateTreeMap, SeriesDtlMap} from './StateTreeMap'
import {TAG_IMAGE_DETAIL, TAG_ANNOTATION_DETAIL, TAG_SERIES_DETAIL} from '@constants/DatasourceConstants'

export class InnerClass{
    static isHighestVerIds(highestVerIds: {}, imageDtl: {imageID: string, imageVersion: string}): boolean{
        const highestVerIdKeys: Array<mixed> = Object.keys(highestVerIds)
        const highestVerIdValues: Array<mixed> = Object.values(highestVerIds)

        for(let i=0;i<highestVerIdKeys.length;i++){
            const highestVerId: string = String(highestVerIdKeys[i])
            const highestVer: string = String(highestVerIdValues[i])
            if(imageDtl.imageID==highestVerId && imageDtl.imageVersion==highestVer){
                return true
            }
        }
        return false
    }

    static findHighestVerIds(imageDtls: Array<{imageID: string, imageVersion: string}>): {}{
        let highestVerMap = {}
        for(let i=0;i<imageDtls.length;i++){
            const imageDtl = imageDtls[i]
            if(highestVerMap[imageDtl.imageID]==undefined || highestVerMap[imageDtl.imageID]<imageDtl.imageVersion){
                highestVerMap[imageDtl.imageID] = imageDtl.imageVersion
            }
        }
        return highestVerMap
    }

    static createSeriesDtlStateTree(seriesDtl: {seriesNo: string, examType: string, examDtm: string, entityID: string, imageDtls: {imageDtl: Array<any>}}): {}{
        let seriesDtlStateTree = {}
            seriesDtlStateTree.seriesNo = seriesDtl.seriesNo
            seriesDtlStateTree.examType = seriesDtl.examType
            seriesDtlStateTree.examDtm = seriesDtl.examDtm
            seriesDtlStateTree.entityID = seriesDtl.entityID
            seriesDtlStateTree.imageDtls = {byId: {},allIds: [],allHighestVerIds: [], idsMapping: {}}
        let counter: number = 1
        const findHighestVerIds: {} = InnerClass.findHighestVerIds(seriesDtl.imageDtls.imageDtl)
        for(let i=0;i<seriesDtl.imageDtls.imageDtl.length;i++){
            const imageDtl = seriesDtl.imageDtls.imageDtl[i]
            const imageId = imageDtl.imageID
            const imageIdxId: string = TAG_IMAGE_DETAIL+counter
            let wrapper: {} = {};
                wrapper[imageIdxId] = InnerClass.createImageDtlStateTree(imageDtl)
            Object.assign(seriesDtlStateTree.imageDtls.byId, wrapper)
            seriesDtlStateTree.imageDtls.allIds.push(imageIdxId)
            seriesDtlStateTree.imageDtls.idsMapping[imageIdxId] = imageId
            if(InnerClass.isHighestVerIds(findHighestVerIds,imageDtl)){
                seriesDtlStateTree.imageDtls.allHighestVerIds.push(imageIdxId)
            }
            counter++;
        }
        return seriesDtlStateTree
    }

    static createImageDtlStateTree(imageDtl: {imageThumbnailBase64: string, imageBase64: string, annotationDtls: {annotationDtl: Array<{}>}, imageStatus: string, imageType: string, imageHandling: string, imageKeyword: string, imageSequence: string,imageVersion: string,imageID: string, imageFile: string, imagePath: string, imageFormat: string}): {imageStatus: string, imageType: string, imageHandling: string, imageKeyword: string, imageSequence: string,imageVersion: string,imageID: string, imageFile: string, imagePath: string, imageFormat: string, annotationDtls: {byId: {},allIds: Array<string>}}{
        let imageDtlStateTree = {}
            imageDtlStateTree.imageBase64 = imageDtl.imageBase64
            imageDtlStateTree.imageThumbnailBase64 = imageDtl.imageThumbnailBase64
            imageDtlStateTree.imageFormat = imageDtl.imageFormat
            imageDtlStateTree.imagePath = imageDtl.imagePath
            imageDtlStateTree.imageFile = imageDtl.imageFile
            imageDtlStateTree.imageID = imageDtl.imageID
            imageDtlStateTree.imageVersion = imageDtl.imageVersion
            imageDtlStateTree.imageSequence = imageDtl.imageSequence
            imageDtlStateTree.imageKeyword = imageDtl.imageKeyword
            imageDtlStateTree.imageHandling = imageDtl.imageHandling
            imageDtlStateTree.imageType = imageDtl.imageType
            imageDtlStateTree.imageStatus = imageDtl.imageStatus
            imageDtlStateTree.annotationDtls = {byId: {},allIds: []}
        let counter: number = 1
        for(let i=0;i<imageDtl.annotationDtls.annotationDtl.length;i++){
            let annDtl: {} = imageDtl.annotationDtls.annotationDtl[i]
            const tag: string = TAG_ANNOTATION_DETAIL+counter
            let wrapper = {};
                wrapper[tag] = InnerClass.createAnnDtlStateTree(annDtl)
            Object.assign(imageDtlStateTree.annotationDtls.byId, wrapper)
            imageDtlStateTree.annotationDtls.allIds.push(tag)
            counter++;
        }
        return imageDtlStateTree
    }

    static createAnnDtlStateTree(annDtl: {}): {}{
        let annDtlStateTree = {}
        Object.assign(annDtlStateTree, annDtl)
        return annDtlStateTree
        //     annDtlStateTree.annotationID = annDtl.annotationID
        //     annDtlStateTree.annotationSeq = annDtl.annotationSeq
        //     annDtlStateTree.annotationType = annDtl.annotationType
        //     annDtlStateTree.annotationText = annDtl.annotationText
        //     annDtlStateTree.annotationCoordinate = annDtl.annotationCoordinate
        //     annDtlStateTree.annotationStatus = annDtl.annotationStatus
        //     annDtlStateTree.annotationEditable = annDtl.annotationEditable
        //     annDtlStateTree.annotationUpdDtm = annDtl.annotationUpdDtm
        // return annDtlStateTree
    }

    static merge(stateTree: {}, jsonString: string, stateTreePath: string, dataTreePath: string){
        let json: {} = JSON.parse(jsonString)
        let dataTree: {} = InnerClass.getTree(json,dataTreePath)
        if(dataTree!=undefined){
            let stateTreeNode: {} = eval('stateTree'+InnerClass.getEvalString(stateTreePath))
            if( typeof stateTreeNode === 'object'){
                Object.assign(stateTreeNode, dataTree)
            }else if(typeof stateTreeNode === 'string'){
                let nodes: Array<string> = stateTreePath.split('/')
                const lastNodeName: string = nodes.pop()
                let objectWrapper = {}
                    objectWrapper[lastNodeName] = dataTree
                stateTreeNode = InnerClass.getTree(stateTree, nodes.join('/'))
                Object.assign(stateTreeNode, objectWrapper)
            }
        }
    }

    static mergeSeriesDtl(stateTree: {}, jsonString: string, stateTreePath: string, dataTreePath: string){
        // special handling for seriesDtls
        let json: {} = JSON.parse(jsonString)
        let seriesDtls: * = InnerClass.getTree(json,dataTreePath)
        if(seriesDtls!=undefined){
            let seriesDtlsStateTree = InnerClass.getTree(stateTree, stateTreePath)
                Object.assign(seriesDtlsStateTree,{byId: {},allIds: []})
            let counter: number = 1
            for(let i=0;i<seriesDtls.seriesDtl.length;i++){
                let seriesDtl = seriesDtls.seriesDtl[i]
                const tag: string = TAG_SERIES_DETAIL+counter
                let wrapper = {};
                    wrapper[tag] = InnerClass.createSeriesDtlStateTree(seriesDtl)
                Object.assign(seriesDtlsStateTree.byId, wrapper)
                seriesDtlsStateTree.allIds.push(tag)
                counter++;
            }
        }
    }

    static getTree(fullTree: {}, targetPath: string): any{
        if('' == targetPath)
            return fullTree

        const evalString: string = InnerClass.getEvalString(targetPath)
        let dataTree
        try{
            dataTree = eval('fullTree'+evalString)
        }catch(ex){
            return undefined
        }
        return dataTree
    }

    // static createNode(tree, path){
    //     // let notes = path.split('/')
    //     // let evalString = ''
    //     // for(let i=0;i<notes.length;i++){}
    //     //     let note = notes[i]
    //     //     evalString += "['" + note + "']"
    //     //     let node = eval('tree'+ evalString)
    //     //     if(node==undefined){
    //     //         node
    //     //     }
    //     // }
    //     let note3 = tree
    //     let notes = path.split('/')
    //     for(let i=0;i<notes.length;i++){}
    //         let note1 = notes[i]
    //         let note2 = node["'"+note+"'"]
    //         if(note2==undefined){
    //             note3["'"+note1+"'"] = null
    //         }
    //     }
    // }

    static getEvalString(path: string): string{
        const nodes: Array<string> = path.split('/')
        let evalString: string = ''
        nodes.forEach((node: string) => {
            evalString += "['" + node + "']"
        })
        return evalString
    }
}

export default class StateTreeUtil{
    static merge(stateTree: {}, jsonString: string, stateTreeMap: {}){
        const dataTreePath: string = String(Object.keys(stateTreeMap)[0])
        const stateTreePath: string = String(Object.values(stateTreeMap)[0])
        const seriesDtlDataTreePath: string = String(Object.keys(SeriesDtlMap)[0])
        const seriesDtlStateTreePath: string = String(Object.values(SeriesDtlMap)[0])
        if(dataTreePath==seriesDtlDataTreePath && stateTreePath==seriesDtlStateTreePath){
            //special handling for seriesDtls
            InnerClass.mergeSeriesDtl(stateTree, jsonString, stateTreePath, dataTreePath)
        }else{
            InnerClass.merge(stateTree, jsonString, stateTreePath, dataTreePath)
        }
    }

    static mergeAll(stateTree: {}, jsonString: string){
        const dataTreePaths: Array<mixed> = Object.keys(StateTreeMap)
        const stateTreePaths: Array<mixed> = Object.values(StateTreeMap)
    
        for(let i=0;i<dataTreePaths.length;i++){
            const dataTreePath: string = String(dataTreePaths[i])
            const stateTreePath: string = String(stateTreePaths[i])
            InnerClass.merge(stateTree, jsonString, stateTreePath, dataTreePath)
        }

        // special handling for seriesDtls
        const seriesDtlDataTreePath: string = String(Object.keys(SeriesDtlMap)[0])
        const seriesDtlStateTreePath: string = String(Object.values(SeriesDtlMap)[0])
        InnerClass.mergeSeriesDtl(stateTree, jsonString, seriesDtlStateTreePath, seriesDtlDataTreePath)
    }
    

}