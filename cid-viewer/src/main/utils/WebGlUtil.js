// @flow
import { fabric } from 'fabric'

export function glErrorToString(glContext: *, errorCode: *): * {
	let errorCodeStr: string
	if(errorCode == undefined) {
		errorCodeStr = 'undefined'
	} else if(errorCode == null) {
		errorCodeStr = 'null'
	} else {
		errorCodeStr = errorCode.toString()
	}

	if (!glContext) {
		return 'Context undefined for error code: ' + errorCodeStr
	}
	else if (typeof errorCode !== 'number') {
		return 'Error code is not a number: ' + errorCodeStr
	}

	switch (errorCode) {
		case glContext.NO_ERROR:
			return 'NO_ERROR'
		case glContext.INVALID_ENUM:
			return 'INVALID_ENUM'
		case glContext.INVALID_VALUE:
			return 'INVALID_VALUE'
		case glContext.INVALID_OPERATION:
			return 'INVALID_OPERATION'
		case glContext.INVALID_FRAMEBUFFER_OPERATION:
			return 'INVALID_FRAMEBUFFER_OPERATION'
		case glContext.OUT_OF_MEMORY:
			return 'OUT_OF_MEMORY'
		case glContext.CONTEXT_LOST_WEBGL:
			return 'CONTEXT_LOST_WEBGL'
		default:
			return 'UNKNOWN_ERROR'
	}
}

export function checkIfErrorOccurred(glContext: *, glError: *): boolean {
	return glError !== glContext.NO_ERROR
}

// Before use, make sure filterBackend has already initialized and it is WebGL
export function getWebGLContext(): * {
	return fabric.filterBackend.gl
}