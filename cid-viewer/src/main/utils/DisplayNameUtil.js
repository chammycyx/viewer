//@flow
function getDisplayName(WrappedComponent: *): string {
    return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}

export default function createDisplayName(hocIdentifier: string = 'HOC', WrappedComponent: *): string {
    return hocIdentifier + '(' + getDisplayName(WrappedComponent) + ')'
}