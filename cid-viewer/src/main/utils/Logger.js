// @flow
import { log as getLogAction, logSnapshot as getLogSnapshotAction, type LogType } from '@actions/data/logs/Action'
import { LOG_LEVEL, LOGGER_NAME } from '@constants/LogConstants'
import { CommonLogController } from '@domains/controllers/rootApp/CommonLogController'

export class Logger {
    dispatch: Function
    setDispatcher: Function
    dispatchLog: Function
    getLogType: Function
    trace: Function
    debug: Function
    info: Function
    warn: Function
    error: Function
    fatal: Function
    servicePerf: Function
    userActions: Function
    userActionsPerf: Function
    apiPerf: Function

    commonLogController: CommonLogController

    constructor() {
        this.setDispatcher = this.setDispatcher.bind(this)
        this.dispatchLog = this.dispatchLog.bind(this)
        this.getLogType = this.getLogType.bind(this)
        this.trace = this.trace.bind(this)
        this.debug = this.debug.bind(this)
        this.info = this.info.bind(this)
        this.warn = this.warn.bind(this)
        this.error = this.error.bind(this)
        this.fatal = this.fatal.bind(this)
        this.servicePerf = this.servicePerf.bind(this)
        this.userActions = this.userActions.bind(this)
        this.userActionsPerf = this.userActionsPerf.bind(this)
        this.apiPerf = this.apiPerf.bind(this)
    }

    setCommonLogController(commonLogController: *) {
        this.commonLogController = commonLogController
    }

    setDispatcher(dispatcher: *){
        this.dispatch = dispatcher
    }

    trace(message: *) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.TRACE, message)
    }

    debug(message: *) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.DEBUG, message)
    }

    servicePerf(loggername: string, message: *) {
        this.log(loggername, LOG_LEVEL.SERVICE_PERF, message)
    }

    userActionsPerf(loggername: string, message: *, isLogBySnapshot?: boolean) {
        this.log(loggername, LOG_LEVEL.USER_ACTIONS_PERF, message, isLogBySnapshot)
    }

    userActions(loggername: string, message: *, isLogBySnapshot?: boolean) {
        this.log(loggername, LOG_LEVEL.USER_ACTIONS, message, isLogBySnapshot)
    }

    info(message: *) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.INFO, message)
    }

    system(loggername: string, message: *) {
        this.log(loggername, LOG_LEVEL.SYSTEM, message)
    }

    apiPerf(loggername: string, message: *) {
        this.log(loggername, LOG_LEVEL.API_PERF, message)
    }

    warn(message: *, msgCode?: string) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.WARN, message, false, msgCode)
    }

    warnWithMsgCode(msgCode: string) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.WARN, null, false, msgCode)
    }

    error(message: *, msgCode?: string) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.ERROR, message.toString(), false, msgCode)
    }

    errorWithMsgCode(msgCode: string) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.ERROR, null, false, msgCode)
    }

    fatal(message: *, msgCode?: string) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.FATAL, message, false, msgCode)
    }

    fatalWithMsgCode(msgCode: string) {
        this.log(LOGGER_NAME.CID, LOG_LEVEL.FATAL, null, false, msgCode)
    }

    dispatchLog(action: LogType){
        if(this.dispatch!=null){
            this.dispatch(action)
        }
    }

    log(loggername: string, level: number, message: *, isLogBySnapshot?: boolean, msgCode?: string) {
        if(this.commonLogController != null) {
            this.commonLogController.handleLog(loggername, level, message, msgCode)
        } else {
            switch(level) {
                case LOG_LEVEL.USER_ACTIONS:
                case LOG_LEVEL.USER_ACTIONS_PERF:
                    this.dispatchLog(this.getLogType(loggername, level, message, isLogBySnapshot, msgCode))
                    break
                default:
                    this.dispatchLog(getLogAction(loggername, level, message, msgCode))
            }
        }
    }

    getLogType(loggername: string, logLevel: number, message: *, isLogBySnapshot?: boolean, msgCode?: string): LogType {
        let logType: LogType
        if(isLogBySnapshot != undefined && isLogBySnapshot == true) {
            logType = getLogSnapshotAction(loggername, logLevel, message, msgCode)
        } else {
            logType = getLogAction(loggername, logLevel, message, msgCode)
        }
        return logType
    }
}

let logger = new Logger()
  
export default logger