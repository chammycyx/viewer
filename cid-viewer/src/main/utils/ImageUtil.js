// @flow

export function getDimensions(file: string): Promise<{width: number, height: number}> {
    return new Promise (function (resolved: Function, rejected: Function) {
      var i = new Image()
      i.onload = function(){
        resolved({width: i.width, height: i.height})
      };
      i.src = file
    })
}