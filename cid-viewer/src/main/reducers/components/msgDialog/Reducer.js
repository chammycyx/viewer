// @flow
import { combineReducers } from 'redux'
import type {SetMsgDialogType} from '@actions/components/msgDialog/Action'
import {MsgDialogActionTypes} from '@actions/components/msgDialog/Action'

const initState = {
	isVisible: false,
	code: null,
	title: '',
	content: null,
	additionalContent: null
}

function rootState(state: * = initState, action: SetMsgDialogType): * {
	switch (action.type) {
		case MsgDialogActionTypes.SET_MSG_DIALOG:
			return setMsgDialog(state, action)
		default:
			return state;
	}
}

function setMsgDialog(state: *, action: SetMsgDialogType): *{
	let newState = Object.assign({}, state)

	newState.isVisible = action.payload.isVisible
	newState.code = action.payload.code
	newState.title = action.payload.title
	newState.content = action.payload.content
	newState.additionalContent = action.payload.additionalContent

	return newState
}

export const msgDialog = combineReducers({
	rootState
});