// @flow
import { combineReducers } from 'redux'
import { CanvasGridActionTypes, RotateDirections, FlipDirections } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import type { SetImageType, SetGridType, RotateImageType, FlipImageType, SetSelectedImageType, SetColorAdjustmentType, ResetImageType, SetImageScaleType, SetImagePositionType, SetCompnentStateType, ResizeImgViewingPanelType} from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { SlideShowToolsActionTypes, type SetImageType as SetSlideShowImageType } from '@actions/components/toolBarPanel/slideShowTools/Action'
import type { ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import { initState as ViewModeDropDownListInitState } from '@reducers/components/toolBarPanel/viewModeTools/viewModeDropDownList/Reducer'
import { initState as pictorialIndexPanelInitState } from '@reducers/components/pictorialIndexPanel/Reducer'
import cidTheme from '@styles/CidTheme'
import { Set, Map } from 'immutable'
import { ToolBarPanelActionTypes, type SetToolType } from '@actions/components/toolBarPanel/Action'
import { ImageSelection } from '@constants/ComponentConstants'
import { ImageTool } from '@constants/ComponentConstants'
import { clone } from '@utils/CloneUtil'

export const initState = {
    width: 0,
    height: 0,
    canvasWidth: 0,
    canvasHeight: 0,
    viewMode: ViewModeDropDownListInitState.viewMode,
    selectedSeriesId: '',
    showImageIds: [],
	selectedImageIds: [],
    imageDlStatus: '',
    caseNo: '',
    canvasProfiles: {
        byId: {},
        allIds: []
    },
    headerTimeText: '',
    displayDateTime: [],
    imageTool: ''
}

export type StateType = {
    width: number,
    height: number,
    canvasWidth: number,
    canvasHeight: number,
    viewMode: ViewModeType,
    selectedSeriesId: string,
    showImageIds: Array<string>,
	selectedImageIds: Array<string>,
    imageDlStatus: string,
    caseNo: string,
    canvasProfiles: {
        byId: {},
        allIds: Array<string>
    },
    headerTimeText: string,
    displayDateTime: Array<string>,
    imageTool: string
}

export const CanvasProfileInitState = {
    header: '',
    imageBase64: '',
    brightness: 0,
    contrast: 0,
    hue: 0,
    saturation: 0,
    invert: false,
    imageXCoor: null,
    imageYCoor: null,
    imageWidth: null,
    imageHeight: null,
    imageScaleX: 1,
    imageScaleY: 1,
    imageOriginX: '',
    imageOriginY: '',
    rotateAngle: 0,
    flipHorizontal: false,
    flipVertical: false,
    fontType: '',
    fontSize: 0,
    fontColor: '',
    bgColor: '',
    headerVisibility: false
}

function rootState(state: StateType = initState, action: SetImageType | SetSelectedImageType | SetGridType | SetToolType | ResetImageType | SetImagePositionType | SetCompnentStateType | ResizeImgViewingPanelType): StateType {//works
	switch (action.type) {
        case CanvasGridActionTypes.SET_IMAGE:
            return handleSetImage(state, action)
        case CanvasGridActionTypes.SET_SELECTED_IMAGE:
            return handleSetSelectedImage(state, action)
        case CanvasGridActionTypes.SET_GRID:
            return handleSetGrid(state, action)
        case CanvasGridActionTypes.RESIZE_IMG_VIEWING_PANEL:
            return handleResizeImgViewingPanel(state, action)
        case CanvasGridActionTypes.SET_IMAGE_POSITION:
            return handleImagePosition(state, action)
        case CanvasGridActionTypes.ROTATE_IMAGE:
            return handleRotateImage(state, action)
        case CanvasGridActionTypes.FLIP_IMAGE:
            return handleFlipImage(state, action)
        case CanvasGridActionTypes.SET_COLOR_ADJUSTMENT:
            return handleColorAdjustment(state, action)
        case ToolBarPanelActionTypes.SET_TOOL:
            return handleImageTool(state, action)
        case CanvasGridActionTypes.SET_IMAGE_SCALE:
            return handleSetImageScale(state, action)
        case SlideShowToolsActionTypes.SET_IMAGE:
            return handleSetSlideShowImage(state, action)
		case CanvasGridActionTypes.RESET_IMAGES:
            return handleResetImages(state, action)
        case CanvasGridActionTypes.SET_COMPONENT_STATE:
            return handleSetComponentState(state, action)
        default:
            return state
	}
}

function handleSetImageScale(state: StateType, action: SetImageScaleType): StateType{
    const seriesId: string = action.payload.seriesId
    const imageId: string = action.payload.imageId
    const scaleX: number = action.payload.scaleX
    const scaleY: number = action.payload.scaleY
    const pannedXCoor: number = action.payload.pannedXCoor
    const pannedYCoor: number = action.payload.pannedYCoor
    
    let newState = Object.assign({}, state)
    newState.canvasProfiles.byId[imageId] = Object.assign({},newState.canvasProfiles.byId[imageId])

    const imgWidth = newState.canvasProfiles.byId[imageId].imageWidth
    const imgHeight = newState.canvasProfiles.byId[imageId].imageHeight
    const imgLeft = newState.canvasProfiles.byId[imageId].imageXCoor
    const imgTop = newState.canvasProfiles.byId[imageId].imageYCoor
    const currentimgScaleX = newState.canvasProfiles.byId[imageId].imageScaleX
    const currentimgScaleY = newState.canvasProfiles.byId[imageId].imageScaleY
    const nextImgScaleX = scaleX
    const nextImgScaleY = scaleY

    //set panned x&y coordinate of image
    newState.canvasProfiles.byId[imageId].imageXCoor = pannedXCoor
    newState.canvasProfiles.byId[imageId].imageYCoor = pannedYCoor

    //set scale factor of image
    newState.canvasProfiles.byId[imageId].imageScaleX = scaleX
    newState.canvasProfiles.byId[imageId].imageScaleY = scaleY

    return newState
}

function handleImageTool(state: StateType, action: SetToolType): StateType{
    let newState = Object.assign({}, state)
        newState.imageTool = action.payload.tool
    return newState
}

function handleColorAdjustment(state: StateType, action: SetColorAdjustmentType): StateType{
    let newState = Object.assign({}, state)

    for(let i=0; i<newState.selectedImageIds.length; i++) {
        const imageId: string = newState.selectedImageIds[i]
        newState.canvasProfiles.byId[imageId] = Object.assign({},newState.canvasProfiles.byId[imageId])
        newState.canvasProfiles.byId[imageId].brightness = action.payload.brightness
        newState.canvasProfiles.byId[imageId].contrast = action.payload.contrast
        newState.canvasProfiles.byId[imageId].hue = action.payload.hue
        newState.canvasProfiles.byId[imageId].saturation = action.payload.saturation
        newState.canvasProfiles.byId[imageId].invert = action.payload.invert
    }

    return newState
}

function handleFlipImage(state: StateType, action: FlipImageType): StateType{
    let newState = Object.assign({}, state)

    const imageId: string = action.payload.imageId
    const flipImageX: boolean = action.payload.flipImageX
    const flipImageY: boolean = action.payload.flipImageY

    newState.canvasProfiles.byId[imageId] = Object.assign({},newState.canvasProfiles.byId[imageId])
    let imageDtl = newState.canvasProfiles.byId[imageId]
        imageDtl.flipHorizontal = flipImageX
        imageDtl.flipVertical = flipImageY

    return newState
}

function handleRotateImage(state: StateType, action: RotateImageType): StateType{
    let newState = Object.assign({}, state)

    const rotateAngle: number = action.payload.rotateAngle
    const imageId: string = action.payload.imageId

    newState.canvasProfiles.byId[imageId] = Object.assign({},newState.canvasProfiles.byId[imageId])
    newState.canvasProfiles.byId[imageId].rotateAngle = rotateAngle

    return newState
}

function handleImagePosition(state: StateType, action: SetImagePositionType): StateType{
    const imageId: string = action.payload.imageId
    const imageXCoor: number = action.payload.xCoor
    const imageYCoor: number = action.payload.yCoor
    
    let newState = Object.assign({}, state)
        newState.canvasProfiles.byId[imageId] = Object.assign({},newState.canvasProfiles.byId[imageId])
        newState.canvasProfiles.byId[imageId].imageXCoor = imageXCoor
        newState.canvasProfiles.byId[imageId].imageYCoor = imageYCoor

    return newState
}

function handleResizeImgViewingPanel(state: StateType, action: ResizeImgViewingPanelType): StateType{
    let newState = Object.assign({}, state)

    //updated viewing area width height
    newState.width = action.payload.viewingAreaWidth
    newState.height = action.payload.viewingAreaHeight

    //updated canvas width height
    newState.canvasWidth = action.payload.canvasWidth
    newState.canvasHeight = action.payload.canvasHeight

    //update image x coordinate
    const imgsProperty = action.payload.imgsProperty
    for(let i=0;i<imgsProperty.length;i++){
        const imgProperty = imgsProperty[i]
        const imgId = imgProperty.imgId
        newState.canvasProfiles.byId[imgId] = Object.assign({},newState.canvasProfiles.byId[imgId])
        newState.canvasProfiles.byId[imgId].imageXCoor = imgProperty.imgXcoor
    }

    return newState
}

function handleSetGrid(state: StateType, action: SetGridType): StateType{
    let newState = Object.assign({}, state)

    //set view mode
    newState.viewMode = action.payload.viewMode

    //set canvas dimensions
    newState.canvasWidth = action.payload.canvasWidth
    newState.canvasHeight = action.payload.canvasHeight

    return newState
}

function handleSetImage(state: StateType, action: SetImageType): StateType{
    let newState = Object.assign({}, state)

    const caseNo = action.payload.caseNo
    const seriesIndexId = action.payload.seriesIndexId
	const imageIndexIds = action.payload.imageIndexIds
    const base64s: Array<string> = action.payload.base64s
    const imagesProperties: Array<*> = action.payload.imageProperties
    
    //set canvasGrid tree
    newState.viewMode = action.payload.viewMode
    newState.selectedSeriesId = seriesIndexId
    newState.showImageIds = imageIndexIds
    newState.caseNo = caseNo
    // newState.studyDateTime = studyDateTime
    newState.selectedImageIds = [imageIndexIds[0]]
    
	for(let i=0;i<imageIndexIds.length;i++){
        const imageIndexId: string = imageIndexIds[i]
        const imageProperties = imagesProperties[i]
        
        newState.canvasProfiles.allIds = Set(newState.canvasProfiles.allIds).union(Set([imageIndexId])).toArray()
        
        newState.canvasProfiles.byId[imageIndexId] = Map(CanvasProfileInitState).toObject()
        newState.canvasProfiles.byId[imageIndexId].imageBase64 = base64s[i]

        //set image alignment
        newState.canvasProfiles.byId[imageIndexId].imageXCoor = imageProperties.imageXCoor
        newState.canvasProfiles.byId[imageIndexId].imageYCoor = imageProperties.imageYCoor
        newState.canvasProfiles.byId[imageIndexId].imageOriginX = imageProperties.imageOriginX
        newState.canvasProfiles.byId[imageIndexId].imageOriginY = imageProperties.imageOriginY
        newState.canvasProfiles.byId[imageIndexId].imageWidth = imageProperties.imageWidth
        newState.canvasProfiles.byId[imageIndexId].imageHeight = imageProperties.imageHeight

        //set image scale for the first times auto fit
        newState.canvasProfiles.byId[imageIndexId].imageScaleX = imageProperties.imageScaleX
        newState.canvasProfiles.byId[imageIndexId].imageScaleY = imageProperties.imageScaleY

        //set display date time
        newState.displayDateTime[i] = imageProperties.displayDateTime
        
        //examType handling
        newState.canvasProfiles.byId[imageIndexId].headerVisibility = imageProperties.headerVisibility
    }
    
    newState.headerTimeText = (imagesProperties[0]).headerTimeText

	return newState
}

function handleSetSlideShowImage(state: StateType, action: SetSlideShowImageType): StateType{
    let newState = Object.assign({}, state)

    const imageIndexId: string = action.payload.imageIndexId
    const seqCanvas: number = action.payload.seqCanvas
    const base64: string = action.payload.base64
    const imageProperties: * = action.payload.imageProperties
    
    //set canvasGrid tree
    newState.showImageIds[seqCanvas] = imageIndexId
    newState.selectedImageIds = [imageIndexId]
    
    //set canvasProfile tree
    //set canvasProfile id for slide show
    newState.canvasProfiles.allIds = Set(newState.canvasProfiles.allIds).union(Set([imageIndexId])).toArray()
        
    newState.canvasProfiles.byId[imageIndexId] = Map(CanvasProfileInitState).toObject()
    newState.canvasProfiles.byId[imageIndexId].imageBase64 = base64

    //set image alignment
    newState.canvasProfiles.byId[imageIndexId].imageXCoor = imageProperties.imageXCoor
    newState.canvasProfiles.byId[imageIndexId].imageYCoor = imageProperties.imageYCoor
    newState.canvasProfiles.byId[imageIndexId].imageOriginX = imageProperties.imageOriginX
    newState.canvasProfiles.byId[imageIndexId].imageOriginY = imageProperties.imageOriginY
    newState.canvasProfiles.byId[imageIndexId].imageWidth = imageProperties.imageWidth
    newState.canvasProfiles.byId[imageIndexId].imageHeight = imageProperties.imageHeight

    //set image scale
    newState.canvasProfiles.byId[imageIndexId].imageScaleX = imageProperties.imageScaleX
    newState.canvasProfiles.byId[imageIndexId].imageScaleY = imageProperties.imageScaleY

    //set image adjustments
    newState.canvasProfiles.byId[imageIndexId].brightness = imageProperties.brightness
    newState.canvasProfiles.byId[imageIndexId].contrast = imageProperties.contrast
    newState.canvasProfiles.byId[imageIndexId].hue = imageProperties.hue
    newState.canvasProfiles.byId[imageIndexId].saturation = imageProperties.saturation
    newState.canvasProfiles.byId[imageIndexId].invert = imageProperties.invert
    newState.canvasProfiles.byId[imageIndexId].rotateAngle = imageProperties.rotateAngle
    newState.canvasProfiles.byId[imageIndexId].flipHorizontal = imageProperties.flipHorizontal
    newState.canvasProfiles.byId[imageIndexId].flipVertical = imageProperties.flipVertical
        
    //examType handling
    newState.canvasProfiles.byId[imageIndexId].headerVisibility=false

    return newState
}

function handleSetSelectedImage(state: StateType, action: SetSelectedImageType): StateType{
    let selectedImageIds: Array<string> = action.payload.selectedImageIds

    let newState = Object.assign({}, state)
        newState.selectedImageIds = selectedImageIds

    return newState
}

function handleResetImages(state: StateType, action: ResetImageType): StateType{
    const seriesIndexId = action.payload.selectedSeriesId
    const imageIndexIds = action.payload.selectedImageIds
    const imgsProperties = action.payload.imgsProperties

    let newState = Object.assign({}, state)

    for(let i=0;i<imageIndexIds.length;i++){
        let imageIndexId: string = imageIndexIds[i]
        let properties = imgsProperties[i]
        let imageBase64: * = newState.canvasProfiles.byId[imageIndexId].imageBase64

        newState.canvasProfiles.byId[imageIndexId] = properties
        newState.canvasProfiles.byId[imageIndexId].imageBase64 = imageBase64
    }
    return newState
}

function handleSetComponentState(state: StateType, action: SetCompnentStateType): StateType{
    const imageHeaderWidth: number = action.payload.imageHeaderWidth
    const imageHeaderHeight: number = action.payload.imageHeaderHeight

    let newState = Object.assign({}, state)
        newState.headerWidth = imageHeaderWidth
        newState.headerHeight = imageHeaderHeight

    return newState
}

export const canvasGrid = combineReducers({
	rootState
})