// @flow
import { combineReducers } from 'redux'
import { canvasGrid } from './canvasGrid/Reducer'

export const imageViewingPanel = combineReducers({
	canvasGrid
})