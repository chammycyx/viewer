// @flow
import { combineReducers } from 'redux'
import { imageViewingPanel } from './imageViewingPanel/Reducer'
import { pictorialIndexPanel } from './pictorialIndexPanel/Reducer'
import { toolBarPanel } from './toolBarPanel/Reducer'
import { contextMenu } from './contextMenu/Reducer'
import { toolTip } from './toolTip/Reducer'
import { msgDialog } from './msgDialog/Reducer'
import { appMask } from './root/appMask/Reducer'
import { type SetAppSizeType, ComponentsActionTypes } from '@actions/components/Action'

export const initState = {
	appWidth: window.innerWidth,
	appHeight: window.innerHeight
}

type StateType = {
	appWidth: number,
	appHeight: number
}

function rootState(state: StateType = initState, action: *): StateType {
    switch (action.type) {
        case ComponentsActionTypes.SET_APP_SIZE:
            return handleSetAppSize(state, action)
        default:
            return state
    }
}

function handleSetAppSize(state: StateType, action: SetAppSizeType): StateType {
    let newState = Object.assign({}, state)
	newState.appWidth = action.payload.width
	newState.appHeight = action.payload.height
    return newState
}

export const components = combineReducers({
	rootState,
	toolBarPanel,
	pictorialIndexPanel,
	imageViewingPanel,
	contextMenu,
	toolTip,
	msgDialog,
	appMask
})