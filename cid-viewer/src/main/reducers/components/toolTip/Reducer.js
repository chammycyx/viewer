// @flow
import { combineReducers } from 'redux'
import { ToolTipActionTypes } from '../../../actions/components/toolTip/Action'
import type { SetToolTipType } from '../../../actions/components/toolTip/Action'

const initState = {
	title: '',
	isOpen: false,
	x: 0,
	y: 0
}

function rootState(state: * = initState, action: SetToolTipType): * {
	let newState;

	switch (action.type) {
		case ToolTipActionTypes.SET_TOOL_TIP:
			newState = Object.assign({}, state);
			newState.title = action.payload.title;
			newState.isOpen = action.payload.isOpen;
			newState.x = action.payload.x;
			newState.y = action.payload.y;
			return newState;
		default:
			return state;
	}
}

export const toolTip = combineReducers({
	rootState
});