// @flow
import { combineReducers } from 'redux'
import { exportImageDialog } from './exportImageDialog/Reducer'

export const dialogs = combineReducers({
	exportImageDialog
})