// @flow
import { combineReducers } from 'redux'
import { ExportImageDialogActionTypes } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'
import type { SetVisibleType , SetDownloadType } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'
import { OCTET_STREAM_HEADERS } from '@utils/Base64Util'

export const initState = {
    isVisible: false,
    isRequesting: false,
    isDownload: false,
    title: "DISCLAIMER",
    agreementLabel: "I have read and agreed to the above terms and condition.",
    passwordQueryLabel: "Please input a security password to encrypt and decrypt the exported image(s)",
    passwordLabel: "Password",
    password2Label: "Confirm Password",
    requestingMsg: "Please wait while requesting for image download",
    exportImageBase64: ""
}

type StateType = {
    isVisible: boolean,
    isRequesting: boolean,
    isDownload: boolean,
    exportImageBase64: string
}

function rootState(state: StateType = initState, action: *): StateType {
    switch (action.type) {
        case ExportImageDialogActionTypes.SET_VISIBLE:
            return handleSetVisible(state, action)
        case ExportImageDialogActionTypes.REQUEST_EXPORT_IMAGES:
            return handleRequesting(state)
        case ExportImageDialogActionTypes.SET_DOWNLOAD:
            return handleDownload(state, action)
        default:
            return state
    }
}

function handleSetVisible(state: StateType, action: SetVisibleType): StateType {
    let newState = Object.assign({}, state)
    newState.isVisible = action.payload.isVisible
    return newState
}

function handleRequesting(state: StateType): StateType {
    let newState = Object.assign({}, state)
    newState.isVisible = false
    newState.isRequesting = true
    return newState
}

function handleDownload(state: StateType, action: SetDownloadType): StateType {
    let newState = Object.assign({}, state)
    newState.isRequesting = false
    newState.isDownload = action.payload.isDownload
    newState.exportImageBase64 = (action.payload.exportImageBase64)?OCTET_STREAM_HEADERS+action.payload.exportImageBase64:""
    return newState
}

export const exportImageDialog = combineReducers({
	rootState
})