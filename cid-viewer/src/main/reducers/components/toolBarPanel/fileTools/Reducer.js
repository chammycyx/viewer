// @flow
import { combineReducers } from 'redux'
import { dialogs } from './dialogs/Reducer'

export const fileTools = combineReducers({
	dialogs
})