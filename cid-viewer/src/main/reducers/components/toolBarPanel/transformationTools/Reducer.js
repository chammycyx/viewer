// @flow
import { combineReducers } from 'redux'
import { propsBox } from './propsBox/Reducer'

export const transformationTools = combineReducers({
	propsBox
})