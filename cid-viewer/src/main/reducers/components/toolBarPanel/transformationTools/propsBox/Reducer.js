// @flow
import { combineReducers } from 'redux'
import { colorAdjustmentPropsBox } from './colorAdjustmentPropsBox/Reducer'

export const propsBox = combineReducers({
	colorAdjustmentPropsBox
})