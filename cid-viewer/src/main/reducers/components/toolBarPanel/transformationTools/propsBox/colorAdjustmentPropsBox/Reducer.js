// @flow
import { combineReducers } from 'redux'
import { ColorAdjustmentPropsBoxActionTypes } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'
import type { SetVisibleType, SetColorAdjustmentConfigType } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'

export const initState = {
	visible: false,
	maxNumOfImage: 1
}

type StateType = {
	visible: boolean,
	maxNumOfImage: number
}

function rootState(state: StateType = initState, action: *): StateType {
	switch (action.type) {
		case ColorAdjustmentPropsBoxActionTypes.SET_VISIBLE:
			return handleSetVisible(state, action)
		case ColorAdjustmentPropsBoxActionTypes.SET_COLOR_ADJUSTMENT_CONFIG:
			return handleSetColorAdjustmentConfig(state, action)
		default:
			return state
	}
}

function handleSetVisible(state: StateType, action: SetVisibleType): StateType {
	let newState = Object.assign({}, state)
	newState.visible = action.payload.visible
	return newState
}

function handleSetColorAdjustmentConfig(state: StateType, action: SetColorAdjustmentConfigType): StateType {
	let newState = Object.assign({}, state)
	newState.maxNumOfImage = action.payload.maxNumOfImage
	return newState
}

export const colorAdjustmentPropsBox = combineReducers({
	rootState
})