// @flow
import { combineReducers } from 'redux'
import { VIEW_MODES, ViewModeDropDownListActionTypes } from '../../../../../actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import type { SetViewModeType, ViewModeType } from '../../../../../actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

export const initState = {
	viewMode: VIEW_MODES.ONE_X_ONE,
    isEnabled: true
}

type StateType = {
	viewMode: ViewModeType,
    isEnabled: boolean
}

function rootState(state: StateType = initState, action: SetViewModeType): StateType {
	let newState;

	switch (action.type) {
		case ViewModeDropDownListActionTypes.SET_VIEW_MODE:
			newState = Object.assign({}, state)
			newState.viewMode = action.payload.viewMode
			return newState
		default:
			return state
	}
}

export const viewModeDropDownList = combineReducers({
	rootState
})