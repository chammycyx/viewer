// @flow
import { combineReducers } from 'redux'
import { viewModeDropDownList } from './viewModeDropDownList/Reducer'

export const viewModeTools = combineReducers({
	viewModeDropDownList
})