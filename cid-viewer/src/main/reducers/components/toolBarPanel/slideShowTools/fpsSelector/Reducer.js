// @flow
import { combineReducers } from 'redux'
import { FpsSelectorActionTypes } from '@actions/components/toolBarPanel/slideShowTools/fpsSelector/Action'
import type { SetFpsType } from '@actions/components/toolBarPanel/slideShowTools/fpsSelector/Action'

export const initState = {
    fps: 1
}

type StateType = {
    fps: number
}

function rootState(state: StateType = initState, action: *): StateType {
    switch (action.type) {
        case FpsSelectorActionTypes.SET_FPS:
            return handleSetFps(state, action)
        default:
            return state
    }
}

function handleSetFps(state: StateType, action: SetFpsType): StateType {
    let newState = Object.assign({}, state)
    newState.fps = action.payload.fps
    return newState
}

export const fpsSelector = combineReducers({
    rootState
})