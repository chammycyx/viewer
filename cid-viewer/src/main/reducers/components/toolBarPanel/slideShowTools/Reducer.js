// @flow
import { combineReducers } from 'redux'

// import actions
import { SlideShowToolsActionTypes } from '@actions/components/toolBarPanel/slideShowTools/Action'
import type { SetModeType, SetPlayingType, SetTimerIdType, SetSeqCanvasType, SetRefOffsetsType } from '@actions/components/toolBarPanel/slideShowTools/Action'

// import reducers
import { fpsSelector } from './fpsSelector/Reducer'

export const initState = {
    isModeOn: false,
    isPlaying: false,
    seqCanvas: -1,
    refOffsets: {
        bySeq: {}
    },
    timerId: null
}

type StateType = {
    isModeOn: boolean,
    isPlaying: boolean,
    seqCanvas: number,
    refOffsets: {
        bySeq: {}
    },
    timerId: *
}

function rootState(state: StateType = initState, action: *): StateType {
    switch (action.type) {
        case SlideShowToolsActionTypes.SET_MODE:
            return handleSetMode(state, action)
        case SlideShowToolsActionTypes.SET_PLAYING:
            return handleSetPlaying(state, action)
        case SlideShowToolsActionTypes.SET_SEQ_CANVAS:
            return handleSetSeqCanvas(state, action)
        case SlideShowToolsActionTypes.SET_REF_OFFSETS:
            return handleSetRefOffsets(state, action)
        case SlideShowToolsActionTypes.SET_TIMER_ID:
            return handleSetTimerId(state, action)
        default:
            return state
    }
}

function handleSetMode(state: StateType, action: SetModeType): StateType {
    let newState = Object.assign({}, state)
    newState.isModeOn = action.payload.isModeOn
    return newState
}

function handleSetPlaying(state: StateType, action: SetPlayingType): StateType {
    let newState = Object.assign({}, state)
    newState.isPlaying = action.payload.isPlaying
    return newState
}

function handleSetSeqCanvas(state: StateType, action: SetSeqCanvasType): StateType {
    let newState = Object.assign({}, state)
    newState.seqCanvas = action.payload.seqCanvas
    return newState
}

function handleSetRefOffsets(state: StateType, action: SetRefOffsetsType): StateType {
    const seqCanvas: number = action.payload.seqCanvas
    const width: number = action.payload.width
    const height: number = action.payload.height

    let refOffset = {
        width,
        height
    }
    
    let newState = Object.assign({}, state)
    newState.refOffsets.bySeq[seqCanvas] = Object.assign({}, refOffset)

    return newState
}

function handleSetTimerId(state: StateType, action: SetTimerIdType): StateType {
    let newState = Object.assign({}, state)
    newState.timerId = action.payload.timerId
    return newState
}

export const slideShowTools = combineReducers({
    rootState,
    fpsSelector
})