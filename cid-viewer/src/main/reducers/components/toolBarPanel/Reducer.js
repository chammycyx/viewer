// @flow
import { combineReducers } from 'redux'
import { fileTools } from './fileTools/Reducer'
import { viewModeTools } from './viewModeTools/Reducer'
import { transformationTools } from './transformationTools/Reducer'
import { slideShowTools } from './slideShowTools/Reducer'
import { ToolBarPanelActionTypes } from '@actions/components/toolBarPanel/Action'
import type { SetBtnStateType } from '@actions/components/toolBarPanel/Action'
import { initBtnState } from '@domains/controllers/ButtonStateController'


export const initState = {
    btnState: 
		initBtnState
}

type StateType = {
    btnState: *
}

function rootState(state: StateType = initState, action: *): StateType {
    switch (action.type) {
        case ToolBarPanelActionTypes.SET_BUTTON_STATE:
            return handleBtnState(state, action)
        default:
            return state
    }
}

function handleBtnState(state: StateType, action: SetBtnStateType): StateType {
    let newState = Object.assign({}, state)
    newState.btnState = action.payload.btnState
    return newState
}

export const toolBarPanel = combineReducers({
	rootState,
	fileTools,	
	viewModeTools,
	transformationTools,
	slideShowTools
})