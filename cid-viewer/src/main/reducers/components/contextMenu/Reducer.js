// @flow
import { combineReducers } from 'redux'
import { aboutDialog } from './aboutDialog/Reducer'

export const contextMenu = combineReducers({
	aboutDialog
})