// @flow
import { combineReducers } from 'redux'
import { AboutDialogActionTypes } from '../../../../actions/components/contextMenu/aboutDialog/Action'
import type { SetIsOpenType } from '../../../../actions/components/contextMenu/aboutDialog/Action'

const initState = {
	isOpen: false
}

function rootState(state: * = initState, action: SetIsOpenType): * {
	let newState;

	switch (action.type) {
		case AboutDialogActionTypes.SET_IS_OPEN:
			newState = Object.assign({}, state);
			newState.isOpen = action.payload.isOpen;
			return newState;
		default:
			return state;
	}
}

export const aboutDialog = combineReducers({
	rootState
})