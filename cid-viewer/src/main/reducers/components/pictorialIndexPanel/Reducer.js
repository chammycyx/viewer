// @flow
import { combineReducers } from 'redux'
import { thumbnailGrid } from './thumbnailGrid/Reducer'
import { PictorialIndexPanelActionTypes } from '../../../actions/components/pictorialIndexPanel/Action'
import type { SetIsHiddenType } from '../../../actions/components/pictorialIndexPanel/Action'

export const initState = {
    isHidden: false
}

type StateType = {
	isHidden: boolean
}

function rootState(state: StateType = initState, action: SetIsHiddenType): * {
	let newState: StateType

	switch (action.type) {
        case PictorialIndexPanelActionTypes.SET_IS_HIDDEN:
            newState = Object.assign({}, state)
            newState.isHidden = action.payload.isHidden
            return newState;
		default:
			return state;
	}
}

export const pictorialIndexPanel = combineReducers({
	rootState,
	thumbnailGrid
})
