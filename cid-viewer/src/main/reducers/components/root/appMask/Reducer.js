// @flow
import { combineReducers } from 'redux'
import type {SetAppMaskType, SetProgressBarType} from '@actions/components/root/appMask/Action'
import {AppMaskActionTypes} from '@actions/components/root/appMask/Action'
import { LOADING } from '@constants/WordingConstants'

const initState = {
	isMasked: false,
	isProgressing: false,
	description: LOADING
}

function rootState(state: * = initState, action: SetAppMaskType): * {
	switch (action.type) {
		case AppMaskActionTypes.SET_APP_MASK:
			return setAppMask(state, action)
		case AppMaskActionTypes.SET_PROGRESS_BAR:
			return setProgressBar(state, action)
		default:
			return state;
	}
}

function setAppMask(state: *, action: SetAppMaskType): *{
	let newState = Object.assign({}, state)
		newState.isMasked = action.payload.isMasked
	return newState
}

function setProgressBar(state: *, action: SetProgressBarType): *{
	let newState = Object.assign({}, state)
		newState.isProgressing = action.payload.isProgressing
		newState.description = action.payload.description
	return newState
}

export const appMask = combineReducers({
	rootState
});