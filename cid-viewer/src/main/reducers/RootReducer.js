// @flow
import { combineReducers } from 'redux'

import { components } from './components/Reducer'
import { data } from './data/Reducer'

const RootReducer = combineReducers({
    components,
    data
})

export default RootReducer
