// @flow
import { combineReducers } from 'redux'
import { application } from './application/Reducer'

export const config = combineReducers({
    application
})