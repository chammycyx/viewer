// @flow
import { combineReducers } from 'redux'
import { ApplicationActionTypes } from '@actions/data/config/application/Action'
import type { SetAppConfigType } from '@actions/data/config/application/Action'
import StateTreeUtil from '@utils/StateTreeUtil'

const initState = {
    viewerControl: {
        idleTimeout: '',
        EPRTimeoutMethodName: '',
        applicationId: '',
        applicationVersion: ''
    },
    export: {
        imageExportFileName: '',
        imageExportDisclaimer: ''
    },
    imageHeader: {

    },
    webServiceIcw: {
        cidWsContextPath: '',
        suid1: '',
        suid2: ''
    },
    webServiceIas: {
        cidWsContextPath: '',
        suid1: '',
        suid2: ''
    },
    webService: {
		timeout: 15000
	}	
}

function rootState(state: * = initState, action: SetAppConfigType): * {
    let newState: *
    let appConfigStateStr: string
    
    switch (action.type) {
        case ApplicationActionTypes.SET_APP_CONFIG:
            newState = Object.assign({}, state)
            appConfigStateStr = JSON.stringify(action.payload)
            StateTreeUtil.mergeAll(newState, appConfigStateStr)
            return newState
        default:
            return state;
    }
}

export const application = combineReducers ({
    rootState
})