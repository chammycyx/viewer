// @flow
import { combineReducers } from 'redux'
import { CommonActionTypes } from '@actions/data/common/Action'
import type { CaseProfileType } from '@domains/readers/CaseProfileReader'

const initState = {
    hospCode: '',
    caseNo: '',
    accessionNo: '',
    seriesNo: '',
    imageSeqNo: '',
    versionNo: '',
    appHost: '',
    appSiteName: '',
    userId: '',
    workstationId: '',
    workstationHostname: '',
    requestSys: '',
    accessKey: '',
    userHospCode: '',
    patientKey: '',
    specialtyCode: '',
    sessionID: '',
    caseProfileTimeToken: '',
    repositionPara: {
        cmsWindowX: 0,
        cmsWindowY: 0,
        cmsWindowcenterX: 0,
        isMainDisplayPortrait: false
    }
}


function rootState(state: * = initState, action: {type: string, payload: CaseProfileType}): * {
    let newState

    switch (action.type) {
        case CommonActionTypes.SET_CASE_PROFILE:
            newState = Object.assign({}, state)
            newState.hospCode = action.payload.hospCde
            newState.caseNo = action.payload.caseNo
            newState.accessionNo = action.payload.accessionNo
            newState.seriesNo = action.payload.seriesSeqNo
            newState.imageSeqNo = action.payload.imageSeqNo
            newState.versionNo = action.payload.versionNo
            newState.appHost = action.payload.appHost
            newState.appSiteName = action.payload.appSiteName
            newState.userId = action.payload.loginID
            newState.workstationId = action.payload.wrkStnID
            newState.workstationHostname = action.payload.wrkStnHostname
            newState.requestSys = action.payload.systemID
            newState.accessKey = action.payload.accessKey
            newState.userHospCode = action.payload.userHospCde
            newState.patientKey = action.payload.patientKey
            newState.specialtyCode = action.payload.specialtyCde
            newState.sessionID = action.payload.sessionID
            newState.caseProfileTimeToken = action.payload.timeToken
            newState.repositionPara = action.payload.repositionPara
            return newState
        default:
            return state;
    }
}

export const common = combineReducers({rootState})