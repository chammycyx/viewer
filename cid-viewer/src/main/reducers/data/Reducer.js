// @flow
import { combineReducers } from 'redux'
import { common } from './common/Reducer'
import { config } from './config/Reducer'
import { study } from './study/Reducer'
import { msgCode } from './msgCode/Reducer'

export const data = combineReducers({
	common,
	config,
	study,
	msgCode
})
