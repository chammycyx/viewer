// @flow
import { combineReducers } from 'redux'
import { PatientDtlActionTypes } from '../../../../actions/data/study/patientDtl/Action'
import type { SetPatientDtlType } from '../../../../actions/data/study/patientDtl/Action'
import StateTreeUtil from '../../../../utils/StateTreeUtil'

const initState = {
    key: '',
    name: '',
    hkid: '',
    sex: '',
    dob: '',
    deathIndicator: ''
}

function rootState(state: * = initState, action: SetPatientDtlType): * {
    let newState: *
	let patientDtlStateStr: string

	switch (action.type) {
		case PatientDtlActionTypes.SET_PATIENT_DTL:
            newState = Object.assign({}, state)
			patientDtlStateStr = JSON.stringify(action.payload.patientDtlState)
			StateTreeUtil.mergeAll(newState, patientDtlStateStr)
			return newState
        default:
            return state
    }
}

export const patientDtl = combineReducers({
    rootState
})