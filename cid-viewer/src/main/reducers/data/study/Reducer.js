// @flow
import { combineReducers } from 'redux'
import { messageDtl } from './messageDtl/Reducer'
import { visitDtl } from './visitDtl/Reducer'
import { patientDtl } from './patientDtl/Reducer'
import { studyDtl } from './studyDtl/Reducer'

export const study = combineReducers({
	// rootState,
    messageDtl,
    visitDtl,
    patientDtl,
    studyDtl
})