// @flow
import { combineReducers } from 'redux'
import { MessageDtlActionTypes } from '../../../../actions/data/study/messageDtl/Action'
import type { SetMessageDtlType } from '../../../../actions/data/study/messageDtl/Action'
import StateTreeUtil from '../../../../utils/StateTreeUtil'

const initState = {
    transactionCode: '',
    serverHosp: '',
    transactionDtm: '',
    transactionID: '',
    sendingApplication: ''
}

function rootState(state: * = initState, action: SetMessageDtlType): * {
    let newState: *
	let messageDtlStateStr: string

	switch (action.type) {
		case MessageDtlActionTypes.SET_MESSAGE_DTL:
			newState = Object.assign({}, state)
			messageDtlStateStr = JSON.stringify(action.payload.messageDtlState)
			StateTreeUtil.mergeAll(newState, messageDtlStateStr)
			return newState
        default:
            return state
    }
}

export const messageDtl = combineReducers({
    rootState
})