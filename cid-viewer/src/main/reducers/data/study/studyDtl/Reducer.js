// @flow 
import { combineReducers } from 'redux'
import { StudyDtlActionTypes } from '../../../../actions/data/study/studyDtl/Action'
import type { SetStudyDtlType, SetImageType } from '../../../../actions/data/study/studyDtl/Action'
import StateTreeUtil from '../../../../utils/StateTreeUtil'
import { clone } from '../../../../utils/CloneUtil'
import JsonParser from '../../../../utils/JsonParser'

const initState = {
    accessionNo: '',
    studyID: '',
    studyType: '',
    studyDtm: '',
    remark: '',
    seriesDtls: {
        byId: {},
		allIds: []
	}
}

function rootState(state: * = initState, action: SetStudyDtlType | SetImageType): * {
	let newState: *
	let studyDtlStateStr: string

	switch (action.type) {
		case StudyDtlActionTypes.SET_STUDY_DTL:
			newState = clone(state)
			studyDtlStateStr = JSON.stringify(action.payload.studyDtlState)
			StateTreeUtil.mergeAll(newState, studyDtlStateStr)
			return newState
		case StudyDtlActionTypes.SET_IMAGE:
            return handleSetImage(state, action)
		default:
			return state;
	}
}

function handleSetImage(state: *, action: SetImageType): *{
	let newState = Object.assign({}, state)
	let imageIndexIds = action.payload.imageIndexIds
	let base64s = action.payload.base64s

	for(let i=0;i<imageIndexIds.length;i++){
		JsonParser.getValue(JsonParser.getValue(newState, action.payload.seriesIndexId), imageIndexIds[i]).imageBase64 = base64s[i]
	}
	return newState
}

export const studyDtl = combineReducers({
    rootState
})