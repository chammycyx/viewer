// @flow
import { combineReducers } from 'redux'
import { VisitDtlActionTypes } from '../../../../actions/data/study/visitDtl/Action'
import type { SetVisitDtlType } from '../../../../actions/data/study/visitDtl/Action'
import StateTreeUtil from '../../../../utils/StateTreeUtil'

const initState = {
    visitHosp: '',
    visitSpec: '',
    visitWardClinic: '',
    payCode: '',
    caseNum: '',
    adtDtm: ''
}

function rootState(state: * = initState, action: SetVisitDtlType): * {
    let newState: *
	let visitDtlStateStr: string

	switch (action.type) {
		case VisitDtlActionTypes.SET_VISIT_DTL:
            newState = Object.assign({}, state)
			visitDtlStateStr = JSON.stringify(action.payload.visitDtlState)
			StateTreeUtil.mergeAll(newState, visitDtlStateStr)
			return newState
        default:
            return state;
    }
}

export const visitDtl = combineReducers({
    rootState
})