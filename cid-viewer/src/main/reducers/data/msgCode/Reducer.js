// @flow
import { combineReducers } from 'redux'
import { MsgCodeActionTypes } from '@actions/data/msgCode/Action'
import type { SetMsgCodeType } from '@actions/data/msgCode/Action'

const initState = {
    errors:{}
}


function rootState(state: * = initState, action: SetMsgCodeType): * {
    switch (action.type) {
        case MsgCodeActionTypes.SET_MSG_CODE:
            return setMsgDialog(state, action)
        default:
            return state;
    }
}

function setMsgDialog(state: *, action: SetMsgCodeType): *{
    let newState = Object.assign({}, state)

        newState.errors = Object.assign({}, newState.errors, action.payload.errors)

    return newState
}

export const msgCode = combineReducers({rootState})