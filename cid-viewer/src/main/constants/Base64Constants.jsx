export const canvasesDemoState = {
    currentImageId: [],
    viewMode: '1x1',
    width: '100%',
    height: '100%',
    canvasWidth: 50,
    canvasHeight: 50,
    canvasProfiles: {
        byId: {
            'imageDtl_2': {
                header: '',
                fabricProfile: {},
                brightness: 50,
                contrast: 50,
                hue: 50,
                saturation: 50,
                zoomValue: 100,
            },

            'imageDtl_4': {
                header: '',
                fabricProfile: {},
                brightness: 50,
                contrast: 50,
                hue: 50,
                saturation: 50,
                zoomValue: 100
            }
        },
        allIds: ['imageDtl_2', 'imageDtl_4']
    }
}