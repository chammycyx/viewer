//@flow

export const TAG_IMAGE_DETAIL: string = 'imageDtl_'
export const TAG_ANNOTATION_DETAIL: string = 'annotationDtl_'
export const TAG_SERIES_DETAIL: string = 'seriesDtl_'