//@flow

//LoadingBarDescription
export const LOADING: string = 'Loading...'
export const APP_INIT: string = 'Initializing'
export const RETRIEVE_IMAGE: string = 'Please wait while image loading into viewing area.'
export const DOWNLOAD_EXPORTED_IMAGE: string = 'Please wait while requesting for image download.'
export const RESIZE_APP: string = 'Please wait while resizing.'

// Hard-code Dialog Message
type HardCodeMsgType = {title: string, content: string}

export const MISSING_INPUT_PARAM: HardCodeMsgType = {
    title: 'Error: Missing input parameter',
    content: 'Not enough information to process.'
}

export const CID_CONFIG_FILE_UNAVAILABLE: HardCodeMsgType = {
    title: 'CID Service Error',
    content: 'Failed to retrieve configuration files.\nPlease retry. Please contact call center for help if the service is not resumed.'
}

export const CID_GET_JSON_FILE_TIMEOUT: HardCodeMsgType = {
    title: 'CID Service Error',
    content: 'Failed to invoke JSON File by REST-ful service.\nPlease retry. Please contact call center for help if the service is not resumed.'
}

export const ACCESS_AUTHEN_FAILED: HardCodeMsgType = {
    title: "Error: Access Authentication",
    content: "You do not have access right to activate the CID ImageViewer."
}

export const UNABLE_TO_READ_CASE_PROFILE: HardCodeMsgType = {
    title: "Error: Invalid Case Profile",
    content: "Unable to read case profile."
}

// Hard-code additional exception Message
export const CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG: string = 'Missing configuration'