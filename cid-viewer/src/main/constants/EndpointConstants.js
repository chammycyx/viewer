export const ENDPOINT = {
    IAS: {
        RETRIEVE_METADATA: '/rs/image-retrieval/getCidStudy',
        RETRIEVE_IMAGE: '/rs/image-retrieval/getCidImage',
        RETRIEVE_IMAGE_THUMBNAIL: '/rs/image-retrieval/getCidImageThumbnail',
        EXPORT_IMAGE: '/rs/image-retrieval/exportImage'
    },

    ICW: {
        RETRIEVE_APP_CONFIG: '/rs/security/security-manager/getApplicationConfig',
        VALIDATE_ACCESS_KEY_BY_HOSTNAME: '/rs/security/security-manager/validateAccessKeyByHostName',
        VALIDATE_APPLICATION_STATUS: '/rs/security/security-manager/validateApplicationStatus',
        WRITE_LOG: '/rs/audit/audit-manager/writeLog',
        WRITE_CLIENT_LOG: '/rs/log/log-manager/writeLog',
        WRITE_CLIENT_DEBUG_LOG: '/rs/log/log-manager/writeDebugLog',
    },

    LOCAL: {
        RETRIEVE_APP_CONFIG: '/config/appConfig.json',
        RETRIEVE_ERR_MSG: '/config/errMsg.json',
        RETRIEVE_LOG_CONFIG: '/config/logConfig.json'
    }
} 