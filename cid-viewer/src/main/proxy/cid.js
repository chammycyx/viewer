/**
 * Change Request History:
 * - CID-593: Initial windows size of image viewer is not fit-to-screen in Windows 7 [Alex LEE 2013-11-07]
 * 
 */
var viewerWindow;
function openCIDImageViewer(systemID, accessKey, loginID, userHospCde, wrkStnID, hospCde, patientKey, specialtyCde, caseNo, accessionNo, seriesSeqNo, imageSeqNo, versionNo, appHost, appSiteName, appName){
 	var str;
	
 	str = "?"
 	str += "systemID=" + escape(systemID);
 	str += "&accessKey=" + escape(accessKey);
 	str += "&loginID=" + escape(loginID);
 	str += "&userHospCde=" + escape(userHospCde);
 	str += "&wrkStnID=" + escape(wrkStnID);
 	str += "&hospCde=" + escape(hospCde);
 	str += "&patientKey=" + escape(patientKey);
 	str += "&specialtyCde=" + escape(specialtyCde);
 	str += "&caseNo=" + escape(caseNo);
 	str += "&accessionNo=" + escape(accessionNo);
 	str += "&seriesSeqNo=" + escape(seriesSeqNo);
 	str += "&imageSeqNo=" + escape(imageSeqNo);	
 	str += "&versionNo=" + escape(versionNo);
	str += "&appHost=" + escape(appHost);
	str += "&appSiteName=" + escape(appSiteName);
	str += "&appName=" + escape(appName);
 	
 	if(viewerWindow != null)
 	{
	 	closeCIDImageViewer();
 	} 	
 	
	// modify by leong
 	var queryString = ('CIDImageViewer.asp' + str).replace(/^[^\?]+\??/,'');

  	var params = parseQuery( queryString ); 	 	
	viewerWindow = window.open('', 'CIDImageViewer', 'status=yes,resizable=yes,location=no'); 		
	post_to_url("CIDImageViewer.asp", params, "post", "CIDImageViewer");

	//viewerWindow = window.open('CIDImageViewer.asp' + str, 'CIDImageViewer', config='height=' + winHeight + ',width=' + winWidth + ',status=yes,resizable=yes,location=no'); 		
	viewerWindow.moveTo(0,0);
	viewerWindow.focus();
	// end modify by leong
	
	// if (document.all) {
		viewerWindow.top.window.resizeTo(screen.availWidth, screen.availHeight);
	// }
	// else if (document.layers || document.getElementById) {
	// 	if (viewerWindow.top.window.outerHeight < screen.availHeight || viewerWindow.top.window.outerWidth < screen.availWidth) {
	// 		viewerWindow.top.window.outerHeight = screen.availHeight;
	// 		viewerWindow.top.window.outerWidth = screen.availWidth;
	// 	}
	// }
}

// modify by leong
function parseQuery ( query ) {
   var Params = new Object ();
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

function post_to_url(path, params, method, target) {
    method = method || "post"; // Set method to post by default, if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    form.target= target;

    for(var key in params) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);

        form.appendChild(hiddenField);
    }

    document.body.appendChild(form);    
    form.submit();
}
// end modify by leong

function closeCIDImageViewer(){
	try
	{
		viewerWindow.close();
	}
	catch(err)
	{
 		var winHeight = (window.screen.height - 25);
 		var winWidth = (window.screen.width - 10);	
		viewerWindow = window.open('', 'CIDImageViewer', config='height=' + winHeight + ',width=' + winWidth + ',status=yes,resizable=yes');		
		viewerWindow.close();
	}
}

function viewerOnTimeout(){
	// Coding that handle the Timeout receive from CID ImageViewer
}




/////////////////////////////////Original cid.js Ends//////////////////////////////////




var cidProxy = {
    browserVersion: null,
    openCIDImageViewerCache: null,
	openCIDImageViewerFlexApi: null,
    detectIE: function() {
        var documentMode = document.documentMode
        if(documentMode!=null && !isNaN(documentMode)){
            return documentMode
        }
        var ieVersion = null
        if(document.all && !document.querySelector){
            //=<7
            ieVersion = 7
        }
        if(document.all && !window.XMLHttpRequest){
            //=<6
            ieVersion = 6
        }
        if(document.all && !document.compatMode){
            //=<5
            ieVersion = 5
        }
        if(ieVersion!==null && ieVersion!=undefined){
            return ieVersion
        }
        // other browser
        return false;
    },
    getScriptSrc: function(scriptId, scriptFileName){
		var scriptElement = null

		if(scriptId!=null){
			scriptElement = document.getElementById(scriptId)
		}else if(scriptFileName!=null){
            var scripts = document.getElementsByTagName("script")
            var i
        	for(i=0;i<scripts.length;i++){
				var item = scripts[i]
				if(this.getJsFileName(item.src)==scriptFileName){
					scriptElement = item
					break
                }
			}
		}

		return scriptElement==null?null:scriptElement.src
	},
    getJsFileName: function(src){
        var splttedItems = src.split("/")
        var jsFile = splttedItems[(splttedItems.length-1)]
        return jsFile
    },
    loadScript: function(src, onLoad, onError, onFinal){
        var script = document.createElement("script");
            script.src = src;
            script.onload = function() {
                if(onLoad){onLoad()}
                if(onFinal){onFinal()}
            }
			script.onerror = function() {
                if(onError){onError()}
                if(onFinal){onFinal()}
            }
        document.getElementsByTagName('head')[0].appendChild(script);
    }
}

try{
    cidProxy.browserVersion = cidProxy.detectIE()
    if(cidProxy.browserVersion>=11 || !cidProxy.browserVersion){
        cidProxy.openCIDImageViewerFlexApi = window.openCIDImageViewer
        window.openCIDImageViewer = function(systemID, accessKey, loginID, userHospCde, wrkStnID, hospCde, patientKey, specialtyCde, caseNo, accessionNo, seriesSeqNo, imageSeqNo, versionNo){
            cidProxy.openCIDImageViewerCache = arguments
        }

        // var scriptSrc = cidProxy.getScriptSrc(null, "cid.js")//used for cms-sPR testing
        var scriptSrc = cidProxy.getScriptSrc("cidImageViewerJavascriptScriptTagId")
        var viewerDns = scriptSrc.substring(0,scriptSrc.lastIndexOf("/"))
        cidProxy.loadScript(viewerDns+"/cidviewerproxy.js", 
            function(){/*on load*/}, 
            function(){
                //on error
                window.openCIDImageViewer = function() {
					alert('<CID Service Error>\nError Code: 3202\nError message: Failed to open Image Viewer. Configuration file download failure.\nPlease search again. Please contact call center for help if the service is not resumed.')
				}
            }, 
            function(){
                //on final
                if(cidProxy.openCIDImageViewerCache!=null){
                    window.openCIDImageViewer.apply(null, cidProxy.openCIDImageViewerCache)
                    cidProxy.openCIDImageViewerCache = null
                }
            }
        )
    }
}catch(exception){
    if(cidProxy.openCIDImageViewerFlexApi){
        window.openCIDImageViewer = cidProxy.openCIDImageViewerFlexApi
    }
}