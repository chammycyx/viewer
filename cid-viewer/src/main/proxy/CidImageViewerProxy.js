// @flow
//Import "babel-polyfill" for async&await works in IE11.
//To reduce the bundle, use either below libs instead
//https://www.npmjs.com/package/regenerator
//https://www.npmjs.com/package/regenerator-runtime
//https://babeljs.io/docs/en/babel-plugin-transform-regenerator
import "babel-polyfill"
import { getJsonFile } from '@utils/WebServiceBackbone'
// import {HANDSHAKE_TOKEN} from '@constants/TokenConstants'
import {WINDOW_NAME} from '@constants/ProxyConstants'
import { ENDPOINT } from '@constants/EndpointConstants'
import uuidv4 from 'uuid/v4'
import { LOGGER_NAME, LOG_MODE } from '@constants/LogConstants'
import CidMagicUtil from '@utils/CidMagicUtil'
import {CPKC} from '@constants/CPKC'
import {EncryptionConstants} from '@constants/EncyptionConstants'
import {CommonLogController} from '@domains/controllers/rootApp/CommonLogController'
import WebSInfo from '@domains/model/vo/WebSInfo'
import logger from '@utils/Logger'

// const windowName: string = 'CIDImageViewer'
const windowName: string = WINDOW_NAME
// const configJsonUrl: string = './config/proxyConfig.json'
const configJsonUrl: string = '/config/proxyConfig.json'
// const loadingPageUrl: string = './cidImageViewerLoading.html'
const aspViewerUrl: string = 'CIDImageViewer.asp'
const PROXY_SCRIPT_TAG_ID: string = "cidImageViewerJavascriptScriptTagId"
let commonLogController = new CommonLogController()

export default class CidImageViewerProxy{
	viewerWindow: any
	openCIDImageViewer: Function
	closeCIDImageViewer: Function
	openAspImageViewer: Function
	openHtml5ImageViewer: Function
	//$FlowFixMe
	proxyConfigJson: json
	icwInfo: *
	logConfig: *
	viewerConfig: *
	isProxyConfReady: boolean

	constructor(){
		this.isProxyConfReady = false
		this.viewerWindow = null
		this.openCIDImageViewer = this.openCIDImageViewer.bind(this)
		this.closeCIDImageViewer = this.closeCIDImageViewer.bind(this)
		this.openAspImageViewer = this.openAspImageViewer.bind(this)
		this.openHtml5ImageViewer = this.openHtml5ImageViewer.bind(this)
		this.getConfigFiles().then(() => {this.initProxy()})
	}
	
	async getConfigFiles(): Promise<void>{
		try {
			// const scriptSrc: string = this.getScriptSrc(null, "cid.js")//used for cms-sPR testing
			//$FlowFixMe
			const scriptSrc: string = this.getScriptSrc(PROXY_SCRIPT_TAG_ID)

			const configDnsWithJsDir: string = scriptSrc.substring(0,scriptSrc.lastIndexOf("/"))
			const configDns: string = configDnsWithJsDir.substring(0,configDnsWithJsDir.lastIndexOf("/"))

			// const configDns: string = scriptSrc.substring(0,scriptSrc.lastIndexOf("/"))
			//$FlowFixMe
			this.proxyConfigJson = await getJsonFile(configDns + configJsonUrl,15000)
			const appHost: string = this.proxyConfigJson.profile.appHost
			const appSiteName: string = this.proxyConfigJson.profile.appSiteName
			const appConfigJson: json = await getJsonFile(appHost + appSiteName + ENDPOINT.LOCAL.RETRIEVE_APP_CONFIG,0)

			this.icwInfo = new WebSInfo(CidMagicUtil.unmagicByECB(appConfigJson.webServiceIcw.suid1, EncryptionConstants.ICW_KEY), 
						CidMagicUtil.unmagicByECB(appConfigJson.webServiceIcw.suid2, EncryptionConstants.ICW_KEY), 
						appConfigJson.webServiceIcw.cidWsContextPath)

			this.viewerConfig = appConfigJson.viewerControl
			
			// Get logConfig.json
			let logConfigPath: string = appHost + appSiteName + ENDPOINT.LOCAL.RETRIEVE_LOG_CONFIG
			this.logConfig = await getJsonFile(logConfigPath,0)
		} catch(error) {
			window.console.log(error)
		}
	}

	initProxy() {
		commonLogController.setConfig(this.logConfig, this.icwInfo, this.viewerConfig, LOG_MODE.FIRST_FULL_LOG_ONLY)
		logger.setCommonLogController(commonLogController)
		this.isProxyConfReady=true
	}

	/**
	 * Open CID Image Viewer
	 * 
	 * @param {string} systemID 
	 * @param {string} accessKey 
	 * @param {string} loginID 
	 * @param {string} userHospCde 
	 * @param {string} wrkStnID 
	 * @param {string} hospCde 
	 * @param {string} patientKey 
	 * @param {string} specialtyCde 
	 * @param {string} caseNo 
	 * @param {string} accessionNo 
	 * @param {string} seriesSeqNo 
	 * @param {string} imageSeqNo 
	 * @param {string} versionNo 
	 * @returns {Promise<void>} 
	 * @memberof CidImageViewerProxy
	 */
	async openCIDImageViewer(systemID: string, accessKey: string, loginID: string, userHospCde: string, wrkStnID: string, hospCde: string, patientKey: string, specialtyCde: string, caseNo: string, accessionNo: string, seriesSeqNo: string, imageSeqNo: string, versionNo: string): Promise<void>{
		commonLogController.resetFirstLogFlag()
		if(!this.isProxyConfReady) await this.waitUntilConfigReady()
		if(!this.proxyConfigJson){
			alert('<CID Service Error>\nError Code: 3203\nError message: Failed to open Image Viewer. Configuration file download failure.\nPlease search again. Please contact call center for help if the service is not resumed.')
			return
		}
		this.closeCIDImageViewer(true)
		
		commonLogController.disableLogging(systemID, loginID, wrkStnID, userHospCde)
		logger.apiPerf(LOGGER_NAME.CID_API_PERF_OPENCIDIMAGEVIEWER_START, 
			"openCIDImageViewer...[systemID="+systemID+"|accessKey="+accessKey+"|loginID="+loginID+
			"|userHospCde="+userHospCde+"|wrkStnID="+wrkStnID+"|hospCde="+hospCde+
			"|patientKey="+patientKey+"|specialtyCde="+specialtyCde+"|caseNo="+caseNo+
			"|accessionNo="+accessionNo+"|seriesSeqNo="+seriesSeqNo+"|imageSeqNo="+imageSeqNo+"|versionNo="+versionNo+"]")
		commonLogController.flushLogs()
		const moveToX: number = 0
		const moveToY: number = 0
		let availWidth = this.getAvailWidth()
		let availHeight = this.getAvailHeight()

		try{
			const dataHospCode: string = userHospCde.trim()
			const hospArr: Array<string> = this.proxyConfigJson.html5Viewer
			const isOpenHtml5Viewer: boolean = (hospArr.indexOf(dataHospCode)>-1
												|| hospArr.indexOf('ALL')>-1)
			if(isOpenHtml5Viewer){
				this.viewerWindow = this.openHtml5ImageViewer(this.proxyConfigJson, availWidth, availHeight, systemID, accessKey, loginID, userHospCde, wrkStnID, hospCde, patientKey, specialtyCde, caseNo, accessionNo, seriesSeqNo, imageSeqNo, versionNo)
			}else{
				this.viewerWindow = this.openAspImageViewer(moveToX, moveToY, availWidth, availHeight, systemID, accessKey, loginID, userHospCde, wrkStnID, hospCde, patientKey, specialtyCde, caseNo, accessionNo, seriesSeqNo, imageSeqNo, versionNo)
			}
			logger.apiPerf(LOGGER_NAME.CID_API_PERF_OPENCIDIMAGEVIEWER_END, 
				"openCIDImageViewer...[isOpenHtml5Viewer="+isOpenHtml5Viewer.toString()+"]")
		}catch(error){
			this.viewerWindow = this.openAspImageViewer(moveToX, moveToY, availWidth, availHeight, systemID, accessKey, loginID, userHospCde, wrkStnID, hospCde, patientKey, specialtyCde, caseNo, accessionNo, seriesSeqNo, imageSeqNo, versionNo)
		}

		window.viewerWindow = this.viewerWindow
	}

	async waitUntilConfigReady(): Promise<void> {
		return new Promise((resolve: Function, reject: Function) => {
			const timer = setInterval(
					() => {
					if (this.isProxyConfReady) {
						clearInterval(timer)
						resolve()
					}
				},
			250)
		})
	}
	/**
	 * Open HTML5 version Image Viewer
	 * 
	 * @param {*} appConfig 
	 * @param {number} windowXcor 
	 * @param {number} windowYcor 
	 * @param {number} windowWidth 
	 * @param {number} windowHeight 
	 * @param {string} systemID 
	 * @param {string} accessKey 
	 * @param {string} loginID 
	 * @param {string} userHospCde 
	 * @param {string} wrkStnID 
	 * @param {string} hospCde 
	 * @param {string} patientKey 
	 * @param {string} specialtyCde 
	 * @param {string} caseNo 
	 * @param {string} accessionNo 
	 * @param {string} seriesSeqNo 
	 * @param {string} imageSeqNo 
	 * @param {string} versionNo 
	 * @returns {*} 
	 * @memberof CidImageViewerProxy
	 */
	openHtml5ImageViewer(appConfig: *, windowWidth: number, windowHeight: number,
		systemID: string, accessKey: string, loginID: string, userHospCde: string, wrkStnID: string, hospCde: string, 
		patientKey: string, specialtyCde: string, caseNo: string, accessionNo: string, seriesSeqNo: string, imageSeqNo: string, versionNo: string): *{
		const confAppHost: string = appConfig.profile.appHost
		const confAppSiteName: string = appConfig.profile.appSiteName
		const confAppFileName: string = appConfig.profile.appFileName
		const confAppName: string = appConfig.profile.appName
		const sessionID: string = uuidv4()

		commonLogController.setCaseProfile(systemID, accessKey, loginID, userHospCde, wrkStnID, hospCde, 
			patientKey, specialtyCde, caseNo, accessionNo, seriesSeqNo, imageSeqNo, versionNo,
			confAppHost, confAppSiteName, confAppName, sessionID)

		logger.userActions(LOGGER_NAME.CID_USERACTIONS_OPENCIDVIEWER,
				"openCIDImageViewer...[systemID="+systemID+"|accessKey="+accessKey+"|loginID="+loginID+
				"|userHospCde="+userHospCde+"|wrkStnID="+wrkStnID+"|hospCde="+hospCde+
				"|patientKey="+patientKey+"|specialtyCde="+specialtyCde+"|caseNo="+caseNo+
				"|accessionNo="+accessionNo+"|seriesSeqNo="+seriesSeqNo+"|imageSeqNo="+imageSeqNo+"|versionNo="+versionNo+"]")

		logger.userActionsPerf(LOGGER_NAME.CID_USERACTIONS_PERF_OPENCIDVIEWER_START, "Open CID Image Viewer....")

		const cmsWindowWidth = window.outerWidth //For ePR, window.innerWidth=0
		const cmsWindowX = window.screenLeft
		const cmsWindowY = window.screenY
        const cmsWindowcenterX = cmsWindowX + (cmsWindowWidth/2)
		
		const isMainDisplayPortrait = screen.width<screen.height

		const msgToSend = {
			caseProfile: {
				hospCde: hospCde,
				caseNo: caseNo,
				accessionNo: accessionNo,
				seriesSeqNo: seriesSeqNo,
				imageSeqNo : imageSeqNo,
				versionNo: versionNo,
				loginID: loginID,
				wrkStnID: wrkStnID,
				systemID: systemID,
				userHospCde: userHospCde,
				patientKey: patientKey,
				specialtyCde: specialtyCde,
				appHost: confAppHost,
				appSiteName: confAppSiteName,
				appName: confAppName,
				wrkStnHostname: window.location.hostname, 
				sessionID: sessionID,
				timeToken: new Date().getTime(),
				repositionPara:{
					cmsWindowX: cmsWindowX,
					cmsWindowY: cmsWindowY,
					cmsWindowcenterX: cmsWindowcenterX,
					isMainDisplayPortrait: isMainDisplayPortrait
				}
			}
		}

		const hash = '#' + encodeURIComponent(CidMagicUtil.magicByECB(JSON.stringify(msgToSend), CPKC.CPEK))
		const url = confAppHost + confAppSiteName + confAppFileName + hash
		const winName = accessKey
		windowHeight -= 65
		windowWidth -= 20
		let viewerWindow = window.open('', winName, 'status=yes,resizable=yes,location=no,height='+windowHeight+',width='+windowWidth)
		viewerWindow.location.href = url
		viewerWindow.focus()
		return viewerWindow
	}

	/**
	 * Open ASP version Image Viewer
	 * 
	 * @param {number} windowXcor 
	 * @param {number} windowYcor 
	 * @param {number} windowWidth 
	 * @param {number} windowHeight 
	 * @param {string} systemID 
	 * @param {string} accessKey 
	 * @param {string} loginID 
	 * @param {string} userHospCde 
	 * @param {string} wrkStnID 
	 * @param {string} hospCde 
	 * @param {string} patientKey 
	 * @param {string} specialtyCde 
	 * @param {string} caseNo 
	 * @param {string} accessionNo 
	 * @param {string} seriesSeqNo 
	 * @param {string} imageSeqNo 
	 * @param {string} versionNo 
	 * @returns {*} 
	 * @memberof CidImageViewerProxy
	 */
	openAspImageViewer(windowXcor: number, windowYcor: number, windowWidth: number, windowHeight: number, 
		systemID: string, accessKey: string, loginID: string, userHospCde: string, wrkStnID: string, 
		hospCde: string, patientKey: string, specialtyCde: string, caseNo: string, accessionNo: string, 
		seriesSeqNo: string, imageSeqNo: string, versionNo: string): *{
		let str: string = ''
			str += "systemID=" + escape(systemID)
			str += "&accessKey=" + escape(accessKey)
			str += "&loginID=" + escape(loginID)
			str += "&userHospCde=" + escape(userHospCde)
			str += "&wrkStnID=" + escape(wrkStnID)
			str += "&hospCde=" + escape(hospCde)
			str += "&patientKey=" + escape(patientKey)
			str += "&specialtyCde=" + escape(specialtyCde)
			str += "&caseNo=" + escape(caseNo)
			str += "&accessionNo=" + escape(accessionNo)
			str += "&seriesSeqNo=" + escape(seriesSeqNo)
			str += "&imageSeqNo=" + escape(imageSeqNo)
			str += "&versionNo=" + escape(versionNo)
		let viewerWindow = window.open('', windowName, 'status=yes,resizable=yes,location=no')
			viewerWindow.moveTo(windowXcor,windowYcor);
			viewerWindow.focus();
			viewerWindow.resizeTo(windowWidth, windowHeight)
		this.actionToUrl(aspViewerUrl, this.parseQuery( str ), 'post', windowName);
		return viewerWindow
	}

	getAvailWidth(): number{
		return screen.availWidth
	}

	getAvailHeight(): number{
		return screen.availHeight
	}

	parseQuery(query: string): {}{
		let params: {} = new Object ();
		if ( ! query ) return params; // return empty object
		let Pairs: Array<string> = query.split(/[;&]/);
		for (let i = 0; i < Pairs.length; i++) {
			let KeyVal: Array<string> = Pairs[i].split('=');
			if (!KeyVal || KeyVal.length != 2) 
							continue;
			let key: string = unescape(KeyVal[0]);
			let val: string = unescape(KeyVal[1]);
				val = val.replace(/\+/g, ' ');
			params[key] = val;
		}
		return params;
	}

	/**
	 * Submit Form request to target.
	 * The key and value could not contain character + at this moment.
	 * When doing Form.submit, characters would be transform as below
	 * space To +
	 * + To +
	 * 
	 * The best handling is to use below API to encode the key and value before Form.submit.
	 * And use specific decoding API when retrieving the URI
	 * encodeURI/encodeURIComponent(key)
	 * encodeURI/encodeURIComponent(value)
	 * decodeURI/decodeURIComponent(key)
	 * decodeURI/decodeURIComponent(value)
	 * 
	 * @param {string} path 
	 * @param {{}} params 
	 * @param {string} method 
	 * @param {string} target 
	 * @memberof CidImageViewerProxy
	 */
	actionToUrl(path: string, params: {}, method: string, target: string) {
		method = method || "post"; // Set method to post by default, if not specified.
	
		// The rest of this code assumes you are not using a library.
		// It can be made less wordy if you use one.
		let form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", path);
			form.target= target
			
		for(let key: string in params) {
			let hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);
	
			form.appendChild(hiddenField);
		}
	
		if(document.body){
			//add null checking to pas flow type checking
			document.body.appendChild(form);    
		}
		form.submit();
	}

	getScreenHeight(): number{
		return window.screen.height
	}

	getScreenWidth(): number{
		return window.screen.width
	}

	closeCIDImageViewer(isInvokeByFunc: boolean = false){
		try
		{
			this.viewerWindow.close();
			if(!isInvokeByFunc) {
				logger.userActions(LOGGER_NAME.CID_USERACTIONS_CLOSECIDVIEWER, "closeCIDImageViewer...Close CID Image Viewer...")
				commonLogController.flushLogs();
			}
		}
		catch(err)
		{
			let winHeight: number = (this.getScreenHeight() - 25)
			let winWidth: number = (this.getScreenWidth() - 10)
			this.viewerWindow = window.open('', windowName, 'height=' + winHeight + ',width=' + winWidth + ',status=yes,resizable=yes')
			this.viewerWindow.close()
			if(!isInvokeByFunc) {
				logger.userActions(LOGGER_NAME.CID_USERACTIONS_CLOSECIDVIEWER, "closeCIDImageViewer...Close CID Image Viewer...")
				commonLogController.flushLogs();
			}
		}
	}
	
	viewerOnTimeout(){
		// Coding that handle the Timeout receive from CID ImageViewer
	}

	getScriptSrc(scriptId: ?string, scriptFileName: ?string): ?string{
		let scriptElement: ?any = null

		if(scriptId!=null){
			scriptElement = document.getElementById(scriptId)
		}else if(scriptFileName!=null){
			let scripts = document.getElementsByTagName("script")
			for(let i=0;i<scripts.length;i++){
				let item = scripts[i]
				if(this.getJsFileName(item.src)==scriptFileName){
					scriptElement = item
					break
                }
			}
		}

		return scriptElement==null?null:scriptElement.src
	}
	
    getJsFileName(src: string): string{
        const splttedItems = src.split("/")
        const jsFile = splttedItems[(splttedItems.length-1)]
        return jsFile
    }


}

let cidImageViewerProxy = new CidImageViewerProxy()

window.openCIDImageViewer = cidImageViewerProxy.openCIDImageViewer
window.closeCIDImageViewer = cidImageViewerProxy.closeCIDImageViewer
//window.viewerWindow = cidImageViewerProxy.viewerWindow
// module.exports = mainFunction

// exports.openCIDImageViewer = imageViewerProxy.openCIDImageViewer