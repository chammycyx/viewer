// @flow

export const LOG: string = 'LOG'

export const LOG_SNAPSHOT: string = 'LOG_SNAPSHOT'

export type LogType = {
	type: string, 
	loggerName: string,
	level: number, 
	logMsg: string,
	msgCode?: string
}

export function log(loggerName: string, level: number, message: string, msgCode?: string): LogType {
	return {
		type: LOG, 
		loggerName: loggerName,
		level: level, 
		logMsg: message,
		msgCode: msgCode
	}
}

export function logSnapshot(loggerName: string, level: number, message: string, msgCode?: string): LogType {
	return {
		type: LOG_SNAPSHOT,
		loggerName: loggerName,
		level: level,
		logMsg: message,
		msgCode: msgCode
	}
}