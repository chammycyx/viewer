// @flow

export const ApplicationActionTypes = {
    SET_APP_CONFIG: 'DATA/CONFIG/APPLICATION/SET_APP_CONFIG'
}

export type SetAppConfigType = {
    type: typeof ApplicationActionTypes.SET_APP_CONFIG,
    payload: *
}

export function setAppConfig(appConfig: *): SetAppConfigType {
    return {
        type: ApplicationActionTypes.SET_APP_CONFIG,
        payload: appConfig
    }
}