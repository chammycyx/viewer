// @flow

export const MsgCodeActionTypes = {
    SET_MSG_CODE: 'DATA/MSG_CODE/SET_MSG_CODE'
}

export type SetMsgCodeType = {
    type: typeof MsgCodeActionTypes.SET_MSG_CODE,
    payload: *
}

export function setMsgCode(msgCode: *): SetMsgCodeType {
    return {
        type: MsgCodeActionTypes.SET_MSG_CODE,
        payload: msgCode
    }
}