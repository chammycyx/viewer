// @flow

export const MessageDtlActionTypes = {
    SET_MESSAGE_DTL: 'DATA/STUDY/MESSAGE_DTL/SET_MESSAGE_DTL'
}

export type SetMessageDtlType = {
    type: typeof MessageDtlActionTypes.SET_MESSAGE_DTL,
    payload: {
        messageDtlState: *
    }
}

export function setMessageDtl(messageDtlState: *): SetMessageDtlType {
    return {
        type: MessageDtlActionTypes.SET_MESSAGE_DTL,
        payload: {
            messageDtlState
        }
    }
}