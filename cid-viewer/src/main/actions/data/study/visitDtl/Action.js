// @flow

export const VisitDtlActionTypes = {
    SET_VISIT_DTL: 'DATA/STUDY/VISIT_DTL/SET_VISIT_DTL'
}

export type SetVisitDtlType = {
    type: typeof VisitDtlActionTypes.SET_VISIT_DTL,
    payload: {
        visitDtlState: *
    }
}

export function setVisitDtl(visitDtlState: *): SetVisitDtlType {
    return {
        type: VisitDtlActionTypes.SET_VISIT_DTL,
        payload: {
            visitDtlState
        }
    }
}