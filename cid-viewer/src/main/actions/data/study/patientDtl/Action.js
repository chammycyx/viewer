// @flow

export const PatientDtlActionTypes = {
    SET_PATIENT_DTL: 'DATA/STUDY/PATIENT_DTL/SET_PATIENT_DTL'
}

export type SetPatientDtlType = {
    type: typeof PatientDtlActionTypes.SET_PATIENT_DTL,
    payload: {
        patientDtlState: *
    }
}

export function setPatientDtl(patientDtlState: *): SetPatientDtlType {
    return {
        type: PatientDtlActionTypes.SET_PATIENT_DTL,
        payload: {
            patientDtlState
        }
    }
}