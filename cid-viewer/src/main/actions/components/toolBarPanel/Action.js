// @flow
export const BACK_TO_EPR: string = 'COMPONENTS/TOOL_BAR_PANEL/BACK_TO_EPR'

export const ToolBarPanelActionTypes = {
    SET_TOOL: 'COMPONENTS/TOOL_BAR_PANEL/SET_TOOL',
    RESET_IMAGES: 'COMPONENTS/TOOL_BAR_PANEL/RESET_IMAGE',
    SET_BUTTON_STATE: 'COMPONENTS/TOOL_BAR_PANEL/SET_BUTTON_STATE',
    ON_COLOR_ADJUSTMENT_BUTTON_CLICK: 'COMPONENTS/TOOL_BAR_PANEL/ON_COLOR_ADJUSTMENT_BUTTON_CLICK'
}

export const PRINT_IMAGE: string = 'COMPONENTS/TOOL_BAR_PANEL/PRINT_IMAGE'
export const PRINT_MODE = {
    PRINT_SELECTED_IMAGES: 'PRINT_SELECTED_IMAGES',
    PRINT_VIEWING_AREA: 'PRINT_VIEWING_AREA'
}

export type SetToolType = {
    type: typeof ToolBarPanelActionTypes.SET_TOOL,
    payload: {
        tool: string
    }
}

export type ResetImageType = {
	type: typeof ToolBarPanelActionTypes.RESET_IMAGES,
    payload: {
        selectedSeriesId: string,
        selectedImageIds: Array<string>
    }
}

export type SetBtnStateType = {
    type: typeof ToolBarPanelActionTypes.SET_BUTTON_STATE,
    payload: {
        btnState: *
    }
}

type BackToEprType = {
    type: string
}

export type PrintImageType = {
    type: string,
    payload: {
        printMode: string
    }
}

type OnColorAdjustmentButtonClickType = {
    type: string
}

export function setTool(tool: string): SetToolType {
    return {
		type: ToolBarPanelActionTypes.SET_TOOL,
		payload: {
            tool
        }
    }
}

export function backToEpr(): BackToEprType{
    return {
		type: BACK_TO_EPR
    }
}

export function resetImages(selectedSeriesId: string, selectedImageIds: Array<string>): ResetImageType{
    return {
        type: ToolBarPanelActionTypes.RESET_IMAGES,
        payload: {
            selectedSeriesId,
            selectedImageIds
        }
    }
}

export function setBtnState(btnState: *): SetBtnStateType {
    return {
		type: ToolBarPanelActionTypes.SET_BUTTON_STATE,
		payload: {
            btnState
        }
    }
}

export function printImage(printMode: string): PrintImageType{
    return {
        type: PRINT_IMAGE,
        payload: {printMode}
    }
}

export function onColorAdjustmentButtonClick(): OnColorAdjustmentButtonClickType {
    return {
        type: ToolBarPanelActionTypes.ON_COLOR_ADJUSTMENT_BUTTON_CLICK
    }
}