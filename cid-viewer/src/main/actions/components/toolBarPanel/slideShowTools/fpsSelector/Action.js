// @flow 
export const FpsSelectorActionTypes = {
    SET_FPS: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/FPS_SELECTOR/SET_FPS'
}

export type SetFpsType = {
    type: typeof FpsSelectorActionTypes.SET_FPS,
    payload: {
        fps: number
    }
}

export function setFps(fps: number = 0.2): SetFpsType {
    return {
        type: FpsSelectorActionTypes.SET_FPS,
        payload: {
            fps
        }
    }
}

