// @flow 
export const SlideShowToolsActionTypes = {
    PLAY: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/PLAY',
    PAUSE: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/PAUSE',
    STOP: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/STOP',    
    SET_PREV: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_PREV',
    SET_NEXT: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_NEXT',
    SET_MODE: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_MODE',
    SET_PLAYING: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_PLAYING',
    SET_SEQ_CANVAS: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_SEQ_CANVAS',
    SET_REF_OFFSETS: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_REF_OFFSETS',
    SET_TIMER_ID: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_TIMER_ID',
    SET_IMAGE: 'COMPONENTS/TOOL_BAR_PANEL/SLIDE_SHOW_TOOLS/SET_IMAGE'
}

export type PlayType = {
    type: typeof SlideShowToolsActionTypes.PLAY,
    payload: {
        fps: number
    }
}

export function play(fps: number): PlayType {
    return {
        type: SlideShowToolsActionTypes.PLAY,
        payload: {
            fps
        }
    }
}


export type PauseType = {
    type: typeof SlideShowToolsActionTypes.PAUSE,
    payload: {
        
    }
}

export function pause(): PauseType {
    return {
        type: SlideShowToolsActionTypes.PAUSE,
        payload: {
            
        }
    }
}


export type StopType = {
    type: typeof SlideShowToolsActionTypes.STOP,
    payload: {
        
    }
}

export function stop(): StopType {
    return {
        type: SlideShowToolsActionTypes.STOP,
        payload: {
            
        }
    }
}


export type SetPrevType = {
    type: typeof SlideShowToolsActionTypes.SET_PREV,
    payload: {
        
    }
}

export function setPrev(): SetPrevType {
    return {
        type: SlideShowToolsActionTypes.SET_PREV,
        payload: {
            
        }
    }
}


export type SetNextType = {
    type: typeof SlideShowToolsActionTypes.SET_NEXT,
    payload: {
        
    }
}

export function setNext(): SetNextType {
    return {
        type: SlideShowToolsActionTypes.SET_NEXT,
        payload: {
            
        }
    }
}


export type SetModeType = {
    type: typeof SlideShowToolsActionTypes.SET_MODE,
    payload: {
        isModeOn: boolean
    }
}

export function setMode(isModeOn: boolean): SetModeType {
    return {
        type: SlideShowToolsActionTypes.SET_MODE,
        payload: {
            isModeOn
        }
    }
}


export type SetPlayingType = {
    type: typeof SlideShowToolsActionTypes.SET_PLAYING,
    payload: {
        isPlaying: boolean
    }
}

export function setPlaying(isPlaying: boolean): SetPlayingType {
    return {
        type: SlideShowToolsActionTypes.SET_PLAYING,
        payload: {
            isPlaying
        }
    }
}


export type SetSeqCanvasType = {
    type: typeof SlideShowToolsActionTypes.SET_SEQ_CANVAS,
    payload: {
        seqCanvas: number
    }
}

export function setSeqCanvas(seqCanvas: number): SetSeqCanvasType {
    return {
        type: SlideShowToolsActionTypes.SET_SEQ_CANVAS,
        payload: {
            seqCanvas
        }
    }
}


export type SetRefOffsetsType = {
	type: typeof SlideShowToolsActionTypes.SET_REF_OFFSETS,
	payload: {
		seqCanvas: number,
		width: number,
		height: number
	}
}

export function setRefOffsets(seqCanvas: number, width: number, height: number): SetRefOffsetsType {
	return {
		type: SlideShowToolsActionTypes.SET_REF_OFFSETS,
		payload: {
			seqCanvas,
			width,
			height
		}
	}
}


export type SetTimerIdType = {
    type: typeof SlideShowToolsActionTypes.SET_TIMER_ID,
    payload: {
        timerId: *
    }
}

export function setTimerId(timerId: *): SetTimerIdType {
    return {
        type: SlideShowToolsActionTypes.SET_TIMER_ID,
        payload: {
            timerId
        }
    }
}


export type SetImageType = {
    type: typeof SlideShowToolsActionTypes.SET_IMAGE,
    payload: {
        imageIndexId: string,
        seqCanvas: number,
        base64: string,
        imageProperties: *
    }
}

export function setImage(imageIndexId: string, seqCanvas: number, base64: string, imageProperties: *): SetImageType {
    return {
        type: SlideShowToolsActionTypes.SET_IMAGE,
        payload: {
            imageIndexId,
            seqCanvas,
            base64,
            imageProperties
        }
    }
}
