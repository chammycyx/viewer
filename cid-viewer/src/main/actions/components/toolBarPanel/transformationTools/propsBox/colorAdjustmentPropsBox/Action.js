// @flow
export const ColorAdjustmentPropsBoxActionTypes = {
    SET_VISIBLE: 'COMPONENTS/TOOL_BAR_PANEL/TRANSFORMATION_TOOLS/PROPS_BOX/COLOR_ADJUSTMENT_PROPS_BOX/SET_VISIBLE',
    SET_COLOR_ADJUSTMENT_CONFIG: 'COMPONENTS/TOOL_BAR_PANEL/TRANSFORMATION_TOOLS/PROPS_BOX/COLOR_ADJUSTMENT_PROPS_BOX/SET_COLOR_ADJUSTMENT_CONFIG',
	CHANGE_COLOR_ADJUSTMENT: 'COMPONENTS/TOOL_BAR_PANEL/TRANSFORMATION_TOOLS/PROPS_BOX/COLOR_ADJUSTMENT_PROPS_BOX/CHANGE_COLOR_ADJUSTMENT'
}

export type SetVisibleType = {
    type: string,
    payload: { visible: boolean }
}

export type SetColorAdjustmentConfigType = {
    type: string,
    payload: { maxNumOfImage: number }
}

export type ChangeColorAdjustmentType = {
    type: typeof ColorAdjustmentPropsBoxActionTypes.CHANGE_COLOR_ADJUSTMENT,
    payload: {
        brightness: number,
        contrast: number,
        hue: number,
        saturation: number,
        invert: boolean,
        contrastToolIdx: number
    }
}

export function setVisible(visible: boolean): SetVisibleType {
    return {
		type: ColorAdjustmentPropsBoxActionTypes.SET_VISIBLE,
		payload: { visible }
    }
}

export function setColorAdjustmentConfig(maxNumOfImage: number): SetColorAdjustmentConfigType {
    return {
		type: ColorAdjustmentPropsBoxActionTypes.SET_COLOR_ADJUSTMENT_CONFIG,
		payload: { maxNumOfImage }
    }
}

export function changeColorAdjustment(brightness: number, contrast: number, hue: number, saturation: number, invert: boolean, contrastToolIdx: number): ChangeColorAdjustmentType{
    return {
        type: ColorAdjustmentPropsBoxActionTypes.CHANGE_COLOR_ADJUSTMENT,
        payload: {
            brightness,
            contrast,
            hue,
            saturation,
            invert,
            contrastToolIdx
        }
    }
}