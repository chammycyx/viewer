// @flow
export const ExportImageDialogActionTypes = {
    SET_VISIBLE: 'COMPONENTS/TOOL_BAR_PANEL/FILE_TOOLS/DIALOGS/EXPORT_IMAGE_DIALOG/SET_VISIBLE',
    SET_DOWNLOAD: 'COMPONENTS/TOOL_BAR_PANEL/FILE_TOOLS/DIALOGS/EXPORT_IMAGE_DIALOG/SET_DOWNLOAD',
    REQUEST_EXPORT_IMAGES: 'COMPONENTS/TOOL_BAR_PANEL/FILE_TOOLS/DIALOGS/EXPORT_IMAGE_DIALOG/REQUEST_EXPORT_IMAGES'
}

export type SetVisibleType = {
    type: typeof ExportImageDialogActionTypes.SET_VISIBLE,
    payload: {
        isVisible: boolean
    }
}

export function setVisible(isVisible: boolean): SetVisibleType {
    return {
		type: ExportImageDialogActionTypes.SET_VISIBLE,
		payload: {
            isVisible
        }
    }
}


export type SetDownloadType = {
    type: typeof ExportImageDialogActionTypes.SET_DOWNLOAD,
    payload: {
        isDownload: boolean,
        exportImageBase64: string
    }
}

export function setDownload(isDownload: boolean, exportImageBase64: string): SetDownloadType {
    return {
		type: ExportImageDialogActionTypes.SET_DOWNLOAD,
		payload: {
            isDownload,
            exportImageBase64
        }
    }
}


export type RequestExportImageType = {
    type: typeof ExportImageDialogActionTypes.REQUEST_EXPORT_IMAGES,
    payload: {
        selectedSeriesId: string,
        selectedImageIds: Array<string>,
        password: string
    }
}

export function requestExportImages(selectedSeriesId: string, selectedImageIds: Array<string>, password: string): RequestExportImageType {
    return {
        type: ExportImageDialogActionTypes.REQUEST_EXPORT_IMAGES,
        payload: {
            selectedSeriesId,
            selectedImageIds,
            password
        }
    }
}