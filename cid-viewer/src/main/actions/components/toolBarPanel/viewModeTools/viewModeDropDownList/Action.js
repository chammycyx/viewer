// @flow
export const ViewModeDropDownListActionTypes = {
    SET_VIEW_MODE: 'COMPONENTS/TOOL_BAR_PANEL/VIEW_MODE_TOOLS/VIEW_MODE_DROP_DOWN_LIST/SET_VIEW_MODE',
    ON_VIEW_MODE_CHANGE: 'COMPONENTS/TOOL_BAR_PANEL/VIEW_MODE_TOOLS/VIEW_MODE_DROP_DOWN_LIST/ON_VIEW_MODE_CHANGE'
}

export type ViewModeType = {
    text: string,
    noOfImage: number
}

export const VIEW_MODES = {
    ONE_X_ONE: {
        text: '1x1',
        noOfImage: 1
    },
    ONE_X_TWO: {
        text: '1x2',
        noOfImage: 2
    },
    TWO_X_TWO: {
        text: '2x2',
        noOfImage: 4
    },
    TWO_X_THREE: {
        text: '2x3',
        noOfImage: 6
    },
    THREE_X_THREE: {
        text: '3x3',
        noOfImage: 9
    },
    FOUR_X_FOUR: {
        text: '4x4',
        noOfImage: 16
    },
    FIVE_X_FIVE: {
        text: '5x5',
        noOfImage: 25
    }
}

export type SetViewModeType = {
    type: typeof ViewModeDropDownListActionTypes.SET_VIEW_MODE,
    payload: {
        viewMode: ViewModeType
    }
}

export function setViewMode(viewMode: ViewModeType): SetViewModeType {
    return {
		type: ViewModeDropDownListActionTypes.SET_VIEW_MODE,
		payload: {
            viewMode
        }
    }
}

export type OnViewModeChnageType = {
    type: typeof ViewModeDropDownListActionTypes.ON_VIEW_MODE_CHANGE,
    payload: {
        viewMode: ViewModeType
    }
}

export function onViewModeChange(viewMode: ViewModeType): OnViewModeChnageType{
    return {
        type: ViewModeDropDownListActionTypes.ON_VIEW_MODE_CHANGE,
        payload: {
            viewMode
        }
    }
}
