// @flow
import type {ViewModeType} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import type {ImgPropertyType} from '@domains/model/vo/ImgProperty'

export const CanvasGridActionTypes = {
    SET_IMAGE: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_IMAGE',
    SET_SELECTED_IMAGE: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_SELECTED_IMAGE',
    SET_GRID: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_GRID',
    RESIZE_IMG_VIEWING_PANEL: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/RESIZE_IMG_VIEWING_PANEL',
    ON_CANVAS_RESIZED: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/ON_CANVAS_RESIZED',
    ON_IMAGE_MOVED: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/ON_IMAGE_MOVED',
    SET_IMAGE_POSITION: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_IMAGE_POSITION',
    ROTATE_IMAGE: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/ROTATE_IMAGE',
    FLIP_IMAGE: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/FLIP_IMAGE',
    SET_IMAGE_HEADER_CONFIG: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_IMAGE_HEADER_CONFIG',
    ON_CANVAS_CLICK: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/ON_CANVAS_CLICK',
    SET_SELECTED_CANVAS: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_SELECTED_CANVAS',
    SET_COLOR_ADJUSTMENT: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_COLOR_ADJUSTMENT',
    ON_IMAGE_ZOOM: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/ON_IMAGE_ZOOM',
    SET_IMAGE_SCALE: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_IMAGE_SCALE',
    RESET_IMAGES: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/RESET_IMAGES',
    SET_COMPONENT_STATE: 'COMPONENTS/IMAGE_VIEWING_PANEL/CANVAS_GRID/SET_COMPONENT_STATE'
}

export const RotateDirections = {
    LEFT: 'LEFT',
    RIGHT: 'RIGHT'
}

export const FlipDirections = {
    HORIZONTAL: 'HORIZONTAL',
    VERTICAL: 'VERTICAL'
}


export type SetImageType = {
    type: typeof CanvasGridActionTypes.SET_IMAGE,
    payload: {caseNo: string, viewMode: ViewModeType, seriesIndexId: string, imageIndexIds: Array<string>, base64s: Array<string>, imageProperties: Array<*>}
}


export type SetSelectedImageType = {
    type: typeof CanvasGridActionTypes.SET_SELECTED_IMAGE,
    payload: {
        selectedImageIds: Array<string>
    }
}

export type SetGridType = {
    type: typeof CanvasGridActionTypes.SET_GRID,
    payload: {
        viewMode: ViewModeType,
        canvasWidth: number,
        canvasHeight: number
    }
}

export type ResizeImgViewingPanelType = {
    type: typeof CanvasGridActionTypes.RESIZE_IMG_VIEWING_PANEL,
    payload: {
        viewingAreaWidth: number,
        viewingAreaHeight: number,
        canvasWidth: number,
        canvasHeight: number,
        imgsProperty: Array<ImgPropertyType>
    }
}

export type OnCanvasResizedType = {
    type: typeof CanvasGridActionTypes.ON_CANVAS_RESIZED,
    payload: {}
}

export type SetImagePositionType = {
    type: typeof CanvasGridActionTypes.SET_IMAGE_POSITION,
    payload: {seriesId: string, imageId: string, xCoor: number, yCoor: number}
}

export type OnImageMovedType = {
    type: typeof CanvasGridActionTypes.ON_IMAGE_MOVED,
    payload: {seriesId: string, imageId: string, xCoor: number, yCoor: number}
}

export type RotateImageType = {
    type: typeof CanvasGridActionTypes.ROTATE_IMAGE,
    payload: { imageId: string, rotateAngle: number }
}

export type FlipImageType = {
    type: typeof CanvasGridActionTypes.FLIP_IMAGE,
    payload: { imageId: string, flipImageX: boolean, flipImageY: boolean }
}

export type OnCanvasClickType = {
    type: typeof CanvasGridActionTypes.ON_CANVAS_CLICK,
    payload: {
        imageId: string, 
        button: string
    }
}

export type SetColorAdjustmentType = {
    type: string,
    payload: {
        brightness: number,
        contrast: number,
        hue: number,
        saturation: number,
        invert: boolean
    }
}

export type SetSelectedCanvasType = {
    type: typeof CanvasGridActionTypes.SET_SELECTED_CANVAS,
    payload: {imageId: string, button: string}
}

export type OnImageZoomType = {
    type: string,
    payload: {
        seriesId: string,
        imageId: string,
        zoomMode: string,
        zoomX: number,
        zoomY: number,
        pannedCoordinateRule: Function
    }
}

export type SetImageScaleType = {
    type: typeof CanvasGridActionTypes.SET_IMAGE_SCALE,
    payload: {
        seriesId: string,
        imageId: string,
        scaleX: number,
        scaleY: number,
        pannedXCoor: number,
        pannedYCoor: number
    }
}

export type ResetImageType = {
    type: typeof CanvasGridActionTypes.RESET_IMAGES,
    payload: {
        selectedSeriesId: string,
        selectedImageIds: Array<string>,
        imgsProperties: Array<*>
    }
}

export type SetCompnentStateType = {
    type: typeof CanvasGridActionTypes.SET_COMPONENT_STATE,
    payload: {
        imageHeaderWidth: number,
        imageHeaderHeight: number
    }
}

export function setImage(caseNo: string, viewMode: ViewModeType, seriesIndexId: string, imageIndexIds: Array<string>, base64s: Array<string>, imageProperties: Array<*>): SetImageType {
    return {
		type: CanvasGridActionTypes.SET_IMAGE,
        payload: {caseNo, viewMode, seriesIndexId, imageIndexIds, base64s, imageProperties}
    }
}

export function setSelectedImage(selectedImageIds: Array<string>): SetSelectedImageType {
    return {
		type: CanvasGridActionTypes.SET_SELECTED_IMAGE,
        payload: {
            selectedImageIds
        }
    }
}

export function setGrid(viewMode: ViewModeType, canvasWidth: number, canvasHeight: number): SetGridType {
    return {
		type: CanvasGridActionTypes.SET_GRID,
		payload: {
            viewMode,
            canvasWidth,
            canvasHeight
        }
    }
}

export function resizeImgViewingPanel(viewingAreaWidth: number, viewingAreaHeight: number, canvasWidth: number, canvasHeight: number, imgsProperty: Array<ImgPropertyType>): ResizeImgViewingPanelType {
    return {
        type: CanvasGridActionTypes.RESIZE_IMG_VIEWING_PANEL,
        payload: {
            viewingAreaWidth,
            viewingAreaHeight,
            canvasWidth,
            canvasHeight,
            imgsProperty
        }
    }
}

export function onCanvasResized(): OnCanvasResizedType {
    return {
        type: CanvasGridActionTypes.ON_CANVAS_RESIZED,
        payload :{}
    }
}

export function onImgMoved(seriesId: string, imageId: string, xCoor: number, yCoor: number): OnImageMovedType {
    return {
        type: CanvasGridActionTypes.ON_IMAGE_MOVED,
        payload :{seriesId: seriesId, imageId: imageId, xCoor: xCoor, yCoor: yCoor}
    }
}

export function rotateImage(imageId: string, rotateAngle: number): RotateImageType {
    return {
        type: CanvasGridActionTypes.ROTATE_IMAGE,
        payload :{ imageId, rotateAngle }
    }
}

export function flipImage(imageId: string, flipImageX: boolean, flipImageY: boolean): FlipImageType {
    return {
        type: CanvasGridActionTypes.FLIP_IMAGE,
        payload :{ imageId, flipImageX, flipImageY }
    }
}

export function onCanvasClick(imageId: string, button: string): OnCanvasClickType {
    return {
        type: CanvasGridActionTypes.ON_CANVAS_CLICK,
        payload :{
            imageId, 
            button
        }
    }
}

export function setSelectedCanvas(imageId: string, button: string): SetSelectedCanvasType {
    return {
        type: CanvasGridActionTypes.SET_SELECTED_CANVAS,
        payload :{imageId: imageId, button: button}
    }
}

export function setImagePosition(seriesId: string, imageId: string, xCoor: number, yCoor: number): SetImagePositionType {
    return {
        type: CanvasGridActionTypes.SET_IMAGE_POSITION,
        payload :{seriesId, imageId, xCoor, yCoor}
    }
}

export function setColorAdjustment(brightness: number, contrast: number, hue: number, saturation: number, invert: boolean): SetColorAdjustmentType {
    return {
		type: CanvasGridActionTypes.SET_COLOR_ADJUSTMENT,
		payload: {
            brightness,
            contrast,
            hue,
            saturation,
            invert
        }
    }
}

export function setImageZoom(seriesId: string, imageId: string, zoomMode: string, zoomX: number, zoomY: number, pannedCoordinateRule: Function): OnImageZoomType{
    return {
		type: CanvasGridActionTypes.ON_IMAGE_ZOOM,
		payload: {
            seriesId,
            imageId,
            zoomMode,
            zoomX,
            zoomY,
            pannedCoordinateRule
        }
    }
}

export function setImageScale(seriesId: string, imageId: string, scaleX: number, scaleY: number, pannedXCoor: number, pannedYCoor: number): SetImageScaleType{
    return {
		type: CanvasGridActionTypes.SET_IMAGE_SCALE,
		payload: {
            seriesId,
            imageId,
            scaleX,
            scaleY,
            pannedXCoor, 
            pannedYCoor
        }
    }
}

export function resetImages(selectedSeriesId: string, selectedImageIds: Array<string>, imgsProperties: Array<*>): ResetImageType {
    return {
        type: CanvasGridActionTypes.RESET_IMAGES,
        payload: {
            selectedSeriesId,
            selectedImageIds,
            imgsProperties
        }
    }
}

export function setComponentState(componentState: *): SetCompnentStateType{
    return {
        type: CanvasGridActionTypes.SET_COMPONENT_STATE,
        payload: {
            imageHeaderWidth: componentState.imageHeaderWidth,
            imageHeaderHeight: componentState.imageHeaderHeight
        }
    }
}