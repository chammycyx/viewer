// @flow
export const PictorialIndexPanelActionTypes = {
    ON_TOGGLE_PANEL_BUTTON_CLICK: 'COMPONENTS/PICTORIAL_INDEX_PANEL/ON_TOGGLE_PANEL_BUTTON_CLICK',
    SET_IS_HIDDEN: 'COMPONENTS/PICTORIAL_INDEX_PANEL/SET_IS_HIDDEN'
}

export type SetIsHiddenType = {
    type: typeof PictorialIndexPanelActionTypes.SET_IS_HIDDEN,
    payload: {
        isHidden: boolean
    }
}

export type OnTogglePanelButtonClickType = {
    type: typeof PictorialIndexPanelActionTypes.ON_TOGGLE_PANEL_BUTTON_CLICK,
    payload: {
        isHidden: boolean
    }
}

export function setIsHidden(isHidden: boolean): SetIsHiddenType {
    return {
		type: PictorialIndexPanelActionTypes.SET_IS_HIDDEN,
		payload: {
            isHidden
        }
    }
}

export function onTogglePanelButtonClick(isHidden: boolean): OnTogglePanelButtonClickType {
    return {
        type: PictorialIndexPanelActionTypes.ON_TOGGLE_PANEL_BUTTON_CLICK,
        payload: {
            isHidden
        }
    }
}