// @flow
export const ThumbnailGridActionTypes = {
    SET_SELECTED_IMAGE_IDS: 'COMPONENTS/PICTORIAL_INDEX_PANEL/THUMBNAIL_GRID/SET_SELECTED_IMAGE_IDS',
    SET_SELECTED_FIRST_IMAGE: 'COMPONENTS/PICTORIAL_INDEX_PANEL/THUMBNAIL_GRID/SET_SELECTED_FIRST_IMAGE',
    ON_THUMBNAIL_CLICK: 'COMPONENTS/PICTORIAL_INDEX_PANEL/THUMBNAIL_GRID/ON_THUMBNAIL_CLICK',
    SET_PICTORIAL_INDEX_CONFIG: 'COMPONENTS/PICTORIAL_INDEX_PANEL/THUMBNAIL_GRID/SET_PICTORIAL_INDEX_CONFIG'
}

export type SetSelectedImageIdsType = {
    type: typeof ThumbnailGridActionTypes.SET_SELECTED_IMAGE_IDS,
    payload: {
        selectedImageIds: Array<string>
    }
}

export type SetSelectedFirstImageType = {
    type: typeof ThumbnailGridActionTypes.SET_SELECTED_FIRST_IMAGE,
    payload: {
        selectedSeriesId: string,
        selectedFirstImageId: string
    }
}

export type OnThumbnailClickType = {
    type: typeof ThumbnailGridActionTypes.ON_THUMBNAIL_CLICK,
    payload: {
        selectedSeriesId: string,
        selectedFirstImageId: string
    }
}

export type SetPictorialIndexConfigType = {
    type: typeof ThumbnailGridActionTypes.SET_PICTORIAL_INDEX_CONFIG,
    payload: {
        numOfColumn: number, 
        isShowSeriesHeading: boolean,
        serieName: string
    }
}

export function setSelectedImageIds(selectedImageIds: Array<string>): SetSelectedImageIdsType {
    return {
		type: ThumbnailGridActionTypes.SET_SELECTED_IMAGE_IDS,
		payload: {
            selectedImageIds
        }
    }
}

export function setSelectedFirstImage(selectedSeriesId: string, selectedFirstImageId: string): SetSelectedFirstImageType {
    return {
		type: ThumbnailGridActionTypes.SET_SELECTED_FIRST_IMAGE,
		payload: {
            selectedSeriesId, selectedFirstImageId
        }
    }
}

export function onThumbnailClick(selectedSeriesId: string, selectedFirstImageId: string): OnThumbnailClickType {
    return {
		type: ThumbnailGridActionTypes.ON_THUMBNAIL_CLICK,
		payload: {
            selectedSeriesId, selectedFirstImageId
        }
    }
}

export function setPictorialIndexConfig(numOfColumn: number, isShowSeriesHeading: boolean, serieName: string): SetPictorialIndexConfigType {
    return {
		type: ThumbnailGridActionTypes.SET_PICTORIAL_INDEX_CONFIG,
		payload: {
            numOfColumn, isShowSeriesHeading, serieName
        }
    }
}