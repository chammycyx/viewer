// @flow
export const ComponentsActionTypes = {
    SET_APP_SIZE: 'COMPONENTS/SET_APP_SIZE'
}

export type SetAppSizeType = {
    type: typeof ComponentsActionTypes.SET_APP_SIZE,
    payload: {
        width: number,
        height: number
    }
}

export function setAppSize(width: number, height: number): SetAppSizeType {
    return {
		type: ComponentsActionTypes.SET_APP_SIZE,
		payload: {
            width,
            height
        }
    }
}