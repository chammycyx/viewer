// @flow
export const MsgDialogActionTypes = {
    SET_MSG_DIALOG: 'COMPONENTS/MSG_DIALOG/SET_MSG_DIALOG'
}

export type SetMsgDialogType = {
    type: typeof MsgDialogActionTypes.SET_MSG_DIALOG,
    payload: {
        isVisible: boolean,
        code: ?string | null,
        title: ?string | null,
        content: ?string | null,
        additionalContent: ?string | null
    }
}

export function setMsgDialog(isVisible: boolean, code: ?string | null, title: ?string | null, content: ?string | null, additionalContent: ?string | null): SetMsgDialogType {
    return {
		type: MsgDialogActionTypes.SET_MSG_DIALOG,
		payload: {
            isVisible,
            code,
            title,
            content,
            additionalContent
        }
    }
}