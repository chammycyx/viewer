// @flow
export const BtnStateWatcherActionTypes = {
    NUM_OF_SELECTED_IMAGE_CHANGED: 'COMPONENTS/ROOT/WATCHER/WATCHER_FOR_BYN_STATE/NUM_OF_SELECTED_IMAGE_CHANGED',
}

export type NumOfSelectImageChangedType = {
    type: typeof BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED,
    payload: {
        numOfSelectImage: number,
        slideShowMode: boolean,
        isPlayingslideShow: boolean
    }
}

export function numOfSelectImageChanged(numOfSelectImage: number, slideShowMode: boolean, isPlayingslideShow: boolean): NumOfSelectImageChangedType {
    return {
		type: BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED,
		payload: {
            numOfSelectImage,
            slideShowMode,
            isPlayingslideShow
        }
    }
}