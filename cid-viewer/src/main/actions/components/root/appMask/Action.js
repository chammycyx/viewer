// @flow
export const AppMaskActionTypes = {
    SET_APP_MASK: 'COMPONENTS/ROOT/APP_MASK/SET_APP_MASK',
    SET_PROGRESS_BAR: 'COMPONENTS/ROOT/APP_MASK/SET_PROGRESS_BAR'
}

export type SetAppMaskType = {
    type: typeof AppMaskActionTypes.SET_APP_MASK,
    payload: {
        isMasked: boolean
    }
}

export type SetProgressBarType = {
    type: typeof AppMaskActionTypes.SET_PROGRESS_BAR,
    payload: {
        isProgressing: boolean,
        description: string
    }
}

export function setAppMask(isMasked: boolean): SetAppMaskType {
    return {
		type: AppMaskActionTypes.SET_APP_MASK,
		payload: {
            isMasked
        }
    }
}
export function setProgressBar(isProgressing: boolean, description: string): SetProgressBarType {
    return {
		type: AppMaskActionTypes.SET_PROGRESS_BAR,
		payload: {
            isProgressing,
            description
        }
    }
}