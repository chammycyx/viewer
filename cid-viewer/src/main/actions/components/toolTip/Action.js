// @flow

export const ToolTipActionTypes = { 
    SET_TOOL_TIP: 'COMPONENTS/TOOL_TIP/SET_TOOL_TIP'
}

export type SetToolTipType = {
    type: typeof ToolTipActionTypes.SET_TOOL_TIP,
    payload: {
        title: string,
        isOpen: boolean,
        x: number,
        y: number
    }
}

export function setIsOpen(title: string, isOpen: boolean, x: number, y: number): SetToolTipType {
    return {
		type: ToolTipActionTypes.SET_TOOL_TIP,
		payload: {
            title,
            isOpen,
            x,
            y
        }
    }
}