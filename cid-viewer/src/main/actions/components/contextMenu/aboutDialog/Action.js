// @flow
export const AboutDialogActionTypes = {
    SET_IS_OPEN: 'COMPONENTS/CONTEXT_MENU/ABOUT_DIALOG/SET_IS_OPEN'
}

export type SetIsOpenType = {
    type: typeof AboutDialogActionTypes.SET_IS_OPEN,
    payload: {
        isOpen: boolean
    }
}

export function setIsOpen(isOpen: boolean): SetIsOpenType {
    return {
		type: AboutDialogActionTypes.SET_IS_OPEN,
		payload: {
            isOpen
        }
    }
}