// import cyan from '@material-ui/core/colors/cyan'
// import pink from '@material-ui/core/colors/pink'
// import grey from '@material-ui/core/colors/grey'
import blue from '@material-ui/core/colors/blue'

import createPalette from '@material-ui/core/styles/createPalette'
import createTypography from '@material-ui/core/styles/createTypography'

const palette = createPalette({
    type: 'dark',
    background: {
        paper: 'rgba(66, 66, 66, 1)',
        default: 'rgba(0, 0, 0, 1)'
    }
})

const dialogPalette = createPalette({
    type: 'light'
})

const muiCommonTheme = {
    palette,
    typography: createTypography(palette, {
        fontFamily: 'Arial',
        fontSize: '10pt'
    }),
    shape: {
        borderRadius: 4
    }
}

const muiCommonThemeForDialog = {
    dialogPalette,
    typography: createTypography(dialogPalette, {
        fontFamily: 'Arial',
        fontSize: '10pt'
    }),
    shape: {
        borderRadius: 4
    }
}

const muiCompTheme = {
    overrides: {

    }
}

const cidCompTheme = {
    toolBarPanel: {
        width: '100%',
        height: 40,
        toolBarBtn: {
            color: {
                normal: 'rgba(255, 255, 255, 1)',
                hover: blue['100'],
                selected: blue['500'],
                bg: 'rgba(0, 0, 0, 1)'
            },
            width: 39,
            height: 38,
            printButtonTools:{
                fontSize: '9pt'
            }
        },
        propsBox: {
            width: 300
        }
    },
    pictorialIndexPanel: {
        width: 220,
        showHideButton: {
            color: {
                bg: 'rgba(48, 48, 48, 1)'
            },
            showButton: {
                width: 15,
                height: 181
            },
            hideButton: {
                width: 22,
                height: 17
            }
        },
        thumbnailGrid: {
            spacing: 1,
            scrollBarWidth: 25,
            subHeader: {
                height: 28,
                lineHeight: '28px',
                paddingLeft: 13,
                fontSize: '9pt',
                color: 'rgba(255, 255, 255, 0.54)'
            },
            thumbnail: {
                borderWidth: 2,
                borderStyle: 'solid',
                borderColor: 'rgba(124, 124, 124, 1)',
                borderColorSelected: 'rgba(0, 209, 255, 1)'
            }
        },
        studyInfo: {
            wrapper: {
                style: {
                    height: 100
                }
            },

            infoBase: {
                fontSize: '8pt'
            }
        }
    },
    imageViewingPanel: {
        canvasGrid: {
            imageFit: 0.9,
            headerPaddingTop: 3,
            headerPaddingLeft: 7,
            headerFontStyle: 'italic'
        }
    },
    msgDialog: {
        width: 440,
        height: 280,
        paddingLeft: 75,
        borderWidth: 4,
        fontSize: '10pt',
        icon: {
            top: '3em', 
            left: '1em'
        }
    },
    debug: {
        debugBorderColor: 'rgba(0, 0, 0, 1)',
        showDebugBorder: true
    },
    overrides: {
        MuiTooltip: {
            tooltip: {
                backgroundColor: 'rgba(242, 242, 193, 1)',
                fontSize: '8pt',
                color: 'rgba(11, 51, 60, 1)',
            }
        }
    }
}

export default Object.assign(muiCommonTheme, muiCompTheme, cidCompTheme)

export const cidThemeForDialog = Object.assign(muiCommonThemeForDialog, muiCompTheme, cidCompTheme)

// export const cidThemeV0 = {
//     fontFamily: 'Sans-serif',
//     fontSize: '10pt',
//     borderRadius: 2,
//     palette: {
//         primary1Color: cyan['700'],
//         primary2Color: cyan['700'],
//         primary3Color: grey['600'],
//         accent1Color: pink['A200'],
//         accent2Color: pink['A400'],
//         accent3Color: pink['A100'],
//         textColor: 'rgba(255, 255, 255, 1)',
//         secondaryTextColor: 'rgba(255, 255, 255, 0.7)',
//         alternateTextColor: 'rgba(48, 48, 48, 1)',
//         canvasColor: 'rgba(0, 0, 0, 1)',
//         borderColor: 'rgba(255, 255, 255, 0.3)',
//         disabledColor: 'rgba(255, 255, 255, 0.3)',
//         pickerHeaderColor: 'rgba(255, 255, 255, 0.12)',
//         clockCircleColor: 'rgba(255, 255, 255, 0.12)',
//         shadowColor: 'rgba(0, 0, 0, 1)',
//     },
//     toolBarPanel: {
//         width: '100%',
//         height: 40,
//         toolBarBtn: {
//             color: {
//                 normal: 'rgba(255, 255, 255, 1)',
//                 hover: blue['100'],
//                 selected: blue['500'],
//                 bg: 'rgba(0, 0, 0, 1)'
//             },
//             width: 39,
//             height: 38
//         },
//         propsBox: {
//             width: 300
//         }
//     },
//     pictorialIndexPanel: {
//         width: 220,
//         showHideButton: {
//             color: {
//                 bg: 'rgba(48, 48, 48, 1)'
//             },
//             showButton: {
//                 width: 15,
//                 height: 181
//             },
//             hideButton: {
//                 width: 22,
//                 height: 17
//             }
//         },
//         thumbnailGrid: {
//             spacing: 1,
//             scrollBarWidth: 25,
//             subHeader: {
//                 height: 28,
//                 lineHeight: '28px',
//                 paddingLeft: 13,
//                 fontSize: '9pt',
//                 color: 'rgba(255, 255, 255, 0.54)'
//             },
//             thumbnail: {
//                 borderWidth: 2,
//                 borderStyle: 'solid',
//                 borderColor: 'rgba(124, 124, 124, 1)',
//                 borderColorSelected: 'rgba(0, 209, 255, 1)'
//             }
//         },
//         studyInfo: {
//             wrapper: {
//                 style: {
//                     height: 100
//                 }
//             },

//             infoBase: {
//                 fontSize: '8pt'
//             }
//         }
//     },
//     imageViewingPanel: {
//         canvasGrid: {
//             imageFit: 0.9,
//             headerPaddingTop: 3,
//             headerPaddingLeft: 7,
//             headerFontStyle: 'italic'
//         }
//     },
//     debug: {
//         debugBorderColor: 'rgba(0, 0, 0, 1)',
//         showDebugBorder: true
//     }
// }