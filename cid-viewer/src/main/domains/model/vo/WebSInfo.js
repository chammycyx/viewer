// @flow
class WebSInfo{
    username: string
    password: string
    contextPath: string

    constructor(username: string, password: string, contextPath: string){
        this.username = username
        this.password = password
        this.contextPath = contextPath
    }
}

export default WebSInfo