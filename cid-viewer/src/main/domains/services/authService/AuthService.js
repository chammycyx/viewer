//@flow

import type { WsRsltObjType } from '@utils/WebServiceBackbone'
import WebSInfo from '@domains/model/vo/WebSInfo'
import { callWs } from '@utils/WebServiceBackbone'
import { ENDPOINT } from '@src/main/constants/EndpointConstants'
import { ACCESS_AUTHENTICATION_FAILED, APPLICATION_VERSION_FAILED, EJB_EXCEPTION} from '@src/main/constants/MsgCodeConstants'
import dataRepository from '@utils/DataRepository'

// for logging
import { LOGGER_NAME } from '@src/main/constants/LogConstants'
import logger from '@utils/Logger'

export class AuthService {
    dataRespository: *
    webSInfo: *
    validateAccessKeyByHostName: Function
    validateApplicationStatus: Function

    constructor(webSInfo: WebSInfo, dataRespository: typeof dataRepository) {
        this.dataRespository = dataRespository
        this.webSInfo = webSInfo
        this.validateApplicationStatus = this.validateApplicationStatus.bind(this)
        this.validateAccessKeyByHostName = this.validateAccessKeyByHostName.bind(this)
    }

    async validateAccessKeyByHostName(systemId: string, hostName: string, accessKey: string, userId: string, workstationId: string, requestSys: string, timeout: number): Promise<boolean> {    
        // Service Performance Log
        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_ICW_VALIDATEACCESSKEY_START, 
            "Invoke 'validateAccessKeyByHostName'...[systemId=" + systemId +
            ",workstationHostname=" + hostName + 
            ",accessKey=" + accessKey +
            ",userId=" + userId + 
            ",workstationId=" + workstationId +
            ",requestSys=" + requestSys + "]"
        )
        let rsltObj: WsRsltObjType | null = null
        try{
            rsltObj = await callWs(
                this.webSInfo.username, 
                this.webSInfo.password, 
                this.webSInfo.contextPath, 
                ENDPOINT.ICW.VALIDATE_ACCESS_KEY_BY_HOSTNAME,
                {
                    systemId,
                    hostName,
                    accessKey,
                    userId,
                    workstationId,
                    requestSys
                },
                timeout
            )
        }catch(cidHttpErr){
            // this.dataRespository.setMsgDialogByCode(EJB_EXCEPTION, error.exceptionMsg)
            // this.dataRespository.setAppMask(true)
            throw cidHttpErr
        }
        if(rsltObj.response=="true"){
            // logger.info("Validate access key success!")
			// Service Performance Log
            logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_ICW_VALIDATEACCESSKEY_END, 
                "Invoke 'validateAccessKeyByHostName' completed...[Validate Access Key Success!]"
            )
            return true
        }else {
            // logger.info("Validate access key fail!")
            // this.dataRespository.setMsgDialogByCode(ACCESS_AUTHENTICATION_FAILED)
            // this.dataRespository.setAppMask(true)
            return false
        }
    }

    async validateApplicationStatus(applicationId: string, hospCode: string, applicationVersion: string, userId: string, workstationId: string, requestSys: string, timeout: number): Promise<boolean> {    
        let rsltObj: WsRsltObjType | null = null
        try{
            rsltObj = await callWs(
                this.webSInfo.username, 
                this.webSInfo.password, 
                this.webSInfo.contextPath, 
                ENDPOINT.ICW.VALIDATE_APPLICATION_STATUS,
                {
                    applicationId,
                    hospCode, //userHospitalCode
                    applicationVersion, 
                    userId,
                    workstationId, 
                    requestSys
                },
                timeout
            )
        }catch(cidHttpErr){
            // this.dataRespository.setMsgDialogByCode(EJB_EXCEPTION, error.exceptionMsg)
            // this.dataRespository.setAppMask(true)
            throw cidHttpErr
        }
        if(rsltObj.response=="true"){
            // logger.info("Validate application version success!")
            return true
        }else{
            // logger.info("Validate application version fail!")
            // this.dataRespository.setMsgDialogByCode(APPLICATION_VERSION_FAILED)
            // this.dataRespository.setAppMask(true)
            return false
        }
    }


}
