//@flow

// import models
import WebSInfo from '@domains/model/vo/WebSInfo'

// import constants
import { ENDPOINT } from '@constants/EndpointConstants'
import { RETRIEVE_IMAGE } from '@constants/WordingConstants'
import { LOGGER_NAME } from '@constants/LogConstants'

// import utils
import logger from '@utils/Logger'
import dataRepository from '@utils/DataRepository'
import JsonParser from '@utils/JsonParser'
import { IMAGE_BASE64_HEADERS } from '@utils/Base64Util'
import { callWs, callWsBatch } from '@utils/WebServiceBackbone'
import type { WsRsltObjType } from '@utils/WebServiceBackbone'
import { InnerClass as StateTreeUtilInnerClass } from '@utils/StateTreeUtil'
import viewerLogController from '@domains/controllers/ViewerLogController'
import { maskLastChars } from '@utils/StringUtil'

export default class StudyService {
    iasWsInfo: WebSInfo
    metadataStr: string

    constructor(iasWsInfo: WebSInfo) {
        this.iasWsInfo = iasWsInfo
        this.metadataStr = ''
    }

    static getStudyService(iasWsInfo: WebSInfo): StudyService {
        return new StudyService(iasWsInfo)
    }

    async retrieveStudy(data: *, timeout: number): Promise<RetrieveStudyType> {
        try {
            let retrieveMetadataRsltObj: WsRsltObjType = await this.retrieveMetadata(data, timeout)
            let metadataStr: string = retrieveMetadataRsltObj.response
            this.metadataStr = metadataStr
            let metadata: * = JSON.parse(metadataStr)
            let seriesDtlList: Array<*> = JsonParser.getValue(metadata, 'seriesDtl')
            for(let i: number = 0; i<seriesDtlList.length; i++) {
                let newData: typeof data = Object.assign({}, data)
                newData.seriesNo = seriesDtlList[i].seriesNo
                let imageDtlList: Array<*> = JsonParser.getValue(seriesDtlList[i], 'imageDtl')
                let highestVerMap: {} = StateTreeUtilInnerClass.findHighestVerIds(imageDtlList)
                let highestVerImageDtlList: Array<*> = []
                for(let j: number = 0; j<imageDtlList.length; j++) {
                    if(StateTreeUtilInnerClass.isHighestVerIds(highestVerMap,imageDtlList[j])){
                        highestVerImageDtlList.push(imageDtlList[j])
                    }
                }
                let retrieveImageThumbnailsRsltObjList: Array<WsRsltObjType> = await this.retrieveImageThumbnails(highestVerImageDtlList, newData, timeout)
                retrieveImageThumbnailsRsltObjList.reverse()//Reverse retrieveImageThumbnailsRsltObjList and use pop() to get the first element.
                for(let j: number = 0; j<imageDtlList.length; j++) {
                    if(StateTreeUtilInnerClass.isHighestVerIds(highestVerMap,imageDtlList[j])){
                        metadata.studyDtl.seriesDtls.seriesDtl[i].imageDtls.imageDtl[j].imageThumbnailBase64 = IMAGE_BASE64_HEADERS + retrieveImageThumbnailsRsltObjList.pop().response
                    }
                }
            }
            return metadata
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
    }

    async retrieveMetadata(data: *, timeout: number): Promise<WsRsltObjType> {
		// Service Performance Log
        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_GETCIDSTUDY_START, 
            "Invoke 'retrieveMetadata'...[data=" + JSON.stringify(data) + "]")
			
		let wsRsltObjType: WsRsltObjType
        try {
            wsRsltObjType = await callWs(this.iasWsInfo.username, this.iasWsInfo.password, this.iasWsInfo.contextPath, ENDPOINT.IAS.RETRIEVE_METADATA, data, timeout)
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
        // Service Performance Log
        // Clone wsRsltObjType from the response for Logging
        let metadataStr: string = wsRsltObjType.response
        let metadata: * = JSON.parse(metadataStr)
        // Mask the HKID response.patientDtl.hkid
        let hkid: string = JsonParser.getValue(metadata, "hkid")
        metadata.patientDtl.hkid = maskLastChars(hkid, 3)
        
        //cloneWsRsltObjType.response.patientDtl.hkid = JsonParser.getValue(clone)
        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_GETCIDSTUDY_END, 
            "Invoke 'retrieveMetadata' completed...[RtnObj=" + JSON.stringify(metadata) + "]")
        return wsRsltObjType
    }

    async retrieveImageThumbnails(imageDtlList: Array<*>, data: *, timeout: number): Promise<Array<WsRsltObjType>> {
        try {
            let dataList: Array<typeof data> = []

            for(let j: number = 0; j<imageDtlList.length; j++) {
                let newData: typeof data = Object.assign({}, data)
                newData.imageId = imageDtlList[j].imageID
                newData.imageSeqNo = imageDtlList[j].imageSequence
                newData.versionNo = imageDtlList[j].imageVersion

                dataList.push(newData)
            }

				// Service Performance Log
			logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_GETCIDIMAGETHUMBNAIL_START, 
            "Invoke 'retrieveImageThumbnails'...[dataList=" + JSON.stringify(dataList) + "]")													
            let wsRsltObjType: Array<WsRsltObjType> = await callWsBatch(this.iasWsInfo.username, this.iasWsInfo.password, this.iasWsInfo.contextPath, ENDPOINT.IAS.RETRIEVE_IMAGE_THUMBNAIL, dataList, timeout)
            
            let loggingMsg: string
            if(viewerLogController.getDisableLoggingImageBase64Config()) {
                loggingMsg = "Invoke 'retrieveImageThumbnails' completed...[RtnObjs.size=" + wsRsltObjType.length + "]"
            } else {
                loggingMsg = "Invoke 'retrieveImageThumbnails' completed...[RtnObjs.size=" + wsRsltObjType.length + ", RtnObjs=" + JSON.stringify(wsRsltObjType) + "]"
            }

			// Service Performance Log
			logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_GETCIDIMAGETHUMBNAIL_END, loggingMsg)
            return wsRsltObjType
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
    }

    getMetadataStr(): string {
        return this.metadataStr
    }

    async retrieveImages(imageDtlList: Array<*>, data: *, timeout: number): * {
        try {
            dataRepository.setProgressBar(true, RETRIEVE_IMAGE)

            let dataList: Array<typeof data> = []
            
            for(let i: number = 0; i<imageDtlList.length; i++) {
                let newData: typeof data = Object.assign({}, data)
                    newData.imageId = imageDtlList[i].imageID
                    newData.imageSeqNo = imageDtlList[i].imageSequence
                    newData.versionNo = imageDtlList[i].imageVersion
                dataList.push(newData)
            }
			
			// Service Performance Log
			logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_GETCIDIMAGE_START, 
            "Invoke 'retrieveImages'...[dataList=" + JSON.stringify(dataList) + "]")

            let retrieveImagesRsltObjList: Array<WsRsltObjType> = await callWsBatch(this.iasWsInfo.username, this.iasWsInfo.password, this.iasWsInfo.contextPath, ENDPOINT.IAS.RETRIEVE_IMAGE, dataList, timeout)
            
            let loggingMsg: string
            if(viewerLogController.getDisableLoggingImageBase64Config()) {
                loggingMsg = "Invoke 'retrieveImages' completed...[RtnObjs.Size=" + retrieveImagesRsltObjList.length + "]"
            } else {
                loggingMsg = "Invoke 'retrieveImages' completed...[RtnObjs.Size=" + retrieveImagesRsltObjList.length + ", RtnObjs=" + JSON.stringify(retrieveImagesRsltObjList) + "]"
            }

			// Service Performance Log
			logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_GETCIDIMAGE_END, loggingMsg)
			
			let imagesResult: Array<*> = []
            for(let i: number = 0; i<retrieveImagesRsltObjList.length; i++) {
                imagesResult.push(retrieveImagesRsltObjList[i].response)
            }
			
            dataRepository.setProgressBar(false, RETRIEVE_IMAGE)
            
            return imagesResult
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
    }

    async retrieveImageForExport(data: *, timeout: number): Promise<WsRsltObjType> {
        try {
            return await callWs(this.iasWsInfo.username, this.iasWsInfo.password, this.iasWsInfo.contextPath, ENDPOINT.IAS.EXPORT_IMAGE, data, timeout)
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
    }
}

export type RetrieveStudyType = {
    messageDtl: *,
    visitDtl: *,
    patientDtl: *,
    studyDtl: *
}