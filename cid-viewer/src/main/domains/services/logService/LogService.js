//@flow

// import utils
import { callWs } from '../../../utils/WebServiceBackbone'
import type { WsRsltObjType } from '../../../utils/WebServiceBackbone'
import { clone } from '../../../utils/CloneUtil'
import WebSInfo from '@domains/model/vo/WebSInfo'

class LogService {

    async icwWriteLog(icwConfig: WebSInfo, endPoint: string, data: *, timeout: number): Promise<WsRsltObjType>{
        let eventValue: Array<{key: string, value: string}> = []
        let reqData = clone(data)

        eventValue[0] = {
            key: '<AccessionNo>',
            value: reqData.accessionNo
        }
        eventValue[1] = {
            key: '<SeriesNo>',
            value: reqData.seriesSeqNo
        }
        eventValue[2] = {
            key: '<ImageSeqNo>',
            value: reqData.imageSeqNo
        }
        eventValue[3] = {
            key: '<VersionNo>',
            value: reqData.versionNo
        }
        reqData.eventValue = eventValue
        delete reqData.seriesSeqNo
        delete reqData.imageSeqNo
        delete reqData.versionNo

        return await callWs(icwConfig.username, icwConfig.password, icwConfig.contextPath, endPoint, reqData, timeout)
    }
}

let logService = new LogService()

export default logService