// @flow
import { getJsonFile } from '@utils/WebServiceBackbone'

class FileReader{
    readFile: Function

    constructor() {
        this.readFile = this.readFile.bind(this)
    }

    async readFile(path: string, timeout: number): * {
        try {
            let configJson = await getJsonFile(path, timeout)
            return configJson
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
    }
}

export let fileReader = new FileReader()
