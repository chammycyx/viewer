// @flow

import {getUrlParameter} from '@utils/HttpUtil'
import CidMagicUtil from '@utils/CidMagicUtil'
import {CPKC} from '@constants/CPKC'

import CidError from '@components/base/CidError'

import {HANDSHAKE_TOKEN} from '@constants/TokenConstants'
import { UNABLE_TO_READ_CASE_PROFILE } from '@constants/WordingConstants'

class CaseProfileReader{
    validateOpener(): boolean{
        return (window.opener==null) ? false : true
    }

    validateCaseProfile(caseProfile: CaseProfileType): boolean {
        let isValid = true

        if(caseProfile.systemID=='') isValid = false
        if(caseProfile.accessKey=='') isValid = false
        if(caseProfile.loginID=='') isValid = false
        if(caseProfile.userHospCde=='') isValid = false
        //if(caseProfile.wrkStnID=='') isValid = false
        if(caseProfile.hospCde=='') isValid = false
        if(caseProfile.patientKey=='') isValid = false
        // if(caseProfile.caseNo=='') isValid = false
        if(caseProfile.accessionNo=='') isValid = false

        return isValid
    }

    pretendOpener(){
        if(process.env.NODE_ENV=='development'){
        // By using process.env.NODE_ENV=='development', this branch would be removed by webpack automatically in prod build 

            //pretend the consumer - ePR
            //     window.addEventListener('message', function(event: *){
            //         let data = event.data
            //         if(data==HANDSHAKE_TOKEN){
            //             let msgToSend = {
            //                 token: HANDSHAKE_TOKEN,
            //                 caseProfile: {
            //                     // hospCde: 'VH',
            //                     hospCde: 'QEH',
            //                     caseNo: 'HN05000207X',
            //                     // accessionNo: 'VH ER1800000530X',
            //                     accessionNo: 'QEHER1800000043F',
            //                     seriesSeqNo: 'e27f34a9-66a7-439f-97bc-a913c3b8d37f',
            //                     imageSeqNo : '',
            //                     versionNo: '',
            //                     loginID: 'ISG',
            //                     wrkStnID: 'dc1cidst02',
            //                     systemID: 'RIS',
            //                     accessKey: '1FD2B404-662E-48C2-8351-BDF2C40E8711',
            //                     userHospCde: 'VH',
            //                     patientKey: '90500231',
            //                     specialtyCde: '',
            //                     appHost: 'http://localhost:7778',
            //                     appSiteName: '',
            //                     appName: 'CID Image Viewer II',
            //                     wrkStnHostname: 'dc1cidst02'
            //                 }
            //             }
            //             window.postMessage(msgToSend, "*")
            //         }
            //     })
            // }
            const msgToSend = {
                caseProfile: {
                    hospCde: 'VH',
                    // hospCde: 'QEH',
                    caseNo: 'HN05000207X',
                    // accessionNo: 'VH ER1800000530X',
                    // accessionNo: 'QEHER1800000043F',
                    // accessionNo: 'VH EH9627843015J', // 6 pics
                    accessionNo: 'VH XR1900000095U', // 25 real pics
                    // accessionNo: 'VH ER1900000014W', // missing img
                    // accessionNo: 'VH ER1800000178Z', // 4 big pics
                    // accessionNo: 'VH ER1800000239W', // William 1234
                    // accessionNo: 'VH ER1800000530X', // Patrick desk
                    // accessionNo: 'VH ER1803842365S',
                    // accessionNo: 'VH DY1542389607O', // SIT3
                    // accessionNo: 'QEHER1800000031E', // SIT3 25 images
                    seriesSeqNo: '256D3273-0A5F-E6C9-87B4-1C65758E6B6D',
                    imageSeqNo : '',
                    versionNo: '',
                    loginID: 'ISG',
                    wrkStnID: 'dc1cidst02',
                    systemID: 'RIS',
                    // accessKey: '1FD2B404-662E-48C2-8351-BDF2C40E8711',
                    userHospCde: 'VH',
                    patientKey: '90500231',
                    specialtyCde: '',
                    appHost: 'http://localhost:7778',
                    appSiteName: '',
                    appName: 'CID Image Viewer II',
                    wrkStnHostname: 'dc1cidst02',
                    sessionID: '358c4ca8-c0a8-4915-ab0a-7fabc475dcxx',
                    timeToken: new Date().getTime(),
                    repositionPara: {
                        cmsWindowX: 0,
                        cmsWindowY: 0,
                        cmsWindowcenterX: 0,
                        isMainDisplayPortrait: false
                    }
                }
            }
            const hash = encodeURIComponent(CidMagicUtil.magicByECB(JSON.stringify(msgToSend), CPKC.CPEK))
            window.name = '1FD2B404-662E-48C2-8351-BDF2C40E8711'
            document.location.hash = hash
        }
    }

    readCaseProfileFromHash(): *{
        try{
            let hash = location.hash
                hash = hash.substring(1, hash.length)
            let decodedHash = JSON.parse(CidMagicUtil.unmagicByECB(decodeURIComponent(hash), CPKC.CPEK))
                decodedHash.caseProfile.accessKey = window.name
            return decodedHash
        }catch(err){
            throw new CidError('src.main.domains.readers.CaseProfileReader', 'readCaseProfileFromHash()', UNABLE_TO_READ_CASE_PROFILE.content)
        }
    }

    async readCaseProfileFromPostMsg(): *{
        let promiseAwait: Function = function(): Promise<void>{
            let promise = new Promise(function(resolve: Function, reject: Function){
                window.addEventListener('message', function(event: *){
                    let data = event.data
                    if(data.token == HANDSHAKE_TOKEN){
                        resolve(data)
                    }
                })
            })
            return promise
        }
        window.opener.postMessage(HANDSHAKE_TOKEN, "*")

        let result = await promiseAwait()
        return result
    }

    readCaseProfileFromQueryString(): *{
        return {
            systemID: getUrlParameter('systemID'),
            accessKey: getUrlParameter('accessKey'),
            loginID: getUrlParameter('loginID'),
            userHospCde: getUrlParameter('userHospCde'),
            wrkStnID: getUrlParameter('wrkStnID'),
            hospCde: getUrlParameter('hospCde'),
            patientKey: getUrlParameter('patientKey'),
            specialtyCde: getUrlParameter('specialtyCde'),
            caseNo: getUrlParameter('caseNo'),
            accessionNo: getUrlParameter('accessionNo'),
            seriesSeqNo: getUrlParameter('seriesSeqNo'),
            imageSeqNo: getUrlParameter('imageSeqNo'),
            versionNo: getUrlParameter('versionNo'),
            appHost: getUrlParameter('appHost'),
            appSiteName: getUrlParameter('appSiteName'),
            appName: getUrlParameter('appName'),
            wrkStnHostname: getUrlParameter('wrkStnHostname'),
            sessionID: getUrlParameter('sessionID')
        }
    }
}

export let caseProfileReader = new CaseProfileReader()

export type CaseProfileType = {
    systemID: string,
    accessKey: string, 
    loginID: string,
    userHospCde: string, 
    wrkStnID: string, 
    hospCde: string, 
    patientKey: string, 
    specialtyCde: string, 
    caseNo: string, 
    accessionNo: string,
    seriesSeqNo: string,
    imageSeqNo: string, 
    versionNo: string, 
    appHost: string,
    appSiteName: string, 
    appName: string,
    wrkStnHostname: string,
    timeToken: string,
    sessionID: string,
    repositionPara: RepositionParaType
}

export type RepositionParaType = {
    cmsWindowX: number,
    cmsWindowY: number,
    cmsWindowcenterX: number,
    isMainDisplayPortrait: boolean
}
