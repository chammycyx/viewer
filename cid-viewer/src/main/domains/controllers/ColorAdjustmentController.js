// @flow

import dataRepository from '@utils/DataRepository'
import { COLOR_ADJUSTMENT_SINGLE_IMAGE } from '@constants/MsgCodeConstants'
import logger from '@src/main/utils/Logger';

export default class ColorAdjustmentController {
    onColorAdjustmentButtonClick() {
        
        const selectedImageIds = dataRepository.findSelectedImageIds()
        const maxNumOfImage = dataRepository.findColorAdjustmentMaxNumOfImage()
        if(selectedImageIds!=undefined && selectedImageIds!=null && selectedImageIds.length<=maxNumOfImage) {
            dataRepository.setColorAdjustmentPropsBoxVisible(true)
        } else {
            dataRepository.setMsgDialogByCode(COLOR_ADJUSTMENT_SINGLE_IMAGE)
            logger.warnWithMsgCode(COLOR_ADJUSTMENT_SINGLE_IMAGE)    
        }
    }
}