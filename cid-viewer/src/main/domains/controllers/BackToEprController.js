//@flow
import logger from '@utils/Logger'
import { LOGGER_NAME } from '@constants/LogConstants'

class BackToEprController {
    backToEpr(){
        logger.userActions(LOGGER_NAME.CID_USERACTIONS_BACKTOEPR, "backToEpr...")
        if(window.opener != null)
        {
            window.opener.focus();
            window.blur();
        }
        else
        {
            window.blur();
        }
    }
}

let backToEprController = new BackToEprController()

export default backToEprController