//@flow
import XRegExp from 'xregexp'

import { CommonLogController } from '@domains/controllers/rootApp/CommonLogController'
import logger from '@utils/Logger'
// import Constant
import { LOGGER_NAME } from '@constants/LogConstants'

export class ViewerLogController extends CommonLogController
{
    handleLogEx: Function
    regExpReplacement: Function
    constructor() {
        super()
        this.handleLogEx = this.handleLogEx.bind(this)
        this.regExpReplacement = this.regExpReplacement.bind(this)
    }

    handleLogEx(loggerName: string, level: number, message: *, msgCode?: string) { 
        //ImageId and SeriesId conversion
        if(message != null && message != undefined) {
            if(loggerName==LOGGER_NAME.CID || loggerName==LOGGER_NAME.CID_EXCEPTION || loggerName==LOGGER_NAME.CID_EXCEPTION_MSGCODE){

                message = XRegExp.replaceEach(message, this.getRegExpReplacement())
            }
        }
        super.handleLog(loggerName, level, message, msgCode)
    }

    regExpReplacement(output: string, startPosition: number, inputString: string): ?string {
        try {
            let replaceStr = output

            if(output.indexOf("seriesDtl_") > -1) {
                const seriesDtl = this.dataRepository.findSeriesDtl(output)
                replaceStr = seriesDtl.seriesNo
            } else if(output.indexOf("imageDtl_") > -1) {
                let seriesIndexId = XRegExp.match(inputString, new XRegExp("seriesDtl_\\d"), 'one')
                if(seriesIndexId != undefined) {
                    replaceStr = this.dataRepository.findImageId(seriesIndexId, output)
                } else {
                    replaceStr = this.dataRepository.findImageIdByImageIdxId(output)
                }
            } else if(output.indexOf("data:image") > -1) {
                replaceStr = "data:image......\""
            } else if(output.indexOf("base64s") > -1) {
                replaceStr = "base64s:[......]"
            } else if(output.indexOf("exportImageBase64") > -1) {
                replaceStr = "exportImageBase64\":\"......\""
            }
            return replaceStr
        } catch(ex) {
            logger.error(ex)
        }
    }

    getRegExpReplacement(): Array<*> {
        let regExpReplacementArr: Array<*> = []

        regExpReplacementArr.push([new XRegExp("imageDtl_\\d"), this.regExpReplacement])
        regExpReplacementArr.push([new XRegExp("seriesDtl_\\d"), this.regExpReplacement])

        if(this.disableLoggingImageBase64) {
            regExpReplacementArr.push([new XRegExp("data:image(.*?\")"), this.regExpReplacement, 'all'])
            regExpReplacementArr.push([new XRegExp("base64s(.*?\"])"), this.regExpReplacement, 'all'])
            regExpReplacementArr.push([new XRegExp("exportImageBase64\":\"(.*?\")"), this.regExpReplacement, 'all'])
        }
        return regExpReplacementArr
    }
}

let viewerLogController = new ViewerLogController()

export default viewerLogController