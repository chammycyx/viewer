// @flow

// import utils
import { ThumbnailGridActionTypes } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { SlideShowToolsActionTypes } from '@actions/components/toolBarPanel/slideShowTools/Action'
import { BtnStateWatcherActionTypes } from '@actions/components/root/watcher/btnStateWatcher/Action'
import dataRepository from '@utils/DataRepository'

//export to ToolbarPanel reducer for initState
export const initBtnState: * = {
    deselectBtnState: false,
    zoomInBtnState: false,
    zoomOutBtnState: false,
    viewModeBtnState: true,
    rotateLeftBtnState: false,
    rotateRightBtnState: false,
    flipVerticalBtnState: false,
    flipHorizonalBtnState: false,
    contrastBtnState: false,
    previousBtnState: false,
    nextBtnState: false,
    pauseBtnState: false,
    stopBtnState: false,
    playBtnState: false,
    fpsBtnState: false,
    resetBtnState: false,
    exportBtnState: false,
    printBtnState: false,
    backToEPRBtnState: true
}

export const normalBtnState: * = {
    deselectBtnState: true,
    zoomInBtnState: true,
    zoomOutBtnState: true,
    viewModeBtnState: true,
    rotateLeftBtnState: true,
    rotateRightBtnState: true,
    flipVerticalBtnState: true,
    flipHorizonalBtnState: true,
    contrastBtnState: true,
    previousBtnState: false,
    nextBtnState: false,
    pauseBtnState: false,
    stopBtnState: false,
    playBtnState: true,
    fpsBtnState: true,
    resetBtnState: true,
    exportBtnState: true,
    printBtnState: true,
    backToEPRBtnState: true
}

export const playSlideshowBtnState: * = {
    deselectBtnState: false,
    zoomInBtnState: false,
    zoomOutBtnState: false,
    viewModeBtnState: false,
    rotateLeftBtnState: false,
    rotateRightBtnState: false,
    flipVerticalBtnState: false,
    flipHorizonalBtnState: false,
    contrastBtnState: false,
    previousBtnState: false,
    nextBtnState: false,
    pauseBtnState: true,
    stopBtnState: true,
    playBtnState: false,
    fpsBtnState: false,
    resetBtnState: false,
    exportBtnState: false,
    printBtnState: false,
    backToEPRBtnState: true
}

export const pauseSlideshowBtnState: * = {
    deselectBtnState: false,
    zoomInBtnState: false,
    zoomOutBtnState: false,
    viewModeBtnState: false,
    rotateLeftBtnState: false,
    rotateRightBtnState: false,
    flipVerticalBtnState: false,
    flipHorizonalBtnState: false,
    contrastBtnState: false,
    previousBtnState: true,
    nextBtnState: true,
    pauseBtnState: false,
    stopBtnState: true,
    playBtnState: true,
    fpsBtnState: true,
    resetBtnState: false,
    exportBtnState: false,
    printBtnState: false,
    backToEPRBtnState: true
}

export const SlideshowMode_multiSelectedImageBtnState: * = {
    deselectBtnState: false,
    zoomInBtnState: false,
    zoomOutBtnState: false,
    viewModeBtnState: false,
    rotateLeftBtnState: false,
    rotateRightBtnState: false,
    flipVerticalBtnState: false,
    flipHorizonalBtnState: false,
    contrastBtnState: false,
    previousBtnState: true,
    nextBtnState: true,
    pauseBtnState: false,
    stopBtnState: true,
    playBtnState: false,
    fpsBtnState: false,
    resetBtnState: false,
    exportBtnState: false,
    printBtnState: false,
    backToEPRBtnState: true
}

export const SlideshowMode_deselectedImageBtnState: * = {
    deselectBtnState: false,
    zoomInBtnState: false,
    zoomOutBtnState: false,
    viewModeBtnState: false,
    rotateLeftBtnState: false,
    rotateRightBtnState: false,
    flipVerticalBtnState: false,
    flipHorizonalBtnState: false,
    contrastBtnState: false,
    previousBtnState: true,
    nextBtnState: true,
    pauseBtnState: false,
    stopBtnState: true,
    playBtnState: false,
    fpsBtnState: false,
    resetBtnState: false,
    exportBtnState: false,
    printBtnState: false,
    backToEPRBtnState: true
}

export const multiSelectedImageBtnState: * = {
    deselectBtnState: true,
    zoomInBtnState: true,
    zoomOutBtnState: true,
    viewModeBtnState: true,
    rotateLeftBtnState: true,
    rotateRightBtnState: true,
    flipVerticalBtnState: true,
    flipHorizonalBtnState: true,
    contrastBtnState: true,
    previousBtnState: false,
    nextBtnState: false,
    pauseBtnState: false,
    stopBtnState: false,
    playBtnState: false,
    fpsBtnState: false,
    resetBtnState: true,
    exportBtnState: true,
    printBtnState: true,
    backToEPRBtnState: true
}

export type PayloadType = {
    numOfSelectImage: number,
    slideShowMode: boolean,
    isPlayingslideShow: boolean
}

export class ButtonStateController{
    setToolbarBtnState(event: string, value: PayloadType) {
        let btnState = null
        switch (event) {
            case BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED:
                if(value.isPlayingslideShow){
                    //Playing slide show
                    btnState = playSlideshowBtnState
                }else if(value.slideShowMode){
                    //Slide show mode without playing slide show (Pause)
                    if(value.numOfSelectImage==0)
                        btnState = SlideshowMode_deselectedImageBtnState
                    else if(value.numOfSelectImage>1)
                        btnState = SlideshowMode_multiSelectedImageBtnState
                    else //(value.numOfSelectedImage==1)
                        btnState = pauseSlideshowBtnState
                }else{ //Non-slideShow mode
                    if(value.numOfSelectImage==0)
                        btnState = initBtnState
                    else if(value.numOfSelectImage>1)
                        btnState = multiSelectedImageBtnState
                    else //(value.numOfSelectedImage==1)
                        btnState = normalBtnState
                }
                break;
            case ThumbnailGridActionTypes.ON_THUMBNAIL_CLICK:
            case SlideShowToolsActionTypes.STOP:
                btnState = normalBtnState
                break;
            case SlideShowToolsActionTypes.PLAY:
                btnState = playSlideshowBtnState
                break;
            case SlideShowToolsActionTypes.PAUSE:
                btnState = pauseSlideshowBtnState
                break;
            default:
            //Return null for checking if btnState remain unchanged in rootSaga#controlButtonState
                btnState = null
                break;
        }
        if(btnState!=null){
            dataRepository.setToolbarBtnState(btnState)
        }
    }
}