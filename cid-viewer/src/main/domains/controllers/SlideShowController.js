// @flow
// import utils
import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'

// import constants
import { LOGGER_NAME } from '@constants/LogConstants'

// import controller
import { PictorialIndexPanelController } from '@domains/controllers/PictorialIndexPanelController'
import { ViewingPanelController } from '@src/main/domains/controllers/ViewingPanelController'
import WindowResizeController from '@domains/controllers/rootApp/WindowResizeController'

// import actions
import type { ViewModeType } from '@src/main/actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

export class SlideShowController{
    dataRepository: typeof dataRepository
    pictorialIndexPanelController: PictorialIndexPanelController
    viewingPanelController: ViewingPanelController
    playSlideShow: Function
    pauseSlideShow: Function
    stopSlideShowAndUpdateShowImages: Function
    stopSlideShow: Function
    setUpSlideShow: Function
    slideShowPlayer: Function
    resetSlideShowProfiles: Function
    updateShowImagesAndSelectCanvasByCurrentSlide: Function
    switchSlide: Function
    getViewMode: Function
    getShowImageIds: Function
    getShowImageIdsBySeqFirstImage: Function
    getAllImageIds: Function
    getSeqCanvasByImageIndexId: Function
    getSeqSlideByImageIndexId: Function
    getSeqSlideFirstImage: Function
    getImageIndexIdBySeqCanvas: Function
    getImageIndexIdBySeqSlide: Function
    incrementSlide: Function
    initSlideShow: Function
    showSlide: Function
    updateShowImagesBySelectingThumbnails: Function
    setMode: Function
    setPlaying: Function
    setSeqCanvas: Function
    setTimer: Function
	isFirstPause: boolean
    selectedSeriesIndexId: string
    selectedSeriesId: string
    startSlideShowTimer: Function
    stopSlideShowTimer: Function
    windowResizeController: WindowResizeController

    constructor(pictorialIndexPanelController: PictorialIndexPanelController, viewingPanelController: ViewingPanelController) {
        this.isFirstPause = false
		this.dataRepository = dataRepository
        this.pictorialIndexPanelController = pictorialIndexPanelController
        this.viewingPanelController = viewingPanelController
        this.playSlideShow = this.playSlideShow.bind(this)
        this.pauseSlideShow = this.pauseSlideShow.bind(this)
        this.stopSlideShowAndUpdateShowImages = this.stopSlideShowAndUpdateShowImages.bind(this)
        this.stopSlideShow = this.stopSlideShow.bind(this)
        this.setUpSlideShow = this.setUpSlideShow.bind(this)
        this.slideShowPlayer = this.slideShowPlayer.bind(this)
        this.resetSlideShowProfiles = this.resetSlideShowProfiles.bind(this)
        this.updateShowImagesAndSelectCanvasByCurrentSlide = this.updateShowImagesAndSelectCanvasByCurrentSlide.bind(this)
        this.switchSlide = this.switchSlide.bind(this)
        this.getViewMode = this.getViewMode.bind(this)
        this.getShowImageIds = this.getShowImageIds.bind(this)
        this.getShowImageIdsBySeqFirstImage = this.getShowImageIdsBySeqFirstImage.bind(this)
        this.getAllImageIds = this.getAllImageIds.bind(this)
        this.getSeqCanvasByImageIndexId = this.getSeqCanvasByImageIndexId.bind(this)
        this.getSeqSlideByImageIndexId = this.getSeqSlideByImageIndexId.bind(this)
        this.getSeqSlideFirstImage = this.getSeqSlideFirstImage.bind(this)
        this.getImageIndexIdBySeqCanvas = this.getImageIndexIdBySeqCanvas.bind(this)
        this.getImageIndexIdBySeqSlide = this.getImageIndexIdBySeqSlide.bind(this)
        this.incrementSlide = this.incrementSlide.bind(this)
        this.initSlideShow = this.initSlideShow.bind(this)
        this.showSlide = this.showSlide.bind(this)
        this.updateShowImagesBySelectingThumbnails = this.updateShowImagesBySelectingThumbnails.bind(this)
        this.setMode = this.setMode.bind(this)
        this.setPlaying = this.setPlaying.bind(this)
        this.setSeqCanvas = this.setSeqCanvas.bind(this)
        this.setTimer = this.setTimer.bind(this)
        this.startSlideShowTimer = this.startSlideShowTimer.bind(this)
        this.stopSlideShowTimer = this.stopSlideShowTimer.bind(this)
    }

    setWindowResizeController(windowResizeController: WindowResizeController) {
        this.windowResizeController = windowResizeController
    }

    // Slide Show - PLAY
    async playSlideShow(fps: number): Promise<void> {

        // initialize slide show in the selected canvas
        await this.initSlideShow(fps)

        // set up slide show player
        this.setMode(true)
        this.setPlaying(true)

        await this.setUpSlideShow(fps)
    }


    // Slide Show - PAUSE
    async pauseSlideShow(): Promise<void> {
        this.setPlaying(false)
    }


    // Slide Show - STOP
    async stopSlideShowAndUpdateShowImages(): Promise<void>{
        this.stopSlideShow()

        await this.updateShowImagesAndSelectCanvasByCurrentSlide()

        this.windowResizeController.resizeApp()
    }

    stopSlideShow() {
        this.setMode(false)
        this.setPlaying(false)
    }


    // Initialize slide show
    async initSlideShow(fps: number): Promise<void> {

        // reset pause flag (for logging purpose)
        if(!this.isFirstPause)
            this.isFirstPause = true

        let canvasGridTree = this.dataRepository.findCanvasGrid()
        this.selectedSeriesIndexId = canvasGridTree.selectedSeriesId

        // sequence of selected canvas within shown images
        let selectedImageIndexId = canvasGridTree.selectedImageIds[0]
        let seqCanvas = this.getSeqCanvasByImageIndexId(selectedImageIndexId)
        this.setSeqCanvas(seqCanvas)

        // series id mapping
        let seriesDtl: * = this.dataRepository.findSeriesDtl(this.selectedSeriesIndexId)
        this.selectedSeriesId = seriesDtl.seriesNo
        // image id mapping
        let imageId = this.dataRepository.findImageId(this.selectedSeriesIndexId, selectedImageIndexId)

		// log User Actions SlideShow Play
        logger.userActions(LOGGER_NAME.CID_USERACTIONS_SLIDESHOW_PLAY, 
            "setSlideShow...[seqCanvas="+seqCanvas+"|fps="+fps
                +"|seriesId="+this.selectedSeriesId+"|imageId="+imageId+"]")

        logger.userActionsPerf(LOGGER_NAME.CID_USERACTIONS_PERF_SLIDESHOW_PLAY_START, 
            "setSlideShow...[seqCanvas="+seqCanvas+"|fps="+fps
                +"|seriesId="+this.selectedSeriesId+"|imageId="+imageId+"]")

        // retrieve and save images (all)
        await this.viewingPanelController.retrieveNSaveImages(this.selectedSeriesIndexId)

        // initialize first slide
        if(selectedImageIndexId.indexOf('_SlideShow') < 0) {
            this.dataRepository.setSlideShowToolsRefOffsets(seqCanvas, canvasGridTree.canvasWidth, canvasGridTree.canvasHeight)
            await this.resetSlideShowProfiles(seqCanvas)
            await this.showSlide(SlideShowController.getImageIndexIdWithSuffix(selectedImageIndexId, seqCanvas), seqCanvas)
        }
    }


	// Set up slide show with timer
    async setUpSlideShow(fps: number): Promise<void> {
        // sequence of selected canvas
        let slideShowToolsTree = this.dataRepository.findSlideShowTools()
        let seqCanvas = slideShowToolsTree.seqCanvas

        // calculate timer for slide show by fps
        let timing = 1000 / fps

        // set up interval timer for playing slide show
        this.startSlideShowTimer(timing, seqCanvas)

        logger.userActions(LOGGER_NAME.CID_USERACTIONS_PERF_SLIDESHOW_PLAY_END, 
            "setSlideShow...End")
    }


    // Timer function for playing slide show in specific canvas
    async slideShowPlayer(seqCanvas: number): * {

        let slideShowToolsTree = this.dataRepository.findSlideShowTools()
        
        let imageIndexId4NextSlide = this.getImageIndexIdBySeqCanvas(seqCanvas)
        let imageIndexId = SlideShowController.getImageIndexIdWithoutSuffix(imageIndexId4NextSlide)
        
        // Proceeds slide if slide show is ON, 
        // otherwise, stop the slide show and reset profiles
        if(slideShowToolsTree.isModeOn) {
            
            imageIndexId4NextSlide = this.incrementSlide(imageIndexId, +1)
            imageIndexId4NextSlide = SlideShowController.getImageIndexIdWithSuffix(imageIndexId4NextSlide, seqCanvas)
            
            if(slideShowToolsTree.isPlaying) {
                await this.showSlide(imageIndexId4NextSlide, seqCanvas)
            } else {
                // log User Actions Pause SlideShow
                if(this.isFirstPause) {
                    this.isFirstPause = false
                    // log User Actions SlideShow PAUSE
                    let imageId = this.dataRepository.findImageId(this.selectedSeriesIndexId, imageIndexId)                       
                    logger.userActions(LOGGER_NAME.CID_USERACTIONS_SLIDESHOW_PAUSE, 
                        "playSlideShow (PAUSE)...[seqCanvas="+seqCanvas
                            +"|seriesId="+this.selectedSeriesId+"|imageId="+imageId+"]")
                }
            }
        } else {
            this.stopSlideShowTimer()
            this.isFirstPause = true
        }
    }

    
    // Reset slide show profiles properties
    async resetSlideShowProfiles(seqCanvas?: number = -1): Promise<void>{
        await this.viewingPanelController.resetSlideShowImages(seqCanvas)
    }

    
    // Stop slide show and update viewing panel
    async updateShowImagesAndSelectCanvasByCurrentSlide(): Promise<void> {
        
        let slideShowToolsTree = this.dataRepository.findSlideShowTools()
        let seqCanvas = slideShowToolsTree.seqCanvas
		let canvasGridTree: * = this.dataRepository.findCanvasGrid()
        let selectedSeriesIndexId: string = canvasGridTree.selectedSeriesId
        let seriesDtl: * = this.dataRepository.findSeriesDtl(selectedSeriesIndexId)
        
        if (seqCanvas > -1) {
            
            let imageIndexId4SlideShow = this.getImageIndexIdBySeqCanvas(seqCanvas)
            let imageIndexId = SlideShowController.getImageIndexIdWithoutSuffix(imageIndexId4SlideShow)
            
            // get the sequence of the first show image based on the slide when stopped
            let seqFirstImage = this.getSeqSlideFirstImage(imageIndexId, seqCanvas)

            // get the list of show images based on the first show image sequence
            let showImageIds = this.getShowImageIdsBySeqFirstImage(seqFirstImage)
			
			let imageId = this.dataRepository.findImageId(selectedSeriesIndexId, showImageIds[0])
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_SLIDESHOW_STOP, 
                "playSlideShow (STOP)...[seqCanvas="+seqCanvas
                    +"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")

            // update images by selecting the first image in Pictorial thumbnails
            await this.updateShowImagesBySelectingThumbnails(showImageIds[0])

            // select the canvas of the slide show after update if position remains unchanged
            if(imageIndexId == showImageIds[seqCanvas]) {
                this.viewingPanelController.selectCanvas(imageIndexId, "")
            }
            
            this.setSeqCanvas(-1)
            
        }
    }


    // Switch slides by certain number
    async switchSlide(noOfSlide: number): Promise<void> {

        let canvasGridTree = this.dataRepository.findCanvasGrid()
        let selectedImageIndexIds = canvasGridTree.selectedImageIds.slice()
		let selectedSeriesIndexId = canvasGridTree.selectedSeriesId
        let seriesDtl = this.dataRepository.findSeriesDtl(selectedSeriesIndexId)
        let selectedSeriesId = seriesDtl.seriesNo

        for(let i = 0; i < selectedImageIndexIds.length; i++) {

            // Only canvas with slide shows will switch slides
            if(selectedImageIndexIds[i].indexOf('_SlideShow') < 0)
                continue
        
            let seqCanvas = this.getSeqCanvasByImageIndexId(selectedImageIndexIds[i])
            let imageIndexId = SlideShowController.getImageIndexIdWithoutSuffix(selectedImageIndexIds[i])
            imageIndexId = this.incrementSlide(imageIndexId, noOfSlide)
			let imageId = this.dataRepository.findImageId(selectedSeriesIndexId, imageIndexId)
            imageIndexId = SlideShowController.getImageIndexIdWithSuffix(imageIndexId, seqCanvas)

			if(noOfSlide == 1) {
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_SLIDESHOW_NEXT, 
                    "switchSlide (NEXT)...[seqCanvas="+seqCanvas
                        +"|seriesId="+selectedSeriesId+"|imageId="+imageId+"]")
            } else if(noOfSlide == -1) {
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_SLIDESHOW_PREVIOUS, 
                    "switchSlide (PREVIOUS)...[seqCanvas="+seqCanvas
                        +"|seriesId="+selectedSeriesId+"|imageId="+imageId+"]")
            }
            await this.showSlide(imageIndexId, seqCanvas)

            selectedImageIndexIds[i] = imageIndexId

        }

        // re-select the canvas after show images
        for(let i = 0; i < selectedImageIndexIds.length; i++) {    
            let button = (i==0)?"":"CTRL"
            this.viewingPanelController.selectCanvas(selectedImageIndexIds[i], button)
        }
    }


    getViewMode(): ViewModeType {
        let canvasGridTree = this.dataRepository.findCanvasGrid()
        let viewMode = canvasGridTree.viewMode

        return viewMode
    }

    getShowImageIds(): Array<string> {
        let canvasGridTree = this.dataRepository.findCanvasGrid()
        let showImageIds = canvasGridTree.showImageIds

        return showImageIds.slice()
    }

    getShowImageIdsBySeqFirstImage(seqFirstImage: number): Array<string> {
        let showImageIds = this.getShowImageIds()

        for(let i = 0; i < showImageIds.length; i++) {
            let seqSlide = (seqFirstImage + i) % this.getAllImageIds().length
            showImageIds[i] = this.getImageIndexIdBySeqSlide(seqSlide)
        }

        return showImageIds
    }
    
    getAllImageIds(): Array<string> {
        let canvasGridTree = this.dataRepository.findCanvasGrid()
        let seriesDtlTree = this.dataRepository.findSeriesDtl(canvasGridTree.selectedSeriesId)

        let allImageIds = seriesDtlTree.imageDtls.allHighestVerIds

        return allImageIds.slice()
    }

    getSeqCanvasByImageIndexId(imageIndexId: string): number {
        let showImageIds = this.getShowImageIds()

        return showImageIds.indexOf(imageIndexId)
    }

    getSeqSlideByImageIndexId(imageIndexId: string): number {
        let allImageIds = this.getAllImageIds()

        return allImageIds.indexOf(imageIndexId)
    }

    getSeqSlideFirstImage(imageIndexId: string, seqCanvas: number): number {
        let seqSlide = this.getSeqSlideByImageIndexId(imageIndexId)

        let seqFirstImage = seqSlide - seqCanvas
        return (seqFirstImage < 0)?0:seqFirstImage
    }

    static getImageIndexIdWithoutSuffix(imageIndexId4SlideShow: string): string {
        let imageIndexId = imageIndexId4SlideShow.valueOf()

        let canvasSuffixPos = imageIndexId4SlideShow.indexOf('_Canvas')
        if(canvasSuffixPos > 0) {
            imageIndexId = imageIndexId4SlideShow.substring(0, canvasSuffixPos)
        }

        return imageIndexId
    }

    static getImageIndexIdWithSuffix(imageIndexId: string, seqCanvas: number): string {
        let imageIndexId4SlideShow = imageIndexId.valueOf()

        return imageIndexId4SlideShow.concat('_Canvas_' + seqCanvas + '_SlideShow')
    }

    getImageIndexIdBySeqCanvas(seqCanvas: number): string {
        let showImageIds = this.getShowImageIds()
        
        return showImageIds[seqCanvas].valueOf()
    }

    getImageIndexIdBySeqSlide(seqSlide: number): string {
        let allImageIds = this.getAllImageIds()

        return allImageIds[seqSlide].valueOf()
    }


    incrementSlide(currentImageIndexId: string, increments: number): string {
        let imageIndexId = currentImageIndexId.valueOf()
        let seqSlide = this.getSeqSlideByImageIndexId(imageIndexId)
        seqSlide = (seqSlide + increments + this.getAllImageIds().length) % this.getAllImageIds().length
        imageIndexId = this.getImageIndexIdBySeqSlide(seqSlide)
        
        return imageIndexId
    }

    async showSlide(imageIndexId: string, seqCanvas: number): Promise<void> {
        let canvasGridTree = this.dataRepository.findCanvasGrid()
        let showImageIds = this.getShowImageIds()
        showImageIds[seqCanvas] = imageIndexId
        await this.viewingPanelController.showImage4SlideShow(canvasGridTree.selectedSeriesId, showImageIds, seqCanvas)
    }

    async updateShowImagesBySelectingThumbnails(imageIndexId: string): Promise<void> {
        let canvasGridTree = this.dataRepository.findCanvasGrid()
        await this.pictorialIndexPanelController.updateShowImagesBySelectedThumbnail(canvasGridTree.selectedSeriesId, imageIndexId)
    }


    // Update Slide Show states
    setMode(isModeOn: boolean) {
        this.dataRepository.setSlideShowMode(isModeOn)
    }

    setPlaying(isPlaying: boolean) {
        this.dataRepository.setSlideShowPlaying(isPlaying)
    }

    setSeqCanvas(seqCanvas: number) {
        this.dataRepository.setSlideShowToolsSeqCanvas(seqCanvas)
    }

    setTimer(timerId: *) {
        let slideShowToolsTree = this.dataRepository.findSlideShowTools()
        let currentTimerId = slideShowToolsTree.timerId
        if (currentTimerId != null) {
            clearInterval(currentTimerId)
        }

        this.dataRepository.setSlideShowTimerId(timerId)
    }

    startSlideShowTimer(timing: number, seqCanvas: number) {
        this.setTimer(setInterval(this.slideShowPlayer, timing, seqCanvas))
    }

    stopSlideShowTimer() {
        this.setTimer(null)
    }
}