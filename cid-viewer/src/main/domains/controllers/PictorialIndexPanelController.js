// @flow

// import utils
import dataRepository from '@utils/DataRepository'
import { ViewingPanelController } from '@domains/controllers/ViewingPanelController'
import { SlideShowController } from '@domains/controllers/SlideShowController'
import { isNullOrEmptyString } from '@utils/StringUtil'
import logger from '@utils/Logger'
import { LOGGER_NAME } from '@constants/LogConstants'
import WindowResizeController from '@domains/controllers/rootApp/WindowResizeController'

// import types
import type { ViewModeType } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

export class PictorialIndexPanelController{
    dataRepository: typeof dataRepository 
    viewingPanelController: ViewingPanelController
    slideShowController: SlideShowController
    setSlideShowController: Function
    handleThumbnailClick: Function
    updateShowImagesBySelectedThumbnail: Function
    stopSlideShow: Function
    updateShowImagesWhenViewModeChanged: Function
    getSelectedByFirstImageAndViewMode: Function
    showImageInViewingPanel: Function
    setSelectedFirstImageId: Function
    setSelectedImageIds: Function
    getAllImageIds: Function
    showPanel: Function
    windowResizeController: WindowResizeController
    setWindowResizeController: Function

    constructor(viewingPanelController: ViewingPanelController, slideShowController: SlideShowController) {
        this.dataRepository = dataRepository
        this.viewingPanelController = viewingPanelController
        this.slideShowController = slideShowController || null
        this.setSlideShowController = this.setSlideShowController.bind(this)
        this.setWindowResizeController = this.setWindowResizeController.bind(this)
        this.handleThumbnailClick = this.handleThumbnailClick.bind(this)
        this.updateShowImagesBySelectedThumbnail = this.updateShowImagesBySelectedThumbnail.bind(this)
        this.stopSlideShow = this.stopSlideShow.bind(this)
        this.updateShowImagesWhenViewModeChanged = this.updateShowImagesWhenViewModeChanged.bind(this)
        this.getSelectedByFirstImageAndViewMode = this.getSelectedByFirstImageAndViewMode.bind(this)
        this.showImageInViewingPanel = this.showImageInViewingPanel.bind(this)
        this.setSelectedFirstImageId = this.setSelectedFirstImageId.bind(this)
        this.setSelectedImageIds = this.setSelectedImageIds.bind(this)
        this.getAllImageIds = this.getAllImageIds.bind(this)
        this.showPanel = this.showPanel.bind(this)
    }

    setSlideShowController(slideShowController: SlideShowController) {
        this.slideShowController = slideShowController
    }

    setWindowResizeController(windowResizeController: WindowResizeController) {
        this.windowResizeController = windowResizeController
    }

    async handleThumbnailClick(selectedSeriesId: string, selectedFirstImageId: string): Promise<void> {
        this.stopSlideShow()

        await this.updateShowImagesBySelectedThumbnail(selectedSeriesId, selectedFirstImageId)

        this.windowResizeController.resizeApp()
    }

    async updateShowImagesBySelectedThumbnail(selectedSeriesId: string, selectedFirstImageId: string): Promise<void> {
        let seriesDtl = dataRepository.findSeriesDtl(selectedSeriesId)
        let imgId = dataRepository.findImageId(selectedSeriesId, selectedFirstImageId)

        logger.userActions(LOGGER_NAME.CID_USERACTIONS_PICTINDEX_SELECT_THUMBNAIL, 
            "selectThumbnail...[seriesId=" + seriesDtl.seriesNo + "|imageId=" + imgId + "]")
        let selectedThumbnails: Array<string> = []

        const canvasGridTree = this.dataRepository.findCanvasGrid()
        const viewMode = canvasGridTree.viewMode

        this.setSelectedFirstImageId(selectedSeriesId, selectedFirstImageId)

        selectedThumbnails = this.getSelectedByFirstImageAndViewMode(selectedFirstImageId, viewMode)
        this.setSelectedImageIds(selectedThumbnails)

        await this.showImageInViewingPanel(selectedSeriesId, selectedThumbnails)
    }

    stopSlideShow() {
        this.slideShowController.stopSlideShow()
    }


    async updateShowImagesWhenViewModeChanged(viewMode: ViewModeType): Promise<void> {
        
        logger.userActions(LOGGER_NAME.CID_USERACTIONS_VIEWMODE, 
            "onViewModeChanged...[viewMode="+JSON.stringify(viewMode)+"]")

        let selectedThumbnails: Array<string> = []

        const canvasGridTree = this.dataRepository.findCanvasGrid()
        const selectedSeriesId = canvasGridTree.selectedSeriesId
        
        const thumbnailGridTree = this.dataRepository.findThumbnailGrid()
        const selectedFirstImageId = thumbnailGridTree.selectedFirstImageId

        selectedThumbnails = this.getSelectedByFirstImageAndViewMode(selectedFirstImageId, viewMode)
        this.setSelectedImageIds(selectedThumbnails)
        
        await this.showImageInViewingPanel(selectedSeriesId, selectedThumbnails)
    }


    getSelectedByFirstImageAndViewMode(selectedFirstImageId: string, viewMode: ViewModeType): Array<string> {
        let selectedImageIds: Array<string> = []

        if(!isNullOrEmptyString(selectedFirstImageId)) {
            const allHighestVerIds = this.getAllImageIds()
            const seqFirstImage = allHighestVerIds.indexOf(selectedFirstImageId)
            const noOfImage = viewMode.noOfImage

            for(let i = seqFirstImage; i < allHighestVerIds.length; i++) {
                selectedImageIds.push(allHighestVerIds[i])

                if(selectedImageIds.length == noOfImage)
                    break
            }
        }

        return selectedImageIds
    }
    
    async showImageInViewingPanel(selectedSeriesId: string, selectedImageIds: Array<string>): Promise<void> {
        if(!isNullOrEmptyString(selectedSeriesId))
            await this.viewingPanelController.showImage(selectedSeriesId, selectedImageIds)
    }

    setSelectedFirstImageId(selectedSeriesId: string, selectedFirstImageId: string) {
        this.dataRepository.setSelectedFirstImageId(selectedSeriesId, selectedFirstImageId)
    }

    setSelectedImageIds(selectedImageIds: Array<string>) {
        this.dataRepository.setSelectedImageIds(selectedImageIds)
    }
    
    getAllImageIds(): Array<string> {
        let thumbnailGridTree = this.dataRepository.findThumbnailGrid()
        let seriesDtlTree = this.dataRepository.findSeriesDtl(thumbnailGridTree.selectedSeriesId)

        let allImageIds = seriesDtlTree.imageDtls.allHighestVerIds

        return allImageIds.slice()
    }

    showPanel(isHidden: boolean) {
        if(isHidden) {
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_PICTINDEX_HIDE, "showPanel...[isHidden=" + isHidden.toString() + "]")
        } else {
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_PICTINDEX_SHOW, "showPanel...[isHidden=" + isHidden.toString() + "]")
        }
        this.dataRepository.setIsHiddenToPictorialIndexPanel(isHidden)
        this.viewingPanelController.resizeImgViewingPanel()
    }
}