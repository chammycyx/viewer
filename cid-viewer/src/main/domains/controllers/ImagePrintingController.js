// @flow

import dataRepository from '@utils/DataRepository'
import { PRINT_MODE } from '@actions/components/toolBarPanel/Action'
import { fabric } from 'fabric'
import cidTheme from '@styles/CidTheme'
import {DateParser, DATE_FORMAT} from '@utils/DateParser'
import {BRIGHTNESS_ADJUSTMENT_FACTOR} from '@constants/ComponentConstants'
import logger from '@utils/Logger'
import { LOGGER_NAME } from '@src/main/constants/LogConstants';
import { data } from '@src/main/reducers/data/Reducer';
import {ViewingPanelController} from '@domains/controllers/ViewingPanelController'

const JPEG_DATA_URI_FORMAT: string = 'jpeg'
const PAGE_LEFT: number = 800 //Move the printable area to left:800px so that the Header(page's title, page number) and Footer(url) would be shown in printout
const PADDING_BOTTOM_FOR_SEPARATED_PAGE_IMAGE: number = 45
const PADDING_TOP_FOR_ONE_PAGE_IMAGE: number = 40
const MAX_WIDTH_OF_PRINTOUT: number = 1150 //max width for A4 size printing

export class ImagePrintingController{
    printImage: Function
    createCanvas: Function
    createImageCanvas: Function
    createHtml: Function
    promptPrintDialog: Function

    constructor(){
        this.printImage = this.printImage.bind(this)
        this.createCanvas = this.createCanvas.bind(this)
        this.createImageCanvas = this.createImageCanvas.bind(this)
        this.createHtml = this.createHtml.bind(this)
        this.promptPrintDialog = this.promptPrintDialog.bind(this)
    }

    async printImage(printMode: string): Promise<void>{
        if(printMode == PRINT_MODE.PRINT_SELECTED_IMAGES){
            //create Fabric Canvas for exporting the image in base64 format
            const canvasGrid = dataRepository.findCanvasGrid()
            let fabricCanvasList = []

            const sortedSelectedImageIds = this.sortImageIds(canvasGrid.showImageIds, canvasGrid.selectedImageIds)
            if(canvasGrid.selectedImageIds!=null){
                // Log User Action Print
                let seriesDtl = dataRepository.findSeriesDtl(canvasGrid.selectedSeriesId)
                let imgIds = dataRepository.findImageIds(canvasGrid.selectedSeriesId, canvasGrid.selectedImageIds)
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_PRINT_SELECTEDIMAGE,
                    "printImage (SelectedImage)...[seriesId="+seriesDtl.seriesNo+
                    "|imageIds="+imgIds.toString()+"]")
                let promiseList = []
                for(let i=0;i<sortedSelectedImageIds.length;i++){
                    promiseList.push(this.createImageCanvas(canvasGrid.selectedSeriesId, sortedSelectedImageIds[i], 'white'))
                }
                fabricCanvasList = await Promise.all(promiseList)
            }

            //export the image in base64 format from canvas
            let imageBase64List = []
            for(let i=0;i<fabricCanvasList.length;i++){
                imageBase64List.push((fabricCanvasList[i]).toDataURL({format:JPEG_DATA_URI_FORMAT}))

                fabricCanvasList[i].dispose()
            }

            //print base64 image
            this.promptPrintDialog(this.createHtml(this.createImagePages(imageBase64List)))
        }

        if(printMode == PRINT_MODE.PRINT_VIEWING_AREA){
            //create Fabric Canvas for exporting the image in base64 format
            const canvasGrid = dataRepository.findCanvasGrid()
            const numRow = canvasGrid.viewMode.text.split('x')[0]
            const numCol = canvasGrid.viewMode.text.split('x')[1]
            const numGrid = numRow*numCol
            let fabricCanvasList = []
            let isImageSelected: Array<boolean> = []
            let promiseList = []

            let seriesDtl = dataRepository.findSeriesDtl(canvasGrid.selectedSeriesId)
            let imgIds = dataRepository.findImageIds(canvasGrid.selectedSeriesId, canvasGrid.showImageIds)

            // Log User Actions
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_PRINT_VIEWINGAREA,
                "printImage (ViewingArea)...[seriesId="+seriesDtl.seriesNo+
                "|imageIds="+imgIds.toString()+"]")

            for(let i=0;i<numGrid;i++){
                if(canvasGrid.showImageIds!=null && i<canvasGrid.showImageIds.length){
                    promiseList.push(this.createImageCanvas(canvasGrid.selectedSeriesId,canvasGrid.showImageIds[i], 'black'))
                    const isSelected = canvasGrid.selectedImageIds.indexOf(canvasGrid.showImageIds[i])>-1
                    isImageSelected.push(isSelected)
                }else{
                    let that = this
                    promiseList.push(new Promise((resolve: Function, reject: Function)=>{resolve(that.createCanvas('black'))}))
                    isImageSelected.push(false)
                }
            }
            fabricCanvasList = await Promise.all(promiseList)

            //export the image in base64 format from canvas
            let imageBase64List = []
            for(let i=0;i<fabricCanvasList.length;i++){
                imageBase64List.push((fabricCanvasList[i]).toDataURL({multiplier:1, format:'jpeg'}))

                fabricCanvasList[i].dispose()
            }

            //print base64 image
            this.promptPrintDialog(this.createHtml(this.createImagesTable(numRow, numCol, imageBase64List, isImageSelected)))
        }
    }

    sortImageIds(allImageIds: Array<string>, toBeSortedImageIds: Array<string>): Array<string>{
        let sortedImageIds: Array<string> = []
        if(allImageIds!=null){
            for(let i=0;i<allImageIds.length;i++){
                const item = allImageIds[i]
                if(toBeSortedImageIds!=null && toBeSortedImageIds.indexOf(allImageIds[i])>-1){
                    sortedImageIds.push(item)
                }
            }
        }
        return sortedImageIds
    }

    createHtml(bodyHtml: string): string{
        let pageMarginTopBottom = 20
        return "<!DOCTYPE html><html lang='en'><head>\n"+
                "<meta charset='utf-8'>\n"+
                "<meta name='viewport' content='width=device-width,height=device-height,initial-scale=1'>\n"+
                "<title>untitled document</title>\n"+
                    "<script>\n" +
                    "</script>\n"+
                "<style>\n"+
                    "/* Move the printable area to left so that the Header(page's title, page number) and Footer(url) would be shown in printout */\n"+
                    "@page {size: auto;margin: "+pageMarginTopBottom+"px -"+PAGE_LEFT+"px;}\n"+
                "</style>\n"+
                '</head>\n' +
                    bodyHtml + 
                "</html>"
    }

    getImageWidth(): number{
        const canvasGrid = dataRepository.findCanvasGrid()
        return canvasGrid.canvasWidth
    }

    createImagePages(imageBase64List: Array<string>): string{
        let result = ""
        const scale = MAX_WIDTH_OF_PRINTOUT/this.getImageWidth()
        for(let i=0;i<imageBase64List.length;i++){
            result += "<div style='padding-bottom:"+PADDING_BOTTOM_FOR_SEPARATED_PAGE_IMAGE+"px;position:relative;left:"+(PAGE_LEFT+428)+"px;page-break-after:always !important;'>\n"+
                            "<div style='position:absolute;left:0px;top:40px;transform: scale("+scale+");transform-origin: left top;'>"+
                                "<img src='"+imageBase64List[i]+"' />\n" +
                            "</div>"+
                        "</div>\n"
        }
        return result
    }

    createImagesTable(numRow: number, numCol: number, imageBase64List: Array<string>, isImageSelected: Array<boolean>): string{
        const borderPx = 2
        const borderWidth = numCol*borderPx

        const expectedTableWidth = (this.getImageWidth()*numCol)+borderWidth

        const scale = MAX_WIDTH_OF_PRINTOUT/expectedTableWidth
        let table = "<body><div>\n"+
                        "<div style='padding-top:"+PADDING_TOP_FOR_ONE_PAGE_IMAGE+"px;position:relative;left:"+(PAGE_LEFT+428)+"px;page-break-after:always !important;'>\n"+
                            "<div style='position:absolute;left:0px;top:0px;transform: scale("+scale+");transform-origin: left top;'>"
            table += "<table cellpadding='0px' cellspacing='0px' style='position:relative;top:"+PADDING_TOP_FOR_ONE_PAGE_IMAGE+"px;border: 1px solid black;border-collapse: separate;border-spacing: 0px;'>"
        let imageCtr = 0
        for(let i=0;i<numRow;i++){
            let tr = "<tr>"
            for(let j=0;j<numCol; j++){
                let img = ""
                let isSelected = false
                if(imageCtr<imageBase64List.length){
                    const bease64 = imageBase64List[imageCtr]
                    isSelected = isImageSelected[imageCtr]
                    imageCtr++
                    img = "<img src='" + bease64 + "'/>"
                }
                
                let td = (isSelected?"<td style='z-index:2;outline:1px solid #00D2FF;border: 1px solid grey'>":"<td style='border: 1px solid grey'>")
                    td += img
                    td += "</td>"
                tr += td
            }
            tr += "</tr>"
            table += tr    
        }
        table += "</table></div>"+
                    "</div>\n"+
                "</div></body>\n"
        return table
    }

    promptPrintDialog(html: string){
        const iframeName = 'iframeForPrinting'

        //remove previous printed images by removing iframe
        let iframe = document.getElementById(iframeName)
        //$FlowFixMe
        if(iframe){document.body.removeChild(iframe)}

        //create iframe for images to print
        iframe = document.createElement("iframe")
            iframe.style.display = "none"
            iframe.id = iframeName
        //$FlowFixMe
        document.body.appendChild(iframe)
        iframe.contentWindow.onload = function(){
            //$FlowFixMe
            if(!iframe.contentWindow.document.execCommand('print', false, null)){
                //in case execCommand doesn't support
                //$FlowFixMe
                iframe.contentWindow.print()
            }
        }
        
        //add images to iframe
        //$FlowFixMe
        iframe.contentWindow.document.body.innerHTML = html
    }

    createCanvas(backgroundColor: string): *{
        const canvasGrid = dataRepository.findCanvasGrid()
        //render Canvas
        let nativeCanvas = document.createElement('canvas')
        let fabricCanvas = new fabric.Canvas(nativeCanvas)
            fabricCanvas.backgroundColor = backgroundColor
            fabricCanvas.setWidth(canvasGrid.canvasWidth)
            fabricCanvas.setHeight(canvasGrid.canvasHeight)
        return fabricCanvas
    }

    createImageCanvas(seriesId: string, imageId: string, backgroundColor: string): Promise<*>{
        const canvasProfile = dataRepository.findCanvasProfile(seriesId, imageId)
        const canvasGrid = dataRepository.findCanvasGrid()
        
        //render Canvas
        let fabricCanvas = this.createCanvas(backgroundColor)

        //render Image
        let fabricImg = new fabric.Image(null)

        let promise = new Promise(function(resolve: Function, reject: Function): *{
            fabric.util.loadImage(canvasProfile.imageBase64, function(img: *): *{
                fabricImg.setElement(img)
                fabricCanvas.add(fabricImg)
                
                
                fabricImg.opacity = 1
                
                //set oringin X and Y to center and rotate angle
                fabricImg.originX = canvasProfile.imageOriginX
                fabricImg.originY = canvasProfile.imageOriginY
                fabricImg.rotate(canvasProfile.rotateAngle)
    
                //set flip
                fabricImg.flipX = canvasProfile.flipHorizontal
                fabricImg.flipY = canvasProfile.flipVertical
    
                //set image coordinate
                fabricImg.left = canvasProfile.imageXCoor
                fabricImg.top = canvasProfile.imageYCoor
    
                //set image scale/auto fit
                fabricImg.scaleX = canvasProfile.imageScaleX
                fabricImg.scaleY = canvasProfile.imageScaleY
    
                // set color adjustment
                if(canvasProfile.invert) {
                    fabricImg.filters[0] = new fabric.Image.filters.Invert()
                }
    
                fabricImg.filters[1] = new fabric.Image.filters.Brightness({
                    brightness: (canvasProfile.brightness / 100) * BRIGHTNESS_ADJUSTMENT_FACTOR
                })
    
                fabricImg.filters[2] = new fabric.Image.filters.Contrast({
                    contrast: canvasProfile.contrast / 100
                })
    
                fabricImg.filters[3] = new fabric.Image.filters.Saturation({
                    saturation: canvasProfile.saturation / 100
                })
    
                fabricImg.filters[4] = new fabric.Image.filters.HueRotation({
                    rotation: canvasProfile.hue / 180
                })
    
                fabricImg.applyFilters()
                fabricImg.setCoords()

                //render header
                if(ViewingPanelController.isHeaderVisible(seriesId)){
                    const caseProfile = dataRepository.findCaseProfile()
                    let fabricHeader = new fabric.IText(caseProfile.caseNo+canvasGrid.headerTimeText+
                        DateParser.parse(canvasGrid.displayDateTime[canvasGrid.showImageIds.indexOf(imageId)], DATE_FORMAT.FULL_YYYYMMDDTHHMMSS_PLAIN,DATE_FORMAT.LONG_DDMMMYYYYTHHMMSS), {
                            top: cidTheme.imageViewingPanel.canvasGrid.headerPaddingTop,
                            left: cidTheme.imageViewingPanel.canvasGrid.headerPaddingLeft,
                            fontStyle: cidTheme.imageViewingPanel.canvasGrid.headerFontStyle,//italic
                            backgroundColor: canvasProfile.bgColor,
                            fill: canvasProfile.fontColor,
                            fontFamily: canvasProfile.fontType,//Arial
                            fontSize: canvasProfile.fontSize,
                            objectCaching: false//fix the issue of fuzzy font
                        })
                    fabricCanvas.add(fabricHeader)

                    let fabricHeaderBackGround = new fabric.Rect({
                        width: canvasGrid.canvasWidth,
                        top: -1,
                        left: -1,
                        fill:canvasProfile.bgColor
                    })
                        fabricHeaderBackGround.height = cidTheme.imageViewingPanel.canvasGrid.headerPaddingTop * 2 + fabricHeader.height
                    fabricCanvas.add(fabricHeaderBackGround)
                    fabricHeader.bringToFront()
                }
    
                //Render Canvas
                fabricCanvas.renderAll()

                resolve(fabricCanvas)
            })

        })
        
        
        
        return promise
    }
}