//@flow
import dataRepository from '@utils/DataRepository'
import { SESSION_TIMEOUT } from '@constants/MsgCodeConstants'

// for logging
import logger from '@utils/Logger'
import { LOGGER_NAME } from '@constants/LogConstants'
import { RESIZE, MOUSE_DOWN, WHEEL, MOUSE_MOVE, KEY_DOWN, FOCUS } from '@constants/WindowEventConstants'
import { TAB, LEFT, UP, RIGHT, DOWN } from '@constants/KeyCodeConstants'
import viewerLogController from '@domains/controllers/ViewerLogController'
//import viewerLogController from '@domains/controllers/rootApp/CommonLogController'

export default class TimeoutController{
    initTimeout: Function
    timerId: IntervalID
    idleTimeout: number
    lastUserActivityTimestamp: number
    lastMouseMoveEvent: *
    timeoutHandler: Function
    resetTimerHandler: Function
    resetTimer4MouseMoveHandler: Function
    resetTimer4KeyDownHandler: Function
    resetTimer4FocusHandler: Function
    addEventListeners: Function
    removeEventListeners: Function
    
    constructor() {
        this.lastMouseMoveEvent = null

        this.initTimeout = this.initTimeout.bind(this)
        this.timeoutHandler = this.timeoutHandler.bind(this)
        this.resetTimerHandler = this.resetTimerHandler.bind(this)
        this.resetTimer4MouseMoveHandler = this.resetTimer4MouseMoveHandler.bind(this)
        this.resetTimer4KeyDownHandler = this.resetTimer4KeyDownHandler.bind(this)
        this.resetTimer4FocusHandler = this.resetTimer4FocusHandler.bind(this)
        this.addEventListeners = this.addEventListeners.bind(this)
        this.removeEventListeners = this.removeEventListeners.bind(this)
    }

    addEventListeners() {
        window.addEventListener(MOUSE_DOWN, this.resetTimerHandler, false)
        window.addEventListener(WHEEL, this.resetTimerHandler, false)
        window.addEventListener(MOUSE_MOVE, this.resetTimer4MouseMoveHandler, false)
        window.addEventListener(KEY_DOWN, this.resetTimer4KeyDownHandler, false)
        window.addEventListener(RESIZE, this.resetTimerHandler, false)
        window.addEventListener(FOCUS, this.resetTimer4FocusHandler, false)
    }

    removeEventListeners() {
        window.removeEventListener(MOUSE_DOWN, this.resetTimerHandler)
        window.removeEventListener(WHEEL, this.resetTimerHandler)
        window.removeEventListener(MOUSE_MOVE, this.resetTimer4MouseMoveHandler)
        window.removeEventListener(KEY_DOWN, this.resetTimer4KeyDownHandler)
        window.removeEventListener(RESIZE, this.resetTimerHandler)
        window.removeEventListener(FOCUS, this.resetTimer4FocusHandler)
    }
    
    initTimeout() {
        this.idleTimeout = parseInt(dataRepository.findViewerControlConfig().idleTimeout) // get idleTimeout from config

        if(isNaN(this.idleTimeout) || this.idleTimeout==null || this.idleTimeout==undefined) { // If invalid value, set idleTimeout = 0
            this.idleTimeout = 0
        }

        if(this.idleTimeout > 0) { // idleTimeout==0 implies no timeout
            this.resetTimerHandler()
            this.addEventListeners()
        }
    }

    resetTimerHandler(e: *) {
        if(this.timerId != undefined && this.timerId != null)
            clearInterval(this.timerId)
        
        this.timerId = setInterval(this.timeoutHandler, this.idleTimeout*60*1000)

        this.lastUserActivityTimestamp = (new Date()).getTime()
    }

    resetTimer4MouseMoveHandler(e: *) {
        let mousePositionChanged = true

        if(this.lastMouseMoveEvent != null) {
            let movementX = e.screenX - this.lastMouseMoveEvent.screenX
            let movementY = e.screenY - this.lastMouseMoveEvent.screenY
            
            mousePositionChanged = (movementX != 0 || movementY != 0)
        }
        
        if(mousePositionChanged) {
            this.resetTimerHandler(e)
        }
        
        this.lastMouseMoveEvent = e
    }

    resetTimer4KeyDownHandler(e: *) {
        let keycode = (window.event) ? window.event.keyCode : e.keyCode
        if(keycode == TAB || keycode == LEFT || keycode == UP || keycode == RIGHT || keycode == DOWN) {
            this.resetTimerHandler(e)
        }
    }

    resetTimer4FocusHandler(e: *) {
        // Specific for the scenario "Print"
        // If the Windows Print Dialog is on hold and the idle time >= idleTimeout, the handler of the passed interval will be ignore and wait for the next interval to trigger the handler
        if((new Date()).getTime() - this.lastUserActivityTimestamp >= this.idleTimeout*60*1000) {
            this.timeoutHandler()
        } else {
            this.resetTimerHandler(e)
        }
    }

    timeoutHandler() {
        clearInterval(this.timerId) // Stop timer when timeout
        this.removeEventListeners()

        // Stop slide show
        dataRepository.setSlideShowToolsStop()

        logger.system(LOGGER_NAME.CID_SYSTEM_CIDIMAGEVIEWER_TIMEOUT, "timeoutHandler...CID Image Viewer Timeout.")
        viewerLogController.flushLogs()

        dataRepository.setMsgDialogByCode(SESSION_TIMEOUT)
        dataRepository.setAppMask(true)
    }
}