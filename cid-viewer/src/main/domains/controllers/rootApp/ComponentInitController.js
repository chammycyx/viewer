//@flow
import {ImageTool} from '@constants/ComponentConstants'
import {initState as ViewModeDropDownListInitState} from '@reducers/components/toolBarPanel/viewModeTools/viewModeDropDownList/Reducer'
import dataRepository from '@utils/DataRepository'
import {ViewingPanelController} from '@domains/controllers/ViewingPanelController'
import WindowResizeController from '@domains/controllers/rootApp/WindowResizeController'
import TimeoutController from '@domains/controllers/rootApp/TimeoutController'
import StudyService from '@domains/services/studyService/StudyService'

export class ComponentInitController {
    // viewingPanelController: *
    // windowResizeController: *

    // constructor(viewingPanelController: *, windowResizeController: *){
    //     this.viewingPanelController = viewingPanelController
    //     this.windowResizeController = windowResizeController
    // }

    // async initComponents(){
    //     //init viewing panel grid by default view mode
    //     this.viewingPanelController.resizeImgViewingPanel()
    //     this.viewingPanelController.viewingPanelController.setGrid(ViewModeDropDownListInitState.viewMode)

    //     this.windowResizeController.setViewingPanelController(this.viewingPanelController)
    //     this.windowResizeController.initSetAppWidthHeightForPanels()

    //     let timeoutController = new TimeoutController()
    //     yield call(timeoutController.initTimeout)

    //     //set default image manipulation tool
    //     yield put(setImageTool(ImageTool.SELECT))
    // }

    async initComponents(): Promise<void>{
        //init viewing panel grid by default view mode
        let webSInfo = dataRepository.findIasWsAuthInfo()
        let studyService = new StudyService(webSInfo)
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
            viewingPanelController.resizeImgViewingPanel()
            viewingPanelController.setGrid(ViewModeDropDownListInitState.viewMode)

        let windowResizeController = new WindowResizeController()
            windowResizeController.setViewingPanelController(viewingPanelController)
            windowResizeController.initResizeApp()

        let timeoutController = new TimeoutController()
            timeoutController.initTimeout()
        
        //set default image manipulation tool
        dataRepository.setImageTool(ImageTool.SELECT)
    }
}

