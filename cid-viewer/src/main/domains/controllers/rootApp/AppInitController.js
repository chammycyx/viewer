//@flow
import AuditLogController from '@domains/controllers/rootApp/AuditLogController'
import PrerequisiteController from '@domains/controllers/rootApp/PrerequisiteController'
import {ComponentInitController} from '@domains/controllers/rootApp/ComponentInitController'
import HotKeyController from '@domains/controllers/rootApp/HotKeyController'

export class AppInitController {
    async initApp(): Promise<void>{
        //do server communication related initialization
        let auditLogController = new AuditLogController()
        let prerequisiteController = new PrerequisiteController()
            prerequisiteController.setAuditLogController(auditLogController)
        await prerequisiteController.initPreliminaryWork()

        //init viewing panel grid by default view mode
        let componentInitController = new ComponentInitController()
        await componentInitController.initComponents()

        let hotKeyController = new HotKeyController()
        hotKeyController.removeHotkey()
    }
}

