//@flow

// import readers
import { caseProfileReader } from '@domains/readers/CaseProfileReader'
import type { CaseProfileType } from '@domains/readers/CaseProfileReader'
import { fileReader } from '@domains/readers/FileReader'

// import services
import { configService } from '@domains/services/configService/ConfigService'
import StudyService from '@domains/services/studyService/StudyService'
import { AuthService } from '@domains/services/authService/AuthService'

// import utils
import JsonParser from '@utils/JsonParser'
import logger from '@utils/Logger'
import type { WsRsltObjType } from '@utils/WebServiceBackbone'
import dataRepository from '@utils/DataRepository'
import {isNullOrEmptyString} from '@utils/StringUtil'

// import constants
import { ENDPOINT } from '@constants/EndpointConstants'
import { ServiceConstants } from '@constants/ServiceConstants'
import { EVENT_GET_CID_STUDY } from '@constants/EventConstants'
import { EJB_EXCEPTION, CID_APPLICATION_CONFIG_UNAVAILABLE, 
    CID_IAS_AUTHEN_UNAVAILABLE, CID_ICW_AUTHEN_UNAVAILABLE,
    ACCESS_AUTHEN_FAILED, CID_CONFIG_FILE_UNAVAILABLE,
    APP_VER_FAILED, CID_IAS_WEBSERVICE_TIMEOUT,
    CID_ICW_WEBSERVICE_TIMEOUT,
    CID_GET_JSON_FILE_TIMEOUT } from '@constants/MsgCodeConstants'
import { APP_INIT, MISSING_INPUT_PARAM,
    ACCESS_AUTHEN_FAILED as ACCESS_AUTHEN_FAILED_HC,
    UNABLE_TO_READ_CASE_PROFILE,
    CID_CONFIG_FILE_UNAVAILABLE as CID_CONFIG_FILE_UNAVAILABLE_HC,
    CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG,
    CID_GET_JSON_FILE_TIMEOUT as CID_GET_JSON_FILE_TIMEOUT_HC} from '@constants/WordingConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'
import {WINDOW_NAME} from '@constants/ProxyConstants'
import { LOGGER_NAME, LOG_MODE } from '@constants/LogConstants'
import viewerLogController from '@domains/controllers/ViewerLogController'
import windowPositionController from '@domains/controllers/rootApp/WindowPositionController'
import CidHttpError from '@components/base/CidHttpError'

export default class PrerequisiteController{
    initPreliminaryWork: Function
    icwWriteLog: Promise<WsRsltObjType>
    metadataStr: *
    auditLogController: *
    
    constructor() {
        this.initPreliminaryWork = this.initPreliminaryWork.bind(this)
    }

    setAuditLogController(auditLogController: *) {
        this.auditLogController = auditLogController
    }

    getCaseProfile(): ?CaseProfileType {
        let decodedHash

        if(process.env.NODE_ENV=='development'){
        // By using process.env.NODE_ENV=='development', this branch would be removed by webpack automatically in prod build 
            if(window.opener==null){
                caseProfileReader.pretendOpener()

                try {
                    decodedHash = caseProfileReader.readCaseProfileFromHash()
                } catch(cidErr) {
                    dataRepository.setMsgDialogByContent(UNABLE_TO_READ_CASE_PROFILE.title, UNABLE_TO_READ_CASE_PROFILE.content)
                    // cidErr.append('D:\project\trunk\CID-Viewer-II_original\cid-viewer\src\main\domains\controllers\rootApp\PrerequisiteController.js', 'getCaseProfile()')
                    throw cidErr
                }

                document.location.hash = ''
                window.name = WINDOW_NAME

                if(!caseProfileReader.validateCaseProfile(decodedHash.caseProfile)) {
                    dataRepository.setMsgDialogByContent(MISSING_INPUT_PARAM.title, MISSING_INPUT_PARAM.content)
                    return null
                }
            }else{
                const isValid = caseProfileReader.validateOpener()
                if(!isValid) {
                    dataRepository.setMsgDialogByContent(ACCESS_AUTHEN_FAILED_HC.title, ACCESS_AUTHEN_FAILED_HC.content)
                    return null
                }

                try {
                    decodedHash = caseProfileReader.readCaseProfileFromHash()
                } catch(cidErr) {
                    dataRepository.setMsgDialogByContent(UNABLE_TO_READ_CASE_PROFILE.title, UNABLE_TO_READ_CASE_PROFILE.content)
                    throw cidErr
                }

                decodedHash.caseProfile.wrkStnHostname = 'dc1cidst02'
                document.location.hash = ''
                window.name = WINDOW_NAME

                if(!caseProfileReader.validateCaseProfile(decodedHash.caseProfile)) {
                    dataRepository.setMsgDialogByContent(MISSING_INPUT_PARAM.title, MISSING_INPUT_PARAM.content)
                    return null
                }
            }
        }else if(process.env.NODE_ENV=='production'){
            const isValid = caseProfileReader.validateOpener()
            if(!isValid) {
                dataRepository.setMsgDialogByContent(ACCESS_AUTHEN_FAILED_HC.title, ACCESS_AUTHEN_FAILED_HC.content)
                return null
            }

            try {
                decodedHash = caseProfileReader.readCaseProfileFromHash()
            } catch(cidErr) {
                dataRepository.setMsgDialogByContent(UNABLE_TO_READ_CASE_PROFILE.title, UNABLE_TO_READ_CASE_PROFILE.content)
                throw cidErr
            }
            
            document.location.hash = ''
            window.name = WINDOW_NAME
            
            if(!caseProfileReader.validateCaseProfile(decodedHash.caseProfile)) {
                dataRepository.setMsgDialogByContent(MISSING_INPUT_PARAM.title, MISSING_INPUT_PARAM.content)
                return null
            }
        }

        if(decodedHash!=null && decodedHash!=undefined)
            return decodedHash.caseProfile
        else
            return null
    }

    readCaseProfile(): boolean{
        try {
            let caseProfile = this.getCaseProfile()
            if(caseProfile!=null) {
                // $FlowFixMe
                dataRepository.saveCaseProfile(caseProfile)
                return true
            } else {
                return false
            }
        } catch(cidErr) {
            throw cidErr
        }
    }

    async readErrMsgCode(): Promise<void>{
        try {
            // read error message code
            let caseProfile = dataRepository.findCaseProfile()
            let timeout = dataRepository.findWebServiceTimeoutConfig()
            let errMsgPath = caseProfile.appHost + caseProfile.appSiteName + ENDPOINT.LOCAL.RETRIEVE_ERR_MSG
            let errMsgCode = await fileReader.readFile(errMsgPath, timeout)
            dataRepository.saveMsgCode(errMsgCode)
        } catch(cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByContent(CID_GET_JSON_FILE_TIMEOUT_HC.title, CID_GET_JSON_FILE_TIMEOUT_HC.content)
            } else{
                dataRepository.setMsgDialogByContent(CID_CONFIG_FILE_UNAVAILABLE_HC.title, CID_CONFIG_FILE_UNAVAILABLE_HC.content)
            }
            throw cidHttpErr
        }
    }

    async doAuth(): Promise<boolean>{
        let caseProfile = dataRepository.findCaseProfile()
        let authService = new AuthService(dataRepository.findIcwWsAuthInfo(), dataRepository)
        let isAccessKeyValid
        let timeout = dataRepository.findWebServiceTimeoutConfig()
        try {
            isAccessKeyValid = await authService.validateAccessKeyByHostName(
                caseProfile.requestSys,
                caseProfile.workstationHostname,
                caseProfile.accessKey,
                caseProfile.userId,
                caseProfile.workstationId,
                caseProfile.requestSys,
                timeout
            )

            if(!isAccessKeyValid){
                dataRepository.setMsgDialogByCode(ACCESS_AUTHEN_FAILED)
                logger.errorWithMsgCode(ACCESS_AUTHEN_FAILED)
                return false
            }
        } catch(cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_ICW_WEBSERVICE_TIMEOUT)
                logger.errorWithMsgCode(CID_ICW_WEBSERVICE_TIMEOUT)
            } else if(cidHttpErr.status==UNAUTHORIZED) {
                dataRepository.setMsgDialogByCode(CID_ICW_AUTHEN_UNAVAILABLE)
                logger.errorWithMsgCode(CID_ICW_AUTHEN_UNAVAILABLE)
            } else {
                dataRepository.setMsgDialogByCode(EJB_EXCEPTION, cidHttpErr.exceptionMsg)
                logger.error(cidHttpErr.exceptionMsg, EJB_EXCEPTION)
            }
            throw cidHttpErr
        }

        const viewerControlConfig = dataRepository.findViewerControlConfig()
        const applicationId = viewerControlConfig.applicationId
        const applicationVersion = viewerControlConfig.applicationVersion
        let isAppStatusValid
        
        try {
            isAppStatusValid = await authService.validateApplicationStatus(
                applicationId,
                caseProfile.userHospCode,
                applicationVersion,
                caseProfile.userId,
                caseProfile.workstationId,
                caseProfile.requestSys,
                timeout
            )

            if(!isAppStatusValid){
                dataRepository.setMsgDialogByCode(APP_VER_FAILED)
                logger.errorWithMsgCode(APP_VER_FAILED)
                return false
            }
        } catch(cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_ICW_WEBSERVICE_TIMEOUT)
                logger.errorWithMsgCode(CID_ICW_WEBSERVICE_TIMEOUT)
            } else if(cidHttpErr.status==UNAUTHORIZED) {
                dataRepository.setMsgDialogByCode(CID_ICW_AUTHEN_UNAVAILABLE)
                logger.errorWithMsgCode(CID_ICW_AUTHEN_UNAVAILABLE)
            } else {
                dataRepository.setMsgDialogByCode(EJB_EXCEPTION, cidHttpErr.exceptionMsg)
                logger.error(cidHttpErr.exceptionMsg, EJB_EXCEPTION)
            }
            throw cidHttpErr
        }

        return true
    }

    async readLocalAppConfig(): Promise<void>{
        try {
            // read app configurations
            const caseProfile = dataRepository.findCaseProfile()
            const timeout = dataRepository.findWebServiceTimeoutConfig()
            let appLocalConfigPath = caseProfile.appHost + caseProfile.appSiteName + ENDPOINT.LOCAL.RETRIEVE_APP_CONFIG
            let appLocalConfig = await fileReader.readFile(appLocalConfigPath, timeout)

            dataRepository.saveAppConfig(appLocalConfig)
            dataRepository.savePictorialIndexConfig(
                JsonParser.getValue(appLocalConfig, 'numOfColumn'), 
                JsonParser.getValue(appLocalConfig, 'isShowSeriesHeading'),
                JsonParser.getValue(appLocalConfig, 'serieName'))
            dataRepository.saveImageHeaderConfig(
                JsonParser.getValue(appLocalConfig, 'fontType'), 
                JsonParser.getValue(appLocalConfig, 'fontSize'),
                JsonParser.getValue(appLocalConfig, 'fontColor'),
                JsonParser.getValue(appLocalConfig, 'bgColor'), 
                JsonParser.getValue(appLocalConfig, 'visibility'))
            dataRepository.saveColorAdjustmentConfig(JsonParser.getValue(appLocalConfig, 'maxNumOfImage'))
        } catch(cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByContent(CID_GET_JSON_FILE_TIMEOUT_HC.title, CID_GET_JSON_FILE_TIMEOUT_HC.content)
            } else{
                dataRepository.setMsgDialogByContent(CID_CONFIG_FILE_UNAVAILABLE_HC.title, CID_CONFIG_FILE_UNAVAILABLE_HC.content)
            }
            throw cidHttpErr
        }
    }
	
	async readLocalLogConfig(): Promise<void>{
        try{
            const caseProfile = dataRepository.findCaseProfile()
            const timeout = dataRepository.findWebServiceTimeoutConfig()
            let logConfigFilePath = caseProfile.appHost + caseProfile.appSiteName + ENDPOINT.LOCAL.RETRIEVE_LOG_CONFIG
            let logConfig = await fileReader.readFile(logConfigFilePath, timeout)
            viewerLogController.setConfigWithDataRepository(logConfig, dataRepository, LOG_MODE.PARTIAL_LOG)
        }catch(cidHttpErr){
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_GET_JSON_FILE_TIMEOUT)
                logger.errorWithMsgCode(CID_GET_JSON_FILE_TIMEOUT)
            } 
            throw cidHttpErr
        }
    }

    async retrieveAppConfig(): Promise<void>{
        // retrieve application configuration from ICW
        const caseProfile = dataRepository.findCaseProfile()
        const icwWsInfo = dataRepository.findIcwWsAuthInfo()
        const icwEndpoint = ENDPOINT.ICW.RETRIEVE_APP_CONFIG
        const timeout = dataRepository.findWebServiceTimeoutConfig()
        const appCommonConfigData = {
            hospCode: caseProfile.hospCode,
            systemId:'',
            applicationId:'',
            profileCode:'',
            section: ServiceConstants.ICW.APPCONFIG.SECTION,
            key:'',
            userId: caseProfile.userId,
            workstationId: caseProfile.workstationId,
            requestSys: caseProfile.requestSys
        }

        // Service Performance Log
        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_ICW_GETAPPCONFIG_START, 
            "Invoke 'retrieveAppConfig'...[" +
            "appCommonConfigData= " + JSON.stringify(appCommonConfigData) +
            "]"
        )

        let appCommonConfigRsltObj
        
        try {
            appCommonConfigRsltObj = await configService.retrieveAppConfig(icwEndpoint, icwWsInfo, appCommonConfigData, timeout)
        } catch(cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_ICW_WEBSERVICE_TIMEOUT)
                logger.errorWithMsgCode(CID_ICW_WEBSERVICE_TIMEOUT)
            } else if(cidHttpErr.status==UNAUTHORIZED) {
                dataRepository.setMsgDialogByCode(CID_ICW_AUTHEN_UNAVAILABLE)
                logger.errorWithMsgCode(CID_ICW_AUTHEN_UNAVAILABLE)
            } else {
                dataRepository.setMsgDialogByCode(EJB_EXCEPTION, cidHttpErr.exceptionMsg)
                logger.error(cidHttpErr.exceptionMsg, EJB_EXCEPTION)
            }
            throw cidHttpErr
        }

        //Check if response is empty, return error
        if( '{}' == JsonParser.getValue(appCommonConfigRsltObj,'response')){
            dataRepository.setMsgDialogByCode(CID_APPLICATION_CONFIG_UNAVAILABLE, CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG)
            logger.error(CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG, CID_APPLICATION_CONFIG_UNAVAILABLE)

            throw new CidHttpError('src.main.domains.controllers.rootApp.PrerequisiteController'
            , 'retrieveAppConfig()', icwWsInfo.contextPath + icwEndpoint
            , CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG, appCommonConfigData)
        }

        // Service Performance Log
        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_ICW_GETAPPCONFIG_END, 
            "Invoke 'retrieveAppConfig' completed...[" +
            "rtnObj= " + JSON.stringify(appCommonConfigRsltObj) +
            "]"
        )
        
        let appCommonConfig = JSON.parse(appCommonConfigRsltObj.response)

        dataRepository.saveAppConfig(appCommonConfig)
    }

    validateCaseProfileTimeToken(): boolean{
        const caseProfileTimeToken = dataRepository.findCaseProfile().caseProfileTimeToken
        const caseProfileTimeout = dataRepository.findViewerControlConfig().caseProfileTimeout

        if(isNullOrEmptyString(caseProfileTimeToken)){
            logger.info("Case Profile invalid!")
            dataRepository.setMsgDialogByCode(ACCESS_AUTHEN_FAILED)
            logger.errorWithMsgCode(ACCESS_AUTHEN_FAILED)
            return false
        }
        
        const timeDiff = new Date().getTime() - caseProfileTimeToken
        const timeDiffInSec = timeDiff/1000
        if(timeDiffInSec>caseProfileTimeout){
            logger.info("Case Profile timed out!")
            dataRepository.setMsgDialogByCode(ACCESS_AUTHEN_FAILED)
            logger.errorWithMsgCode(ACCESS_AUTHEN_FAILED)
            return false
        }

        return true
    }

    async retrieveStudy(): Promise<void>{
        // retrieve study
        const caseProfile = dataRepository.findCaseProfile()
        const iasWsInfo = dataRepository.findIasWsAuthInfo()
        const timeout = dataRepository.findWebServiceTimeoutConfig()
        const studyData = {
            patientKey: caseProfile.patientKey,
            hospCde: caseProfile.hospCode,
            caseNo: caseProfile.caseNo,
            accessionNo: caseProfile.accessionNo,
            userId: caseProfile.userId,
            workstationId: caseProfile.workstationId,
            requestSys: caseProfile.requestSys
        }
        const studyService = StudyService.getStudyService(iasWsInfo)


        let studyState

        try {
            studyState = await studyService.retrieveStudy(studyData, timeout)
        } catch (cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_IAS_WEBSERVICE_TIMEOUT)
                logger.errorWithMsgCode(CID_IAS_WEBSERVICE_TIMEOUT)
            } else if(cidHttpErr.status==UNAUTHORIZED) {
                dataRepository.setMsgDialogByCode(CID_IAS_AUTHEN_UNAVAILABLE)
                logger.errorWithMsgCode(CID_IAS_AUTHEN_UNAVAILABLE)
            } else {
                dataRepository.setMsgDialogByCode(EJB_EXCEPTION, cidHttpErr.exceptionMsg)
                logger.error(cidHttpErr.exceptionMsg, EJB_EXCEPTION)
            }
            
            throw cidHttpErr
        }

        this.metadataStr = studyService.getMetadataStr()

        dataRepository.saveStudy(studyState)
    }

    async writeGetStudyIcwLog(): Promise<void> {
        try {
            await this.auditLogController.writeIcwLog(EVENT_GET_CID_STUDY, this.metadataStr)
        } catch(cidHttpErr) {
            throw cidHttpErr
        }
    }
    
    async initPreliminaryWork(): Promise<void> {
        try{
            dataRepository.setProgressBar(true, APP_INIT)

            const isReadCaseProfileSuccess = this.readCaseProfile()
            if(!isReadCaseProfileSuccess) {
                dataRepository.setAppMask(true)
                return
            }

            windowPositionController.reposition()

            await this.readLocalAppConfig()
            await this.readErrMsgCode()
			await this.readLocalLogConfig()
            
            let caseProfile: * = dataRepository.findCaseProfile()
            viewerLogController.disableLogging(caseProfile.requestSys, caseProfile.userId, caseProfile.workstationId, caseProfile.userHospCode)

			logger.system(LOGGER_NAME.CID_SYSTEM_CIDIMAGEVIEWER_LAUNCH, "CID Image Viewer is launched.")
            const isCaseProfileValid = this.validateCaseProfileTimeToken()
            if(!isCaseProfileValid){
                dataRepository.setAppMask(true)
                return
            }
            const isAuthValid = await this.doAuth()
            if(!isAuthValid){
                dataRepository.setAppMask(true)
                return
            }
            await this.retrieveAppConfig()
            await this.retrieveStudy()

            // Log the study IDs mapping
            const seriesDtls: * = dataRepository.findAllSeriesDtl()
            //$FlowFixMe
            Object.values(seriesDtls.byId).map((seriesDtl: json) => {
                logger.system(LOGGER_NAME.CID_SYSTEM_CIDIMAGEVIEWER_IMAGEIDSMAPPING, 
                    "[seriesId=" + String(seriesDtl.seriesNo) + "||idsMapping=" + JSON.stringify(seriesDtl.imageDtls.idsMapping) + "]")
            })

            await this.writeGetStudyIcwLog()
        }catch(err){
            dataRepository.setAppMask(true)
            throw err
        }finally{
            dataRepository.setProgressBar(false, APP_INIT)
            logger.userActionsPerf(LOGGER_NAME.CID_USERACTIONS_PERF_OPENCIDVIEWER_END, "CID Image Viewer is opened.")
        }
    }
}