//@flow

// import utils
//import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'
// import { JL } from 'jsnlog'
import { JL } from '@utils/jsnlog/jsnlog'
import {sleep} from '@utils/CommonUtil'

// import Constant
import { LOGGER_NAME, LOG_LEVEL, LOG_MODE } from '@constants/LogConstants'
import { ENDPOINT } from '@constants/EndpointConstants'
import { UNEXPECTED_ERROR } from '@constants/MsgCodeConstants'

// import vo
import WebSInfo from '@domains/model/vo/WebSInfo'

export class CommonLogController {
    logConfig: *
    setConfig: Function
    beforeSend: Function
    initJSNLog: Function
    dataRepository: *
    appenderMap: *
    JL: *
    CID_EXCEPTION: string
    ERROR_LEVEL: number
    allowConfig: *
    denyConfig: *
    handleLog: Function
    flushLogs: Function
    icwInfo: WebSInfo
    caseProfile: *
    viewerConfig: *
    messageSizeLimit: number
    limitLogMessageSize: Function
    disableLoggingImageBase64: boolean
    getDisableLoggingImageBase64Config: Function
    logMode: string
    isFirstLog: boolean

    getDisableLoggingImageBase64Config(): boolean {
        return this.disableLoggingImageBase64
    }

    constructor() {
        this.disableLoggingImageBase64 = true					  
        this.isFirstLog = true
        this.logMode = LOG_MODE.PARTIAL_LOG
        this.initJSNLog = this.initJSNLog.bind(this)
        this.handleLog = this.handleLog.bind(this)
        this.setConfig = this.setConfig.bind(this)
        this.beforeSend = this.beforeSend.bind(this)
        this.flushLogs = this.flushLogs.bind(this)
        this.limitLogMessageSize = this.limitLogMessageSize.bind(this)
        this.getDisableLoggingImageBase64Config = this.getDisableLoggingImageBase64Config.bind(this)
        this.initJSNLog()
    }

    setConfigWithDataRepository(config: *, dataRepository: *, logMode: string) {
        this.logMode = logMode
        this.dataRepository = dataRepository
        this.setConfig(config, this.dataRepository.findIcwWsAuthInfo(), this.dataRepository.findViewerControlConfig())
    }

    setCaseProfile(systemID: string, accessKey: string, loginID: string, userHospCde: string, wrkStnID: string, 
		hospCde: string, patientKey: string, specialtyCde: string, caseNo: string, accessionNo: string, 
		seriesSeqNo: string, imageSeqNo: string, versionNo: string, appHost: string,
		appSiteName: string, appName: string, sessionID: string) {
        this.caseProfile = {
            hospCode: hospCde,
            caseNo: caseNo,
            accessionNo: accessionNo,
            seriesNo: seriesSeqNo,
            imageSeqNo: imageSeqNo,
            versionNo: versionNo,
            appHost: appHost,
            appSiteName: appSiteName,
            userId: loginID,
            workstationId: wrkStnID,
            workstationHostname: appHost,
            requestSys: systemID,
            accessKey: accessKey,
            userHospCode: userHospCde,
            patientKey: patientKey,
            specialtyCode: specialtyCde,
            sessionID: sessionID
        }
    }

    setConfig(config: *, webServiceIcwInfo: WebSInfo, viewerConfig: *, logMode: string){
        this.logMode = logMode
        
        if(config != undefined) {
            this.appenderMap = new Map()
            this.messageSizeLimit = config.JL.messageSizeLimit
            let appenders: *
            let loggers: *
            let disabledLoggers: *

            this.icwInfo = webServiceIcwInfo
            this.viewerConfig = viewerConfig
            this.allowConfig = config.allowDenyConfig.allow
            this.denyConfig = config.allowDenyConfig.deny

            appenders = config.appenders
            loggers = config.loggers
            disabledLoggers = config.disabledLoggers
            this.disableLoggingImageBase64 = config.JL.disableLoggingImageBase64

            config.JL.setOptions.defaultAjaxUrl = this.icwInfo.contextPath + ENDPOINT.ICW.WRITE_CLIENT_LOG
            config.JL.setOptions.defaultBeforeSend = this.beforeSend

            // JL setconfig
            JL.setOptions(config.JL.setOptions)

            for(let key in appenders) {
                // Config Appenders
                if(appenders[key].type == "AJAX") {
                    let ajaxAppender = JL.createAjaxAppender(key)
                    let options = appenders[key].setOptions

                    if(appenders[key].isDebugLog != undefined && appenders[key].isDebugLog) {
                        let url = this.icwInfo.contextPath + ENDPOINT.ICW.WRITE_CLIENT_DEBUG_LOG
                        options = Object.assign({"url": url}, options)
                    }

                    ajaxAppender.setOptions(options)
                    this.appenderMap.set(key, ajaxAppender)
                } else if(appenders[key].type == "CONSOLE") {
                    let consoleAppender = JL.createConsoleAppender(key)
                    consoleAppender.setOptions(appenders[key].setOptions)
                    this.appenderMap.set(key, consoleAppender)
                } else {
                    throw 'unknown configuration'
                }
            }

            for(let key in loggers) {
                // Configure Logger
                let log = JL(key)
                let options = loggers[key].setOptions
                let arr: Array<*> = new Array(0)
                if(loggers[key].enabled) {
                    for(let index in options.appenders) {
                        arr.push(this.appenderMap.get(options.appenders[index]))
                    }
                }
                options.appenders = arr;
                log.setOptions(options)
            }

            for(let namekey in disabledLoggers) {
                let disabledLogger = JL(disabledLoggers[namekey])
                let options = { "appenders": [] }
                disabledLogger.setOptions(options)
            }
        }
    }

    initJSNLog(){					  
        //console appender for development use       
        let consoleAppender = JL.createConsoleAppender('consoleAppender')
            consoleAppender.setOptions({
                'level': LOG_LEVEL.TRACE
            })

        let rootLogger = JL()
        rootLogger = JL()
        rootLogger.setOptions({
            'appenders': [consoleAppender]
        })

        let that: * = this
        that.CID_EXCEPTION = LOGGER_NAME.CID_EXCEPTION
        that.ERROR_LEVEL = LOG_LEVEL.ERROR
        that.JL = JL
        that.dataRepository = this.dataRepository

        window.addEventListener('beforeunload', (ev: *) => 
		{
            JL.setIsAllXhrAsync(false)

            if(that.dataRepository != null) {
                logger.system(LOGGER_NAME.CID_SYSTEM_CIDIMAGEVIEWER_SESSION_TERMINATED,
                        "beforeunload...CID Image Viewer is closing.")
            }

            that.flushLogs()
        })
      
        window.onerror = function(errorMsg: *, url: *, lineNumber: *, column: *, errorObj: *): boolean {
            that.dataRepository.setMsgDialogByCode(UNEXPECTED_ERROR, errorMsg)
            that.JL(that.CID_EXCEPTION).log(that.ERROR_LEVEL, errorMsg)
            that.flushLogs()
			return true
        }
        
        window.onunhandledrejection = function (event: *) {
            let reason: * = event.reason	 
            that.jsnLogging(LOGGER_NAME.CID_EXCEPTION, LOG_LEVEL.ERROR, reason.stack)
            that.flushLogs()
		}
    }

    beforeSend(xhr: XMLHttpRequest, json: any) {
        xhr.setRequestHeader('Authorization', 'Basic ' + window.btoa(this.icwInfo.username+":"+this.icwInfo.password))
        xhr.setRequestHeader('Cache-Control', 'no-cache')
        xhr.setRequestHeader('Pragma', 'no-cache')
        
        let profile: *
        if(this.dataRepository != undefined) {
            profile = this.dataRepository.findCaseProfile()
        } else {
            profile = this.caseProfile
        }

        let isFullLogging: boolean = false

        if(this.logMode == "FULL_LOG"
            || (this.logMode == "FIRST_FULL_LOG_ONLY" && this.isFirstLog)) {
            isFullLogging = true
            this.isFirstLog = false
        }

        let logContent = {
            sessionId: profile.sessionID,
            pc: profile.workstationId,
            user: profile.userId,
            appid: this.viewerConfig.applicationId,
            appver: this.viewerConfig.applicationVersion,
            sysid: profile.requestSys,
            hospcde: profile.hospCode,
            accno: profile.accessionNo,
            caseno: profile.caseNo,
            userhospcde: profile.userHospCode,
            isfulllogging: isFullLogging
        }

        json = Object.assign(json, logContent)
    }

    limitLogMessageSize(message: *): string {
        //Limit message size
        if(this.messageSizeLimit != undefined && this.messageSizeLimit > 0){
            if(typeof message == "object") {
                message = JSON.stringify(message)
            }
            if(message.length>this.messageSizeLimit){
                message = message.substring(0, this.messageSizeLimit)
                message += "\n"
            }
        }
        return message
    }

    handleLog(loggerName: string, level: number, message: *, msgCode: string) { 
        if(level >= LOG_LEVEL.WARN) {
            let loggerNameSpace = LOGGER_NAME.CID_EXCEPTION
            if(msgCode != undefined && msgCode != "") {
                if(level == LOG_LEVEL.WARN) {
                    loggerNameSpace = LOGGER_NAME.CID_SYSTEM_WARNING_MSGCODE + "." + msgCode
                } else {
                    loggerNameSpace = LOGGER_NAME.CID_EXCEPTION_MSGCODE + "." + msgCode
                }

                if(message == undefined || message == null) {
                    let msgCodeContent = this.dataRepository.findMsgCodeContent(msgCode)
                    message = msgCodeContent.content
                }
            }
            message = this.limitLogMessageSize(message)
            this.jsnLogging(loggerNameSpace, level, message)
            this.flushLogs()
        } else {
            message = this.limitLogMessageSize(message)
            this.jsnLogging(loggerName, level, message)
        }
    }

    async flushLogs(): Promise<void> {
        if(this.appenderMap != undefined) {
            for(var [key, value] of this.appenderMap.entries()) {
                value.sendBatch()
                await sleep(0)
            }
        }
    }

    disableLogging(systemID: string, loginID: string, workstationId: string, hospcde: string) {
        // Allow
        if(this.allowConfig != null) {
            // Check systemID
            if(this.allowConfig.sysId!=null && this.allowConfig.sysId.length>0) {
                if(!this.allowConfig.sysId.includes(systemID)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
            // Check loginID
            if(this.allowConfig.userId!=null && this.allowConfig.userId.length>0) {
                if(!this.allowConfig.userId.includes(loginID)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
            // Check workstationId
            if(this.allowConfig.wrkId!=null && this.allowConfig.wrkId.length>0) {
                if(!this.allowConfig.wrkId.includes(workstationId)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
            // Check hospcde
            if(this.allowConfig.hospcde!=null && this.allowConfig.hospcde.length>0) {
                if(!this.allowConfig.hospcde.includes(hospcde)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
        }
        // Deny
        if(this.denyConfig != null) {
            // Check systemID
            if(this.denyConfig.sysId!=null && this.denyConfig.sysId.length>0) {
                if(this.denyConfig.sysId.includes(systemID)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
            // Check loginID
            if(this.denyConfig.userId!=null && this.denyConfig.userId.length>0) {
                if(this.denyConfig.userId.includes(loginID)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
            // Check workstationId
            if(this.denyConfig.wrkId!=null && this.denyConfig.wrkId.length>0) {
                if(this.denyConfig.wrkId.includes(workstationId)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
            // Check hospcde
            if(this.denyConfig.hospcde!=null && this.denyConfig.hospcde.length>0) {
                if(this.denyConfig.hospcde.includes(hospcde)) {
                    // Disable JL
                    JL.setOptions({
                        'enabled': false
                    })
                }
            }
        }
    }

    jsnLogging(namespace: string, level: number, message: *) {
        JL(namespace).log(level, message)
    }

    resetFirstLogFlag() {
        this.isFirstLog = true
    }
}

// export default new CommonLogController()