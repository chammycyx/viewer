//@flow
import dataRepository from '@utils/DataRepository'
class WindowPositionController {
    reposition(){
        const repostionParm = dataRepository.findRepositionParm()
        const resolutionWidth = screen.width //Main Display Width

        const cmsWindowcenterX = repostionParm.cmsWindowcenterX
        const isMainDisplayPortrait = repostionParm.isMainDisplayPortrait
        const cmsWindowX = repostionParm.cmsWindowX
        const cmsWindowY = repostionParm.cmsWindowY

        if((cmsWindowcenterX < 0||cmsWindowcenterX > resolutionWidth) && isMainDisplayPortrait){
            window.moveTo(cmsWindowX,cmsWindowY)
        }else{
            const viewerX = Math.floor(cmsWindowcenterX/resolutionWidth) * resolutionWidth
            const viewerY = 0
            window.moveTo(viewerX,viewerY)
        }
    }
}

let windowPositionController = new WindowPositionController()

export default windowPositionController