//@flow
import logService from '@domains/services/logService/LogService'
import { ENDPOINT } from '@constants/EndpointConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'
import { CID_ICW_AUTHEN_UNAVAILABLE, CID_WRITE_LOG_RO_UNAVAILABLE,
    CID_ICW_WEBSERVICE_TIMEOUT } from '@constants/MsgCodeConstants'
import dataRepository from '@utils/DataRepository'

// for logging
import logger from '@utils/Logger'

export default class AuditLogController {
    async writeIcwLog(eventId: string, eventTextData: string): Promise<void> {
        try {
            //write log - web service call
            const caseProfile = dataRepository.findCaseProfile()
            const timeout = dataRepository.findWebServiceTimeoutConfig()
            const computer = caseProfile.workstationId
            const hospitalCde = caseProfile.userHospCode

            const icwLogData = {
                accessionNo: caseProfile.accessionNo,
                caseNo: caseProfile.caseNo,
                computer,
                eventId: eventId,
                hospitalCde,
                patientKey: caseProfile.patientKey,
                requestSys: caseProfile.requestSys,
                specialtyCde: caseProfile.specialtyCode,
                userId: caseProfile.userId,
                eventTextData,
                seriesSeqNo: caseProfile.seriesNo, 
                imageSeqNo: caseProfile.imageSeqNo,
                versionNo: caseProfile.versionNo
                //eventValue will merge in logService.icwWriteLog
            }

            const icwWsInfo = dataRepository.findIcwWsAuthInfo()
            await logService.icwWriteLog(icwWsInfo, ENDPOINT.ICW.WRITE_LOG, icwLogData, timeout)
        } catch(cidHttpErr) {
            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_ICW_WEBSERVICE_TIMEOUT)
                logger.errorWithMsgCode(CID_ICW_WEBSERVICE_TIMEOUT)
            }else if(cidHttpErr.status==UNAUTHORIZED) {
                dataRepository.setMsgDialogByCode(CID_ICW_AUTHEN_UNAVAILABLE)
                logger.errorWithMsgCode(CID_ICW_AUTHEN_UNAVAILABLE)
            } else {
                dataRepository.setMsgDialogByCode(CID_WRITE_LOG_RO_UNAVAILABLE)
                logger.errorWithMsgCode(CID_WRITE_LOG_RO_UNAVAILABLE)
            }
            throw cidHttpErr
        }
    }
}