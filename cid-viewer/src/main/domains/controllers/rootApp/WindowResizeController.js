//@flow
import dataRepository from '@utils/DataRepository'
import { RESIZE_WARNING } from '@constants/MsgCodeConstants'
import { RESIZE } from '@constants/WindowEventConstants'
import { RESIZE_APP as RESIZE_APP_LOADING_MSG } from '@constants/WordingConstants'
import logger from '@src/main/utils/Logger'

export default class WindowResizeController{
    viewingPanelController: *

    initResizeApp: Function
    checkIfNeedResizeApp: Function
    bufferResizeApp: Function
    resizeBufferTimerId: TimeoutID
    resizeApp: Function

    constructor() {
        this.initResizeApp = this.initResizeApp.bind(this)
        this.checkIfNeedResizeApp = this.checkIfNeedResizeApp.bind(this)
        this.bufferResizeApp = this.bufferResizeApp.bind(this)
        this.resizeApp = this.resizeApp.bind(this)
    }

    setViewingPanelController(viewingPanelController: *) {
        this.viewingPanelController = viewingPanelController
    }
    
    initResizeApp() {
        window.addEventListener(RESIZE, this.bufferResizeApp, false)
    }

    bufferResizeApp(e: *) {
        dataRepository.setProgressBar(true, RESIZE_APP_LOADING_MSG)

        clearTimeout(this.resizeBufferTimerId)
        this.resizeBufferTimerId = setTimeout(this.checkIfNeedResizeApp, 500) // checkIfNeedResizeApp() will be called after 500ms
    }

    checkIfNeedResizeApp() {
        const slideShowTools = dataRepository.findSlideShowTools()
        
        if(slideShowTools.isModeOn) { // Show warning msg and not resize app if resize app when slide show is playing
            dataRepository.setMsgDialogByCode(RESIZE_WARNING)
            logger.warnWithMsgCode(RESIZE_WARNING)
        } else {
            this.resizeApp()
        }

        dataRepository.setProgressBar(false, RESIZE_APP_LOADING_MSG)
    }

    resizeApp() {
        // Set app width and height, other panels will adjust the width and height themselves then
        dataRepository.setAppSize(window.innerWidth, window.innerHeight)

        this.viewingPanelController.resizeImgViewingPanel()

        // Auto fit, align and set image width and height when resize app
        this.viewingPanelController.resetShownImages()
    }
}