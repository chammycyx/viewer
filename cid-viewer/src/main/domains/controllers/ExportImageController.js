//@flow
import dataRepository from '@utils/DataRepository'
import { DOWNLOAD_EXPORTED_IMAGE } from '@constants/WordingConstants'
import { EVENT_EXPORT_IMAGE } from '@constants/EventConstants'
import { EJB_EXCEPTION, CID_IAS_WEBSERVICE_TIMEOUT,
    CID_IAS_AUTHEN_UNAVAILABLE } from '@constants/MsgCodeConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'
import type { WsRsltObjType } from '@utils/WebServiceBackbone'

// for logging
import logger from '@utils/Logger'
import { LOGGER_NAME } from '@constants/LogConstants'
import viewerLogController from '@domains/controllers/ViewerLogController'

export default class ExportImageController {
    studyService: *
    auditLogController: *

    setStudyService(studyService: *) {
        this.studyService = studyService
    }

    setAuditLogController(auditLogController: *) {
        this.auditLogController = auditLogController
    }

    async exportImage(selectedSeriesId: string, selectedImageIds: Array<string>, password: string): Promise<*> {
        let seriesDtl: * = dataRepository.findSeriesDtl(selectedSeriesId)
        let imgIds: Array<string> = dataRepository.findImageIds(selectedSeriesId, selectedImageIds)
        let timeout = dataRepository.findWebServiceTimeoutConfig()

        logger.userActions(LOGGER_NAME.CID_USERACTIONS_EXPORTIMAGE,
            "exportImage...[seriesId="+seriesDtl.seriesNo+
                "|imageIds="+imgIds.toString()+
                "]"
        )
        logger.userActionsPerf(LOGGER_NAME.CID_USERACTIONS_PERF_EXPORTIMAGE_START,
            "exportImage...[seriesId="+seriesDtl.seriesNo+
                "|imageIds="+imgIds.toString()+
                "]"
        )

        dataRepository.setProgressBar(true, DOWNLOAD_EXPORTED_IMAGE)

        let data = await dataRepository.findExportImageParam(
            selectedSeriesId, 
            selectedImageIds, 
            password
        )

        let exportImageRsltObj: WsRsltObjType

        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_EXPORTIMAGE_START,
            "Invoke 'retrieveImageForExport'...[seriesId="+seriesDtl.seriesNo+
            "|imageIds="+imgIds.toString()+
            "]"
        )

        try {
            exportImageRsltObj = await this.studyService.retrieveImageForExport(data, timeout)
        } catch(cidHttpErr) {
            dataRepository.setProgressBar(false, DOWNLOAD_EXPORTED_IMAGE)

            if(cidHttpErr.isTimeout){
                dataRepository.setMsgDialogByCode(CID_IAS_WEBSERVICE_TIMEOUT)
                logger.errorWithMsgCode(CID_IAS_WEBSERVICE_TIMEOUT)
            } else if(cidHttpErr.status==UNAUTHORIZED) {
                dataRepository.setMsgDialogByCode(CID_IAS_AUTHEN_UNAVAILABLE)
                logger.errorWithMsgCode(CID_IAS_AUTHEN_UNAVAILABLE)
            } else {
                dataRepository.setMsgDialogByCode(EJB_EXCEPTION, cidHttpErr.exceptionMsg)
                logger.error(cidHttpErr.exceptionMsg, EJB_EXCEPTION)
            }
            
            throw cidHttpErr
        }

        let loggingMsg: string
        if(viewerLogController.getDisableLoggingImageBase64Config()) {
            loggingMsg = "Invoke 'retrieveImageForExport' completed."
        } else {
            loggingMsg = "Invoke 'retrieveImageForExport' completed...[RetObj=" + JSON.stringify(exportImageRsltObj) + "]"
        }

        logger.servicePerf(LOGGER_NAME.CID_SERVICE_PERF_IAS_EXPORTIMAGE_END,
            loggingMsg
        )

        let exportImageResponse = exportImageRsltObj.response

        dataRepository.setExportImageDialogForDownload((exportImageResponse)?true:false, exportImageResponse)
    
        logger.userActionsPerf(LOGGER_NAME.CID_USERACTIONS_PERF_EXPORTIMAGE_END,
            "exportImage...END"
        )

        try {
            await this.auditLogController.writeIcwLog(EVENT_EXPORT_IMAGE, JSON.stringify(data))
        } catch(cidHttpErr) {
            dataRepository.setProgressBar(false, DOWNLOAD_EXPORTED_IMAGE)
            throw cidHttpErr
        }

        dataRepository.setProgressBar(false, DOWNLOAD_EXPORTED_IMAGE)
    }
}