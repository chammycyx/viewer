//@flow
// import utils
import dataRepository from '@utils/DataRepository'
import { clone } from '@utils/CloneUtil'
import { isNullOrEmptyString } from '@utils/StringUtil'
import logger from '@utils/Logger'

// import actions
import { RotateDirections, FlipDirections } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import type {ViewModeType} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

// import services
import StudyService from '@domains/services/studyService/StudyService'

// import controllers
import { SlideShowController } from '@domains/controllers/SlideShowController'

// import constants
import { ImageSelection, ImageTool, RELOCATE_IMAGE_IN_CANVAS_FACTOR,
     ContrastPanelTool } from '@constants/ComponentConstants'
import { EJB_EXCEPTION, CID_IAS_WEBSERVICE_TIMEOUT,
     CID_IAS_AUTHEN_UNAVAILABLE } from '@constants/MsgCodeConstants'
import { LOGGER_NAME } from '@constants/LogConstants'
import { CanvasProfileInitState } from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'
import { RETRIEVE_IMAGE } from '@constants/WordingConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'
import cidTheme from '@styles/CidTheme'


const MAX_WIDTH_HEIGHT: number = 5000
const MIN_WIDTH_HEIGHT: number = 20
const IMAGE_ZOOM_FACTOR: number = 0.1
const IMAGE_FIT_FACTOR: number = 0.9

export class ViewingPanelController{
    imgService: StudyService
    dataRepository: typeof dataRepository
    loadingHandler: *
    showImage: Function
    showImage4SlideShow: Function
    zoomImage: Function
    setGrid: Function
    resetSlideShowImages: Function
    privateMembers: PrivateMembers
    resetImages: Function
    resetShownImages: Function
    resetSelectedImages: Function
    rotateImage: Function
    flipImage: Function
    setColorAdjustment: Function
    selectCanvas: Function
    moveImage: Function
    resizeImgViewingPanel: Function
    getInitImageProperty: Function

    constructor(imgService: StudyService, repository: typeof dataRepository, loadingHandler: *){
        this.privateMembers = new PrivateMembers()

        this.imgService = imgService
        this.dataRepository = repository
        this.loadingHandler = loadingHandler

        this.showImage = this.showImage.bind(this)
        this.zoomImage = this.zoomImage.bind(this)
        this.setGrid = this.setGrid.bind(this)

        this.showImage4SlideShow = this.showImage4SlideShow.bind(this)
        this.resetImages = this.resetImages.bind(this)
        this.resetSlideShowImages = this.resetSlideShowImages.bind(this)
        this.resetSelectedImages = this.resetSelectedImages.bind(this)
        this.resetShownImages = this.resetShownImages.bind(this)

        this.rotateImage = this.rotateImage.bind(this)
        this.flipImage = this.flipImage.bind(this)
        this.setColorAdjustment = this.setColorAdjustment.bind(this)

        this.selectCanvas = this.selectCanvas.bind(this)
        this.moveImage = this.moveImage.bind(this)
        this.resizeImgViewingPanel = this.resizeImgViewingPanel.bind(this)
        this.getInitImageProperty = this.getInitImageProperty.bind(this)
    }

    async retrieveNSaveImages(seriesIndexId?: string = '', imgIndexIds?: Array<string> = []): Promise<void> {
        let seriesIndexIdInFunc
        let imgIndexIdsInFunc

        let seriesDtl
        let toDlImgDtls: Array<*> = []
        let toDlImgIndexIds = []

        // If seriesIndexId is empty, the current seriesId will be used
        if(seriesIndexId != '') {
            seriesIndexIdInFunc = seriesIndexId
        } else {
            let canvasGridTree = this.dataRepository.findCanvasGrid()
            seriesIndexIdInFunc = canvasGridTree.selectedSeriesId
        }

        // Init seriesDtl
        seriesDtl = this.dataRepository.findSeriesDtl(seriesIndexIdInFunc)
        
        // If imgIndexIds is empty array, all hightest ver image ids of current series will be used
        if(imgIndexIds.length > 0) {
            imgIndexIdsInFunc = imgIndexIds
        } else {
            imgIndexIdsInFunc = seriesDtl.imageDtls.allHighestVerIds
        }
        
        imgIndexIdsInFunc.forEach((imgIndexId: string)=>{
            let isImageExist = this.dataRepository.isImageExist(seriesIndexIdInFunc, imgIndexId)
            if(!isImageExist){
                toDlImgIndexIds.push(imgIndexId)

                let imageDtl = this.dataRepository.findImageDtl(seriesIndexIdInFunc, imgIndexId)
                let vo = {}
                vo.seriesNo = seriesDtl.seriesNo
                vo.imageID = imageDtl.imageID
                vo.imageSequence = imageDtl.imageSequence
                vo.imageVersion = imageDtl.imageVersion
                toDlImgDtls.push(vo) 
            }
        })

        if(toDlImgDtls.length>0){
            let baseParm = this.dataRepository.findStudyServiceBaseParm(seriesIndexIdInFunc)
            const timeout = this.dataRepository.findWebServiceTimeoutConfig()
            try{
                let base64s = await this.imgService.retrieveImages(toDlImgDtls, baseParm, timeout)
                this.dataRepository.saveImages(seriesIndexIdInFunc, toDlImgIndexIds , base64s)
            }catch(cidHttpErr){
                dataRepository.setProgressBar(false, RETRIEVE_IMAGE)

                if(cidHttpErr.isTimeout){
                    dataRepository.setMsgDialogByCode(CID_IAS_WEBSERVICE_TIMEOUT)
                    logger.errorWithMsgCode(CID_IAS_WEBSERVICE_TIMEOUT)
                } else if(cidHttpErr.status==UNAUTHORIZED) {
                    dataRepository.setMsgDialogByCode(CID_IAS_AUTHEN_UNAVAILABLE)
                    logger.errorWithMsgCode(CID_IAS_AUTHEN_UNAVAILABLE)
                } else {
                    dataRepository.setMsgDialogByCode(EJB_EXCEPTION, cidHttpErr.exceptionMsg)
                    logger.error(cidHttpErr.exceptionMsg, EJB_EXCEPTION)
                }
                
                throw cidHttpErr
            }
        }
    }

    getInitImageProperty(canvasWidth: number, canvasHeight: number, imageWidth: number, imageHeight: number, imageFitFactor: number): *{
        let imgProperties= {}

        //set image alignment
        Object.assign(imgProperties, this.privateMembers.imgAlignmentRule(canvasWidth, canvasHeight))

        //set image auto fit
        Object.assign(imgProperties, this.privateMembers.imgAutoFitRule(canvasWidth, canvasHeight, imageWidth, imageHeight, imageFitFactor))

        return imgProperties
    }

    async showImage(seriesIndexId: string, imgIndexIds: Array<string>): Promise<void>{
        await this.retrieveNSaveImages(seriesIndexId, imgIndexIds)

        //set images to the datasource of Viewing Panel
        const imagesDimensions = await this.dataRepository.findImageDimensions(seriesIndexId, imgIndexIds)
        const canvasGrid = this.dataRepository.findCanvasGrid()
        const imageHeaderConfig = this.dataRepository.findImageHeaderConfig()
        const studyDtlTree = this.dataRepository.findStudyDtl()
        const studyDateTime = studyDtlTree.rootState.studyDtm


        let imgsProperties = []

        for(let i=0; i<imgIndexIds.length; i++){
            const imgIndexId = imgIndexIds[i]
            
            let imgProperties = {}
            
            //set image alignment and auto fit
            const imageWidth = imagesDimensions[i].width
            const imageHeight = imagesDimensions[i].height
            Object.assign(imgProperties, this.getInitImageProperty(canvasGrid.canvasWidth, canvasGrid.canvasHeight, imageWidth, imageHeight, IMAGE_FIT_FACTOR))
            
            //set image width height
            imgProperties.imageWidth = imageWidth
            imgProperties.imageHeight = imageHeight
            
            //set header properties
            imgProperties.headerVisibility = ViewingPanelController.isHeaderVisible(seriesIndexId)
            imgProperties.headerTimeText = imageHeaderConfig.rtcImageCreateDateTime?' Image Create Date/Time ':' Study Date/Time: '
            //set displayDateTime of header
            const imgDtl = this.dataRepository.findImageDtl(seriesIndexId, imgIndexId)
            const imageCreateTimeStampAnnotation = this.dataRepository.findAnnotationDtl(seriesIndexId, imgIndexId, 'ImageCreateTimeStamp')
            let imageCreateTimeStamp = ""
            if(imageCreateTimeStampAnnotation!=null){
                imageCreateTimeStamp = imageCreateTimeStampAnnotation.annotationText
            }
            imgProperties.displayDateTime = imageHeaderConfig.rtcImageCreateDateTime?imageCreateTimeStamp.substr(0,14):studyDateTime


            imgsProperties.push(imgProperties)
        }

        const seriesDtl: * = dataRepository.findSeriesDtl(seriesIndexId)
        const imageIds: Array<string> = dataRepository.findImageIds(seriesIndexId, imgIndexIds)
        logger.userActions(LOGGER_NAME.CID_USERACTIONS_PERF_VIEWPANEL_SHOWIMAGES_START, 
            "showImage...[seriesId="+seriesDtl.seriesNo+"|imageIds="+imageIds.toString()+"]")

        await this.dataRepository.setImagesToViewingPanel(seriesIndexId, imgIndexIds, imgsProperties)
    }

    async showImage4SlideShow(seriesIndexId: string, imgIndexIds: Array<string>, seqCanvas: number): Promise<void>{
        const imageIndexId4SlideShow = imgIndexIds[seqCanvas]
        const imageIndexId = SlideShowController.getImageIndexIdWithoutSuffix(imageIndexId4SlideShow)

        //set images to the datasource of Viewing Panel
        const canvasGrid = this.dataRepository.findCanvasGrid()
        
        let imageProperties = {}
        
        let canvasProfile4SlideShow = canvasGrid.canvasProfiles.byId[imageIndexId4SlideShow]
        
        if(canvasProfile4SlideShow == null) {
            //set image width height
            const imagesDimensions = await this.dataRepository.findImageDimensions(seriesIndexId, [imageIndexId])
            const imageWidth = imagesDimensions[0].width
            const imageHeight = imagesDimensions[0].height
            Object.assign(imageProperties, {imageWidth, imageHeight})

            const slideShowTools = this.dataRepository.findSlideShowTools()
            const offset = slideShowTools.refOffsets.bySeq[seqCanvas]
            const offsetWidth = offset.width
            const offsetHeight = offset.height
            
            //set image alignment
            Object.assign(imageProperties, this.privateMembers.imgAlignmentRule(offsetWidth, offsetHeight))

            //set image auto fit
            Object.assign(imageProperties, this.privateMembers.imgAutoFitRule(offsetWidth, offsetHeight, imageWidth, imageHeight, IMAGE_FIT_FACTOR))

        } else {

            let imgAlignments = { 
                imageOriginX: canvasProfile4SlideShow.imageOriginX, 
                imageOriginY: canvasProfile4SlideShow.imageOriginY, 
                imageXCoor: canvasProfile4SlideShow.imageXCoor, 
                imageYCoor: canvasProfile4SlideShow.imageYCoor
            }
            
            Object.assign(imageProperties, imgAlignments)
            
            let imgScales = {
                imageScaleX: canvasProfile4SlideShow.imageScaleX, 
                imageScaleY: canvasProfile4SlideShow.imageScaleY
            } 

            Object.assign(imageProperties, imgScales)
            
            Object.assign(imageProperties, {imageWidth:canvasProfile4SlideShow.imageWidth, imageHeight: canvasProfile4SlideShow.imageHeight})
        }
        
        //set image adjustments. Follow the adjustment of the first slide show image
        const showImageIds = canvasGrid.showImageIds
        const profile4Ref = canvasGrid.canvasProfiles.byId[showImageIds[seqCanvas]]

        const brightness = profile4Ref.brightness
        const contrast = profile4Ref.contrast
        const hue = profile4Ref.hue
        const saturation = profile4Ref.saturation
        const invert = profile4Ref.invert
        const rotateAngle = profile4Ref.rotateAngle
        const flipHorizontal = profile4Ref.flipHorizontal
        const flipVertical = profile4Ref.flipVertical    
        Object.assign(imageProperties, {brightness, contrast, hue, saturation, invert, rotateAngle, flipHorizontal, flipVertical})

        await this.dataRepository.setImageToSlideShow(seriesIndexId, imageIndexId4SlideShow, seqCanvas, imageProperties)
    }

    rotateImage(rotateDirection: string) {
        const canvasGridTree = this.dataRepository.findCanvasGrid()
        const selectedImgIdxIds = canvasGridTree.selectedImageIds
        const selectedSeriesIndexId: string = canvasGridTree.selectedSeriesId
        const seriesDtl: * = this.dataRepository.findSeriesDtl(selectedSeriesIndexId)

        for(let i=0; i<selectedImgIdxIds.length; i++) {
            const imageIdxId: string = selectedImgIdxIds[i]
            const imageId: string = this.dataRepository.findImageId(selectedSeriesIndexId, imageIdxId)
            let rotateAngle: number = canvasGridTree.canvasProfiles.byId[imageIdxId].rotateAngle

            if(rotateDirection == RotateDirections.LEFT) {
                rotateAngle = (rotateAngle - 90) % 360
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_FR_ROTATE_LEFT, 
                    "rotateImage...[rotateDir="+rotateDirection+"|rotateAngle="+rotateAngle+"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")
            } else {
                rotateAngle = (rotateAngle + 90) % 360
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_FR_ROTATE_RIGHT, 
                    "rotateImage...[rotateDir="+rotateDirection+"|rotateAngle="+rotateAngle+"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")
            }

            this.dataRepository.setImageRotation(imageIdxId, rotateAngle)
        }
    }

    /**
     * Flip image
     * 
     * @param {string} flipDirectionToUser The direction of image flipping in user's perspective
     * @memberof ViewingPanelController
     */
    flipImage(flipDirectionToUser: string) {
        const canvasGridTree = this.dataRepository.findCanvasGrid()
        const selectedImgIdxIds = canvasGridTree.selectedImageIds
        const selectedSeriesIndexId: string = canvasGridTree.selectedSeriesId
        const seriesDtl = dataRepository.findSeriesDtl(selectedSeriesIndexId)

        for(let i=0; i<selectedImgIdxIds.length; i++) {
            const imgIdxId: string = selectedImgIdxIds[i]
            
            // Flip by user's perspective
            const canvasProfile = canvasGridTree.canvasProfiles.byId[imgIdxId]
            const isOrientationChanged = (canvasProfile.rotateAngle%180!=0)
            
            //The x-axis and y-axis of image remains unchanged after rotated. It means that making horizontal flip(image.flipX) to image when it is rotated 90 would appear to flip vertically to user.
            //In respect to rotation angle 90 and 270, image flipped Vertically to user's perspective is actually flipping image vertically in image itself, and vice versa.
            let flipDirectionToImage: string = flipDirectionToUser
            if(isOrientationChanged){
                flipDirectionToImage = (flipDirectionToUser==FlipDirections.HORIZONTAL)?FlipDirections.VERTICAL:FlipDirections.HORIZONTAL
            }

            let flipVertical: boolean = canvasProfile.flipVertical
            let flipHorizontal: boolean = canvasProfile.flipHorizontal
            if(flipDirectionToImage==FlipDirections.HORIZONTAL){
                flipHorizontal = !flipHorizontal
            }else{
                flipVertical = !flipVertical
            }

            //Logging
            const imageId: string = this.dataRepository.findImageId(selectedSeriesIndexId, imgIdxId)
            if(flipDirectionToUser == FlipDirections.HORIZONTAL) {
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_FR_FLIP_HORIZONTAL,
                    "setFlipImage...[flipDir="+flipDirectionToUser+"|flipDirByUserPerpective="+flipDirectionToImage+"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")
            } else if(flipDirectionToUser == FlipDirections.VERTICAL) {
                logger.userActions(LOGGER_NAME.CID_USERACTIONS_FR_FLIP_VERTICAL,
                    "setFlipImage...[flipDir="+flipDirectionToUser+"|flipDirByUserPerpective="+flipDirectionToImage+"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")
            }

            this.dataRepository.setImageFlip(imgIdxId, flipHorizontal, flipVertical)
        }
    }

    zoomImage(seriesIdxId: string, imageIdxId: string, zoomMode: string, zoomX: number, zoomY: number, pannedCoordinateRule: Function){
        if(([ImageTool.ZOOM_IN, ImageTool.ZOOM_OUT]).indexOf(zoomMode)<0){
            return
        }

        const seriesDtl: * = this.dataRepository.findSeriesDtl(seriesIdxId)
        const imageId: string = this.dataRepository.findImageId(seriesIdxId, imageIdxId)

        let scaleFactor = 0
        if(ImageTool.ZOOM_IN==zoomMode){
            scaleFactor = 1+IMAGE_ZOOM_FACTOR
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_ZOOM_IN,
                "zoomImage...[zoomMode="+zoomMode+"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")
        }else if(ImageTool.ZOOM_OUT==zoomMode){
            scaleFactor = 1-IMAGE_ZOOM_FACTOR
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_ZOOM_OUT,
                "zoomImage...[zoomMode="+zoomMode+"|seriesId="+seriesDtl.seriesNo+"|imageId="+imageId+"]")
        }

        const canvasProfile = this.dataRepository.findCanvasProfile(seriesIdxId, imageIdxId)
        const imageWidth = canvasProfile.imageWidth
        const imageHeight = canvasProfile.imageHeight
        const currentScaleXFactor = canvasProfile.imageScaleX
        const currentScaleYFactor = canvasProfile.imageScaleY
        const nextScaleXFactor = currentScaleXFactor*scaleFactor
        const nextScaleYFactor = currentScaleYFactor*scaleFactor
        const nextScaledWidth = imageWidth * nextScaleXFactor
        const nextScaledHeight = imageHeight * nextScaleYFactor
        
        //1. max width/height is 5000
        //2. max size is 5000*5000
        if(nextScaledWidth>MAX_WIDTH_HEIGHT || nextScaledHeight>MAX_WIDTH_HEIGHT){
            return
        }

        //3. lower limit scale is 20px
        if(nextScaledWidth<MIN_WIDTH_HEIGHT || nextScaledHeight<MIN_WIDTH_HEIGHT){
            return
        }

        const imageXCoor = canvasProfile.imageXCoor
        const imageYCoor = canvasProfile.imageYCoor
        const pannedCoor = pannedCoordinateRule(imageWidth, imageHeight, imageXCoor, imageYCoor, zoomX, zoomY, currentScaleXFactor, currentScaleYFactor, nextScaleXFactor, nextScaleYFactor)

        this.dataRepository.setImageScale(seriesIdxId, imageIdxId, nextScaleXFactor, nextScaleYFactor, pannedCoor.xCoor, pannedCoor.yCoor)
    }

    async resetImages(seriesIndexId: string, imgIndexIds: Array<string>, isResetOtherProperties?: boolean = true): Promise<void>{
        const canvasGrid = this.dataRepository.findCanvasGrid()
        
        let imgsProperties = []
        for(let i=0; i<imgIndexIds.length; i++){
            const imgIndexId = imgIndexIds[i]

            let imgProperties = {}

            const canvasProfile = this.dataRepository.findCanvasProfile(seriesIndexId, imgIndexId)
            const imageWidth = canvasProfile.imageWidth
            const imageHeight = canvasProfile.imageHeight

            if(isResetOtherProperties){
                Object.assign(imgProperties, CanvasProfileInitState, {imageWidth, imageHeight})
            }else{
                Object.assign(imgProperties, canvasProfile)
            }
            
            //reset image alignment and image auto fit
            Object.assign(imgProperties, this.getInitImageProperty(canvasGrid.canvasWidth, canvasGrid.canvasHeight, imageWidth, imageHeight, IMAGE_FIT_FACTOR))

            imgsProperties.push(imgProperties)
        }

        // logging
        if(isResetOtherProperties){
            const selectedSeriesIndexId: string = canvasGrid.selectedSeriesId
            const seriesDtl: * = this.dataRepository.findSeriesDtl(selectedSeriesIndexId)
            const imageIds: string[] = this.dataRepository.findImageIds(selectedSeriesIndexId, imgIndexIds)
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_RESET,
                "resetImages...[seriesId="+seriesDtl.seriesNo+"|imageIds="+imageIds.toString()+"]")
        }

        await this.dataRepository.setResetImagesToViewingPanel(seriesIndexId, imgIndexIds, imgsProperties)
    }

    async resetSelectedImages(): Promise<void> {
        const canvasGrid = this.dataRepository.findCanvasGrid()
        const seriesId = canvasGrid.selectedSeriesId
        const selectedImageIds = canvasGrid.selectedImageIds

        await this.resetImages(seriesId, selectedImageIds, true)
    }

    async resetShownImages(): Promise<void> {
        const canvasGrid = this.dataRepository.findCanvasGrid()
        const seriesId = canvasGrid.selectedSeriesId
        const showImageIds = canvasGrid.showImageIds

        await this.resetImages(seriesId, showImageIds, false)
    }
	
    setGrid(viewMode: ViewModeType){
        const canvasGrid = this.dataRepository.findCanvasGrid()
        const seriesId: string = canvasGrid.selectedSeriesId
        const showImageIds: Array<string> = canvasGrid.showImageIds

        //calculate new canvas width and height
        const panelHeight = canvasGrid.height
        const panelWidth = canvasGrid.width
        const gridDimensions: {width: number, height: number} = this.privateMembers.calculateGridDimensions(panelWidth, panelHeight, viewMode)
        const canvasWidth = gridDimensions.width
        const canvasHeight = gridDimensions.height

        this.dataRepository.setGridsToViewingPanel(viewMode, canvasWidth, canvasHeight)
    }

    async resetSlideShowImages(seqCanvas?: number = -1): Promise<void>{
        const canvasGrid = this.dataRepository.findCanvasGrid()
        const viewMode: ViewModeType = this.dataRepository.findViewMode()
        const seriesId: string = canvasGrid.selectedSeriesId
        let allImageIds: Array<string> = []
        if(seriesId != null && seriesId != '') {
            allImageIds = this.dataRepository.findSeriesDtl(seriesId).imageDtls.allHighestVerIds
        }

        let slideShowImageIds = []
        let startCanvas = 0
		let endCanvas = viewMode.noOfImage
		if(seqCanvas > -1) {
			startCanvas = seqCanvas
			endCanvas = seqCanvas + 1
		}
		for (let g=startCanvas; g<endCanvas; g++) {
            for(let i=0; i<allImageIds.length; i++){
                const imgIndexId = allImageIds[i]
                let imgIndexId4SlideShow = SlideShowController.getImageIndexIdWithSuffix(imgIndexId, g)

                const canvasProfile = this.dataRepository.findCanvasProfile(seriesId, imgIndexId4SlideShow)
                if (canvasProfile == null){
                    continue
                }

                slideShowImageIds.push(imgIndexId4SlideShow)
            }

        }

        if(slideShowImageIds.length > 0){
            await this.resetImages(seriesId, slideShowImageIds, false)
        }
    }

    // select canvas based on clicking 
    selectCanvas(clickedImageId: string, button: string) {
        const canvasGridTree = this.dataRepository.findCanvasGrid()
        let selectedImageIds = clone(canvasGridTree.selectedImageIds)

        if(!isNullOrEmptyString(clickedImageId)) {

            const imageTool = canvasGridTree.imageTool

            if(button == ImageSelection.CTRL) {
                // CTRL + Click
                if(selectedImageIds.indexOf(clickedImageId) > -1){
                    // deselect if not using zoom in / zoom out tool
                    if(imageTool != ImageTool.ZOOM_IN && imageTool != ImageTool.ZOOM_OUT) {
                        selectedImageIds.splice(selectedImageIds.indexOf(clickedImageId), 1)
                    }
                } else {
                    // select
                    selectedImageIds.push(clickedImageId)
                }
            } else if(button == ImageSelection.SHIFT && selectedImageIds.length > 0) {
                // SHIFT + Click
                const showImageIds: Array<string> = canvasGridTree.showImageIds.slice()

                // selected image id for the start of SHIFT selection
                const startingSelectedImageID: string = selectedImageIds[selectedImageIds.length-1]
                
                const indexStartImage: number = showImageIds.indexOf(startingSelectedImageID)
                const indexEndImage: number = showImageIds.indexOf(clickedImageId)

                // select images from start to end(clicked)
                if(indexStartImage < indexEndImage){
                    selectedImageIds = showImageIds.slice(indexStartImage, indexEndImage + 1).reverse()
                }else{
                    selectedImageIds = showImageIds.slice(indexEndImage, indexStartImage + 1)
                }
            } else {
                // Click
                selectedImageIds = [clickedImageId]
            }

            // logging
            const selectedSeriesIndexId: string = canvasGridTree.selectedSeriesId
            const seriesDtl: * = this.dataRepository.findSeriesDtl(selectedSeriesIndexId)
            const imageIds: string[] = this.dataRepository.findImageIds(selectedSeriesIndexId, selectedImageIds)
            logger.userActions(LOGGER_NAME.CID_USERACTIONS_VIEWPANEL_SELECT, 
                "setSelectedCanvas...[seriesId="+seriesDtl.seriesNo+"|imageIds="+imageIds.toString()+"|button="+button+"]")

            this.dataRepository.setCanvasSelectedImageId(selectedImageIds)
        }
    }

    moveImage(seriesIndexId: string, imageIndexId: string, imageXCoor: number, imageYCoor: number){
        //Canvas border is a css border (eg. border: 1px solid #00D2FF')
        //It is out of the top and left side of canvas but in the bottom and right side of canvas
        //Therefore, border width affect the cal. of 'isOutsideBottom' and 'isOutsideRight'.
        const borderWidth = 2
        const buffer = 2
        
        const canvasGrid = this.dataRepository.findCanvasGrid()
        const canvasProfile = this.dataRepository.findCanvasProfile(seriesIndexId, imageIndexId)

        const canvasHeaderHeight = canvasGrid.headerHeight
        
        const resetHeight: number = canvasGrid.canvasHeight * RELOCATE_IMAGE_IN_CANVAS_FACTOR
        const resetWidth: number = canvasGrid.canvasWidth * RELOCATE_IMAGE_IN_CANVAS_FACTOR
        
        const isOrientationChanged = (canvasProfile.rotateAngle%180!=0)
        let halfScaledHeight: number = canvasProfile.imageHeight * canvasProfile.imageScaleY / 2
        let halfScaledWidth: number = canvasProfile.imageWidth * canvasProfile.imageScaleX / 2
        if(isOrientationChanged){
            const temp = halfScaledHeight
            halfScaledHeight = halfScaledWidth
            halfScaledWidth = temp
        }
        
        const isOutsideTop: boolean = (imageYCoor + halfScaledHeight - canvasHeaderHeight) <= buffer
        const isOutsideBottom: boolean = (imageYCoor - canvasGrid.canvasHeight 
                                        - halfScaledHeight + borderWidth) >= -buffer
        const isOutsideLeft: boolean = (imageXCoor + halfScaledWidth) <= buffer
        const isOutsideRight: boolean = (imageXCoor - canvasGrid.canvasWidth 
                                        - halfScaledWidth + borderWidth) >= -buffer

        let adjustedImageYCoor: number = imageYCoor
        if(isOutsideTop){
            adjustedImageYCoor = canvasHeaderHeight - halfScaledHeight + resetHeight
        }else if(isOutsideBottom){
            adjustedImageYCoor = canvasGrid.canvasHeight + halfScaledHeight - resetHeight
        }

        let adjustedImageXCoor: number = imageXCoor
        if(isOutsideLeft){
            adjustedImageXCoor = resetWidth - halfScaledWidth
        }else if(isOutsideRight){
            adjustedImageXCoor = canvasGrid.canvasWidth + halfScaledWidth - resetWidth
        }

        const seriesDtl = this.dataRepository.findSeriesDtl(seriesIndexId)
        let imageId = this.dataRepository.findImageId(seriesIndexId, SlideShowController.getImageIndexIdWithoutSuffix(imageIndexId))
        logger.userActions(LOGGER_NAME.CID_USERACTIONS_VIEWPANEL_PAN, 
            "moveImage...[seriesId="+seriesDtl.seriesNo+",imageId="+imageId+",xCoor="+adjustedImageXCoor+",yCoor="+adjustedImageYCoor+"]", true)

        this.dataRepository.setImagePosition(seriesIndexId, imageIndexId, adjustedImageXCoor, adjustedImageYCoor)
    }

    /**
     * This function would 
     * 1. Resize Image Viewing Panel 
     * 2. Resize width/height of canvas grid
     * 3. Adjust the x/y coor images
     * 
     * @memberof ViewingPanelController
     */
    resizeImgViewingPanel(){
        const appDimension = this.dataRepository.findAppDimension()
        const {appWidth} = appDimension
        const {appHeight} = appDimension
        const isPictorialIndexPanelShown = this.dataRepository.isPictorialIndexPanelShown()
        const canvasGridState = this.dataRepository.findCanvasGrid()

        //updated viewing area width height
        const nextViewingAreaWidth = isPictorialIndexPanelShown?(appWidth - cidTheme.pictorialIndexPanel.width):(appWidth - cidTheme.pictorialIndexPanel.showHideButton.showButton.width)
        const nextViewingAreaHeight = appHeight - cidTheme.toolBarPanel.height

        //updated canvas width height
        const nextCanvasDimensions: {width: number, height: number} = this.privateMembers.calculateGridDimensions(nextViewingAreaWidth, nextViewingAreaHeight, this.dataRepository.findViewMode())
        const currentCanvasWidth = canvasGridState.canvasWidth
        const nextCanvasWidth = nextCanvasDimensions.width
        const nextCanvasHeight = nextCanvasDimensions.height

        //updated image x&y coordinate
        const selectedSeriesId = canvasGridState.selectedSeriesId
        const showImageIds = canvasGridState.showImageIds
        let imgsProperty = []
        for(let i=0;i<showImageIds.length;i++){
            const imageId = showImageIds[i]
            // const imgDetail = this.dataRepository.findImageDtl(selectedSeriesId, imageId)
            const canvasProfile = this.dataRepository.findCanvasProfile(selectedSeriesId, imageId)
            const rotateAngle = canvasProfile.rotateAngle
            const halfWidth = canvasProfile.imageWidth*canvasProfile.imageScaleX/2
            const halfHeight = canvasProfile.imageHeight*canvasProfile.imageScaleY/2
            let tlCornerCoorOfImageAfterRotated = canvasProfile.imageXCoor
            if((rotateAngle%180)==0){
                tlCornerCoorOfImageAfterRotated -= halfWidth
            }else{
                tlCornerCoorOfImageAfterRotated -= halfHeight
            }

            //Image is invisible after pictorial index expanded
            if(tlCornerCoorOfImageAfterRotated>nextCanvasDimensions.width){
                const imgsXcoor = canvasProfile.imageXCoor - (currentCanvasWidth-nextCanvasWidth)
                imgsProperty.push({imgSeriesId:selectedSeriesId, imgId:imageId, imgXcoor:imgsXcoor})
            }
        }

        this.dataRepository.setViewingAreaDimension(nextViewingAreaWidth, nextViewingAreaHeight, nextCanvasWidth, nextCanvasHeight, imgsProperty)
    }

	setColorAdjustment(brightness: number, contrast: number, hue: number, saturation: number, invert: boolean, contrastToolIdx: number) {
        let canvasGrid: * = this.dataRepository.findCanvasGrid()
        let selectedSeriesIndexId: string = canvasGrid.selectedSeriesId
        let selectedImageIndexIds: Array<string> = canvasGrid.selectedImageIds
        let seriesDtl = this.dataRepository.findSeriesDtl(selectedSeriesIndexId)
        let imageIds = this.dataRepository.findImageIds(selectedSeriesIndexId, selectedImageIndexIds)
        let loggerName: string = ""
        switch(contrastToolIdx) {
            case ContrastPanelTool.BRIGHTNESS:
                loggerName = LOGGER_NAME.CID_USERACTIONS_CONTRASTPANEL_BRIGHTNESS
                break
            case ContrastPanelTool.CONTRAST:
                loggerName = LOGGER_NAME.CID_USERACTIONS_CONTRASTPANEL_CONTRAST
                break
            case ContrastPanelTool.HUE:
                loggerName = LOGGER_NAME.CID_USERACTIONS_CONTRASTPANEL_HUE
                break
            case ContrastPanelTool.SATURATION:
                loggerName = LOGGER_NAME.CID_USERACTIONS_CONTRASTPANEL_SATURATION
                break
            case ContrastPanelTool.INVERT:
                loggerName = LOGGER_NAME.CID_USERACTIONS_CONTRASTPANEL_INVERT
                break
            case ContrastPanelTool.RESET:
                loggerName = LOGGER_NAME.CID_USERACTIONS_CONTRASTPANEL_RESET
                break
        }
        if(loggerName != null) {
            logger.userActions(loggerName, 
                "setColorAdjustment...[seriesId="+seriesDtl.seriesNo+"|imageIds="+imageIds.toString()+
                "|brightness="+brightness+"|contrast="+contrast+"|hue="+hue+
                "|saturation="+saturation+"|invert="+invert.toString()+"]",
                true)
        }
        this.dataRepository.setColorAdjustmentToViewingPanel(brightness, contrast, hue, saturation, invert)
    }

    static isHeaderVisible(seriesIdxId: string): boolean{
        const imageHeaderConfig: * = dataRepository.findImageHeaderConfig()
        const seriesExamType: string = dataRepository.findSeriesDtl(seriesIdxId).examType
        return (imageHeaderConfig.visibility && imageHeaderConfig.rtcExamType==seriesExamType)
    }
    
}

class PrivateMembers{

    constructor(){
    }

    calculateGridDimensions(panelWidth: number, panelHeight: number, viewMode: ViewModeType): {width: number, height: number}{
    //grid info for view mode
    let noOfRow: number = Number(viewMode.text.split('x')[0])
    let noOfCol: number = Number(viewMode.text.split('x')[1])
    let rowHeight: number = panelHeight/noOfRow
    let rowWidth: number = panelWidth/noOfCol
    
        return {width: rowWidth, height: rowHeight}
    }

    imgAutoFitRule(canvasWidth: number, canvasHeight: number, imgWidth: number, imgHeight: number, fitFactor: number): {imageScaleX: number, imageScaleY: number}{
        let maxWidthForImg: number = canvasWidth * fitFactor
        let maxHeightForImg: number = canvasHeight * fitFactor


        if(imgWidth<=maxWidthForImg && imgHeight<=maxHeightForImg){
            return {imageScaleX: 1, imageScaleY: 1}
        }else{
            let factorX: number = maxWidthForImg / imgWidth
            let factorY: number = maxHeightForImg / imgHeight
            let finalFactor = Math.min( factorX , factorY )
            return {imageScaleX: finalFactor, imageScaleY: finalFactor} 
        }
    }

    imgAlignmentRule(canvasWidth: number, canvasHeight: number): {imageOriginX: string, imageOriginY: string, imageXCoor: number, imageYCoor: number}{
        let imageXCoor = canvasWidth/2
        let imageYCoor = canvasHeight/2
        return {imageOriginX: 'center', imageOriginY: 'center', imageXCoor, imageYCoor}
    }

    toRelativeX(imgWidth: number, imgLeft: number, absoluteX: number, imgOriginX: string = 'center'): number{
        if(imgOriginX=='center'){
            return (imgWidth/2)+(absoluteX-imgLeft)
        }
        return 0
    }


    toScaledRelativeX(relativeX: number, width: number, scaledWidth: number): number{
        return relativeX * scaledWidth / width
    }

    toAbsoluteX(imgWidth: number, imgLeft: number, relativeX: number): number{
        return relativeX - (imgWidth/2) + imgLeft
    }


    toRelativeY(imgHeight: number, imgTop: number, absoluteY: number, imgOriginY: string = 'center'): number{
        if(imgOriginY=='center'){
            return (imgHeight/2)+(absoluteY-imgTop)
        }
        return 0
    }
    
    toScaledRelativeY(relativeY: number, height: number, scaledHeight: number): number{
        return relativeY * scaledHeight / height
    }

    toAbsoluteY(imgHeight: number, imgTop: number, relativeY: number): number{
        return relativeY - (imgHeight/2) + imgTop
    }

}