//@flow
import type {ViewModeType} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import dataRepository from '@utils/DataRepository'
import {ViewingPanelController} from '@domains/controllers/ViewingPanelController'
import {PictorialIndexPanelController} from '@domains/controllers/PictorialIndexPanelController'

export class ViewModeController{
    viewingPanelController: ViewingPanelController
    pictorialIndexPanelController: PictorialIndexPanelController
    changeViewMode: Function

    constructor(viewingPanelController: ViewingPanelController, pictorialIndexPanelController: PictorialIndexPanelController){
        this.viewingPanelController = viewingPanelController
        this.pictorialIndexPanelController = pictorialIndexPanelController

        this.changeViewMode = this.changeViewMode.bind(this)
    }


    async changeViewMode(viewMode: ViewModeType): Promise<void>{
        dataRepository.setViewMode(viewMode)

        this.viewingPanelController.setGrid(viewMode)
        await this.pictorialIndexPanelController.updateShowImagesWhenViewModeChanged(viewMode)
        this.viewingPanelController.resetSlideShowImages()
    }

}