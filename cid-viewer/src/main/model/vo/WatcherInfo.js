//@flow

export default class WatcherInfo{

    watchState: Function
    callback: Function

    constructor(watchState: Function, callback: Function){
        this.watchState = watchState
        this.callback = callback
    }
}