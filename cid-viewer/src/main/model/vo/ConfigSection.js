import ConfigEntry from './ConfigEntry'

class ConfigSection{
    constructor(){
        this.name = null
        this.entries = new Array(/*ConfigEntry*/);
        this.addEntry = this.addEntry.bind(this)
    }

    addEntry(key, value){
        this.entries.push(new ConfigEntry(key, value))
    }

}

export default ConfigSection