import CidImageViewerProxy from '@src/main/proxy/CidImageViewerProxy'
import {clone} from '@utils/CloneUtil'
import logger from '@utils/Logger'
import { getJsonFile } from '@utils/WebServiceBackbone'
jest.mock('@utils/WebServiceBackbone')//Note that calling jest.mock(‘./dependency’) will replace all exported functions of dependency.js with mock functions.

const html5ViewerStub = {
    html5Viewer: ["VH","PMH"]
}

describe('Test Class: <CidImageViewerProxy>', ()=>{
    let oriCreateElement
    beforeAll(()=>{
        oriCreateElement = document.createElement
    })

    afterEach(() => {
        document.createElement = oriCreateElement
    });

    beforeEach(()=>{

    })

    describe('Test Function: <CidImageViewerProxy#openCIDImageViewer>', ()=>{
        describe('Open Html5 Image Viewer', ()=>{
            test('Open Html5 Image Viewer by HospCode VH', async ()=>{
                //Arrange
                let cidImageViewerProxy = new CidImageViewerProxy()
                    cidImageViewerProxy.proxyConfigJson = clone(html5ViewerStub)
                    cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
                    cidImageViewerProxy.openAspImageViewer = jest.fn()
                    cidImageViewerProxy.closeCIDImageViewer = jest.fn()
                    // cidImageViewerProxy.getScriptSrc = jest.fn().mockImplementation(() => {
                    //     return "http://localhost/cid.js"
                    // })

                //Act
                // await cidImageViewerProxy.openCIDImageViewer(null, null, null, null, null, null, null, null, null, "VH ER1800000530X")
                await cidImageViewerProxy.openCIDImageViewer(null, null, null, "VH", null, null, null, null, null, null)

                //Assert
                expect(cidImageViewerProxy.openHtml5ImageViewer).toHaveBeenCalledTimes(1)
                expect(cidImageViewerProxy.openAspImageViewer).toHaveBeenCalledTimes(0)
            })

            test('Open Html5 Image Viewer by HospCode PMH', async ()=>{
                //Arrange
                let cidImageViewerProxy = new CidImageViewerProxy()
                    cidImageViewerProxy.proxyConfigJson = clone(html5ViewerStub)
                    cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
                    cidImageViewerProxy.openAspImageViewer = jest.fn()
                    cidImageViewerProxy.closeCIDImageViewer = jest.fn()
                    // cidImageViewerProxy.getScriptSrc = jest.fn().mockImplementation(() => {
                    //     return "http://localhost/cid.js"
                    // })

                //Act
                // await cidImageViewerProxy.openCIDImageViewer(null, null, null, null, null, null, null, null, null, "PMH ER1800000530X")
                await cidImageViewerProxy.openCIDImageViewer(null, null, null, "PMH", null, null, null, null, null, null)

                //Assert
                expect(cidImageViewerProxy.openHtml5ImageViewer).toHaveBeenCalledTimes(1)
                expect(cidImageViewerProxy.openAspImageViewer).toHaveBeenCalledTimes(0)
            })
        })

        test('Open Flex Image Viewer', async ()=>{
            //Arrange
            const proxyConfigJson = {
                html5Viewer: ["VH","PMH1"]
            }
            let cidImageViewerProxy = new CidImageViewerProxy()
                cidImageViewerProxy.proxyConfigJson = proxyConfigJson
                cidImageViewerProxy.closeCIDImageViewer = jest.fn()
                cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
                cidImageViewerProxy.openAspImageViewer = jest.fn()

            //Act
            await cidImageViewerProxy.openCIDImageViewer(null, null, null, "PMH", null, null, null, null, null, null)

            //Assert
            expect(cidImageViewerProxy.openHtml5ImageViewer).toHaveBeenCalledTimes(0)
            expect(cidImageViewerProxy.openAspImageViewer).toHaveBeenCalledTimes(1)
        })

        test('Open Flex Image Viewer due to exception thrown when retrieving proxyConfig.json', async ()=>{
            //Arrange
            getJsonFile.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    reject("ErrorABC")
                })
            })
            const proxyConfigJson = {
                html5Viewer: ["VH"]
            }
            let cidImageViewerProxy = new CidImageViewerProxy()
                cidImageViewerProxy.proxyConfigJson = proxyConfigJson
                cidImageViewerProxy.closeCIDImageViewer = jest.fn()
                cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
                cidImageViewerProxy.openAspImageViewer = jest.fn()

            //Act
            // await cidImageViewerProxy.openCIDImageViewer(null, null, null, null, null, null, null, null, null, "PMH ER1800000530X")
            await cidImageViewerProxy.openCIDImageViewer(null, null, null, null, null, null, null, null, null, null)

            //Assert
            expect(cidImageViewerProxy.openHtml5ImageViewer).toHaveBeenCalledTimes(0)
            expect(cidImageViewerProxy.openAspImageViewer).toHaveBeenCalledTimes(1)
        })

        test('Cannot open Image Viewer due to proxyConfigJson download failure', async ()=>{
            //Arrange
            const proxyConfigJson = null
            let cidImageViewerProxy = new CidImageViewerProxy()
                cidImageViewerProxy.proxyConfigJson = proxyConfigJson
                cidImageViewerProxy.closeCIDImageViewer = jest.fn()
                cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
                cidImageViewerProxy.openAspImageViewer = jest.fn()
                window.alert = jest.fn()

            //Act
            await cidImageViewerProxy.openCIDImageViewer(null, null, null, null, null, null, null, null, null, null)

            //Assert
            expect(window.alert).toBeCalledWith('<CID Service Error>\nError Code: 3203\nError message: Failed to open Image Viewer. Configuration file download failure.\nPlease search again. Please contact call center for help if the service is not resumed.')
            expect(cidImageViewerProxy.openHtml5ImageViewer).toHaveBeenCalledTimes(0)
            expect(cidImageViewerProxy.openAspImageViewer).toHaveBeenCalledTimes(0)
        })
        
        test('Open Html5 Image Viewer when proxyConfigJson is not ready', async ()=>{
            const CidImageViewerProxyExports = require('../../main/proxy/CidImageViewerProxy.js')
            CidImageViewerProxyExports.isProxyConfReady = true
            let cidImageViewerProxyClass = CidImageViewerProxyExports.default

            let getConfigFilesReturnObj = jest.fn()
            cidImageViewerProxyClass.prototype.getConfigFiles = () => {
                return new Promise((resolve, reject) => {
                    resolve(getConfigFilesReturnObj())
                })  
            }  

            let cidImageViewerProxy = new cidImageViewerProxyClass()

            let waitUntilConfigReadyReturnObj = jest.fn()
            cidImageViewerProxy.waitUntilConfigReady = () => {
                return new Promise((resolve, reject) => {
                    resolve(waitUntilConfigReadyReturnObj())
              })
            }

            cidImageViewerProxy.isProxyConfReady = false
            cidImageViewerProxy.proxyConfigJson = clone(html5ViewerStub)
            cidImageViewerProxy.initProxy = jest.fn()
            cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
            cidImageViewerProxy.openAspImageViewer = jest.fn()
            cidImageViewerProxy.closeCIDImageViewer = jest.fn()

            //Act
            await cidImageViewerProxy.openCIDImageViewer(null, null, null, "VH", null, null, null, null, null, null)

            //Assert
            expect(waitUntilConfigReadyReturnObj).toHaveBeenCalledTimes(1)
            expect(getConfigFilesReturnObj).toHaveBeenCalledTimes(1)
            expect(cidImageViewerProxy.initProxy).toHaveBeenCalledTimes(1)
        })

        test('Open Html5 Image Viewer when proxyConfigJson is ready', async ()=>{
            const CidImageViewerProxyExports = require('../../main/proxy/CidImageViewerProxy.js')
            CidImageViewerProxyExports.isProxyConfReady = true
            let cidImageViewerProxyClass = CidImageViewerProxyExports.default

            let getConfigFilesReturnObj = jest.fn()
            cidImageViewerProxyClass.prototype.getConfigFiles = () => {
                return new Promise((resolve, reject) => {
                    resolve(getConfigFilesReturnObj())
                })  
            }  

            let cidImageViewerProxy = new cidImageViewerProxyClass()


            let waitUntilConfigReadyReturnObj = jest.fn()
            cidImageViewerProxy.waitUntilConfigReady = () => {
                return new Promise((resolve, reject) => {
                    resolve(waitUntilConfigReadyReturnObj())
              })
            }


            cidImageViewerProxy.isProxyConfReady = true
            cidImageViewerProxy.proxyConfigJson = clone(html5ViewerStub)
            cidImageViewerProxy.initProxy = jest.fn()
            cidImageViewerProxy.openHtml5ImageViewer = jest.fn()
            cidImageViewerProxy.openAspImageViewer = jest.fn()
            cidImageViewerProxy.closeCIDImageViewer = jest.fn()

            //Act
            await cidImageViewerProxy.openCIDImageViewer(null, null, null, "VH", null, null, null, null, null, null)

            //Assert
            expect(waitUntilConfigReadyReturnObj).toHaveBeenCalledTimes(0)
            expect(getConfigFilesReturnObj).toHaveBeenCalledTimes(1)
            expect(cidImageViewerProxy.initProxy).toHaveBeenCalledTimes(1)
            expect(cidImageViewerProxy.openHtml5ImageViewer).toHaveBeenCalledTimes(1)
            expect(cidImageViewerProxy.openAspImageViewer).toHaveBeenCalledTimes(0)
        })
    })

    describe('Test Function: <CidImageViewerProxy#closeCIDImageViewer>', ()=>{
        test('Close successfully', ()=>{
            //Arrange
            let imageViewerProxy = new CidImageViewerProxy()
                imageViewerProxy.viewerWindow = {close:jest.fn()}
    
            //Act
            imageViewerProxy.closeCIDImageViewer()
    
            //Assert
            expect(imageViewerProxy.viewerWindow.close).toHaveBeenCalledTimes(1)
        })
        
        test('Exception thrown during closing',()=>{
            //Arrange
            let width = 200
            let heighr = 100
            let imageViewerProxy = new CidImageViewerProxy()
                imageViewerProxy.getScreenHeight = function(){return heighr}
                imageViewerProxy.getScreenWidth = function(){return width}
            
            window.open = jest.fn()
            window.open.mockReturnValue({close:jest.fn()})

            //Act
            imageViewerProxy.closeCIDImageViewer()
    
            //Assert
            expect(window.open.mock.calls[0][0]).toBe('')
            expect(window.open.mock.calls[0][1]).toBe('CIDImageViewer')
            expect(window.open.mock.calls[0][2]).toBe('height=75,width=190,status=yes,resizable=yes')
            expect(imageViewerProxy.viewerWindow.close).toHaveBeenCalledTimes(1)
        })
    })

})

