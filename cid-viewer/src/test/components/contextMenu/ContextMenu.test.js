import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

jest.mock('../../../main/utils/VersionUtil.js', function() {
    return {
        VERSION: jest.requireActual('../../../main/utils/VersionUtil.js').VERSION,
        _appVersion: jest.requireActual('../../../main/utils/VersionUtil.js')._appVersion,
        getVersion: jest.fn().mockReturnValue('6.0.0.1'),
        getConfigVersion: jest.requireActual('../../../main/utils/VersionUtil.js').getConfigVersion,
        getServiceVersion: jest.requireActual('../../../main/utils/VersionUtil.js').getServiceVersion
    }
})


import ContextMenu, { styles } from '@components/contextMenu/ContextMenu'
import MenuItem from '@material-ui/core/MenuItem'

import AboutDialog from '@components/contextMenu/dialogs/AboutDialog'

configure({ adapter: new Adapter() })

describe('Test Suite: <ContextMenu>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // Declare testing values for props
    const onOpen = jest.fn()
    const aboutTitle = 'About CID Viewer (6.0.0.1)...'

    // The node to be tested
    const createNode = function(isAboutDialogOpen) {
        return <ContextMenu
            store={store}
            theme={theme}
            classes={classes}
            onAboutDialogOpen={onOpen}/>
    }
    const node1 = createNode()

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'ContextMenu')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of MenuItem', () => {
        expect(wrapper1.find(MenuItem)).toHaveLength(1)
    })

    test('+++ Verify the props passed to children', () => {
        let menuItem = wrapper1.find(MenuItem).at(0)
        expect(menuItem.props().children).toEqual(aboutTitle)
        menuItem.props().onClick()
        expect(onOpen).toHaveBeenCalledTimes(1)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
