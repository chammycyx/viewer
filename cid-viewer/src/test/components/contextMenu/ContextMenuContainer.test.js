import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import ContextMenuContainer from '@components/contextMenu/ContextMenuContainer'
import ContextMenu from '@components/contextMenu/ContextMenu'
import { setIsOpen } from '@actions/components/contextMenu/aboutDialog/Action'

configure({ adapter: new Adapter() })

describe('Test Suite: <ContextMenuContainer>', () => {

    // Mock store
    const mockStore = configureStore()
    const store1 = mockStore()

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

     // The node to be tested
     const createNode = function(store){
        return <ContextMenuContainer
            store={store}
            theme={theme}
        />
    }
    const node1 = createNode(store1)

    // Shallow render the wrapped component
    const wrapper1 = shallow(node1)

    test('+++ Verify the container component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the action dispatchers', () => {
        let contextMenuContainer = wrapper1.find(ContextMenu)
        contextMenuContainer.props().onAboutDialogOpen()
        const actions = store1.getActions()
        expect(actions).toEqual([setIsOpen(true)])
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
