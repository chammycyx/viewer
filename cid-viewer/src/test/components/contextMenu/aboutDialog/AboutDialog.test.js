import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import AboutDialog, { styles } from '@components/contextMenu/dialogs/AboutDialog'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogActions from '@material-ui/core/DialogActions'
import Button from '@material-ui/core/Button'

import { getConfigVersion, getServiceVersion } from '@utils/VersionUtil'

configure({ adapter: new Adapter() })

describe('Test Suite: <AboutDialog>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // Declare testing values for props
    const onClose = jest.fn()
    const testingValue1 = {
        isOpen: true,
        applicationVersion: '5.0.0',
        cidWsContextPath: 'http://test/ICW/Path_1_0_1',
        requestSys: 'RIS',
        userId: 'ISG',
        hospCode: 'QEH'
    }
    const testingValue2 = {
        isOpen: false,
        applicationVersion: '6.0.0.0',
        cidWsContextPath: 'http://test/ICW/Path_2_3_4_6',
        requestSys: 'TEST123',
        userId: 'USER_ID',
        hospCode: 'VH'
    }

    // The node to be tested
    const createNode = function(testingValue) {
        return <AboutDialog
            store={store}
            theme={theme}
            classes={classes}
            isOpen={testingValue.isOpen}
            onClose={onClose}
            applicationVersion={testingValue.applicationVersion}
            cidWsContextPath={testingValue.cidWsContextPath}
            requestSys={testingValue.requestSys}
            userId={testingValue.userId}
            hospCode={testingValue.hospCode}
            />
    }
    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'AboutDialog')
    const wrapper2 = shallowWrappedComponent(node2, 'AboutDialog')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of MenuItem', () => {
        expect(wrapper1.find(Dialog)).toHaveLength(1)
        expect(wrapper1.find(DialogTitle)).toHaveLength(1)
        expect(wrapper1.find(DialogContent)).toHaveLength(1)
        expect(wrapper1.find(DialogActions)).toHaveLength(1)
        expect(wrapper1.find(Button)).toHaveLength(1)
    })

    test('+++ Verify the props passed to children', () => {
        let okButton = wrapper1.find(Button).at(0)
        okButton.props().onClick()
        expect(onClose).toHaveBeenCalledTimes(1)
    })

    const createTest = function (wrapper, testingValue) {
        let dialog = wrapper.find(Dialog).at(0)
        expect(dialog.props().open).toEqual(testingValue.isOpen)

        let versionConfig = wrapper.find('span').at(5)
        expect(versionConfig.props().children)
            .toEqual(": "+getConfigVersion(testingValue.applicationVersion))

        let versionService = wrapper.find('span').at(7)
        expect(versionService.props().children)
            .toEqual(": "+getServiceVersion(testingValue.cidWsContextPath))

        let requestSys = wrapper.find('span').at(10)
        expect(requestSys.props().children).toEqual(": "+testingValue.requestSys)

        let userId = wrapper.find('span').at(12)
        expect(userId.props().children).toEqual(": "+testingValue.userId)

        let hospCode = wrapper.find('span').at(14)
        expect(hospCode.props().children).toEqual(": "+testingValue.hospCode)
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(wrapper2, testingValue2)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
