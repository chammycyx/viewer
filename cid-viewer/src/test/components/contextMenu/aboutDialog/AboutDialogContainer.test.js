import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import AboutDialogContainer from '@components/contextMenu/dialogs/AboutDialogContainer'
import AboutDialog from '@components/contextMenu/dialogs/AboutDialog'
import { setIsOpen } from '@actions/components/contextMenu/aboutDialog/Action'

configure({ adapter: new Adapter() })

describe('Test Suite: <AboutDialogContainer>', () => {
    // Declare testing state
    const initialState1 = {
        components: {
            contextMenu: {
                aboutDialog: {
                    rootState: {
                        isOpen: true
                    }
                }
            }
        },
        data: {
            config: {
                application: {
                    rootState: {
                        viewerControl: {
                            applicationVersion: "5.0.0"
                        },
                        webServiceIcw: {
                            cidWsContextPath: "http://test/ICW/Path1"
                        }
                    }
                }
            },
            common:{
                rootState:{
                    requestSys: 'RIS1',
                    userId: 'ISG1',
                    hospCode: 'VH1'
                }
            }
        }
    }

    const initialState2 = {
        components: {
            contextMenu: {
                aboutDialog: {
                    rootState: {
                        isOpen: false
                    }
                }
            }
        },
        data: {
            config: {
                application: {
                    rootState: {
                        viewerControl: {
                            applicationVersion: '6.0.0.0'
                        },
                        webServiceIcw: {
                            cidWsContextPath: 'http://test/ICW/Path2'
                        }
                    }
                }
            },
            common:{
                rootState:{
                    requestSys: 'RIS2',
                    userId: 'ISG2',
                    hospCode: 'VH2'
                }
            }
        }
    }

    // Mock store
    const mockStore = configureStore()
    const store1 = mockStore(initialState1)
    const store2 = mockStore(initialState2)

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // The node to be tested
    const createNode = function(store){
        return <AboutDialogContainer
            store={store}
            theme={theme}
        />
    }
    const node1 = createNode(store1)
    const node2 = createNode(store2)

    // Shallow render the wrapped component
    const wrapper1 = shallow(node1)
    const wrapper2 = shallow(node2)

    test('+++ Verify the container component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the action dispatchers', () => {
        let aboutDialogContainer = wrapper1.find(AboutDialog)
        aboutDialogContainer.props().onClose()
        const actions = store1.getActions()
        expect(actions).toEqual([setIsOpen(false)])
    })

    test('+++ Verify if there is any hard-code state - Case 1', () => {
        let aboutDialogContainer = wrapper1.find(AboutDialog)
        expect(aboutDialogContainer.props().isOpen).toEqual(initialState1.components.contextMenu.aboutDialog.rootState.isOpen)
    })

    test('+++ Verify if there is any hard-code state - Case 2', () => {
        let aboutDialogContainer = wrapper2.find(AboutDialog)
        expect(aboutDialogContainer.props().isOpen).toEqual(initialState2.components.contextMenu.aboutDialog.rootState.isOpen)
    })
    
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
