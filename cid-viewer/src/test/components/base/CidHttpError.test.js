import CidHttpError from '@components/base/CidHttpError'

describe('Test Suite: <CidHttpError>', () => {
    test('Test CidHttpError - 4 arguments', ()=>{

        //Arrange mock object
        let className = 'CLASS1'
        let funcName = 'FUNC1'
        let url = 'TEST2/testing'
        
        let data = {
            data1: 'TEST4_1',
            data2: 2,
            data3: {
                data3_1: 'TEST4_3'
            }
        }

        let exceptionMsg = 'TEST5'
        let getResponseHeader = jest.fn().mockImplementationOnce(function(header){
            if(header=='Exception-Message'){
                return exceptionMsg
            }
            return 'Non-Exception-Message'
        })
        let status = 500
        let statusText = 'TEST7'
        let err = {
            request: {
                status,
                statusText,
                getResponseHeader: getResponseHeader
            }
        }
        
        //Act
        let cidHttpErr = new CidHttpError(className, funcName, url, err, data)

        //Assert

        let msg = className+'.'+funcName+'\n'
        + 'Context Path: ' + url + '\n'
        + 'Status & Text: ' + status + ' ' + statusText + '\n'
        + 'Exception message: ' + exceptionMsg + '\n'
        + 'Data: ' + JSON.stringify(data) + '\n'
        + 'Error Content: ' + JSON.stringify(err) + err

        expect(cidHttpErr.message).toEqual(msg)
        expect(cidHttpErr.exceptionMsg).toEqual(exceptionMsg)
        expect(cidHttpErr.status).toEqual(status)
        expect(getResponseHeader).toHaveBeenCalledTimes(1)
        
    })

    test('Test CidError - 3 arguments', ()=>{

        //Arrange mock object
        let className = 'FUNC1'
        let funcName = 'TEST1'
        let url = 'TEST2/testing'
        let err = 'TEST3_Error'
        let exceptionMsg = ''
        let status = -1
        let statusText = ''
        let data = ''
        
        //Act
        let cidHttpErr = new CidHttpError(className, funcName, url, err)

        //Assert
        let msg = className+'.'+funcName+'\n'
        + 'Context Path: ' + url + '\n'
        + 'Status & Text: ' + status + ' ' + statusText + '\n'
        + 'Exception message: ' + exceptionMsg + '\n'
        + 'Data: ' + JSON.stringify(data) + '\n'
        + 'Error Content: ' + JSON.stringify(err) + err

        expect(cidHttpErr.message).toEqual(msg)
        expect(cidHttpErr.exceptionMsg).toEqual(exceptionMsg)
    })
    
})