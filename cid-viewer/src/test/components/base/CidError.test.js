import CidError from '@components/base/CidError'

describe('Test Suite: <CidError>', () => {
    test('Test CidError', ()=>{

        //Arrange mock object
        let className = 'CLASS_1'
        let funcName = 'FUNC_1'
        let plainMsg = 'PLAIN_MSG_1'
        
        //Act
        let cidErr = new CidError(className, funcName, plainMsg)

        //Assert
        let classFunc = className+'.'+funcName
        let msgWithClassFuncPath = classFunc+'\n'+plainMsg

        expect(cidErr.message).toEqual(msgWithClassFuncPath)
        
    })
})