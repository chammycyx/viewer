import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import cidPropsBox, { styles } from '@components/base/CidPropsBox'
import Paper from '@material-ui/core/Paper'
import DummyComponent from '@utils/DummyComponent'

jest.mock('@components/base/CidPropsBox.jsx')

// Bypass DOM related logic
const mockReactRef = {
    current: {
        style: {
            top: 0,
            left: 0
        }
    }
}
jest.mock('../../../main/utils/ValidationByPassUtil.js', () => ({
    getReactCreateRef() {
        return mockReactRef
    }
}))

configure({ adapter: new Adapter() })

describe('Test Suite: <CidPropsBox>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles()(theme)

    // Declare testing values for props
    const mockOnDragMouseDown = jest.fn()
    const mockResetPosition = jest.fn()
    
    const testingValue1 = {
        visible: true,
        opacity: 1,
        resetPositionWhenReopen: true,
        x: '0px',
        y: '800px'
    }
    
    const testingValue2 = {
        visible: false,
        opacity: 0.7,
        resetPositionWhenReopen: false,
        x: '10px',
        y: '810px'
    }

    // The node to be tested
    const createNode = function(CidPropsBox, testingValue) {
        return <CidPropsBox
            store={store}
            theme={theme}
            classes={classes}
            visible={testingValue.visible}
            opacity={testingValue.opacity}
            resetPositionWhenReopen={testingValue.resetPositionWhenReopen}
            x={testingValue.x}
            y={testingValue.y}/>
    }

    const CidPropsBox = cidPropsBox(undefined, mockOnDragMouseDown, mockResetPosition)(DummyComponent)
    const node1 = createNode(CidPropsBox, testingValue1)
    const node2 = createNode(CidPropsBox, testingValue2)

    // Shallow render the wrapped component
    const parentWrapper1 = shallowWrappedComponentParent(node1, 'CidPropsBox(DummyComponent)')
    const wrapper1 = shallowWrappedComponent(node1, 'CidPropsBox(DummyComponent)')
    const parentWrapper2 = shallowWrappedComponentParent(node2, 'CidPropsBox(DummyComponent)')
    const wrapper2 = shallowWrappedComponent(node2, 'CidPropsBox(DummyComponent)')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of button', () => {
        expect(wrapper1.find('div')).toHaveLength(1)
        expect(wrapper1.find(Paper)).toHaveLength(1)
        expect(wrapper1.find(DummyComponent)).toHaveLength(1)
    })

    test('+++ Verify the props passed to children (callbacks))', () => {
        let wrappedComp = wrapper1.find(DummyComponent).at(0)
        wrappedComp.props().onDragMouseDown()
        // CidPropsBox2.prototype.onDragMouseDown()
        expect(mockOnDragMouseDown).toHaveBeenCalledTimes(1)
    })

    //Verify the props passed to children
    const createTest = function (parentWrapper, wrapper, testingValue){
        let parent = parentWrapper.find('CidPropsBox(DummyComponent)').at(0)

        let outerDiv = wrapper.find('div').at(0)
        expect(outerDiv.props().className).toEqual(classes.outerDiv)
        expect(outerDiv.props().className.visibility(parent.props())).toEqual(testingValue.visible ? 'visible' : 'hidden')

        let paper = wrapper.find(Paper).at(0)
        expect(paper.props().className).toEqual(classes.paper)
        expect(paper.props().className.opacity(parent.props())).toEqual(testingValue.opacity)

        let wrappedComp = wrapper.find(DummyComponent).at(0)
        expect(wrappedComp.props().propsBoxRef).toEqual(mockReactRef)
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, testingValue2)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
