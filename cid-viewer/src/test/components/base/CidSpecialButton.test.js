import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import cidSpecialButton, { styles } from '@components/base/CidSpecialButton'

configure({ adapter: new Adapter() })

describe('Test Suite: <CidSpecialButton>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    const theme = createMuiTheme(cidTheme)
    const classes = styles(theme)

    // Declare testing values for props
    const onClick = jest.fn()

    const testingValue1 = {
        width: 10,
        height: 20,
        iconNormal: 'iconNormal1',
        iconOver: 'iconOver1',
        iconDisabled: 'iconDisabled1',
        onClickParams: [],
        isEnabled: true,
        toolTipTitle: 'toolTipTitle1'
    }
    
    const testingValue2 = {
        width: 12,
        height: 22,
        iconNormal: 'iconNormal2',
        iconOver: 'iconOver2',
        iconDisabled: 'iconDisabled2',
        onClickParams: ['onClickParams2'],
        isEnabled: true
    }

    const testingValue3 = {
        width: 14,
        height: 24,
        iconNormal: 'iconNormal3',
        iconOver: 'iconOver3',
        iconDisabled: 'iconDisabled3',
        onClickParams: [],
        isEnabled: false,
        toolTipTitle: 'toolTipTitle2'
    }

    // The node to be tested
    const createNode = function(CidSpecialButton, testingValue) {
        return <CidSpecialButton
            store={store}
            theme={theme}
            classes={classes}
            onClick={onClick}
            width={testingValue.width}
            height={testingValue.height}
            isEnabled={testingValue.isEnabled}
            toolTipTitle={testingValue.toolTipTitle}/>
    }
    const CidSpecialButton1 = cidSpecialButton(testingValue1.iconNormal, testingValue1.iconOver, testingValue1.iconDisabled, 'CidSpecialButton1', testingValue1.onClickParams)
    const node1 = createNode(CidSpecialButton1, testingValue1)
    const CidSpecialButton2 = cidSpecialButton(testingValue2.iconNormal, testingValue2.iconOver, testingValue2.iconDisabled, 'CidSpecialButton2', testingValue2.onClickParams)
    const node2 = createNode(CidSpecialButton2, testingValue2)
    const CidSpecialButton3 = cidSpecialButton(testingValue3.iconNormal, testingValue3.iconOver, testingValue3.iconDisabled, 'CidSpecialButton3', testingValue3.onClickParams)
    const node3 = createNode(CidSpecialButton3, testingValue3)

    // Shallow render the wrapped component
    const parentWrapper1 = shallowWrappedComponentParent(node1, 'CidSpecialButton1')
    const wrapper1 = shallowWrappedComponent(node1, 'CidSpecialButton1')
    const parentWrapper2 = shallowWrappedComponentParent(node2, 'CidSpecialButton2')
    const wrapper2 = shallowWrappedComponent(node2, 'CidSpecialButton2')
    const parentWrapper3 = shallowWrappedComponentParent(node3, 'CidSpecialButton3')
    const wrapper3 = shallowWrappedComponent(node3, 'CidSpecialButton3')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of button', () => {
        expect(wrapper1.find('button')).toHaveLength(1)
        expect(wrapper1.find('img')).toHaveLength(1)
    })

    //Verify the props passed to children
    const createTest = function (parentWrapper, wrapper, testingValue, wrappedComponentDisplayName){
        let parent = parentWrapper.find(wrappedComponentDisplayName).at(0)
        
        let div = wrapper.find('div').at(0)
        expect(div.props().className).toEqual(classes.div)
        expect(div.props().className.width(parent.props())).toEqual(testingValue.width)
        expect(div.props().className.height(parent.props())).toEqual(testingValue.height)

        let button = wrapper.find('button').at(0)
        expect(button.props().className).toEqual(classes.button)
        expect(button.props().className.width(parent.props())).toEqual(testingValue.width)
        expect(button.props().className.height(parent.props())).toEqual(testingValue.height)

        if(testingValue.toolTipTitle != undefined && testingValue.toolTipTitle != '') {
            expect(button.parent().props().title).toEqual(testingValue.toolTipTitle)
        } else {
            expect(button.parent().props().title).toBeUndefined()
        }
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, testingValue1, 'CidSpecialButton1')
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, testingValue2, 'CidSpecialButton2')
    })

    test('+++ Verify the props passed to children without hard-code output - Case 3', () => {
        createTest(parentWrapper3, wrapper3, testingValue3, 'CidSpecialButton3')
    })

    test('+++ Verify behaviors of children (Case 1: Enabled, no param in onClick)', () => {
        let button
        let img

        // Check on click
        button = wrapper1.find('button').at(0)
        button.simulate('click')
        expect(onClick).toHaveBeenCalledTimes(1)

        // Before mouse enter
        expect(wrapper1.state().hover).toEqual(false)
        img = wrapper1.find('img').at(0)
        expect(img.props().src).toEqual(testingValue1.iconNormal)

        // When mouse enter
        button.simulate('mouseEnter')
        expect(wrapper1.state().hover).toEqual(true)
        wrapper1.update() //After state change, shallow wrapper needs to be updated to trigger re-render
        img = wrapper1.find('img').at(0) //Get img after re-render
        expect(img.props().src).toEqual(testingValue1.iconOver)
        
        // When mouse leave
        button.simulate('mouseLeave')
        expect(wrapper1.state().hover).toEqual(false)
        wrapper1.update()
        img = wrapper1.find('img').at(0)
        expect(img.props().src).toEqual(testingValue1.iconNormal)
    })

    test('+++ Verify behaviors of children (Case 2: Enabled, 1 param in onClick)', () => {
        let button

        // Check on click
        button = wrapper2.find('button').at(0)
        button.simulate('click')
        expect(onClick).toHaveBeenCalledTimes(2)
        expect(onClick.mock.calls[1][0]).toEqual(testingValue2.onClickParams[0])
    })

    test('+++ Verify behaviors of children (Case 3: Disabled)', () => {
        let button
        let img

        // Check on click
        button = wrapper3.find('button').at(0)
        button.simulate('click')
        expect(onClick).toHaveBeenCalledTimes(2) //Disabled so still called twice

        // Before mouse enter
        expect(wrapper3.state().hover).toEqual(false)
        img = wrapper3.find('img').at(0)
        expect(img.props().src).toEqual(testingValue3.iconDisabled)

        // When mouse enter
        button.simulate('mouseEnter')
        expect(wrapper3.state().hover).toEqual(true)
        wrapper3.update() //After state change, shallow wrapper needs to be updated to triggered re-render
        img = wrapper3.find('img').at(0) //Get img after re-render
        expect(img.props().src).toEqual(testingValue3.iconDisabled)
        
        // When mouse leave
        button.simulate('mouseLeave')
        expect(wrapper3.state().hover).toEqual(false)
        wrapper3.update()
        img = wrapper3.find('img').at(0)
        expect(img.props().src).toEqual(testingValue3.iconDisabled)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
