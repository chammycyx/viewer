import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import cidToolBarButton, { styles } from '@components/base/CidToolBarButton'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'

configure({ adapter: new Adapter() })

describe('Test Suite: <CidToolBarButton>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    const theme = createMuiTheme(cidTheme)
    const classes = styles(theme)

    // Declare testing values for props
    const onClick = jest.fn()

    const mockIcon = '@assets/images/btnExport.png'
    const mockIcon_over = '@assets/images/btnExport_over.png'
    const mockIcon_disable = '@assets/images/btnExport_disable.png'

    const testingValue1 = {
        isEnabled: true
    }

    const testingValue2 = {
        isEnabled: false
    }

    const testingValue3 = {
        isEnabled: true,
        toolTipTitle: 'toolTipTitle3'
    }

    const testingValue4 = {
        isEnabled: false,
        toolTipTitle: 'toolTipTitle4'
    }

    // The node to be tested
    const createNode = function(CidToolBarButton, testingValue) {
        return <CidToolBarButton
            store={store}
            theme={theme}
            classes={classes}
            onClick={onClick}
            isEnabled={testingValue.isEnabled}
            toolTipTitle={testingValue.toolTipTitle}
            />
    }

    const CidToolBarButton1 = cidToolBarButton(mockIcon, mockIcon_over, mockIcon_disable, 'CidToolBarButton1')
    const node1 = createNode(CidToolBarButton1, testingValue1)

    const CidToolBarButton2 = cidToolBarButton(mockIcon, mockIcon_over, mockIcon_disable, 'CidToolBarButton2')
    const node2 = createNode(CidToolBarButton2, testingValue2)

    const CidToolBarButton3 = cidToolBarButton(mockIcon, mockIcon_over, mockIcon_disable, 'CidToolBarButton3')
    const node3 = createNode(CidToolBarButton3, testingValue3)

    const CidToolBarButton4 = cidToolBarButton(mockIcon, mockIcon_over, mockIcon_disable, 'CidToolBarButton4')
    const node4 = createNode(CidToolBarButton4, testingValue4)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'CidToolBarButton1')
    const wrapper2 = shallowWrappedComponent(node2, 'CidToolBarButton2')
    const wrapper3 = shallowWrappedComponent(node3, 'CidToolBarButton3')
    const wrapper4 = shallowWrappedComponent(node4, 'CidToolBarButton4')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of button', () => {
        expect(wrapper1.find(IconButton)).toHaveLength(1)
    })

    //Verify the props passed to children
    const createTest = function (wrapper, testingValue){
        let button = wrapper.find(IconButton).at(0)

        expect(button.props().disabled).toEqual(!testingValue.isEnabled)
        
        let tooltip = wrapper.find(Tooltip)
        if(testingValue.toolTipTitle != undefined && testingValue.toolTipTitle != '') {
            expect(tooltip).toHaveLength(1)
            expect(tooltip.props().title).toEqual(testingValue.toolTipTitle)
        } else {
            expect(tooltip).toHaveLength(0)
        }
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(wrapper2, testingValue2)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 3', () => {
        createTest(wrapper3, testingValue3)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 4', () => {
        createTest(wrapper4, testingValue4)
    })

    test('+++ Verify behaviors of children (Case 1: Enabled, onClick)', () => {
        let button

        // Check on click
        button = wrapper1.find(IconButton).at(0)
        button.simulate('click')
        expect(onClick).toHaveBeenCalledTimes(1)
    })

    test('+++ Verify behaviors of children (Case 2: Disabled, onClick)', () => {
        let button

        // Check on click
        button = wrapper2.find(IconButton).at(0)
        button.simulate('click')
        expect(onClick).toHaveBeenCalledTimes(1) //Disabled so still called once
    })

    test('+++ Verify behaviors of children (Case 3: Enabled, Hover)', () => {
        //Arrange
        let iconButton
        let image_beforeMouseEnter
        let image_afterMouseEnter

        //Act
        image_beforeMouseEnter= wrapper1.find('image').at(0)
        iconButton= wrapper1.find(IconButton).at(0)
        iconButton.simulate('mouseenter')
        image_afterMouseEnter= wrapper1.find('image').at(0)

        //Assert
        expect(image_beforeMouseEnter.props().href).toBe(mockIcon)
        expect(image_afterMouseEnter.props().href).toBe(mockIcon_over)
    })

    test('+++ Verify behaviors of children (Case 4: Disabled, Hover)', () => {
        //Arrange
        let iconButton
        let image_beforeMouseEnter
        let image_afterMouseEnter

        //Act
        image_beforeMouseEnter= wrapper2.find('image').at(0)
        iconButton= wrapper2.find(IconButton).at(0)
        iconButton.simulate('mouseenter')
        image_afterMouseEnter= wrapper2.find('image').at(0)

        //Assert
        expect(image_beforeMouseEnter.props().href).toBe(mockIcon_disable)
        expect(image_afterMouseEnter.props().href).toBe(mockIcon_disable)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
