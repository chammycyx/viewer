import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import RootApp, { styles } from '@components/RootApp'
import { ContextMenu as ContextMenuWrapper, ContextMenuTrigger } from 'react-contextmenu'
import ContextMenuContainer from '@components/contextMenu/ContextMenuContainer'
import ToolBarPanelContainer from '@components/toolBarPanel/ToolBarPanelContainer'
import PictorialIndexPanelContainer from '@components/pictorialIndexPanel/PictorialIndexPanelContainer'
import ImageViewingPanelContainer from '@components/imageViewingPanel/ImageViewingPanelContainer'
import MsgDialogContainer from '@components/msgDialog/MsgDialogContainer'
import AboutDialogContainer from '@components/contextMenu/dialogs/AboutDialogContainer'
import AppMaskContainer from '@components/root/appMask/AppMaskContainer'
import BtnStateWatcherContainer from '@components/root/watcher/btnStateWatcher/BtnStateWatcherContainer'

configure({ adapter: new Adapter() })

describe('Test Suite: <RootApp>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)
    
    // Declare testing values for props

    const testingValue1 = {
        appWidth: 10,
        appHeight: 20
    }

    const testingValue2 = {
        appWidth: 640,
        appHeight: 480
    }

    // The node to be tested
    const createNode = function(testingValue) {
        return <RootApp
            store={store}
            theme={theme}
            classes={classes}
            appWidth={testingValue.appWidth}
            appHeight={testingValue.appHeight}
        />
    }

    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'RootApp')
    const parentWrapper1 = shallowWrappedComponentParent(node1, 'RootApp')
    const wrapper2 = shallowWrappedComponent(node2, 'RootApp')
    const parentWrapper2 = shallowWrappedComponentParent(node2, 'RootApp')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find(ContextMenuTrigger)).toHaveLength(1)
        expect(wrapper1.find(ContextMenuWrapper)).toHaveLength(1)
        expect(wrapper1.find(ContextMenuContainer)).toHaveLength(1)

        expect(wrapper1.find(BtnStateWatcherContainer)).toHaveLength(1)
        expect(wrapper1.find(AppMaskContainer)).toHaveLength(1)
        expect(wrapper1.find(ToolBarPanelContainer)).toHaveLength(1)
        expect(wrapper1.find(PictorialIndexPanelContainer)).toHaveLength(1)
        expect(wrapper1.find(ImageViewingPanelContainer)).toHaveLength(1)
        expect(wrapper1.find(MsgDialogContainer)).toHaveLength(1)
        expect(wrapper1.find(AboutDialogContainer)).toHaveLength(1)
    })

    test('+++ Verify the functions passed to children', () => {
        
    })

    //Verify the props passed to children
    const createTest = function (parentWrapper, wrapper, testingValue){
        let parent = parentWrapper.find('RootApp').at(0)

        let div = wrapper.find('div').at(1)
        expect(div).toHaveLength(1)
        expect(div.props().className).toEqual(classes.rootApp)
        expect(div.props().className.width(parent.props())).toEqual(testingValue.appWidth)
        expect(div.props().className.height(parent.props())).toEqual(testingValue.appHeight)
    }

    test('+++ Verify the props passed to children  without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children  without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, testingValue2)
    })

    //Temporary comment to bypass the validation 
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})