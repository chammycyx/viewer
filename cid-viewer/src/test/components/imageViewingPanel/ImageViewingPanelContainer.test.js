import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import ImageViewingPanelContainer from '@components/imageViewingPanel/ImageViewingPanelContainer'
import { onImgMoved, onCanvasClick} from '@actions/components/imageViewingPanel/canvasGrid/Action'
import {ImageSelection} from '@constants/ComponentConstants'

configure({ adapter: new Adapter() })

describe('Test Suite: <ImageViewingPanelContainer>', () => {
    // Declare testing state
    const initialState1 = {
        components: {
            imageViewingPanel:{
                canvasGrid: {
                    rootState: {
                        width: 100,               
                        height: 200,
                        viewMode: {
                            text: '1x1',
                            noOfImage: 1
                        },
                        selectedSeriesId: '5',
                        showImageIds: [
                            "imageDtl_1",
                            "imageDtl_2"
                        ],
                        selectedImageIds: [
                            "imageDtl_1"
                        ],
                        slideShowCurrentImgId: 'imageDtl_1',
                        slideShowStopImgId: 'imageDtl_2',
                        caseNo: '',
                        studyDateTime: '20180730141001.123',
                        canvasProfiles: {
                            byId: {
                                'imageDtl_1': {
                                    header: '',
                                    imageBase64: '',
                                    fabricProfile: {},
                                    brightness: 50,
                                    contrast: 50,
                                    hue: 50,
                                    saturation: 50,
                                    zoomValue: 100,
                                    imageXCoor: 'Center',
                                    imageYCoor: 'Center',
                                    imageWidth: null,
                                    imageHeight: null
                                },
                                'imageDtl_2': {
                                    header: '',
                                    imageBase64: '',
                                    fabricProfile: {},
                                    brightness: 50,
                                    contrast: 50,
                                    hue: 50,
                                    saturation: 50,
                                    zoomValue: 100,
                                    imageXCoor: 'Center',
                                    imageYCoor: 'Center',
                                    imageWidth: null,
                                    imageHeight: null
                                }
                            },
                            allIds: ['imageDtl_1', 'imageDtl_2']
                        },
                        imageCreateTimeStamp: [20180730141001.123,20180730141002.123], 
                        rtcExamType: 'ES',
                        rtcImageCreateDateTime: false,
                        headerTimeText: 'Study Date/Time',
                        displayDateTime: [20180730142001.123,20180730142002.123]
                    }
                }
            }
        }
    }

    const initialState2 = {
        components: {
            imageViewingPanel:{
                canvasGrid: {
                    rootState: {
                        width: 200,               
                        height: 400,
                        viewMode: {
                            text: '2x2',
                            noOfImage: 4
                        },
                        selectedSeriesId: '7',
                        showImageIds: [
                            "imageDtl_3",
                            "imageDtl_4"
                        ],
                        selectedImageIds: [
                            "imageDtl_3"
                        ],
                        slideShowCurrentImgId: 'imageDtl_3',
                        slideShowStopImgId: 'imageDtl_4',
                        caseNo: '',
                        studyDateTime: '20180730142001.123',
                        canvasProfiles: {
                            byId: {
                                'imageDtl_3': {
                                    header: '',
                                    imageBase64: '',
                                    fabricProfile: {},
                                    brightness: 60,
                                    contrast: 60,
                                    hue: 60,
                                    saturation: 60,
                                    zoomValue: 200,
                                    imageXCoor: 'Center',
                                    imageYCoor: 'Center',
                                    imageWidth: null,
                                    imageHeight: null
                                },
                                'imageDtl_4': {
                                    header: '',
                                    imageBase64: '',
                                    fabricProfile: {},
                                    brightness: 40,
                                    contrast: 40,
                                    hue: 40,
                                    saturation: 40,
                                    zoomValue: 400,
                                    imageXCoor: 'Center',
                                    imageYCoor: 'Center',
                                    imageWidth: null,
                                    imageHeight: null
                                }
                            },
                            allIds: ['imageDtl_3', 'imageDtl_4']
                        },
                        imageCreateTimeStamp: [20180730142001.123, 20180730142002.123], 
                        rtcExamType: 'ES',
                        rtcImageCreateDateTime: false,
                        headerTimeText: 'Study Date/Time',
                        displayDateTime: [20180730143001.123,20180730143002.123]
                    }
                }
            }
        }
    }


    // Mock store
    const mockStore = configureStore()
    const store1 = mockStore(initialState1)
    const store2 = mockStore(initialState2)

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // The node to be tested
    const createNode = function(store){
        return <ImageViewingPanelContainer
            store={store}
            theme={theme}
        />
    }
    const node1 = createNode(store1)
    const node2 = createNode(store2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponentParent(node1, 'ImageViewingPanel')
    const wrapper2 = shallowWrappedComponentParent(node2, 'ImageViewingPanel')

    test('+++ Verify the container component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify if there is any hard-code output of the action dispatchers - Case 1', () => {
        let canvasGridRootState = initialState1.components.imageViewingPanel.canvasGrid.rootState
        let imageId = canvasGridRootState.showImageIds[0]
        let xCoor = 11
        let yCoor = 22
        let button = ImageSelection.CTRL


        let imageViewingPanel = wrapper1.find('ImageViewingPanel')
        imageViewingPanel.props().onImgMoved(canvasGridRootState.selectedSeriesId,imageId,xCoor,yCoor)
        imageViewingPanel.props().onCanvasClick(imageId, button)
        
        const actions = store1.getActions()
        expect(actions).toEqual([
            onImgMoved(canvasGridRootState.selectedSeriesId,imageId,xCoor,yCoor), 
            onCanvasClick(imageId, button)
        ])
    })

    test('+++ Verify if there is any hard-code output of the action dispatchers - Case 2', () => {
        let canvasGridRootState = initialState2.components.imageViewingPanel.canvasGrid.rootState
        let imageId = canvasGridRootState.showImageIds[1]
        let xCoor = 222
        let yCoor = 444
        let button = ImageSelection.SHIFT

        let imageViewingPanel = wrapper2.find('ImageViewingPanel')
        imageViewingPanel.props().onImgMoved(canvasGridRootState.selectedSeriesId,imageId,xCoor,yCoor)
        imageViewingPanel.props().onCanvasClick(imageId, button)
        
        const actions = store2.getActions()
        expect(actions).toEqual([
            onImgMoved(canvasGridRootState.selectedSeriesId,imageId,xCoor,yCoor), 
            onCanvasClick(imageId, button)
        ])
    })

    test('+++ Verify if there is any hard-code state - Case 1', () => {
        let imageViewingPanel = wrapper1.find('ImageViewingPanel')
        expect(imageViewingPanel.props().canvasGridData).toEqual(initialState1.components.imageViewingPanel.canvasGrid.rootState)
    })

    test('+++ Verify if there is any hard-code state - Case 2', () => {
        let imageViewingPanel = wrapper2.find('ImageViewingPanel')
        expect(imageViewingPanel.props().canvasGridData).toEqual(initialState2.components.imageViewingPanel.canvasGrid.rootState)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

})