// import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
// import ShallowRenderer from 'react-test-renderer/shallow'
// import configureStore from 'redux-mock-store'
// import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
// import { createMuiTheme } from '@material-ui/core/styles'
// import cidTheme from '@styles/CidTheme'
// import {getStore, stop} from '@test/dataFlow/DataFlowHelper'

import {Canvas} from '@components/imageViewingPanel/canvasGrid/canvas/Canvas'

configure({ adapter: new Adapter() })

jest.mock('@constants/ComponentConstants', function() {
    return {
        RELOCATE_IMAGE_IN_CANVAS_FACTOR: 0.25,
    }
})

// let store = null
// beforeEach(() => {
//     store = getStore()
// })

describe('Test Suite: <Canvas>', () => {
//     // Mock store
//     const mockStore = configureStore()
//     const store = mockStore()
    
//     // Create Material UI theme
//     const muiTheme = getMuiTheme(cidTheme)

//     // Declare testing values for props
//     const onImgMoved = jest.fn()
//     const onCanvasClick = jest.fn()
    
//     const canvasGridData1 = {
//         width: 100,               
//         height: 200,
//         viewMode: {
//             text: '1x1',
//             noOfImage: 1
//         },
//         selectedSeriesId: '5',
//         showImageIds: [
//             "imageDtl_1",
//             "imageDtl_2"
//         ],
//         selectedImageIds: [
//             "imageDtl_1"
//         ],
//         slideShowCurrentImgId: 'imageDtl_1',
//         slideShowStopImgId: 'imageDtl_2',
//         caseNo: '',
//         studyDateTime: '20180730141001.123',
//         canvasProfiles: {
//             byId: {
//                 'imageDtl_1': {
//                     header: '',
//                     imageBase64: '',
//                     fabricProfile: {},
//                     brightness: 50,
//                     contrast: 50,
//                     hue: 50,
//                     saturation: 50,
//                     zoomValue: 100,
//                     imageXCoor: 'Center',
//                     imageYCoor: 'Center',
//                     imageWidth: null,
//                     imageHeight: null
//                 },
//                 'imageDtl_2': {
//                     header: '',
//                     imageBase64: '',
//                     fabricProfile: {},
//                     brightness: 50,
//                     contrast: 50,
//                     hue: 50,
//                     saturation: 50,
//                     zoomValue: 100,
//                     imageXCoor: 'Center',
//                     imageYCoor: 'Center',
//                     imageWidth: null,
//                     imageHeight: null
//                 }
//             },
//             allIds: ['imageDtl_1', 'imageDtl_2']
//         },
//         imageCreateTimeStamp: [20180730141001.123,20180730141002.123], 
//         rtcExamType: 'ES',
//         rtcImageCreateDateTime: false,
//         headerTimeText: 'Study Date/Time',
//         displayDateTime: [20180730142001.123,20180730142002.123]
// }


//     // The node to be tested
//     const createNode = function(canvasGridData) {
//         let toShowIgIdexId = canvasGridData.showImageIds[0]
//         let imgDtl = canvasGridData.canvasProfiles.byId[toShowIgIdexId]
//         return <Canvas
//             store={store}
//             muiTheme={muiTheme}
//             width={canvasGridData.width}
//             height={canvasGridData.height}
//             imgDtl={imgDtl}
//             seriesIndexId={canvasGridData.selectedSeriesId}
//             imgIdexId={canvasGridData.showImageIds[0]}
//             caseNo={canvasGridData.caseNo}
//             displayDateTime={canvasGridData.displayDateTime[0]}
//             onImgMoved={onImgMoved}
//             onCanvasClick={onCanvasClick}
//             fontType={imgDtl.fontType}
//             fontSize={imgDtl.fontSize}
//             fontColor={imgDtl.fontColor}
//             bgColor={imgDtl.bgColor}
//             visibility={imgDtl.visibility}
//             headerTimeText={canvasGridData.headerTimeText}
//             />
//     }
//     const node1 = createNode(canvasGridData1)
    //  const wrapper1 = shallowWrappedComponent(node1, 'Canvas')
    test('+++ Verify the presentational component rendered', () => {
        // expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify multiple onClick selection', () => {
        // let canvas = wrapper1.find('canvas')
        // expect(canvas).toHaveLength(1)
        // expect(wrapper1.props()).toHaveLength(0)

        // canvas.simulate('mousedown',{})
        // expect(onCanvasClick).toHaveBeenCalledTimes(1)
    })

    test('+++ Verify auto fit function', () => {

    })

    test('+++ Verify the snapshot', () => {
        // const renderer = new ShallowRenderer()
        // renderer.render(node1)
        // const result = renderer.getRenderOutput()
        // expect(result).toMatchSnapshot()
    })

})