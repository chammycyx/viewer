import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import CanvasGrid, { styles } from '@components/imageViewingPanel/canvasGrid/CanvasGrid'
import Canvas from '@components/imageViewingPanel/canvasGrid/canvas/Canvas'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'

configure({ adapter: new Adapter() })

describe('Test Suite: <CanvasGrid>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    const theme = createMuiTheme(cidTheme)
    const classes = styles(theme)

    // Declare testing values for props
    const onImgMoved = jest.fn()
    const onCanvasClick = jest.fn()

    const canvasGridData1 = {
        width: 100,               
        height: 200,
        canvasWidth: 100,
        canvasHeight: 200,
        viewMode: {
            text: '1x1',
            noOfImage: 1
        },
        selectedSeriesId: '5',
        showImageIds: [
            "imageDtl_1",
            "imageDtl_2"
        ],
        selectedImageIds: [
            "imageDtl_1"
        ],
        slideShowCurrentImgId: 'imageDtl_1',
        slideShowStopImgId: 'imageDtl_2',
        caseNo: '',
        studyDateTime: '20180730141001.123',
        canvasProfiles: {
            byId: {
                'imageDtl_1': {
                    header: '',
                    imageBase64: '',
                    fabricProfile: {},
                    brightness: 50,
                    contrast: 50,
                    hue: 50,
                    saturation: 50,
                    zoomValue: 100,
                    imageXCoor: 'Center',
                    imageYCoor: 'Center',
                    imageWidth: null,
                    imageHeight: null
                },
                'imageDtl_2': {
                    header: '',
                    imageBase64: '',
                    fabricProfile: {},
                    brightness: 50,
                    contrast: 50,
                    hue: 50,
                    saturation: 50,
                    zoomValue: 100,
                    imageXCoor: 'Center',
                    imageYCoor: 'Center',
                    imageWidth: null,
                    imageHeight: null
                }
            },
            allIds: ['imageDtl_1', 'imageDtl_2']
        },
        imageCreateTimeStamp: [20180730141001.123,20180730141002.123], 
        rtcExamType: 'ES',
        rtcImageCreateDateTime: false,
        headerTimeText: 'Study Date/Time',
        displayDateTime: [20180730142001.123,20180730142002.123]
    }

    const canvasGridData2 = {
        width: 200,               
        height: 400,
        viewMode: {
            text: '2x2',
            noOfImage: 4
        },
        selectedSeriesId: '7',
        showImageIds: [
            "imageDtl_3",
            "imageDtl_4"
        ],
        selectedImageIds: [
            "imageDtl_3"
        ],
        slideShowCurrentImgId: 'imageDtl_3',
        slideShowStopImgId: 'imageDtl_4',
        caseNo: '',
        studyDateTime: '20180730142001.123',
        canvasProfiles: {
            byId: {
                'imageDtl_3': {
                    header: '',
                    imageBase64: '',
                    fabricProfile: {},
                    brightness: 60,
                    contrast: 60,
                    hue: 60,
                    saturation: 60,
                    zoomValue: 200,
                    imageXCoor: 'Center',
                    imageYCoor: 'Center',
                    imageWidth: null,
                    imageHeight: null
                },
                'imageDtl_4': {
                    header: '',
                    imageBase64: '',
                    fabricProfile: {},
                    brightness: 40,
                    contrast: 40,
                    hue: 40,
                    saturation: 40,
                    zoomValue: 400,
                    imageXCoor: 'Center',
                    imageYCoor: 'Center',
                    imageWidth: null,
                    imageHeight: null
                }
            },
            allIds: ['imageDtl_3', 'imageDtl_4']
        },
        imageCreateTimeStamp: [20180730142001.123, 20180730142002.123], 
        rtcExamType: 'ES',
        rtcImageCreateDateTime: false,
        headerTimeText: 'Study Date/Time',
        displayDateTime: [20180730143001.123,20180730143002.123]
    }

    // The node to be tested
    const createNode = function(canvasGridData) {
        return <CanvasGrid
        store={store}
        theme={theme}
        classes={classes}
        onImgMoved={onImgMoved}
        onCanvasClick={onCanvasClick}
        canvasGridData={canvasGridData}
        />
    }

    const node1 = createNode(canvasGridData1)
    const node2 = createNode(canvasGridData2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'CanvasGrid')
    const parentWrapper1 = shallowWrappedComponentParent(node1, 'CanvasGrid')
    const wrapper2 = shallowWrappedComponent(node2, 'CanvasGrid')
    const parentWrapper2 = shallowWrappedComponentParent(node2, 'CanvasGrid')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find(GridListTile)).toHaveLength(1)
        expect(wrapper1.find(GridList)).toHaveLength(1)
        expect(wrapper1.find(Canvas)).toHaveLength(1)
    })

    
    test('+++ Verify the props passed to children (callbacks)', () => {
        let canvasGrid = parentWrapper1.find('CanvasGrid')
        expect(canvasGrid).toHaveLength(1)

        canvasGrid.props().onCanvasClick()
        expect(onCanvasClick).toHaveBeenCalledTimes(1)

        canvasGrid.props().onImgMoved()
        expect(onImgMoved).toHaveBeenCalledTimes(1)
    })

    const createTest = function (parentWrapper, wrapper, canvasGridData){
        //Sample of gridTile
        let toShowIgIdexId = canvasGridData.showImageIds[0]
        let styleSample = {
            border: (canvasGridData.selectedImageIds.indexOf(toShowIgIdexId) > -1) ? '1px solid #00D2FF' : '1px solid #7C7C7C',
            boxSizing: 'border-box'
        }
        
        let parent = parentWrapper.find('CanvasGrid').at(0)

        let div = wrapper.find('div').at(0)
        expect(div.props().className).toEqual(classes.div)
        expect(div.props().className.width(parent.props())).toEqual(canvasGridData.width)
        expect(div.props().className.height(parent.props())).toEqual(canvasGridData.height)

        //Verify gridTile
        let gridTile = wrapper.find(GridListTile).at(0)
        expect(gridTile).toHaveLength(1)
        expect(gridTile.key()).toEqual('GridTile-0')
        expect(gridTile.props().style).toEqual(styleSample)

        //Sample of gridList
        let noOfRow = Number(canvasGridData.viewMode.text.split('x')[0])
        let noOfCol = Number(canvasGridData.viewMode.text.split('x')[1])
        let rowHeight = canvasGridData.canvasHeight

        //Verify gridList
        let gridList = wrapper.find(GridList).at(0)
        expect(gridList).toHaveLength(1)
        expect(gridList.props().rows).toEqual(noOfRow)
        expect(gridList.props().cols).toEqual(noOfCol)
        expect(gridList.props().cellHeight).toEqual(rowHeight)
        expect(gridList.props().spacing).toEqual(0)

        //Sample of canvas
        let rowWidth = canvasGridData.canvasWidth
        let canvasProfile = canvasGridData.canvasProfiles.byId[toShowIgIdexId]

        //Verify canvas
        let canvas = wrapper.find(Canvas).at(0)
        expect(canvas).toHaveLength(1)
        expect(canvas.props().height).toEqual(rowHeight)
        expect(canvas.props().width).toEqual(rowWidth)
        expect(canvas.props().seriesIndexId).toEqual(canvasGridData.selectedSeriesId)
        expect(canvas.props().imgIdexId).toEqual(toShowIgIdexId)
        expect(canvas.props().canvasProfile).toEqual(canvasProfile)
        expect(canvas.props().caseNo).toEqual(canvasGridData.caseNo)
        expect(canvas.props().displayDateTime).toEqual(canvasGridData.displayDateTime[0])
        expect(canvas.props().fontType).toEqual(canvasProfile.fontType)
        expect(canvas.props().fontSize).toEqual(canvasProfile.fontSize)
        expect(canvas.props().fontColor).toEqual(canvasProfile.fontColor)
        expect(canvas.props().bgColor).toEqual(canvasProfile.bgColor)
        expect(canvas.props().headerVisibility).toEqual(canvasProfile.headerVisibility)
        expect(canvas.props().headerTimeText).toEqual(canvasGridData.headerTimeText)
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, canvasGridData1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, canvasGridData2)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

})