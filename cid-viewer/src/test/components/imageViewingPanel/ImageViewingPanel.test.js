import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import ImageViewingPanel from '@components/imageViewingPanel/ImageViewingPanel'
import CanvasGrid from '@components/imageViewingPanel/canvasGrid/CanvasGrid'

configure({ adapter: new Adapter() })

describe('Test Suite: <ImageViewingPanel>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    const theme = createMuiTheme(cidTheme)

    // Declare testing values for props
    const onImgMoved = jest.fn()
    const onCanvasClick = jest.fn()
    const canvasGridData1 = {a:'1'}
    const canvasGridData2 = {b:'2'}
    
    // The node to be tested
    const createNode = function(canvasGridData) {
        return <ImageViewingPanel
        store={store}
        theme={theme}
        onImgMoved={onImgMoved}
        onCanvasClick={onCanvasClick}
        canvasGridData={canvasGridData}
        />
    }

    const node1 = createNode(canvasGridData1)
    const node2 = createNode(canvasGridData2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'ImageViewingPanel')
    const wrapper2 = shallowWrappedComponent(node2, 'ImageViewingPanel')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find(CanvasGrid)).toHaveLength(1)
    })

    
    test('+++ Verify the props passed to children', () => {
        let canvasGrid = wrapper1.find(CanvasGrid).at(0)
        expect(canvasGrid).toHaveLength(1)
        canvasGrid.props().onCanvasClick()
        expect(onCanvasClick).toHaveBeenCalledTimes(1)
        canvasGrid.props().onImgMoved()
        expect(onImgMoved).toHaveBeenCalledTimes(1)
    })

    const createTest = function (wrapper,canvasGridData){
        let canvasGrid = wrapper.find(CanvasGrid).at(0)
        expect(canvasGrid.props().canvasGridData).toEqual(canvasGridData)
    }

    test('+++ Verify the props passed to children  without hard-code output - Case 1', () => {
        createTest(wrapper1,canvasGridData1)
    })

    test('+++ Verify the props passed to children  without hard-code output - Case 2', () => {
        createTest(wrapper2,canvasGridData2)
    })
   
    //Temporary comment to bypass the validation 
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

})

