import React from 'react'
import PropTypes from 'prop-types'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import PictorialIndexPanel, { styles } from '@components/pictorialIndexPanel/PictorialIndexPanel'
import ShowButton from '@components/pictorialIndexPanel/hideButton/ShowButton'
import HideButton from '@components/pictorialIndexPanel/hideButton/HideButton'
import StudyInfo from '@components/pictorialIndexPanel/studyInfo/StudyInfo'
import ThumbnailsGrid from '@components/pictorialIndexPanel/thumbnailGrid/ThumbnailGrid'

configure({ adapter: new Adapter() })

describe('Test Suite: <PictorialIndexPanel>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)
    
    // Declare testing values for props
    const onShowHideButtonClick = jest.fn()
    const onThumbnailClick = jest.fn()

    const testingValue1 = {
        patName: 'a',
        patHkid: 'b',
        patSex: 'c',
        patDob: 'd',
        accessionNumber: '12345678',
        seriesDtls: {a:1},
        selectedSeriesId: 'SeriesId',
        selectedImageIds: {a:1},
        selectedFirstImageId: 'FirstImageId',
        viewMode: {a:1},
        isHidden: true,
        numOfColumn: 2,
        isShowSeriesHeading: true,
        serieName: 'serieName'
    }

    const testingValue2 = {
        patName: 'a2',
        patHkid: 'b2',
        patSex: 'c2',
        patDob: 'd2',
        accessionNumber: '123456782',
        seriesDtls: {b:2},
        selectedSeriesId: 'SeriesId2',
        selectedImageIds: {b:2},
        selectedFirstImageId: 'FirstImageId',
        viewMode: {b:2},
        isHidden: false,
        numOfColumn: 3,
        isShowSeriesHeading: false,
        serieName: 'serieName2'
    }

    // The node to be tested
    const createNode = function(testingValue) {
        return <PictorialIndexPanel
        store={store}
        theme={theme}
        classes={classes}
        patName={testingValue.patName}
        patHkid={testingValue.patHkid}
        patSex={testingValue.patSex}
        patDob={testingValue.patDob}
        accessionNumber={testingValue.accessionNumber}
        seriesDtls={testingValue.seriesDtls}
        selectedSeriesId={testingValue.selectedSeriesId}
        selectedImageIds={testingValue.selectedImageIds}
        selectedFirstImageId={testingValue.selectedFirstImageId}
        viewMode={testingValue.viewMode}
        onShowHideButtonClick={onShowHideButtonClick}
        onThumbnailClick={onThumbnailClick}
        isHidden={testingValue.isHidden}
        numOfColumn={testingValue.numOfColumn}
        isShowSeriesHeading={testingValue.isShowSeriesHeading}
        serieName={testingValue.serieName}/>
    }

    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const parentWrapper1 = shallowWrappedComponentParent(node1, 'PictorialIndexPanel')
    const wrapper1 = shallowWrappedComponent(node1, 'PictorialIndexPanel')
    const parentWrapper2 = shallowWrappedComponentParent(node2, 'PictorialIndexPanel')
    const wrapper2 = shallowWrappedComponent(node2, 'PictorialIndexPanel')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find(ShowButton)).toHaveLength(1)
        expect(wrapper1.find(HideButton)).toHaveLength(1)
        expect(wrapper1.find(StudyInfo)).toHaveLength(1)
        expect(wrapper1.find(ThumbnailsGrid)).toHaveLength(1)
    })

    
    test('+++ Verify the props passed to children', () => {
        let showButton = wrapper1.find(ShowButton).at(0)
        expect(showButton).toHaveLength(1)
        expect(showButton.props().width).toEqual(theme.pictorialIndexPanel.showHideButton.showButton.width)
        expect(showButton.props().height).toEqual(theme.pictorialIndexPanel.showHideButton.showButton.height)
        showButton.props().onClick()
        expect(onShowHideButtonClick).toHaveBeenCalledTimes(1)

        let hideButton = wrapper1.find(HideButton).at(0)
        expect(hideButton).toHaveLength(1)
        expect(hideButton.props().width).toEqual(theme.pictorialIndexPanel.showHideButton.hideButton.width)
        expect(hideButton.props().height).toEqual(theme.pictorialIndexPanel.showHideButton.hideButton.height)
        hideButton.props().onClick()
        expect(onShowHideButtonClick).toHaveBeenCalledTimes(2)

        let thumbnailsGrid = wrapper1.find(ThumbnailsGrid).at(0)
        thumbnailsGrid.props().onThumbnailClick()
        expect(onThumbnailClick).toHaveBeenCalledTimes(1)
    })

    //Verify the props passed to children
    const createTest = function (parentWrapper, wrapper, testingValue){
        let parent = parentWrapper.find('PictorialIndexPanel').at(0)

        let hideDiv = wrapper.find('div').at(1)
        expect(hideDiv).toHaveLength(1)
        expect(hideDiv.props().className).toEqual(classes.hideDiv)
        expect(hideDiv.props().className.display(parent.props())).toEqual(!testingValue.isHidden ? 'none' : 'block')

        let showDiv = wrapper.find('div').at(2)
        expect(showDiv).toHaveLength(1)
        expect(showDiv.props().className).toEqual(classes.showDiv)
        expect(showDiv.props().className.display(parent.props())).toEqual(testingValue.isHidden ? 'none' : 'block')

        let studyInfo = wrapper.find(StudyInfo).at(0)
        expect(studyInfo).toHaveLength(1)
        expect(studyInfo.props().patName).toEqual(testingValue.patName)
        expect(studyInfo.props().patHkid).toEqual(testingValue.patHkid)
        expect(studyInfo.props().patSex).toEqual(testingValue.patSex)
        expect(studyInfo.props().patDob).toEqual(testingValue.patDob)
        expect(studyInfo.props().accessionNumber).toEqual(testingValue.accessionNumber)

        let thumbnailsGrid = wrapper.find(ThumbnailsGrid).at(0)
        expect(thumbnailsGrid).toHaveLength(1)
        expect(thumbnailsGrid.props().selectedImageIds).toEqual(testingValue.selectedImageIds)
        expect(thumbnailsGrid.props().selectedSeriesId).toEqual(testingValue.selectedSeriesId)
        expect(thumbnailsGrid.props().selectedFirstImageId).toEqual(testingValue.selectedFirstImageId)
        expect(thumbnailsGrid.props().seriesDtls).toEqual(testingValue.seriesDtls)
        expect(thumbnailsGrid.props().viewMode).toEqual(testingValue.viewMode)
        expect(thumbnailsGrid.props().numOfColumn).toEqual(testingValue.numOfColumn)
        expect(thumbnailsGrid.props().isShowSeriesHeading).toEqual(testingValue.isShowSeriesHeading)
        expect(thumbnailsGrid.props().serieName).toEqual(testingValue.serieName)
    }

    test('+++ Verify the props passed to children  without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children  without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, testingValue2)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})