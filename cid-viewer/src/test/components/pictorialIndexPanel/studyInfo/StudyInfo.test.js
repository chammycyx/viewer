import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import { DateParser, DATE_FORMAT } from '@utils/DateParser'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import StudyInfo, { styles } from '@components/pictorialIndexPanel/studyInfo/StudyInfo'

configure({ adapter: new Adapter() })

describe('Test Suite: <StudyInfo>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // Declare testing values for props
    const testingValueLabel = {
        patName: 'Name:',
        patHkid: 'HKID:',
        patSex: 'Sex:',
        patDob: 'DOB:',
        accessionNumber: 'AccNum:'
    }

    const testingValue1 = {
        patName: 'Chan Siu Ming',
        patHkid: 'A1234567',
        patSex: 'M',
        patDob: '20180101010101.001',
        accessionNumber: '12345678'
    }

    const testingValue2 = {
        patName: 'Leung Dai Ming',
        patHkid: ' B1234567',
        patSex: 'F',
        patDob: '2000',
        accessionNumber: '9876543210'
    }
    
    const createNode = function(testingValue) {
        // The node to be tested
        let node = 
        <StudyInfo
            store={store}
            theme={theme}
            classes={classes}
            patName={testingValue.patName}
            patHkid={testingValue.patHkid}
            patSex={testingValue.patSex}
            patDob={testingValue.patDob}
            accessionNumber={testingValue.accessionNumber}
        />

        return node
    }

    // Create nodes
    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Create wrappers
    const wrapper1 = shallowWrappedComponent(node1, 'StudyInfo')
    const wrapper2 = shallowWrappedComponent(node2, 'StudyInfo')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
        expect(wrapper2).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find('span')).toHaveLength(10)
        expect(wrapper2.find('span')).toHaveLength(10)
    })


    // Verify the props passed to children

    const createTest = function(wrapper, testingValue, labels) {
        let studyInfoSpans = wrapper.find('span')
        expect(studyInfoSpans).toHaveLength(10)

        let patNameLabel = studyInfoSpans.at(0)
        let patNameValue = studyInfoSpans.at(1)
        expect(patNameLabel.text()).toEqual(labels.patName)
        expect(patNameValue.text()).toEqual(testingValue.patName)
        expect(patNameValue.parent().props().title).toEqual(testingValue.patName)

        let patHkidLabel = studyInfoSpans.at(2)
        let patHkidValue = studyInfoSpans.at(3)
        expect(patHkidLabel.text()).toEqual(labels.patHkid)
        expect(patHkidValue.text()).toEqual(testingValue.patHkid)

        let patSexLabel = studyInfoSpans.at(4)
        let patSexValue = studyInfoSpans.at(5)
        expect(patSexLabel.text()).toEqual(labels.patSex)
        expect(patSexValue.text()).toEqual(testingValue.patSex)

        let patDobLabel = studyInfoSpans.at(6)
        let patDobValue = studyInfoSpans.at(7)
        expect(patDobLabel.text()).toEqual(labels.patDob)
        expect(patDobValue.text()).toEqual(DateParser.parse(testingValue.patDob, DATE_FORMAT.FULL_YYYYMMDDTHHMMSSMS_PLAIN, DATE_FORMAT.SHORT_DDMMMYYYY))

        let accessionNumberLabel = studyInfoSpans.at(8)
        let accessionNumberValue = studyInfoSpans.at(9)
        expect(accessionNumberLabel.text()).toEqual(labels.accessionNumber)
        expect(accessionNumberValue.text()).toEqual(testingValue.accessionNumber)
    }

    test('+++ Verify the props passed to children with testing values set 1', () => {
        createTest(wrapper1, testingValue1, testingValueLabel)
    })

    test('+++ Verify the props passed to children with testing values set 2', () => {
        createTest(wrapper2, testingValue2, testingValueLabel)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node2)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})