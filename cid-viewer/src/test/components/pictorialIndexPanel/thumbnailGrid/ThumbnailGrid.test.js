import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import ThumbnailGrid, { styles } from '@components/pictorialIndexPanel/thumbnailGrid/ThumbnailGrid'
import Thumbnail from '@components/pictorialIndexPanel/thumbnailGrid/thumbnail/Thumbnail'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import ListSubheader from '@material-ui/core/ListSubheader'

configure({ adapter: new Adapter() })

describe('Test Suite: <ThumbnailGrid>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // Declare testing values for props
    const onThumbnailClick = jest.fn()
    const width = theme.pictorialIndexPanel.width
    const scrollBarWidth = theme.pictorialIndexPanel.thumbnailGrid.scrollBarWidth

    const testingValue1 = {
        seriesDtls: {
            allIds: ["seriesDtl_1"],
            byId: {
                seriesDtl_1: {
                    entityID: 0,
                    examDtm: "20141208175647",
                    examType: "HADRW",
                    imageDtls: {
                        allHighestVerIds: ["imageDtl_2"],
                        allIds: ["imageDtl_1","imageDtl_2"],
                        byId: {
                            imageDtl_1: {
                                imageBase64: undefined
                            },
                            imageDtl_2: {
                                imageBase64: '12345',
                                imageThumbnailBase64: 'data:image/jpeg;base64,/abcde'
                            }
                        }
                    },
                    seriesNo: "c5ef5dae-1e9c-41f1-b0c2-ed2df211c1b5"
                }
            }
        },
        selectedSeriesId: 'seriesDtl_1',
        selectedImageIds: ['imageDtl_1','imageDtl_2'],
        selectedFirstImageId: 'imageDtl_1',
        numOfColumn: 2,
        isShowSeriesHeading: true,
        serieName: 'SName',
        cellHeight: ((width-scrollBarWidth)/2)
    }

    const testingValue2 = {
        seriesDtls: {
            allIds: ["seriesDtl_12"],
            byId: {
                seriesDtl_12: {
                    entityID: 2,
                    examDtm: "201412081756472",
                    examType: "HADRW2",
                    imageDtls: {
                        allHighestVerIds: ["imageDtl_22"],
                        allIds: ["imageDtl_12","imageDtl_22"],
                        byId: {
                            imageDtl_12: {
                                imageBase64: undefined
                            },
                            imageDtl_22: {
                                imageBase64: '123452',
                                imageThumbnailBase64: 'data:image/jpeg;base64,/abcde2'
                            }
                        }
                    },
                    seriesNo: "c5ef5dae-1e9c-41f1-b0c2-ed2df211c1b52"
                }
            }
        },
        selectedSeriesId: 'seriesDtl_12',
        selectedImageIds: ['imageDtl_12','imageDtl_22'],
        selectedFirstImageId: 'imageDtl_12',
        numOfColumn: 3,
        isShowSeriesHeading: false,
        serieName: 'SName2',
        cellHeight: ((width-scrollBarWidth)/3)
    }

    // The node to be tested

    const createNode = function(testingValue) {
        return <ThumbnailGrid
        store={store}
        theme={theme}
        classes={classes}
        seriesDtls={testingValue.seriesDtls}
        onThumbnailClick={onThumbnailClick}
        setSelectedImageIds={testingValue.setSelectedImageIds}
        selectedSeriesId={testingValue.selectedSeriesId}
        selectedImageIds={testingValue.selectedImageIds}
        selectedFirstImageId={testingValue.selectedFirstImageId}
        numOfColumn={testingValue.numOfColumn}
        isShowSeriesHeading={testingValue.isShowSeriesHeading}
        serieName={testingValue.serieName}
        />
    }
    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)
    
    // Create wrapper
    const wrapper1 = shallowWrappedComponent(node1, 'ThumbnailGrid')
    const wrapper2 = shallowWrappedComponent(node2, 'ThumbnailGrid')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find(ListSubheader)).toHaveLength(1)
        expect(wrapper1.find(GridList)).toHaveLength(1)
        expect(wrapper1.find(GridListTile)).toHaveLength(1)
        expect(wrapper1.find(Thumbnail)).toHaveLength(1)
    })

    test('+++ Verify the props passed to children', () => {
        let thumbnail = wrapper1.find(Thumbnail).at(0)
        expect(thumbnail).toHaveLength(1)
        thumbnail.props().onClick()
        expect(onThumbnailClick).toHaveBeenCalledTimes(1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        let listSubheader = wrapper1.find(ListSubheader)
        expect(listSubheader).toHaveLength(1)
        
        let gridList = wrapper1.find(GridList).at(0)
        expect(gridList).toHaveLength(1)
        expect(gridList.props().cols).toEqual(testingValue1.numOfColumn)
        expect(gridList.props().cellHeight).toEqual(testingValue1.cellHeight)

        let gridListTile = wrapper1.find(GridListTile).at(0)
        expect(gridListTile).toHaveLength(1)
        expect(gridListTile.key()).toEqual(testingValue1.seriesDtls.byId.seriesDtl_1.imageDtls.allHighestVerIds[0])
        
        let thumbnail = wrapper1.find(Thumbnail).at(0)
        expect(thumbnail).toHaveLength(1)
        expect(thumbnail.props().isSelected).toEqual(testingValue1.selectedSeriesId==testingValue1.seriesDtls.allIds[0]
                &&testingValue1.selectedImageIds.includes(testingValue1.seriesDtls.byId.seriesDtl_1.imageDtls.allHighestVerIds[0]))
        expect(thumbnail.props().src).toEqual(testingValue1.seriesDtls.byId.seriesDtl_1.imageDtls.byId.imageDtl_2.imageThumbnailBase64)
        expect(thumbnail.props().imageId).toEqual(testingValue1.seriesDtls.byId.seriesDtl_1.imageDtls.allHighestVerIds[0])
        expect(thumbnail.props().seriesId).toEqual(testingValue1.seriesDtls.allIds[0])
        expect(thumbnail.props().cellHeight).toEqual(testingValue1.cellHeight)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        let listSubheader = wrapper2.find(ListSubheader)
        expect(listSubheader).toHaveLength(0)
        
        let gridList = wrapper2.find(GridList).at(0)
        expect(gridList).toHaveLength(1)
        expect(gridList.props().cols).toEqual(testingValue2.numOfColumn)
        expect(gridList.props().cellHeight).toEqual(testingValue2.cellHeight)
        
        let gridListTile = wrapper2.find(GridListTile).at(0)
        expect(gridListTile).toHaveLength(1)
        expect(gridListTile.key()).toEqual(testingValue2.seriesDtls.byId.seriesDtl_12.imageDtls.allHighestVerIds[0])
        
        let thumbnail = wrapper2.find(Thumbnail).at(0)
        expect(thumbnail).toHaveLength(1)
        expect(thumbnail.props().isSelected).toEqual(testingValue2.selectedSeriesId==testingValue2.seriesDtls.allIds[0]
                &&testingValue2.selectedImageIds.includes(testingValue2.seriesDtls.byId.seriesDtl_12.imageDtls.allHighestVerIds[0]))
        expect(thumbnail.props().src).toEqual(testingValue2.seriesDtls.byId.seriesDtl_12.imageDtls.byId.imageDtl_22.imageThumbnailBase64)
        expect(thumbnail.props().imageId).toEqual(testingValue2.seriesDtls.byId.seriesDtl_12.imageDtls.allHighestVerIds[0])
        expect(thumbnail.props().seriesId).toEqual(testingValue2.seriesDtls.allIds[0])
        expect(thumbnail.props().cellHeight).toEqual(testingValue2.cellHeight)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

})