import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import Thumbnail, { styles } from '@components/pictorialIndexPanel/thumbnailGrid/thumbnail/Thumbnail'

configure({ adapter: new Adapter() })

describe('Test Suite: <Thumbnail>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // Declare testing values for props
    const onClick = jest.fn()

    const testingValue1 = {
        isSelected: true,
        seriesId: 'SID1',
        imageId: 'ImID1',
        cellHeight: '111',
        src: '1111'
    }

    const testingValue2 = {
        isSelected: false,
        seriesId: 'SID2',
        imageId: 'ImID2',
        cellHeight: '222',
        src: '2222'
    }

    // The node to be tested
    const createNode = function(testingValue) {
        return <Thumbnail
            store={store}
            theme={theme}
            classes={classes}
            isSelected={testingValue.isSelected}
            seriesId={testingValue.seriesId}
            imageId={testingValue.imageId}
            cellHeight={testingValue.cellHeight}
            src={testingValue.src}
            onClick={onClick}
            />
    }
    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const parentWrapper1 = shallowWrappedComponentParent(node1,'Thumbnail')
    const wrapper1 = shallowWrappedComponent(node1,'Thumbnail')
    const parentWrapper2 = shallowWrappedComponentParent(node2,'Thumbnail')
    const wrapper2 = shallowWrappedComponent(node2,'Thumbnail')
 
    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })
 
    test('+++ Verify the props passed to children', () => {
        let img = wrapper1.find('img').at(0)
        expect(img).toHaveLength(1)
        img.props().onClick()
        expect(onClick).toHaveBeenCalledTimes(1)
    })

    const createTest = function (parentWrapper, wrapper, testingValue) {
        let parent = parentWrapper.find('Thumbnail').at(0)

        let img = wrapper.find('img').at(0)
        expect(img).toHaveLength(1)
        expect(img.props().src).toEqual(testingValue.src)
        expect(img.props().className).toEqual(classes.img)
        expect(img.props().className.height(parent.props())).toEqual(testingValue.cellHeight)
        expect(img.props().className.borderColor(parent.props())).toEqual(
            !testingValue.isSelected ?
            theme.pictorialIndexPanel.thumbnailGrid.thumbnail.borderColor :
            theme.pictorialIndexPanel.thumbnailGrid.thumbnail.borderColorSelected)
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, testingValue2)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})
