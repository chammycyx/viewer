import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import PictorialIndexPanel, { styles } from '@components/pictorialIndexPanel/PictorialIndexPanel'
import PictorialIndexPanelContainer from '@components/pictorialIndexPanel/PictorialIndexPanelContainer'
import { onThumbnailClick } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { onTogglePanelButtonClick } from '@actions/components/pictorialIndexPanel/Action'

configure({ adapter: new Adapter() })

describe('Test Suite: <PictorialIndexPanelContainer>', () => {
    // Declare testing state
    const initialState1 = {
        data: {
            study: {
                patientDtl: {
                    rootState: {
                        name: 'a',
                        hkid: 'A123456',
                        sex: 'M',
                        dob: '19460108000000.000'
                    }
                },
                studyDtl: {
                    rootState: {
                        accessionNo: 'R9MLJ5KPEVED1SMQ',
                        seriesDtls: {}
                    }
                }
            }
		},
		components: {
            rootState: {
                appHeight: 100
            },
            pictorialIndexPanel: {
                thumbnailGrid: {
                    rootState: {
                        selectedSeriesId: '123456',
                        selectedImageIds: ['123456','234567'],
                        selectedFirstImageId: '234567',
                        numOfColumn: 2,
                        isShowSeriesHeading: true,
                        serieName: ''
                    }
                },
                rootState: {
                    isHidden: false
                }
            },
            toolBarPanel: {
                viewModeTools: {
                    viewModeDropDownList: {
                        rootState: {
                            viewMode: {
                                text: '1x1',
                                noOfImage: 1
                            }
                        }
                    }
                }
            }
        }
    }
    
    //For action dispatchers testing
    const initialState2 = { 
        data: {
            study: {
                patientDtl: {
                    rootState: {
                        name: '',
                        hkid: '',
                        sex: 'M',
                        dob: '19460108000000.000'
                    }
                },
                studyDtl: {
                    rootState: {
                        accessionNo: 'R9MLJ5KPEVED1SMQ',
                        seriesDtls: {}
                    }
                }
            }
		},
		components: {
            rootState: {
                appHeight: 20
            },
            pictorialIndexPanel: {
                thumbnailGrid: {
                    rootState: {
                        selectedSeriesId: '135790',
                        selectedImageIds: ['135790','246802'],
                        selectedFirstImageId: '246802',
                        numOfColumn: 2,
                        isShowSeriesHeading: true,
                        serieName: ''
                    }
                },
                rootState: {
                    isHidden: true
                }
            },
            toolBarPanel: {
                viewModeTools: {
                    viewModeDropDownList: {
                        rootState: {
                            viewMode: {
                                text: '1x1',
                                noOfImage: 1
                            }
                        }
                    }
                }
            }
        }
    }

    // Mock store
    const mockStore = configureStore()
    const store1 = mockStore(initialState1)
    const store2 = mockStore(initialState2)

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // The node to be tested
    const createNode = function(store){
        return <PictorialIndexPanelContainer
            store={store}
            theme={theme}
            classes={classes}
        />
    }
    const node1 = createNode(store1)
    const node2 = createNode(store2)

    // Shallow render the wrapped component
    const wrapper1 = shallow(node1)
    const wrapper2 = shallow(node2)
    

    test('+++ Verify the container component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify if there is any hard-code output of the action dispatchers - Case 1', () => {
        let PictorialIndexPanelContainer = wrapper1.find(PictorialIndexPanel)
        PictorialIndexPanelContainer.props().onShowHideButtonClick(initialState1.components.pictorialIndexPanel.rootState.isHidden)
        PictorialIndexPanelContainer.props().onThumbnailClick(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId,
            initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId)
        
        const actions = store1.getActions()
        expect(actions).toEqual([
            //setIsHidden(initialState1.components.pictorialIndexPanel.rootState.isHidden), 
            onTogglePanelButtonClick(initialState1.components.pictorialIndexPanel.rootState.isHidden), 
            onThumbnailClick(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId,
                initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId)
        ])
        
    })

    test('+++ Verify if there is any hard-code output of the action dispatchers - Case 2', () => {
        let PictorialIndexPanelContainer = wrapper2.find(PictorialIndexPanel)
        PictorialIndexPanelContainer.props().onShowHideButtonClick(initialState2.components.pictorialIndexPanel.rootState.isHidden)
        PictorialIndexPanelContainer.props().onThumbnailClick(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId,
            initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId)
        
        const actions = store2.getActions()
        expect(actions).toEqual([
            onTogglePanelButtonClick(initialState2.components.pictorialIndexPanel.rootState.isHidden), 
            onThumbnailClick(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId,
                initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId)
        ])
    })
    

    test('+++ Verify if there is any hard-code state - Case 1', () => {
        let PictorialIndexPanelContainer = wrapper1.find(PictorialIndexPanel)
        expect(PictorialIndexPanelContainer.props().patName).toEqual(initialState1.data.study.patientDtl.rootState.name)
        expect(PictorialIndexPanelContainer.props().patHkid).toEqual(initialState1.data.study.patientDtl.rootState.hkid)
        expect(PictorialIndexPanelContainer.props().patSex).toEqual(initialState1.data.study.patientDtl.rootState.sex)
        expect(PictorialIndexPanelContainer.props().patDob).toEqual(initialState1.data.study.patientDtl.rootState.dob)
        expect(PictorialIndexPanelContainer.props().accessionNumber).toEqual(initialState1.data.study.studyDtl.rootState.accessionNo)
        expect(PictorialIndexPanelContainer.props().seriesDtls).toEqual(initialState1.data.study.studyDtl.rootState.seriesDtls)
        expect(PictorialIndexPanelContainer.props().selectedSeriesId).toEqual(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId)
        expect(PictorialIndexPanelContainer.props().selectedImageIds).toEqual(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedImageIds)
        expect(PictorialIndexPanelContainer.props().selectedFirstImageId).toEqual(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId)
        expect(PictorialIndexPanelContainer.props().viewMode).toEqual(initialState1.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode)
        expect(PictorialIndexPanelContainer.props().isHidden).toEqual(initialState1.components.pictorialIndexPanel.rootState.isHidden)
        expect(PictorialIndexPanelContainer.props().numOfColumn).toEqual(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.numOfColumn)
        expect(PictorialIndexPanelContainer.props().isShowSeriesHeading).toEqual(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.isShowSeriesHeading)
        expect(PictorialIndexPanelContainer.props().serieName).toEqual(initialState1.components.pictorialIndexPanel.thumbnailGrid.rootState.serieName)    
    })

    test('+++ Verify if there is any hard-code state - Case 2', () => {
        let PictorialIndexPanelContainer = wrapper2.find(PictorialIndexPanel)
        expect(PictorialIndexPanelContainer.props().patName).toEqual(initialState2.data.study.patientDtl.rootState.name)
        expect(PictorialIndexPanelContainer.props().patHkid).toEqual(initialState2.data.study.patientDtl.rootState.hkid)
        expect(PictorialIndexPanelContainer.props().patSex).toEqual(initialState2.data.study.patientDtl.rootState.sex)
        expect(PictorialIndexPanelContainer.props().patDob).toEqual(initialState2.data.study.patientDtl.rootState.dob)
        expect(PictorialIndexPanelContainer.props().accessionNumber).toEqual(initialState2.data.study.studyDtl.rootState.accessionNo)
        expect(PictorialIndexPanelContainer.props().seriesDtls).toEqual(initialState2.data.study.studyDtl.rootState.seriesDtls)
        expect(PictorialIndexPanelContainer.props().selectedSeriesId).toEqual(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedSeriesId)
        expect(PictorialIndexPanelContainer.props().selectedImageIds).toEqual(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedImageIds)
        expect(PictorialIndexPanelContainer.props().selectedFirstImageId).toEqual(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.selectedFirstImageId)
        expect(PictorialIndexPanelContainer.props().viewMode).toEqual(initialState2.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode)
        expect(PictorialIndexPanelContainer.props().isHidden).toEqual(initialState2.components.pictorialIndexPanel.rootState.isHidden)
        expect(PictorialIndexPanelContainer.props().numOfColumn).toEqual(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.numOfColumn)
        expect(PictorialIndexPanelContainer.props().isShowSeriesHeading).toEqual(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.isShowSeriesHeading)
        expect(PictorialIndexPanelContainer.props().serieName).toEqual(initialState2.components.pictorialIndexPanel.thumbnailGrid.rootState.serieName)    
    })
    
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})