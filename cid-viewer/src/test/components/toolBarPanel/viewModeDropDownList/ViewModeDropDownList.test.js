import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'

import ViewModeDropDownList, { styles } from '@components/toolBarPanel/viewModeTools/ViewModeDropDownList'
import { VIEW_MODES } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

configure({ adapter: new Adapter() })

describe('Test Suite: <ViewModeDropDownList>', ()=>{
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // Declare options for passing to children
    // const options = {
    //     context: { store, muiTheme },
    //     childContextTypes: { store: PropTypes.object, muiTheme: PropTypes.object }
    // }

    

    describe('Test component structure', ()=>{
        let viewMode = VIEW_MODES.ONE_X_ONE
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>
    
        // Shallow render the wrapped component
        const wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')
    
        expect(wrapper).toHaveLength(1)
        expect(wrapper.find(MenuItem)).toHaveLength(8)

        let viewModeOption1 = wrapper.find(MenuItem).at(0)
        expect(viewModeOption1.children().at(1).text()).toBe(VIEW_MODES.ONE_X_ONE.text)
        expect(viewModeOption1.props().value).toBe(VIEW_MODES.ONE_X_ONE.text)
        // console.log("viewModeOption1.props().leftIcon: " + JSON.stringify(viewModeOption1.props().leftIcon))
        // console.log("viewModeOption1.props().primaryText: " + viewModeOption1.props().primaryText)
        let viewModeOption2 = wrapper.find(MenuItem).at(1)
        expect(viewModeOption2.children().at(1).text()).toBe(VIEW_MODES.ONE_X_TWO.text)
        expect(viewModeOption2.props().value).toBe(VIEW_MODES.ONE_X_TWO.text)

        let viewModeOption3 = wrapper.find(MenuItem).at(2)
        expect(viewModeOption3.children().at(1).text()).toBe(VIEW_MODES.TWO_X_TWO.text)
        expect(viewModeOption3.props().value).toBe(VIEW_MODES.TWO_X_TWO.text)

        let viewModeOption4 = wrapper.find(MenuItem).at(3)
        expect(viewModeOption4.children().at(1).text()).toBe(VIEW_MODES.TWO_X_THREE.text)
        expect(viewModeOption4.props().value).toBe(VIEW_MODES.TWO_X_THREE.text)

        let viewModeOption5 = wrapper.find(MenuItem).at(4)
        expect(viewModeOption5.children().at(1).text()).toBe(VIEW_MODES.THREE_X_THREE.text)
        expect(viewModeOption5.props().value).toBe(VIEW_MODES.THREE_X_THREE.text)

        let viewModeOption6 = wrapper.find(MenuItem).at(5)
        expect(viewModeOption6.children().at(1).text()).toBe(VIEW_MODES.FOUR_X_FOUR.text)
        expect(viewModeOption6.props().value).toBe(VIEW_MODES.FOUR_X_FOUR.text)

        let viewModeOption7 = wrapper.find(MenuItem).at(6)
        expect(viewModeOption7.children().at(1).text()).toBe(VIEW_MODES.FIVE_X_FIVE.text)
        expect(viewModeOption7.props().value).toBe(VIEW_MODES.FIVE_X_FIVE.text)
    })

    describe('Test Behaviour/Interaction', ()=>{

        test('When View Mode options on click', ()=>{
            let viewMode = VIEW_MODES.ONE_X_ONE
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            const wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')
        
            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.ONE_X_ONE.text}})
            expect(onViewModeChange.mock.calls[0][0]).toBe(VIEW_MODES.ONE_X_ONE)

            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.ONE_X_TWO.text}})
            expect(onViewModeChange.mock.calls[1][0]).toBe(VIEW_MODES.ONE_X_TWO)

            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.TWO_X_TWO.text}})
            expect(onViewModeChange.mock.calls[2][0]).toBe(VIEW_MODES.TWO_X_TWO)

            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.TWO_X_THREE.text}})
            expect(onViewModeChange.mock.calls[3][0]).toBe(VIEW_MODES.TWO_X_THREE)

            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.THREE_X_THREE.text}})
            expect(onViewModeChange.mock.calls[4][0]).toBe(VIEW_MODES.THREE_X_THREE)

            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.FOUR_X_FOUR.text}})
            expect(onViewModeChange.mock.calls[5][0]).toBe(VIEW_MODES.FOUR_X_FOUR)

            wrapper.find(Select).simulate('change',{target: {value: VIEW_MODES.FIVE_X_FIVE.text}})
            expect(onViewModeChange.mock.calls[6][0]).toBe(VIEW_MODES.FIVE_X_FIVE)
        })

        test('When 1x1 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.ONE_X_ONE
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption1.props().disabled).toBe(true)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption2.props().disabled).toBe(false)

            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption3.props().disabled).toBe(false)

            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption4.props().disabled).toBe(false)

            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption5.props().disabled).toBe(false)

            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption6.props().disabled).toBe(false)

            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption7.props().disabled).toBe(false)
        })

        test('When 1x2 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.ONE_X_TWO
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption1.props().disabled).toBe(false)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption2.props().disabled).toBe(true)

            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption3.props().disabled).toBe(false)

            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption4.props().disabled).toBe(false)

            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption5.props().disabled).toBe(false)

            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption6.props().disabled).toBe(false)

            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption7.props().disabled).toBe(false)
        })

        test('When 2x2 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.TWO_X_TWO
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption1.props().disabled).toBe(false)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption2.props().disabled).toBe(false)
            
            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption3.props().disabled).toBe(true)

            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption4.props().disabled).toBe(false)

            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption5.props().disabled).toBe(false)

            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption6.props().disabled).toBe(false)

            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption7.props().disabled).toBe(false)
        })

        test('When 2x3 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.TWO_X_THREE
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption1.props().disabled).toBe(false)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption2.props().disabled).toBe(false)
            
            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption3.props().disabled).toBe(false)
            
            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption4.props().disabled).toBe(true)

            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption5.props().disabled).toBe(false)

            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption6.props().disabled).toBe(false)

            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption7.props().disabled).toBe(false)
        })

        test('When 3x3 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.THREE_X_THREE
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption1.props().disabled).toBe(false)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption2.props().disabled).toBe(false)
            
            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption3.props().disabled).toBe(false)
            
            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption4.props().disabled).toBe(false)
            
            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption5.props().disabled).toBe(true)

            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption6.props().disabled).toBe(false)

            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption7.props().disabled).toBe(false)
        })

        test('When 4x4 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.FOUR_X_FOUR
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption1.props().disabled).toBe(false)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption2.props().disabled).toBe(false)
            
            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption3.props().disabled).toBe(false)
            
            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption4.props().disabled).toBe(false)
            
            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption5.props().disabled).toBe(false)
            
            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption6.props().disabled).toBe(true)

            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption7.props().disabled).toBe(false)
        })

        test('When 5x5 View Mode Active', ()=>{
            let viewMode = VIEW_MODES.FIVE_X_FIVE
        
            const onViewModeChange = jest.fn()
        
            // The node to be tested
            const node = <ViewModeDropDownList
                store={store}
                theme={theme}
                classes={classes}
                viewMode={viewMode} 
                onChange={onViewModeChange}/>
        
            // Shallow render the wrapped component
            let wrapper = shallowWrappedComponent(node, 'ViewModeDropDownList')

            //Expect MenuItem dimmed and not clickale when particular viewmode is active

            let viewModeOption1 = wrapper.find(MenuItem).at(0)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption1.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption1.props().disabled).toBe(false)
            
            let viewModeOption2 = wrapper.find(MenuItem).at(1)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption2.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption2.props().disabled).toBe(false)
            
            let viewModeOption3 = wrapper.find(MenuItem).at(2)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption3.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption3.props().disabled).toBe(false)
            
            let viewModeOption4 = wrapper.find(MenuItem).at(3)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption4.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption4.props().disabled).toBe(false)
            
            let viewModeOption5 = wrapper.find(MenuItem).at(4)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption5.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption5.props().disabled).toBe(false)
            
            let viewModeOption6 = wrapper.find(MenuItem).at(5)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption6.props().leftIcon), 'opacity')).toBeUndefined()
            expect(viewModeOption6.props().disabled).toBe(false)
            
            let viewModeOption7 = wrapper.find(MenuItem).at(6)
            // expect(JsonParser.getValueFromString(JSON.stringify(viewModeOption7.props().leftIcon), 'opacity')).toEqual(0.5)
            expect(viewModeOption7.props().disabled).toBe(true)
        })

    })

    test('Verify the View Mode 1x1 snapshot', ()=>{
        let viewMode = VIEW_MODES.ONE_X_ONE
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    test('Verify the View Mode 1x2 snapshot', ()=>{
        let viewMode = VIEW_MODES.ONE_X_TWO
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    test('Verify the View Mode 2x2 snapshot', ()=>{
        let viewMode = VIEW_MODES.TWO_X_TWO
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    test('Verify the View Mode 2x3 snapshot', ()=>{
        let viewMode = VIEW_MODES.TWO_X_THREE
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    test('Verify the View Mode 3x3 snapshot', ()=>{
        let viewMode = VIEW_MODES.THREE_X_THREE
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    test('Verify the View Mode 4x4 snapshot', ()=>{
        let viewMode = VIEW_MODES.FOUR_X_FOUR
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    test('Verify the View Mode 5x5 snapshot', ()=>{
        let viewMode = VIEW_MODES.FIVE_X_FIVE
    
        const onViewModeChange = jest.fn()
    
        // The node to be tested
        const node = <ViewModeDropDownList
            store={store}
            theme={theme}
            classes={classes}
            viewMode={viewMode} 
            onChange={onViewModeChange}/>

        const renderer = new ShallowRenderer()
        renderer.render(node)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

})