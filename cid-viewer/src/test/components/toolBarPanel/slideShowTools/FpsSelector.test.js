import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import Tooltip from '@material-ui/core/Tooltip'
import IncreaseFpsButton from '@components/toolBarPanel/slideShowTools/IncreaseFpsButton'
import DecreaseFpsButton from '@components/toolBarPanel/slideShowTools/DecreaseFpsButton'
import { getReactCreateRef } from '@utils/ValidationByPassUtil'

import FpsSelector, { styles } from '@components/toolBarPanel/slideShowTools/FpsSelector'
import ShallowRenderer from 'react-test-renderer/shallow'

const mockReactRef = {
    current: {
        value: 0
    }
}
jest.mock('../../../../main/utils/ValidationByPassUtil.js', () => ({
    getReactCreateRef() {
        return mockReactRef
    }
}))

configure({ adapter: new Adapter() })

describe('Test Suite: <FpsSelector>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    const testingValue1 = {
        toolTipTitle: 'TestingTitle',
        fps: 1,
        setFps: jest.fn(),
        isEnabled: true
    }

    const testingValue2 = {
        toolTipTitle: undefined,
        fps: 2.8,
        setFps: jest.fn(),
        isEnabled: false
    }
    
    // The node to be tested
    const createNode = function(testingValue) {
        return <FpsSelector
            store={store}
            theme={theme}
            classes={classes}
            toolTipTitle={testingValue.toolTipTitle}
            fps={testingValue.fps}
            setFps={testingValue.setFps}
            isEnabled={testingValue.isEnabled}
        />
    }

    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'FpsSelector')
    const wrapper2 = shallowWrappedComponent(node2, 'FpsSelector')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find('div')).toHaveLength(4)
        expect(wrapper1.find('input')).toHaveLength(1)
        expect(wrapper1.find('span')).toHaveLength(1)
        expect(wrapper1.find(Tooltip)).toHaveLength(1)
        expect(wrapper1.find(IncreaseFpsButton)).toHaveLength(1)
        expect(wrapper1.find(DecreaseFpsButton)).toHaveLength(1)
    })

    //Verify the props passed to children
    const createTest = function (wrapper, testingValue){
        let increaseFpsButton = wrapper.find(IncreaseFpsButton).at(0)
        expect(increaseFpsButton).toHaveLength(1)
        expect(increaseFpsButton.props().isEnabled).toEqual(testingValue.isEnabled)

        let decreaseFpsButton = wrapper.find(DecreaseFpsButton).at(0)
        expect(decreaseFpsButton).toHaveLength(1)
        expect(decreaseFpsButton.props().isEnabled).toEqual(testingValue.isEnabled)

        let inputTextBox = wrapper.find('input').at(0)
        expect(inputTextBox).toHaveLength(1)
        expect(inputTextBox.props().disabled).toEqual(!testingValue.isEnabled)

    }

    test('+++ Verify the props passed to children  without hard-code output - Case 1', () => {
        createTest(wrapper1, testingValue1)

        let toolTip = wrapper1.find(Tooltip).at(0)
        expect(toolTip).toHaveLength(1)
        expect(toolTip.props().title).toEqual(testingValue1.toolTipTitle)
    })

    test('+++ Verify the props passed to children  without hard-code output - Case 2', () => {
        createTest(wrapper2, testingValue2)

        let toolTip = wrapper2.find(Tooltip).at(0)
        expect(toolTip).toHaveLength(0)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })

    describe('Test of Use Cases', () => {
        test('+++ Verify key down of the fps text box - Restricted button (lower letter - a)(keycode:65)', () => {
            //Arrange
            let fpsSelectorTextBox = wrapper1.find('input').at(0)
            let event = {
                target: {
                    value: 1
                },
                which: 65,
                preventDefault: jest.fn()
            }

            //Act
            fpsSelectorTextBox.props().onKeyDown(event)

            //Assert
            expect(event.preventDefault).toHaveBeenCalledTimes(1)
        })

        test('+++ Verify key down of the fps text box - Significant button (number - 1)(keycode:49)', () => {
            //Arrange
            let fpsSelectorTextBox = wrapper1.find('input').at(0)
            let event = {
                target: {
                    value: 1
                },
                which: 49,
                preventDefault: jest.fn()
            }

            //Act
            fpsSelectorTextBox.props().onKeyDown(event)

            //Assert
            expect(event.preventDefault).toHaveBeenCalledTimes(0)
        })

        test('+++ Verify key down of the fps text box - ArrowUp button (keycode:38)', () => {
            //Arrange
            testingValue1.setFps.mockClear()
            let fpsSelectorTextBox = wrapper1.find('input').at(0)
            let event = {
                target: {
                    value: 1
                },
                which: 38
            }

            //Act
            fpsSelectorTextBox.props().onKeyDown(event)

            //Assert
            expect(event.target.value).toEqual('1.2')
            expect(testingValue1.setFps).toHaveBeenCalledTimes(1)
        })

        test('+++ Verify key down of the fps text box - ArrowDown button (keycode:40)', () => {
            //Arrange
            testingValue1.setFps.mockClear()
            let fpsSelectorTextBox = wrapper1.find('input').at(0)
            let event = {
                target: {
                    value: 1
                },
                which: 40
            }
            //Act
            fpsSelectorTextBox.props().onKeyDown(event)

            //Assert
            expect(event.target.value).toEqual('0.8')
            expect(testingValue1.setFps).toHaveBeenCalledTimes(1)
        })

        test('+++ Verify key down of the fps text box - Enter button (keycode:13)', () => {
            //Arrange
            let fpsSelectorTextBox = wrapper1.find('input').at(0)
            let event = {
                target: {
                    value: 1,
                    blur: jest.fn()
                },
                which: 13,
                preventDefault: jest.fn()
            }
            //Act
            fpsSelectorTextBox.props().onKeyDown(event)

            //Assert
            expect(event.target.blur).toHaveBeenCalledTimes(1)
        })
        
        test('+++ Verify clicking of increaseFpsButton', () => {
            //Arrange
            testingValue1.setFps.mockClear()
            let increaseFpsButton = wrapper1.find(IncreaseFpsButton).at(0)

            //Act
            increaseFpsButton.props().onClick()

            //Assert
            expect(testingValue1.setFps).toHaveBeenCalledTimes(1)
        })

        test('+++ Verify clicking of decreaseFpsButton', () => {
            //Arrange
            testingValue1.setFps.mockClear()
            let decreaseFpsButton = wrapper1.find(DecreaseFpsButton).at(0)

            //Act
            decreaseFpsButton.props().onClick()

            //Assert
            expect(testingValue1.setFps).toHaveBeenCalledTimes(1)
        })

        test('+++ Verify blur of the fps text box', () => {
            //Arrange
            testingValue1.setFps.mockClear()
            wrapper1.instance().verifyFps = jest.fn()

            let fpsSelectorTextBox = wrapper1.find('input').at(0)
            let event = {
                target: {
                    value: 1,
                }
            }

            //Act
            fpsSelectorTextBox.props().onBlur(event)

            //Assert
            expect(wrapper1.instance().verifyFps).toHaveBeenCalledTimes(1)
            expect(testingValue1.setFps).toHaveBeenCalledTimes(1)
        })

    })

    describe('Test of calculation funtions', () => {
        test('+++ Verify increaseFps function', () => {
            //Arrange
            //Act
            let result1 =  wrapper1.instance().increaseFps('1')
            let result2 =  wrapper1.instance().increaseFps('5.6')
            let result3 =  wrapper1.instance().increaseFps('3.33333333')
            let result4 =  wrapper1.instance().increaseFps('12.2')
            let result5 =  wrapper1.instance().increaseFps('-2')
            let result6 =  wrapper1.instance().increaseFps('')
    
            //Assert
            expect(result1).toEqual('1.2')
            expect(result2).toEqual('5.8')
            expect(result3).toEqual('3.4')
            expect(result4).toEqual('10.0')
            expect(result5).toEqual('0.2')
            expect(result6).toEqual('0.2')
        })
    
        test('+++ Verify decreaseFps function', () => {
            //Arrange
            //Act
            let result1 =  wrapper1.instance().decreaseFps('1')
            let result2 =  wrapper1.instance().decreaseFps('5.6')
            let result3 =  wrapper1.instance().decreaseFps('3.33333333')
            let result4 =  wrapper1.instance().decreaseFps('12.2')
            let result5 =  wrapper1.instance().decreaseFps('-2')
            let result6 =  wrapper1.instance().decreaseFps('')
    
            //Assert
            expect(result1).toEqual('0.8')
            expect(result2).toEqual('5.4')
            expect(result3).toEqual('3.2')
            expect(result4).toEqual('10.0')
            expect(result5).toEqual('0.2')
            expect(result6).toEqual('0.2')
        })
        
    })

})