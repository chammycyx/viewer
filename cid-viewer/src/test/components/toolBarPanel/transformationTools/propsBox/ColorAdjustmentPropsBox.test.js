import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import ColorAdjustmentPropsBox, { styles, PIXEL_PER_VALUE } from '@components/toolBarPanel/transformationTools/propsBox/ColorAdjustmentPropsBox'

import Slider from '@material-ui/lab/Slider'
import Tooltip from '@material-ui/core/Tooltip'
import Button from '@material-ui/core/Button'

jest.mock('@components/base/CidPropsBox.jsx')

configure({ adapter: new Adapter() })

describe('Test Suite: <ColorAdjustmentPropsBox>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)
    
    // Declare testing values for props
    const setVisible = jest.fn()
    const onChange = jest.fn()
    const onDragMouseDown = jest.fn()

    const testingValueLabel = {
        title: 'Window Level and Contrast',
        brightness: 'Brightness',
        contrast: 'Contrast',
        hue: 'Hue',
        saturation: 'Saturation',
        invert: 'Invert',
        reset: 'Reset',
        close: 'Close'
    }

    const testingValue1 = {
        brightness: 0,
        contrast: 0,
        hue: 0,
        saturation: 0,
        invert: false
    }

    const testingValue2 = {
        brightness: 10,
        contrast: 20,
        hue: 30,
        saturation: 40,
        invert: true
    }

    // The node to be tested
    const createNode = function(testingValue) {
        return <ColorAdjustmentPropsBox
            store={store}
            theme={theme}
            classes={classes}
            brightness={testingValue.brightness}
            contrast={testingValue.contrast}
            hue={testingValue.hue}
            saturation={testingValue.saturation}
            invert={testingValue.invert}
            setVisible={setVisible}
            onChange={onChange}
            onDragMouseDown={onDragMouseDown}
        />
    }

    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'ColorAdjustmentPropsBox')
    const parentWrapper1 = shallowWrappedComponentParent(node1, 'ColorAdjustmentPropsBox')
    const wrapper2 = shallowWrappedComponent(node2, 'ColorAdjustmentPropsBox')
    const parentWrapper2 = shallowWrappedComponentParent(node2, 'ColorAdjustmentPropsBox')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find('div').length).toBeGreaterThan(1)
        expect(wrapper1.find('span')).toHaveLength(8)
        expect(wrapper1.find(Tooltip)).toHaveLength(4)
        expect(wrapper1.find(Slider)).toHaveLength(4)
        expect(wrapper1.find('input')).toHaveLength(1)
        expect(wrapper1.find(Button)).toHaveLength(2)
    })

    
    test('+++ Verify the props passed to children', () => {
        let titleDiv = wrapper1.find('div').at(1)
        titleDiv.props().onMouseDown()
        expect(onDragMouseDown).toHaveBeenCalledTimes(1)

        let brightnessSlider = wrapper1.find(Slider).at(0)
        brightnessSlider.props().onChange()
        expect(onChange).toHaveBeenCalledTimes(1)

        let contrastSlider = wrapper1.find(Slider).at(1)
        contrastSlider.props().onChange()
        expect(onChange).toHaveBeenCalledTimes(2)

        let hueSlider = wrapper1.find(Slider).at(1)
        hueSlider.props().onChange()
        expect(onChange).toHaveBeenCalledTimes(3)

        let saturationSlider = wrapper1.find(Slider).at(1)
        saturationSlider.props().onChange()
        expect(onChange).toHaveBeenCalledTimes(4)

        let invertChkBox = wrapper1.find('input').at(0)
        invertChkBox.props().onChange()
        expect(onChange).toHaveBeenCalledTimes(5)

        let resetButton = wrapper1.find(Button).at(0)
        resetButton.props().onClick()
        expect(onChange).toHaveBeenCalledTimes(6)

        let closeButton = wrapper1.find(Button).at(1)
        closeButton.props().onClick()
        expect(setVisible).toHaveBeenCalledTimes(1)
    })

    test('+++ Verify mouse enter and leave of the slider - brightnessToolTip', () => {
        //Arrange
        let brightnessToolTip = wrapper1.find(Tooltip).at(0)
        let brightnessSlider = wrapper1.find(Slider).at(0)
        onChange.mockClear()

        //Act
        brightnessSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        brightnessToolTip.props().onClose()
        brightnessSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        brightnessToolTip.props().onOpen()
        brightnessSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(2)
    })

    test('+++ Verify mouse enter and leave of the slider - contrast', () => {
        //Arrange
        let contrastToolTip = wrapper1.find(Tooltip).at(1)
        let contrastSlider = wrapper1.find(Slider).at(1)
        onChange.mockClear()

        //Act
        contrastSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        contrastToolTip.props().onClose()
        contrastSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        contrastToolTip.props().onOpen()
        contrastSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(2)
    })

    test('+++ Verify mouse enter and leave of the slider - hue', () => {
        //Arrange
        let hueToolTip = wrapper1.find(Tooltip).at(1)
        let hueSlider = wrapper1.find(Slider).at(1)
        onChange.mockClear()

        //Act
        hueSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        hueToolTip.props().onClose()
        hueSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        hueToolTip.props().onOpen()
        hueSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(2)
    })

    test('+++ Verify mouse enter and leave of the slider - saturation', () => {
        //Arrange
        let saturationToolTip = wrapper1.find(Tooltip).at(1)
        let saturationSlider = wrapper1.find(Slider).at(1)
        onChange.mockClear()
        
        //Act
        saturationSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        saturationToolTip.props().onClose()
        saturationSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(1)

        //Act
        saturationToolTip.props().onOpen()
        saturationSlider.props().onChange()
        //Assert
        expect(onChange).toHaveBeenCalledTimes(2)
    })

    //Verify the props passed to children (Label)
    test('+++ Verify the props passed to children without hard-code output (Label)', () => {
        let titleSpan = wrapper1.find('span').at(0)
        expect(titleSpan.text()).toEqual(testingValueLabel.title)

        let brightnessSpan = wrapper1.find('span').at(1)
        expect(brightnessSpan.text()).toEqual(testingValueLabel.brightness)

        let contrastSpan = wrapper1.find('span').at(2)
        expect(contrastSpan.text()).toEqual(testingValueLabel.contrast)

        let hueSpan = wrapper1.find('span').at(3)
        expect(hueSpan.text()).toEqual(testingValueLabel.hue)

        let saturationSpan = wrapper1.find('span').at(4)
        expect(saturationSpan.text()).toEqual(testingValueLabel.saturation)

        let invertSpan = wrapper1.find('span').at(5)
        expect(invertSpan.text()).toEqual(testingValueLabel.invert)

        let resetSpan = wrapper1.find('span').at(6)
        expect(resetSpan.text()).toEqual(testingValueLabel.reset)

        let closeSpan = wrapper1.find('span').at(7)
        expect(closeSpan.text()).toEqual(testingValueLabel.close)
    })

    //Verify the props passed to children
    const createTest = function (parentWrapper, wrapper, testingValue){
        let parent = parentWrapper.find('ColorAdjustmentPropsBox').at(0)

        let brightnessToolTip = wrapper.find(Tooltip).at(0)
        expect(brightnessToolTip.props().title).toEqual(testingValue.brightness)
        let brightnessMovementValue =  testingValue.brightness * PIXEL_PER_VALUE
        expect(brightnessToolTip.props().classes.tooltipPlacementTop.margin(parent.props())).toEqual(
            '-4px '+ (-brightnessMovementValue).toString() + 'px -4px ' 
            + brightnessMovementValue.toString() + 'px')

        let contrastToolTip = wrapper.find(Tooltip).at(1)
        expect(contrastToolTip.props().title).toEqual(testingValue.contrast)
        let contrastMovementValue =  testingValue.contrast * PIXEL_PER_VALUE
        expect(contrastToolTip.props().classes.tooltipPlacementTop.margin(parent.props())).toEqual(
            '-4px '+ (-contrastMovementValue).toString() + 'px -4px ' 
            + contrastMovementValue.toString() + 'px')

        let hueToolTip = wrapper.find(Tooltip).at(2)
        expect(hueToolTip.props().title).toEqual(testingValue.hue)
        let hueMovementValue =  testingValue.hue * PIXEL_PER_VALUE / 1.8
        expect(hueToolTip.props().classes.tooltipPlacementTop.margin(parent.props())).toEqual(
            '-4px '+ (-hueMovementValue).toString() + 'px -4px ' 
            + hueMovementValue.toString() + 'px')


        let saturationToolTip = wrapper.find(Tooltip).at(3)
        expect(saturationToolTip.props().title).toEqual(testingValue.saturation)
        let saturationMovementValue =  testingValue.saturation * PIXEL_PER_VALUE
        expect(saturationToolTip.props().classes.tooltipPlacementTop.margin(parent.props())).toEqual(
            '-4px '+ (-saturationMovementValue).toString() + 'px -4px ' 
            + saturationMovementValue.toString() + 'px')

        let brightnessSlider = wrapper.find(Slider).at(0)
        expect(brightnessSlider.props().value).toEqual(testingValue.brightness)

        let contrastSlider = wrapper.find(Slider).at(1)
        expect(contrastSlider.props().value).toEqual(testingValue.contrast)

        let hueSlider = wrapper.find(Slider).at(2)
        expect(hueSlider.props().value).toEqual(testingValue.hue)

        let saturationSlider = wrapper.find(Slider).at(3)
        expect(saturationSlider.props().value).toEqual(testingValue.saturation)

        let invertChkBox = wrapper.find('input').at(0)
        expect(invertChkBox.props().checked).toEqual(testingValue.invert)
    }

    test('+++ Verify the props passed to children without hard-code output - Case 1', () => {
        createTest(parentWrapper1, wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children without hard-code output - Case 2', () => {
        createTest(parentWrapper2, wrapper2, testingValue2)
    })

    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})