import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import { VIEW_MODES } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import ToolBarPanel, { styles } from '@components/toolBarPanel/ToolBarPanel'
import SelectorButton from '@components/toolBarPanel/selectorTools/SelectorButton'
import Separator from '@components/toolBarPanel/Separator'
import ZoomInButton from '@components/toolBarPanel/zoomTools/ZoomInButton'
import ZoomOutButton from '@components/toolBarPanel/zoomTools/ZoomOutButton'
import RotateLeftButton from '@components/toolBarPanel/transformationTools/RotateLeftButton'
import RotateRightButton from '@components/toolBarPanel/transformationTools/RotateRightButton'
import FlipVerticalButton from '@components/toolBarPanel/transformationTools/FlipVerticalButton'
import FlipHorizontalButton from '@components/toolBarPanel/transformationTools/FlipHorizontalButton'
import ColorAdjustmentButton from '@components/toolBarPanel/transformationTools/ColorAdjustmentButton'
import PrevButton from '@components/toolBarPanel/slideShowTools/PrevButton'
import NextButton from '@components/toolBarPanel/slideShowTools/NextButton'
import PauseButton from '@components/toolBarPanel/slideShowTools/PauseButton'
import StopButton from '@components/toolBarPanel/slideShowTools/StopButton'
import PlayButton from '@components/toolBarPanel/slideShowTools/PlayButton'
import FpsSelector from '@components/toolBarPanel/slideShowTools/FpsSelector'
import ResetButton from '@components/toolBarPanel/fileTools/ResetButton'
import ExportButton from '@components/toolBarPanel/fileTools/ExportButton'
import PrintButtonTools from '@components/toolBarPanel/printTools/PrintButtonTools'
import ViewModeDropDownList from '@components/toolBarPanel/viewModeTools/ViewModeDropDownList'
import BackToEprButton from '@components/toolBarPanel/appIntegrationTools/BackToEprButton'
import ColorAdjustmentPropsBox from '@components/toolBarPanel/transformationTools/propsBox/ColorAdjustmentPropsBox'

import { getInitColorAdjustmentValues } from '../../../main/utils/ColorAdjustmentUtil'

configure({ adapter: new Adapter() })

describe('Test Suite: <ToolBarPanel>', () => {
    // Mock store
    const mockStore = configureStore()
    const store = mockStore()
    
    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)
    
    // Declare testing values for props
    
    const setTool = jest.fn()
    const onRotateButtonClick = jest.fn()
    const onFlipButtonClick = jest.fn()
    const setSlideShowPrev = jest.fn()
    const setSlideShowNext = jest.fn()
    const pauseSlideShow = jest.fn()
    const stopSlideShow = jest.fn()
    const playSlideShow = jest.fn()
    const setExportImageDialogVisible = jest.fn()
    const onViewModeChange = jest.fn()
    const backToEpr = jest.fn()
    const setColorAdjustmentPropsBoxVisible = jest.fn()
    const onColorAdjustmentButtonClick = jest.fn()
    const onColorAdjustmentChange = jest.fn()
	const onResetButtonClick = jest.fn()

    const testingValue1 = {
        canvasGrid: {
            width: 20,
            height: 20,
            viewMode: VIEW_MODES.ONE_X_ONE,
            selectedSeriesId: '',
            showImageIds: [],
            selectedImageIds: [],
            imageDlStatus: '',
            caseNo: '',
            studyDateTime: '',
            canvasProfiles: {
                byId: {},
                allIds: []
            }
        },
        viewMode: {
            viewMode: VIEW_MODES.ONE_X_ONE,
            isEnabled: true
        },
        colorAdjustmentPropsBox: {
            visible: false
        },
        slideShow: {
            isModeOn: false,
            isPlaying: false,
            timer: null
        },
        fps: 1,
        btnState: {
            deselectBtnState: false,
            zoomInBtnState: false,
            zoomOutBtnState: false,
            viewModeBtnState: true,
            rotateLeftBtnState: false,
            rotateRightBtnState: false,
            flipVerticalBtnState: false,
            flipHorizonalBtnState: false,
            contrastBtnState: false,
            previousBtnState: false,
            nextBtnState: false,
            pauseBtnState: false,
            stopBtnState: false,
            playBtnState: false,
            fpsBtnState: false,
            resetBtnState: false,
            exportBtnState: false,
            printBtnState: false,
            backToEPRBtnState: true
        }
    }

    const testingValue2 = {
        canvasGrid: {
            width: 20,
            height: 20,
            viewMode: VIEW_MODES.ONE_X_ONE,
            selectedSeriesId: '',
            showImageIds: [],
            selectedImageIds: ['imageId_1', 'imageId_2', 'imageId_3'],
            imageDlStatus: '',
            caseNo: '',
            studyDateTime: '',
            canvasProfiles: {
                byId: {
                    'imageId_1': {
                        header: '',
                        imageBase64: '',
                        fabricProfile: {},
                        brightness: 0,
                        contrast: 0,
                        hue: 0,
                        saturation: 0,
                        invert: false,
                        zoomValue: 100,
                        imageXCoor: null,
                        imageYCoor: null,
                        imageWidth: null,
                        imageHeight: null,
                        rotateAngle: 0,
                        flipHorizontal: false,
                        flipVertical: false,
                        fontType: '',
                        fontSize: 0,
                        fontColor: '',
                        bgColor: '',
                        headerVisibility: false
                    },
                    'imageId_2': {
                        header: '',
                        imageBase64: '',
                        fabricProfile: {},
                        brightness: 20,
                        contrast: 30,
                        hue: 10,
                        saturation: 0,
                        invert: true,
                        zoomValue: 100,
                        imageXCoor: null,
                        imageYCoor: null,
                        imageWidth: null,
                        imageHeight: null,
                        rotateAngle: 0,
                        flipHorizontal: false,
                        flipVertical: false,
                        fontType: '',
                        fontSize: 0,
                        fontColor: '',
                        bgColor: '',
                        headerVisibility: false
                    },
                    'imageId_3': {
                        header: '',
                        imageBase64: '',
                        fabricProfile: {},
                        brightness: 0,
                        contrast: 0,
                        hue: 0,
                        saturation: 0,
                        invert: false,
                        zoomValue: 100,
                        imageXCoor: null,
                        imageYCoor: null,
                        imageWidth: null,
                        imageHeight: null,
                        rotateAngle: 0,
                        flipHorizontal: false,
                        flipVertical: false,
                        fontType: '',
                        fontSize: 0,
                        fontColor: '',
                        bgColor: '',
                        headerVisibility: false
                    }
                },
                allIds: []
            }
        },
        viewMode: {
            viewMode: VIEW_MODES.ONE_X_ONE,
            isEnabled: true
        },
        colorAdjustmentPropsBox: {
            visible: true
        },
        slideShow: {
            isModeOn: false,
            isPlaying: false,
            timer: null
        },
        fps: 1,
        btnState: {
            deselectBtnState: false,
            zoomInBtnState: false,
            zoomOutBtnState: false,
            viewModeBtnState: true,
            rotateLeftBtnState: false,
            rotateRightBtnState: false,
            flipVerticalBtnState: false,
            flipHorizonalBtnState: false,
            contrastBtnState: false,
            previousBtnState: false,
            nextBtnState: false,
            pauseBtnState: false,
            stopBtnState: false,
            playBtnState: false,
            fpsBtnState: false,
            resetBtnState: false,
            exportBtnState: false,
            printBtnState: false,
            backToEPRBtnState: true
        }
    }

    // The node to be tested
    const createNode = function(testingValue) {
        return <ToolBarPanel
            store={store}
            theme={theme}
            classes={classes}
            setTool={setTool}
            onRotateButtonClick={onRotateButtonClick}
            onFlipButtonClick={onFlipButtonClick}
            setSlideShowPrev={setSlideShowPrev}
            setSlideShowNext={setSlideShowNext}
            pauseSlideShow={pauseSlideShow}
            stopSlideShow={stopSlideShow}
            playSlideShow={playSlideShow}
            setExportImageDialogVisible={setExportImageDialogVisible}
            onViewModeChange={onViewModeChange}
            backToEpr={backToEpr}
            canvasGrid={testingValue.canvasGrid}
            viewMode={testingValue.viewMode.viewMode}
            colorAdjustmentPropsBox={testingValue.colorAdjustmentPropsBox}
            setColorAdjustmentPropsBoxVisible={setColorAdjustmentPropsBoxVisible}
            onColorAdjustmentButtonClick={onColorAdjustmentButtonClick}
            onColorAdjustmentChange={onColorAdjustmentChange}
            slideShow={testingValue.slideShow}
            fps={testingValue.fps}
            btnState={testingValue.btnState}
			onReset={onResetButtonClick}
        />
    }

    const node1 = createNode(testingValue1)
    const node2 = createNode(testingValue2)

    // Shallow render the wrapped component
    const wrapper1 = shallowWrappedComponent(node1, 'ToolBarPanel')
    const wrapper2 = shallowWrappedComponent(node2, 'ToolBarPanel')

    test('+++ Verify the presentational component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    test('+++ Verify the count no of children', () => {
        expect(wrapper1.find(SelectorButton)).toHaveLength(1)
        expect(wrapper1.find(ZoomInButton)).toHaveLength(1)
        expect(wrapper1.find(ZoomOutButton)).toHaveLength(1)
        expect(wrapper1.find(RotateLeftButton)).toHaveLength(1)
        expect(wrapper1.find(RotateRightButton)).toHaveLength(1)
        expect(wrapper1.find(FlipVerticalButton)).toHaveLength(1)
        expect(wrapper1.find(FlipHorizontalButton)).toHaveLength(1)
        expect(wrapper1.find(ColorAdjustmentButton)).toHaveLength(1)
        expect(wrapper1.find(ColorAdjustmentPropsBox)).toHaveLength(1)
        expect(wrapper1.find(PrevButton)).toHaveLength(1)
        expect(wrapper1.find(NextButton)).toHaveLength(1)
        expect(wrapper1.find(PauseButton)).toHaveLength(1)
        expect(wrapper1.find(StopButton)).toHaveLength(1)
        expect(wrapper1.find(PlayButton)).toHaveLength(1)
        expect(wrapper1.find(FpsSelector)).toHaveLength(1)
        expect(wrapper1.find(ResetButton)).toHaveLength(1)
        expect(wrapper1.find(ExportButton)).toHaveLength(1)
        expect(wrapper1.find(PrintButtonTools)).toHaveLength(1)
        expect(wrapper1.find(ViewModeDropDownList)).toHaveLength(1)
        expect(wrapper1.find(BackToEprButton)).toHaveLength(1)

        expect(wrapper1.find(Separator)).toHaveLength(6)
    })

    
    test('+++ Verify the props passed to children', () => {
        let selectorButton = wrapper1.find(SelectorButton).at(0)
        expect(selectorButton).toHaveLength(1)
        selectorButton.props().onClick()
        expect(setTool).toHaveBeenCalledTimes(1)
        
        let zoomInButton = wrapper1.find(ZoomInButton).at(0)
        expect(zoomInButton).toHaveLength(1)
        zoomInButton.props().onClick()
        expect(setTool).toHaveBeenCalledTimes(2)

        let zoomOutButton = wrapper1.find(ZoomOutButton).at(0)
        expect(zoomOutButton).toHaveLength(1)
        zoomOutButton.props().onClick()
        expect(setTool).toHaveBeenCalledTimes(3)

        let rotateLeftButton = wrapper1.find(RotateLeftButton).at(0)
        expect(rotateLeftButton).toHaveLength(1)
        rotateLeftButton.props().onClick()
        expect(onRotateButtonClick).toHaveBeenCalledTimes(1)

        let rotateRightButton = wrapper1.find(RotateRightButton).at(0)
        expect(rotateRightButton).toHaveLength(1)
        rotateRightButton.props().onClick()
        expect(onRotateButtonClick).toHaveBeenCalledTimes(2)

        let flipVerticalButton = wrapper1.find(FlipVerticalButton).at(0)
        expect(flipVerticalButton).toHaveLength(1)
        flipVerticalButton.props().onClick()
        expect(onFlipButtonClick).toHaveBeenCalledTimes(1)

        let flipHorizontalButton = wrapper1.find(FlipHorizontalButton).at(0)
        expect(flipHorizontalButton).toHaveLength(1)
        flipHorizontalButton.props().onClick()
        expect(onFlipButtonClick).toHaveBeenCalledTimes(2)

        let colorAdjustmentButton = wrapper1.find(ColorAdjustmentButton).at(0)
        expect(colorAdjustmentButton).toHaveLength(1)
        colorAdjustmentButton.props().onClick()
        expect(onColorAdjustmentButtonClick).toHaveBeenCalledTimes(1)

        let colorAdjustmentPropsBox = wrapper1.find(ColorAdjustmentPropsBox).at(0)
        expect(colorAdjustmentPropsBox).toHaveLength(1)
        colorAdjustmentPropsBox.props().setVisible()
        expect(setColorAdjustmentPropsBoxVisible).toHaveBeenCalledTimes(1)
        colorAdjustmentPropsBox.props().onChange()
        expect(onColorAdjustmentChange).toHaveBeenCalledTimes(1)

        let prevButton = wrapper1.find(PrevButton).at(0)
        expect(prevButton).toHaveLength(1)
        prevButton.props().onClick()
        expect(setSlideShowPrev).toHaveBeenCalledTimes(1)

        let nextButton = wrapper1.find(NextButton).at(0)
        expect(nextButton).toHaveLength(1)
        nextButton.props().onClick()
        expect(setSlideShowNext).toHaveBeenCalledTimes(1)

        let pauseButton = wrapper1.find(PauseButton).at(0)
        expect(pauseButton).toHaveLength(1)
        pauseButton.props().onClick()
        expect(pauseSlideShow).toHaveBeenCalledTimes(1)

        let stopButton = wrapper1.find(StopButton).at(0)
        expect(stopButton).toHaveLength(1)
        stopButton.props().onClick()
        expect(stopSlideShow).toHaveBeenCalledTimes(1)

        let playButton = wrapper1.find(PlayButton).at(0)
        expect(playButton).toHaveLength(1)
        playButton.props().onClick()
        expect(playSlideShow).toHaveBeenCalledTimes(1)

        let fpsSelector = wrapper1.find(FpsSelector).at(0)
        expect(fpsSelector).toHaveLength(1)

        let resetButton = wrapper1.find(ResetButton).at(0)
        expect(resetButton).toHaveLength(1)
        resetButton.props().onClick()
        expect(onResetButtonClick).toHaveBeenCalledTimes(1)

        let exportButton = wrapper1.find(ExportButton).at(0)
        expect(exportButton).toHaveLength(1)
        exportButton.props().onClick()
        expect(setExportImageDialogVisible).toHaveBeenCalledTimes(1)

        let viewModeDropDownList = wrapper1.find(ViewModeDropDownList).at(0)
        expect(viewModeDropDownList).toHaveLength(1)
        expect(viewModeDropDownList.props().viewMode).toEqual(testingValue1.viewMode.viewMode)
        viewModeDropDownList.props().onChange()
        expect(onViewModeChange).toHaveBeenCalledTimes(1)

        let backToEprButton = wrapper1.find(BackToEprButton).at(0)
        expect(backToEprButton).toHaveLength(1)
        backToEprButton.props().onClick()
        expect(backToEpr).toHaveBeenCalledTimes(1)
    })

    //Verify the props passed to children
    const createTest = function (wrapper, testingValue){

        let selectorButton = wrapper1.find(SelectorButton).at(0)
        expect(selectorButton).toHaveLength(1)
        expect(selectorButton.props().isEnabled).toEqual(testingValue.btnState.deselectBtnState)
        
        let zoomInButton = wrapper1.find(ZoomInButton).at(0)
        expect(zoomInButton).toHaveLength(1)
        expect(zoomInButton.props().isEnabled).toEqual(testingValue.btnState.zoomInBtnState)

        let zoomOutButton = wrapper1.find(ZoomOutButton).at(0)
        expect(zoomOutButton).toHaveLength(1)
        expect(zoomOutButton.props().isEnabled).toEqual(testingValue.btnState.zoomOutBtnState)

        let rotateLeftButton = wrapper.find(RotateLeftButton).at(0)
        expect(rotateLeftButton).toHaveLength(1)
        expect(rotateLeftButton.props().isEnabled).toEqual(testingValue.btnState.rotateLeftBtnState)

        let rotateRightButton = wrapper.find(RotateRightButton).at(0)
        expect(rotateRightButton).toHaveLength(1)
        expect(rotateRightButton.props().isEnabled).toEqual(testingValue.btnState.rotateRightBtnState)

        let flipVerticalButton = wrapper.find(FlipVerticalButton).at(0)
        expect(flipVerticalButton).toHaveLength(1)
        expect(flipVerticalButton.props().isEnabled).toEqual(testingValue.btnState.flipVerticalBtnState)

        let flipHorizontalButton = wrapper.find(FlipHorizontalButton).at(0)
        expect(flipHorizontalButton).toHaveLength(1)
        expect(flipHorizontalButton.props().isEnabled).toEqual(testingValue.btnState.flipHorizonalBtnState)

        let colorAdjustmentButton = wrapper.find(ColorAdjustmentButton).at(0)
        expect(colorAdjustmentButton).toHaveLength(1)
        expect(colorAdjustmentButton.props().isEnabled).toEqual(testingValue.btnState.contrastBtnState)

        const initColorAdjustmentValues = getInitColorAdjustmentValues(testingValue.canvasGrid.selectedImageIds, testingValue.canvasGrid.canvasProfiles)
        let colorAdjustmentPropsBox = wrapper.find(ColorAdjustmentPropsBox).at(0)
        expect(colorAdjustmentPropsBox).toHaveLength(1)
        expect(colorAdjustmentPropsBox.props().visible).toEqual(testingValue.colorAdjustmentPropsBox.visible)
        expect(colorAdjustmentPropsBox.props().brightness).toEqual(initColorAdjustmentValues.brightness)
        expect(colorAdjustmentPropsBox.props().contrast).toEqual(initColorAdjustmentValues.contrast)
        expect(colorAdjustmentPropsBox.props().hue).toEqual(initColorAdjustmentValues.hue)
        expect(colorAdjustmentPropsBox.props().saturation).toEqual(initColorAdjustmentValues.saturation)
        expect(colorAdjustmentPropsBox.props().invert).toEqual(initColorAdjustmentValues.invert)

        let prevButton = wrapper1.find(PrevButton).at(0)
        expect(prevButton).toHaveLength(1)
        expect(prevButton.props().isEnabled).toEqual(testingValue.btnState.previousBtnState)

        let nextButton = wrapper1.find(NextButton).at(0)
        expect(nextButton).toHaveLength(1)
        expect(nextButton.props().isEnabled).toEqual(testingValue.btnState.nextBtnState)

        let pauseButton = wrapper1.find(PauseButton).at(0)
        expect(pauseButton).toHaveLength(1)
        expect(pauseButton.props().isEnabled).toEqual(testingValue.btnState.pauseBtnState)

        let stopButton = wrapper1.find(StopButton).at(0)
        expect(stopButton).toHaveLength(1)
        expect(stopButton.props().isEnabled).toEqual(testingValue.btnState.stopBtnState)

        let playButton = wrapper1.find(PlayButton).at(0)
        expect(playButton).toHaveLength(1)
        expect(playButton.props().isEnabled).toEqual(testingValue.btnState.playBtnState)

        let fpsSelector = wrapper1.find(FpsSelector).at(0)
        expect(fpsSelector).toHaveLength(1)
        expect(fpsSelector.props().isEnabled).toEqual(testingValue.btnState.nextBtnState)

        let resetButton = wrapper1.find(ResetButton).at(0)
        expect(resetButton).toHaveLength(1)
        expect(resetButton.props().isEnabled).toEqual(testingValue.btnState.resetBtnState)

        let exportButton = wrapper1.find(ExportButton).at(0)
        expect(exportButton).toHaveLength(1)
        expect(exportButton.props().isEnabled).toEqual(testingValue.btnState.exportBtnState)

        let printButtonTools = wrapper1.find(PrintButtonTools).at(0)
        expect(printButtonTools).toHaveLength(1)
        expect(printButtonTools.props().isEnabled).toEqual(testingValue.btnState.printBtnState)

        let viewModeDropDownList = wrapper1.find(ViewModeDropDownList).at(0)
        expect(viewModeDropDownList).toHaveLength(1)
        expect(viewModeDropDownList.props().isEnabled).toEqual(testingValue.btnState.viewModeBtnState)

        let backToEprButton = wrapper1.find(BackToEprButton).at(0)
        expect(backToEprButton).toHaveLength(1)
        expect(backToEprButton.props().isEnabled).toEqual(testingValue.btnState.backToEPRBtnState)
    }

    test('+++ Verify the props passed to children  without hard-code output - Case 1', () => {
        createTest(wrapper1, testingValue1)
    })

    test('+++ Verify the props passed to children  without hard-code output - Case 2', () => {
        createTest(wrapper2, testingValue2)
    })

    //Temporary comment to bypass the validation 
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})