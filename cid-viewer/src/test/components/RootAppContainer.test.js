import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import RootApp, { styles } from '@components/RootApp'
import RootAppContainer from '@components/RootAppContainer'

configure({ adapter: new Adapter() })

describe('Test Suite: <RootAppContainer>', () => {
    // Declare testing state
    const initialState1 = {
        components: {
            rootState: {
                appWidth: 640,
                appHeight: 480
            }
        }
    }

    const initialState2 = {
        components: {
            rootState: {
                appWidth: 100,
                appHeight: 200
            }
        }
    }
    
    //For action dispatchers testing
    

    // Mock store
    const mockStore = configureStore()
    const store1 = mockStore(initialState1)
    const store2 = mockStore(initialState2)

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // The node to be tested
    const createNode = function(store){
        return <RootAppContainer
            store={store}
            theme={theme}
            classes={classes}
        />
    }
    const node1 = createNode(store1)
    const node2 = createNode(store2)

    // Shallow render the wrapped component
    const wrapper1 = shallow(node1)
    const wrapper2 = shallow(node2)
    

    test('+++ Verify the container component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    // const createTest = function (wrapper, store, appWidth, appHeight) {
    //     let rootAppContainer = wrapper.find(RootApp)
    //     rootAppContainer.props().resizeApp(appWidth, appHeight)
        
    //     const actions = store.getActions()
    //     expect(actions).toEqual([
    //         resizeApp(appWidth, appHeight)
    //     ])
    // }

    // test('+++ Verify if there is any hard-code output of the action dispatchers - Case 1', () => {
    //     createTest(wrapper1, store1, 800, 600)
    // })

    // test('+++ Verify if there is any hard-code output of the action dispatchers - Case 2', () => {
    //     createTest(wrapper2, store2, 1024, 768)
    // })

    test('+++ Verify if there is any hard-code state - Case 1', () => {
        let rootAppContainer = wrapper1.find(RootApp)
        expect(rootAppContainer.props().appWidth).toEqual(initialState1.components.rootState.appWidth)
        expect(rootAppContainer.props().appHeight).toEqual(initialState1.components.rootState.appHeight)
    })

    test('+++ Verify if there is any hard-code state - Case 2', () => {
        let rootAppContainer = wrapper2.find(RootApp)
        expect(rootAppContainer.props().appWidth).toEqual(initialState2.components.rootState.appWidth)
        expect(rootAppContainer.props().appHeight).toEqual(initialState2.components.rootState.appHeight)
    })
    
    //Temporary comment to bypass the validation 
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})