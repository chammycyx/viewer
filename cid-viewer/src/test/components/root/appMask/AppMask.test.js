import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponent, shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import AppMask, { styles } from '@components/root/appMask/AppMask'

configure({ adapter: new Adapter() })

describe('Test Suite: <AppMask>', () => {
    test("+++ Verify component's props", () => {
        //Arrange
        const mockStore = configureStore()
        const store = mockStore()
        const theme = createMuiTheme(cidTheme)
        const classes = styles(theme)
        const appMaskData = {
            isMasked:true
        }
        //Arrange

        //Act
        const createNode = function(store, appMaskData) {
            return <AppMask
                store={store}
                theme={theme}
                classes={classes}
                appMaskData={appMaskData}
                />
        }
        const node = createNode(store, appMaskData)
        //Act

        //Assert
        const containerWrapper = shallowWrappedComponentParent(node, 'AppMask')
        const appMask = containerWrapper.find('AppMask')
        expect(appMask.props().appMaskData).toBe(appMaskData)
        //Assert
    })

    // test("+++ Verify component's events", () => {})

    describe('+++ Verify rendered components', () => {
        test('+++ Verify no. of rendered components', () => {
            //Arrange
            const mockStore = configureStore()
            const store = mockStore()
            const theme = createMuiTheme(cidTheme)
            const classes = styles(theme)
            const appMaskData = {
                isMasked:true
            }
            //Arrange

            //Act
            const createNode = function(store, appMaskData) {
                return <AppMask
                    store={store}
                    theme={theme}
                    classes={classes}
                    appMaskData={appMaskData}
                    />
            }
            const node = createNode(store, appMaskData)
            //Act

            //Assert
            const appMask = shallowWrappedComponent(node, 'AppMask')
            expect(appMask).toHaveLength(1)
            //Assert
        })

        describe("+++ Verify rendered component's props", () => {
            test("+++ Verify rendered component's props when isMasked=true", () => {
                //Arrange
                const mockStore = configureStore()
                const store = mockStore()
                const theme = createMuiTheme(cidTheme)
                const classes = styles(theme)
                const appMaskData = {
                    isMasked:true
                }
                //Arrange

                //Act
                const createNode = function(store, appMaskData) {
                    return <AppMask
                        store={store}
                        theme={theme}
                        classes={classes}
                        appMaskData={appMaskData}
                        />
                }
                const node = createNode(store, appMaskData)
                //Act

                //Assert
                const containerWrapper = shallowWrappedComponentParent(node, 'AppMask')
                const appMask = containerWrapper.find('AppMask').at(0)

                const appMaskWrapper = shallowWrappedComponent(node, 'AppMask')
                const div = appMaskWrapper.find('div').at(0)

                expect(div.props().className).toEqual(classes.div)
                expect(div.props().className.display(appMask.props())).toEqual('block')
                //Assert
            })

            test("+++ Verify rendered component's props when isMasked=false", () => {
                //Arrange
                const mockStore = configureStore()
                const store = mockStore()
                const theme = createMuiTheme(cidTheme)
                const classes = styles(theme)
                const appMaskData = {
                    isMasked:false
                }
                //Arrange

                //Act
                const createNode = function(store, appMaskData) {
                    return <AppMask
                        store={store}
                        theme={theme}
                        classes={classes}
                        appMaskData={appMaskData}
                        />
                }
                const node = createNode(store, appMaskData)
                //Act

                //Assert
                const containerWrapper = shallowWrappedComponentParent(node, 'AppMask')
                const appMask = containerWrapper.find('AppMask').at(0)

                const appMaskWrapper = shallowWrappedComponent(node, 'AppMask')
                const div = appMaskWrapper.find('div').at(0)

                expect(div.props().className).toEqual(classes.div)
                expect(div.props().className.display(appMask.props())).toEqual('none')
                //Assert
            })
        })
    })

})