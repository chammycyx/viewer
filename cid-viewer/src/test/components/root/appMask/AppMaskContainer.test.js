import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import configureStore from 'redux-mock-store'
import { shallowWrappedComponentParent } from '@utils/DomRenderTestUtil'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import cidTheme from '@styles/CidTheme'

import AppMaskContainer from '@components/root/appMask/AppMaskContainer'
import { styles } from '@components/root/appMask/AppMask'

configure({ adapter: new Adapter() })

describe('Test Suite: <AppMaskContainer>', () => {
    test('+++ Verify if component rendered', () => {
        //Arrange
        let state = {
            components: {
                appMask: {
                    rootState:{a:1, b:2}
                }
            }
        }
        const mockStore = configureStore()
        const store = mockStore(state)
        const theme = createMuiTheme(cidTheme)
        const classes = styles(theme)
        //Arrange

        //Act
        const createNode = function(store){
            return <AppMaskContainer
                store={store}
                theme={theme}
                classes={classes}
                />
        }
        const node = createNode(store)
        //Act

        //Assert
        const container = shallowWrappedComponentParent(node, 'AppMask')
        expect(container).toHaveLength(1)
        //Assert
    })

    test('+++ Verify mapStateToProps', () => {
        //Arrange
        let state = {
            components: {
                appMask: {
                    rootState:{a:1, b:2}
                }
            }
        }
        // Mock store
        const mockStore = configureStore()
        const store = mockStore(state)
        const theme = createMuiTheme(cidTheme)
        const classes = styles(theme)
        //Arrange

        //Act
        const createNode = function(store){
            return <AppMaskContainer
                store={store}
                theme={theme}
                classes={classes}
                />
        }
        const node = createNode(store)
        //Act

        //Assert
        const container = shallowWrappedComponentParent(node, 'AppMask')
        let appMask = container.find('AppMask')
        expect(appMask.props().appMaskData).toEqual(state.components.appMask.rootState)
        //Assert
    })

    // test('+++ Verify mapDispatchToProps', () => {})
})