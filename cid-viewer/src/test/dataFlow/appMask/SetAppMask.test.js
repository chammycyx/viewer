import {getStore, stop} from '@test/dataFlow/DataFlowHelper'
import dataRespository from '@utils/DataRepository'


let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <Test data flow AppMaskActionTypes.SET_APP_MASK>',async ()=>{
    test('+++ Default setting pf application mask', async ()=>{
        //Arrange
        let prevAppMaskState = store.getState().components.appMask

        //Act
        // dataRespository.setStore(store)
        // dataRespository.setAppMask(true)


        //Assert
        let nextAppMaskState = store.getState().components.appMask
        let expectedMaskValue = false

        //assert value updated, assert the prev and next state value could assure that value update by Object.assign
        expect(nextAppMaskState.rootState.isMasked).toEqual(expectedMaskValue)
    })
	test('+++ Mask application', async ()=>{
        //Arrange
        let prevAppMaskState = store.getState().components.appMask

        //Act
        dataRespository.setStore(store)
        dataRespository.setAppMask(true)


        //Assert
        let nextAppMaskState = store.getState().components.appMask
        let expectedMaskValue = true

        //assert value updated, assert the prev and next state value could assure that value update by Object.assign
        expect(nextAppMaskState.rootState.isMasked).toEqual(expectedMaskValue)

        //assert immutation takes place in reducer
        expect(prevAppMaskState.rootState).not.toBe(nextAppMaskState.rootState)
    })

    test('+++ Unmask application', async ()=>{
        //Arrange
        let prevAppMaskState = store.getState().components.appMask

        //Act
        dataRespository.setStore(store)
        dataRespository.setAppMask(false)


        //Assert
        let nextAppMaskState = store.getState().components.appMask
        let expectedMaskValue = false

        //assert value updated, assert the prev and next state value could assure that value update by Object.assign
        expect(nextAppMaskState.rootState.isMasked).toEqual(expectedMaskValue)

        //assert immutation takes place in reducer
        expect(prevAppMaskState.rootState).not.toBe(nextAppMaskState.rootState)
    })
})