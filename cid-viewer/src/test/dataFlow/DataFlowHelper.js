import createSagaMiddleware from 'redux-saga'
import { createStore, applyMiddleware } from 'redux'

import RootReducer from '@reducers/RootReducer'
import rootSaga from '../../main/sagas/rootSaga'

import {clone} from '@utils/CloneUtil'

/**
 * Introduce window.initState to store the initial state of store for solving the RootReducer get polluted issue(Note1)
 * 
 * 
 * Note1: The constant variable in RootReducer would be modified after the first time call of createStore(RootReducer)
 * Note2: Each test file <name>.test.js has its own window reference. This window reference is globally shared among all class/function/file including DataFlowHelper
 * @export
 * @returns store with intial state tree
 */
export function getStore(){
    let context = window
    if(context.store==null){
        const sagaMiddleware = createSagaMiddleware()
        let store = createStore(RootReducer, applyMiddleware(sagaMiddleware))
        sagaMiddleware.run(rootSaga)

        context.initState = clone(store.getState())
        context.store = store

        return store
    }

    //set state to initial state
    context.store.getState().components = clone(context.initState.components)       
    context.store.getState().data = clone(context.initState.data)

    return context.store
}

export function stop(ms, callback){
	return new Promise((resolve, reject) => {
		setTimeout(function(){
            if(callback!=null){
                callback()
            }
			resolve()
		},ms)
	})
}