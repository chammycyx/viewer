import {readCaseProfile} from '../../main/actions/data/common/Action'
import store from './DataFlowHelper'
import {clone} from '../../main/utils/CloneUtil'

test('Test the data flow of ReadCaseProfile', ()=>{
    // //Arrange
    // let previousStateExceptCommon = clone(store.getState())
    //     previousStateExceptCommon.data.common = null
    // jsdom.reconfigure({
    //     url: "http://www.test.com/test?systemID=RIS&accessKey=1FD2B404-662E-48C2-8351-BDF2C40E8711&loginID=ISG&userHospCde=VH1&wrkStnID=dc1cidst02&hospCde=VH&patientKey=90500231&specialtyCde=%40%7B%7D%25%24%5B%5D%3F<>%2F%27%3A-*%21~%23%5E%26&caseNo=HN05000207X&accessionNo=VH+ER1810000412U&seriesSeqNo=1&imageSeqNo=2&versionNo=3&appHost=dc1cidst02&appSiteName=CIDImageViewingTester%2F6_0_0%2FcidImageViewer.html&appName=name"
    // })

    // //Act
    // store.dispatch(readCaseProfile())

    // //Assert
    // let state = store.getState()
    // let currentStateExceptCommon = clone(store.getState())
    //     currentStateExceptCommon.data.common = null
    // expect(JSON.stringify(currentStateExceptCommon)).toBe(JSON.stringify(previousStateExceptCommon))
    // expect(JSON.stringify(state.data.common)).toBe(JSON.stringify(
    //     {
    //         rootState:{
    //             hospCode: 'VH',
    //             caseNo: 'HN05000207X',
    //             accessionNo: 'VH ER1810000412U',
    //             seriesNo: '1',
    //             imageSeqNo: '2',
    //             versionNo: '3',
    //             userId: 'ISG',
    //             workstationId: 'dc1cidst02',
    //             requestSys: 'RIS',
    //             accessKey: '1FD2B404-662E-48C2-8351-BDF2C40E8711',
    //             userHospCode: 'VH1',
    //             patientKey: '90500231',
    //             specialtyCode: '@{}%$[]?<>/\':-*!~#^&'
    //         }
    //     }
    // ))
})