import {getStore, stop} from '@test/dataFlow/DataFlowHelper'
import dataRepository from '@utils/DataRepository'
import { setAppSize } from '@actions/components/Action'

let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <The data flow of setAppSize>', async ()=>{
    test('Testing of resizing app to 800x600', async ()=>{
        // Previous states
        let prevState = store.getState()

        // Get changed values in states for assertions
        let prevRootAppRootState = prevState.components.rootState
        
        // Arrange
        dataRepository.setStore(store)
        store.dispatch(setAppSize(800, 600))
        await stop(2000)

        // Next states
        let nextState = store.getState()

        // Assert
        let nextRootAppRootState = nextState.components.rootState
        expect(nextRootAppRootState.appWidth).toEqual(800)
        expect(nextRootAppRootState.appHeight).toEqual(600)

        // Assert immutation takes place in reducer
        expect(nextRootAppRootState).not.toBe(prevRootAppRootState)
    })

    test('Testing of resizing app to 1024x768', async ()=>{
        // Previous states
        let prevState = store.getState()

        // Get changed values in states for assertions
        let prevRootAppRootState = prevState.components.rootState
        
        // Arrange
        dataRepository.setStore(store)
        store.dispatch(setAppSize(1024, 768))
        await stop(2000)

        // Next states
        let nextState = store.getState()

        // Assert
        let nextRootAppRootState = nextState.components.rootState
        expect(nextRootAppRootState.appWidth).toEqual(1024)
        expect(nextRootAppRootState.appHeight).toEqual(768)

        // Assert immutation takes place in reducer
        expect(nextRootAppRootState).not.toBe(prevRootAppRootState)
    })
})
