import {getStore, stop} from '@test/dataFlow/DataFlowHelper'
import {clone} from '@utils/CloneUtil'
import JsonParser from '@utils/JsonParser'
import {initState as ViewModeDropDownListInitState} from '@reducers/components/toolBarPanel/viewModeTools/viewModeDropDownList/Reducer'
import {showImage, setGrid, resizeImgViewingPanel, onImgMoved, setImageHeaderConfig, onCanvasClick, onViewModeChange, setImageZoom, setImageScale} from '@actions/components/imageViewingPanel/canvasGrid/Action'
import {VIEW_MODES} from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import {CanvasProfileInitState} from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'

import cidTheme from '@root/src/main/styles/CidTheme'

import {ViewingPanelController} from '@domains/controllers/ViewingPanelController'
jest.mock('@domains/controllers/ViewingPanelController')
import {setImage as setImageToData} from '@actions/data/study/studyDtl/Action'
import {setImage as setImageToViewPanel} from '@actions/components/imageViewingPanel/canvasGrid/Action'

import dataRespository from '@utils/DataRepository'
import {ImageSelection, ImageTool} from '@constants/ComponentConstants'

let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <The data flow of ImageViewingPanel>', ()=>{

    describe('Test Action: <CanvasGridActionTypes.SET_GRID>', ()=>{
        test('Test default Grid(default view mode)', ()=>{
            //Arrange
            const previousState = clone(store.getState())

            //Act
    
    
            //Assert
            let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
            //It should be the same as default view mode in ViewModeDropDownList
            expect(prevCanvasGridState.rootState.viewMode).toEqual(ViewModeDropDownListInitState.viewMode)
        })

        // test('Test set Grid - no any image shown', ()=>{
        //     //Arrange
        //     const previousState = store.getState()
        //     const prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")

            
        //     //arrange mock
        //     const viewMode = VIEW_MODES.ONE_X_TWO
        //     const canvasWidth = 1
        //     const canvasHeight = 2
        //     const seriesIndexId = ''
        //     const showImageIds = []
        //     const imgsProperties = []
        //     ViewingPanelController.mockImplementation(()=>{
        //         return {
        //             setGrid: function(){
        //                 store.dispatch(setGrid(viewMode, canvasWidth, canvasHeight, seriesIndexId, showImageIds, imgsProperties))
        //             }
        //         }
        //     })
    
        //     //Act
        //     store.dispatch(onViewModeChange(viewMode))
    
    
        //     //Assert
        //     const nextState = store.getState()
        //     const nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
        //     //assert value updated
        //     expect(nextCanvasGridState.rootState.viewMode).toEqual(VIEW_MODES.ONE_X_TWO)
        //     expect(nextCanvasGridState.rootState.canvasWidth).toEqual(canvasWidth)
        //     expect(nextCanvasGridState.rootState.canvasHeight).toEqual(canvasHeight)
        //     //assert immutation takes place in reducer
        //     expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
        // })

        // test('Test set Grid - 2 images on shown', ()=>{
        //     //Arrange
        //     let previousState = store.getState()
        //     const prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")

            
        //     //arrange mock
        //     const viewMode = VIEW_MODES.ONE_X_TWO
        //     const canvasWidth = 1
        //     const canvasHeight = 2
        //     const seriesIndexId = 'seriesDtl_1'
        //     const showImageIds = ["imageDtl_1", "imageDtl_2"]
        //     const imgsProperties = [
        //         {imageXCoor: 3, imageYCoor: 4, imageScaleX: 5, imageScaleY: 6},
        //         {imageXCoor: 7, imageYCoor: 8, imageScaleX: 9, imageScaleY: 10}
        //     ]
        //     ViewingPanelController.mockImplementation(()=>{
        //         return {
        //             setGrid: function(){
        //                 store.dispatch(setGrid(viewMode, canvasWidth, canvasHeight, seriesIndexId, showImageIds, imgsProperties))
        //             }
        //         }
        //     })
    
        //     //Act
        //     store.dispatch(onViewModeChange(viewMode))
    
    
        //     //Assert
        //     const nextState = store.getState()
        //     const nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
        //     //assert value updated
        //     expect(nextCanvasGridState.rootState.viewMode).toEqual(VIEW_MODES.ONE_X_TWO)
        //     expect(nextCanvasGridState.rootState.canvasWidth).toEqual(canvasWidth)
        //     expect(nextCanvasGridState.rootState.canvasHeight).toEqual(canvasHeight)
        //     const nextImageDtl1State = nextCanvasGridState.rootState.canvasProfiles.byId[showImageIds[0]]
        //     const nextImageDtl2State = nextCanvasGridState.rootState.canvasProfiles.byId[showImageIds[1]]
        //     expect(nextImageDtl1State.imageXCoor).toEqual(3)
        //     expect(nextImageDtl1State.imageYCoor).toEqual(4)
        //     expect(nextImageDtl1State.imageScaleX).toEqual(5)
        //     expect(nextImageDtl1State.imageScaleY).toEqual(6)
        //     expect(nextImageDtl2State.imageXCoor).toEqual(7)
        //     expect(nextImageDtl2State.imageYCoor).toEqual(8)
        //     expect(nextImageDtl2State.imageScaleX).toEqual(9)
        //     expect(nextImageDtl2State.imageScaleY).toEqual(10)
        //     //assert immutation takes place in reducer
        //     expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
        // })
    })


    // describe('Test Action: <CanvasGridActionTypes.SHOW_IMAGE>', ()=>{
    //     test('Show one image', ()=>{
    //         //Arrange
    //         //set initial state
    //         let previousState = store.getState()
    //         const previousCanvasGridState = previousState.components.imageViewingPanel.canvasGrid

    //         //arrange mock
    //         dataRespository.findIasWsAuthInfo = jest.fn()

    //         const caseNo = "1"
    //         const studyDtm = "2"
    //         const viewMode = {a:1, b:2}
    //         const seriesIndexId = "seriesDtl_1"
    //         const imgIndexIds = ["imageDtl_1"]
    //         const imageBase64s = ["6"]
    //         const imgsProperties = [
    //             {imageWidth: 8.1, imageHeight: 9.1, imageOriginX: "center", imageOriginY: "center", imageXCoor: 10, imageYCoor: 11, imageScaleX: 12.1, imageScaleY: 13.1}
    //         ]
    //         const imageCreateTimeStamp = ["14"]
    //         const examType = "15"
    //         ViewingPanelController.mockImplementation(()=>{
    //             return{
    //                 showImage: function(){
    //                     store.dispatch(setImageToViewPanel(caseNo, studyDtm, viewMode, seriesIndexId, imgIndexIds, imageBase64s, imgsProperties, imageCreateTimeStamp, examType))
    //                 }
    //             }
    //         })

    //         //Act
    //         let selectedSeriesId = "seriesDtl_1"
    //         let selectedImageIds = ["imageDtl_1"]
    //         store.dispatch(showImage(selectedSeriesId, selectedImageIds))

    //         //Assert
    //         const expectedViewMode = viewMode
    //         const expectedSelectedSeriesId = seriesIndexId
    //         const expectedShowImageIds = imgIndexIds
    //         const expectedCaseNo = caseNo
    //         const expectedStudyDateTime = studyDtm
    //         const expectedSelectedImageIds = [imgIndexIds[0]]
    //         const expectedHeaderTimeText = ' Study Date/Time: '
    //         const expectedDisplayDateTime = [studyDtm]
    //         const expectedCanvasProfiles = {
    //             byId: {
    //                 'imageDtl_1': {
    //                     header: '',
    //                     imageBase64: "6",
    //                     brightness: 0,
    //                     contrast: 0,
    //                     hue: 0,
    //                     saturation: 0,
    //                     invert: false,
    //                     imageXCoor: 10,
    //                     imageYCoor: 11,
    //                     imageWidth: 8.1,
    //                     imageHeight: 9.1,
    //                     imageScaleX: 12.1,
    //                     imageScaleY: 13.1,
    //                     imageOriginX: 'center',
    //                     imageOriginY: 'center',
    //                     rotateAngle: 0,
    //                     flipHorizontal: false,
    //                     flipVertical: false,
    //                     fontType: '',
    //                     fontSize: 0,
    //                     fontColor: '',
    //                     bgColor: '',
    //                     visibility: false
    //                 }
    //             },
    //             allIds: ['imageDtl_1']
    //         }
    //         const nextState = store.getState()
    //         const nextCanvasGridState = nextState.components.imageViewingPanel.canvasGrid
    //         //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //         expect(nextCanvasGridState.rootState.viewMode).toEqual(expectedViewMode)
    //         expect(nextCanvasGridState.rootState.selectedSeriesId).toEqual(expectedSelectedSeriesId)
    //         expect(nextCanvasGridState.rootState.showImageIds).toEqual(expectedShowImageIds)
    //         expect(nextCanvasGridState.rootState.caseNo).toEqual(expectedCaseNo)
    //         expect(nextCanvasGridState.rootState.studyDateTime).toEqual(expectedStudyDateTime)
    //         expect(nextCanvasGridState.rootState.selectedImageIds).toEqual(expectedSelectedImageIds)
    //         expect(nextCanvasGridState.rootState.headerTimeText).toEqual(expectedHeaderTimeText)
    //         expect(nextCanvasGridState.rootState.displayDateTime).toEqual(expectedDisplayDateTime)
    //         expect(nextCanvasGridState.rootState.canvasProfiles).toEqual(expectedCanvasProfiles)
    //         //assert immutation takes place in reducer
    //         expect(previousCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    //     test('Show two images', ()=>{
    //         //Arrange

    //         //set initial state
    //         let previousState = store.getState()
    //         let previousCanvasGridState = previousState.components.imageViewingPanel.canvasGrid
    //             previousCanvasGridState.rootState.rtcImageCreateDateTime = true

    //         //mock module
    //         dataRespository.findIasWsAuthInfo = jest.fn()

    //         const caseNo = "1"
    //         const studyDtm = "2"
    //         const viewMode = {a:1, b:2}
    //         const seriesIndexId = "seriesDtl_1"
    //         const imgIndexIds = ["imageDtl_1", "imageDtl_2"]
    //         const imageBase64s = ["6", "6.1"]
    //         const imgsProperties = [
    //             {imageWidth: 8.1, imageHeight: 9.1, imageOriginX: "center1", imageOriginY: "center2", imageXCoor: 10, imageYCoor: 11, imageScaleX: 12.1, imageScaleY: 13.1},
    //             {imageWidth: 8.2, imageHeight: 9.2, imageOriginX: "center3", imageOriginY: "center4", imageXCoor: 14, imageYCoor: 15, imageScaleX: 16, imageScaleY: 17}
    //         ]
    //         const imageCreateTimeStamp = ["20180730142001.123", "20180730142002.123"]
    //         const examType = "19"
    //         ViewingPanelController.mockImplementation(()=>{
    //             return{
    //                 showImage: function(){
    //                     store.dispatch(setImageToViewPanel(caseNo, studyDtm, viewMode, seriesIndexId, imgIndexIds, imageBase64s, imgsProperties, imageCreateTimeStamp, examType))
    //                 }
    //             }
    //         })

    //         //Act
    //         let selectedSeriesId = "seriesDtl_1"
    //         let selectedImageIds = ["imageDtl_1", "imageDtl_2"]
    //         store.dispatch(showImage(selectedSeriesId, selectedImageIds))

    //         //Assert
    //         const expectedViewMode = viewMode
    //         const expectedSelectedSeriesId = seriesIndexId
    //         const expectedShowImageIds = imgIndexIds
    //         const expectedCaseNo = caseNo
    //         const expectedStudyDateTime = studyDtm
    //         const expectedSelectedImageIds = [imgIndexIds[0]]
    //         const expectedHeaderTimeText = ' Image Create Date/Time '
    //         const expectedDisplayDateTime = ["20180730142001", "20180730142002"]
    //         const expectedCanvasProfiles = {
    //             byId: {
    //                 'imageDtl_1': {
    //                     header: '',
    //                     imageBase64: "6",
    //                     brightness: 0,
    //                     contrast: 0,
    //                     hue: 0,
    //                     saturation: 0,
    //                     invert: false,
    //                     imageXCoor: 10,
    //                     imageYCoor: 11,
    //                     imageWidth: 8.1,
    //                     imageHeight: 9.1,
    //                     imageScaleX: 12.1,
    //                     imageScaleY: 13.1,
    //                     imageOriginX: 'center1',
    //                     imageOriginY: 'center2',
    //                     rotateAngle: 0,
    //                     flipHorizontal: false,
    //                     flipVertical: false,
    //                     fontType: '',
    //                     fontSize: 0,
    //                     fontColor: '',
    //                     bgColor: '',
    //                     visibility: false
    //                 },
    //                 'imageDtl_2': {
    //                     header: '',
    //                     imageBase64: "6.1",
    //                     brightness: 0,
    //                     contrast: 0,
    //                     hue: 0,
    //                     saturation: 0,
    //                     invert: false,
    //                     imageXCoor: 14,
    //                     imageYCoor: 15,
    //                     imageWidth: 8.2,
    //                     imageHeight: 9.2,
    //                     imageScaleX: 16,
    //                     imageScaleY: 17,
    //                     imageOriginX: 'center3',
    //                     imageOriginY: 'center4',
    //                     rotateAngle: 0,
    //                     flipHorizontal: false,
    //                     flipVertical: false,
    //                     fontType: '',
    //                     fontSize: 0,
    //                     fontColor: '',
    //                     bgColor: '',
    //                     visibility: false
    //                 }
    //             },
    //             allIds: ['imageDtl_1', 'imageDtl_2']
    //         }
    //         const nextState = store.getState()
    //         const nextCanvasGridState = nextState.components.imageViewingPanel.canvasGrid
    //         //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //         expect(nextCanvasGridState.rootState.viewMode).toEqual(expectedViewMode)
    //         expect(nextCanvasGridState.rootState.selectedSeriesId).toEqual(expectedSelectedSeriesId)
    //         expect(nextCanvasGridState.rootState.showImageIds).toEqual(expectedShowImageIds)
    //         expect(nextCanvasGridState.rootState.caseNo).toEqual(expectedCaseNo)
    //         expect(nextCanvasGridState.rootState.studyDateTime).toEqual(expectedStudyDateTime)
    //         expect(nextCanvasGridState.rootState.selectedImageIds).toEqual(expectedSelectedImageIds)
    //         expect(nextCanvasGridState.rootState.headerTimeText).toEqual(expectedHeaderTimeText)
    //         expect(nextCanvasGridState.rootState.displayDateTime).toEqual(expectedDisplayDateTime)
    //         expect(nextCanvasGridState.rootState.canvasProfiles).toEqual(expectedCanvasProfiles)
    //         //assert immutation takes place in reducer
    //         expect(previousCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })
    // })

    // describe('Test Action: <CanvasGridActionTypes.RESIZE_IMG_VIEWING_PANEL>', ()=>{
    //     describe('When PictorialIndexPanel is collapsed',()=>{
    //         test('1x1 View Mode',()=>{
    //             //Arrange
    //             let previousState = store.getState()
    //             let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //                 prevCanvasGridState.rootState.viewMode = VIEW_MODES.ONE_X_ONE
    //             //record the value for value assertion
    //             const prevCanvasGridRootStateWidth = prevCanvasGridState.rootState.width
    //             const prevCanvasGridRootStateHeight = prevCanvasGridState.rootState.height

    //             calculateGridDimensions.mockImplementation((panelWidth: number, panelHeight: number, viewMode: ViewModeType)=>{
    //                 return require.requireActual('@domains/controllers/ViewingPanelController').calculateGridDimensions(panelWidth, panelHeight, viewMode)
    //             })
        
    //             //Act
    //             const isPictorialIndexPanelHidden = true
    //             store.dispatch(resizeImgViewingPanel(isPictorialIndexPanelHidden))
        
        
    //             //Assert
    //             const nextState = store.getState()
    //             const nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //             //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //             expect(prevCanvasGridState.rootState.width).toEqual(prevCanvasGridRootStateWidth)
    //             expect(prevCanvasGridState.rootState.height).toEqual(prevCanvasGridRootStateHeight)
    //             expect(nextCanvasGridState.rootState.width).toEqual((window.innerWidth-cidTheme.pictorialIndexPanel.showHideButton.showButton.width))
    //             expect(nextCanvasGridState.rootState.height).toEqual((window.innerHeight-cidTheme.toolBarPanel.height))
    //             expect(nextCanvasGridState.rootState.canvasWidth).toEqual(nextCanvasGridState.rootState.width)
    //             expect(nextCanvasGridState.rootState.canvasHeight).toEqual((nextCanvasGridState.rootState.height))
    //             //assert immutation takes place in reducer
    //             expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //         })

    //         test('2x3 View Mode',()=>{
    //             //Arrange
    //             let previousState = store.getState()
    //             let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //                 prevCanvasGridState.rootState.viewMode = VIEW_MODES.TWO_X_THREE
    //             //record the value for value assertion
    //             const prevCanvasGridRootStateWidth = prevCanvasGridState.rootState.width
    //             const prevCanvasGridRootStateHeight = prevCanvasGridState.rootState.height

    //             calculateGridDimensions.mockImplementation((panelWidth: number, panelHeight: number, viewMode: ViewModeType)=>{
    //                 return require.requireActual('@domains/controllers/ViewingPanelController').calculateGridDimensions(panelWidth, panelHeight, viewMode)
    //             })
        
    //             //Act
    //             const isPictorialIndexPanelHidden = true
    //             store.dispatch(resizeImgViewingPanel(isPictorialIndexPanelHidden))
        
        
    //             //Assert
    //             const nextState = store.getState()
    //             const nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //             //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //             expect(prevCanvasGridState.rootState.width).toEqual(prevCanvasGridRootStateWidth)
    //             expect(prevCanvasGridState.rootState.height).toEqual(prevCanvasGridRootStateHeight)
    //             expect(nextCanvasGridState.rootState.width).toEqual((window.innerWidth-cidTheme.pictorialIndexPanel.showHideButton.showButton.width))
    //             expect(nextCanvasGridState.rootState.height).toEqual((window.innerHeight-cidTheme.toolBarPanel.height))
    //             expect(nextCanvasGridState.rootState.canvasWidth).toEqual(nextCanvasGridState.rootState.width/3)
    //             expect(nextCanvasGridState.rootState.canvasHeight).toEqual((nextCanvasGridState.rootState.height/2))
    //             //assert immutation takes place in reducer
    //             expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //             //TODO: assert the x coordinate of image should remain the same
    //         })

    //     })

    //     describe('When PictorialIndexPanel is expanded',()=>{
    //         test('1x1 View Mode',()=>{
    //             //Arrange
    //             let previousState = store.getState()
    //             let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //                 prevCanvasGridState.rootState.viewMode = VIEW_MODES.ONE_X_ONE
    //             const prevCanvasGridRootStateWidth = prevCanvasGridState.rootState.width
    //             const prevCanvasGridRootStateHeight = prevCanvasGridState.rootState.height

    //             calculateGridDimensions.mockImplementation((panelWidth: number, panelHeight: number, viewMode: ViewModeType)=>{
    //                 return require.requireActual('@domains/controllers/ViewingPanelController').calculateGridDimensions(panelWidth, panelHeight, viewMode)
    //             })
        
    //             //Act
    //             const isPictorialIndexPanelHidden = false
    //             store.dispatch(resizeImgViewingPanel(isPictorialIndexPanelHidden))
        
        
    //             //Assert
    //             const nextState = store.getState()
    //             const nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //             //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //             expect(prevCanvasGridState.rootState.width).toEqual(prevCanvasGridRootStateWidth)
    //             expect(prevCanvasGridState.rootState.height).toEqual(prevCanvasGridRootStateHeight)
    //             expect(nextCanvasGridState.rootState.width).toEqual((window.innerWidth-cidTheme.pictorialIndexPanel.width))
    //             expect(nextCanvasGridState.rootState.height).toEqual((window.innerHeight-cidTheme.toolBarPanel.height))
    //             expect(nextCanvasGridState.rootState.canvasWidth).toEqual(nextCanvasGridState.rootState.width)
    //             expect(nextCanvasGridState.rootState.canvasHeight).toEqual((nextCanvasGridState.rootState.height))
    //             //assert immutation takes place in reducer
    //             expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //         })

    //         test('2x3 View Mode',()=>{
    //             //Arrange
    //             let previousState = store.getState()
    //             let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //                 prevCanvasGridState.rootState.viewMode = VIEW_MODES.TWO_X_THREE
    //             //record the value for value assertion
    //             const prevCanvasGridRootStateWidth = prevCanvasGridState.rootState.width
    //             const prevCanvasGridRootStateHeight = prevCanvasGridState.rootState.height

    //             calculateGridDimensions.mockImplementation((panelWidth: number, panelHeight: number, viewMode: ViewModeType)=>{
    //                 return require.requireActual('@domains/controllers/ViewingPanelController').calculateGridDimensions(panelWidth, panelHeight, viewMode)
    //             })
        
    //             //Act
    //             const isPictorialIndexPanelHidden = false
    //             store.dispatch(resizeImgViewingPanel(isPictorialIndexPanelHidden))
        
        
    //             //Assert
    //             const nextState = store.getState()
    //             const nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //             //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //             expect(prevCanvasGridState.rootState.width).toEqual(prevCanvasGridRootStateWidth)
    //             expect(prevCanvasGridState.rootState.height).toEqual(prevCanvasGridRootStateHeight)
    //             expect(nextCanvasGridState.rootState.width).toEqual((window.innerWidth-cidTheme.pictorialIndexPanel.width))
    //             expect(nextCanvasGridState.rootState.height).toEqual((window.innerHeight-cidTheme.toolBarPanel.height))
    //             expect(nextCanvasGridState.rootState.canvasWidth).toEqual(nextCanvasGridState.rootState.width/3)
    //             expect(nextCanvasGridState.rootState.canvasHeight).toEqual((nextCanvasGridState.rootState.height/2))
    //             //assert immutation takes place in reducer
    //             expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //         })

    //         test('Image will move out of the canvas after PictorialIndexPanel is expanded',()=>{
    //             //TODO: assert the x coordinate of image should be adjusted to move the image inside the canvas
    //         })
    //     })

    // })

    // test('Test Action: <CanvasGridActionTypes.ON_IMAGE_MOVED>', ()=>{
    //     //Arrange
    //     let previousState = store.getState()
                    
    //     // prepare imageDtl stub in canvasGrid.rootState.canvasProfiles.byId
    //     let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //     prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //     //record the reference of rootState and rootState.viewMode for immutation assertion
    //     let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //     let prevCanvasGridRootState = prevCanvasGridState.rootState
    //     //record the reference of imageDtl immutation assertion
    //     let prevImageDtl1State = prevCanvasGridRootState.canvasProfiles.byId['imageDtl_1']


    //     //Act
    //     let isPictorialIndexPanelHidden = false
    //     let seriesId = '5'
    //     let imageId = 'imageDtl_1'
    //     let nextXCoor = 20
    //     let nextYCoor = 21
    //     // expect(prevImageDtl1State).toBe(1)
    //     store.dispatch(onImgMoved(seriesId, imageId, nextXCoor, nextYCoor))


    //     //Assert
    //     let nextState = store.getState()
    //     let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //     //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //     expect(prevImageDtl1State.imageXCoor).toEqual(1)
    //     expect(prevImageDtl1State.imageYCoor).toEqual(2)
    //     expect(nextCanvasGridState.rootState.canvasProfiles.byId['imageDtl_1'].imageXCoor).toEqual(nextXCoor)
    //     expect(nextCanvasGridState.rootState.canvasProfiles.byId['imageDtl_1'].imageYCoor).toEqual(nextYCoor)
    //     //assert immutation takes place in reducer
    //     expect(prevCanvasGridRootState).not.toBe(nextCanvasGridState.rootState)
    //     expect(prevImageDtl1State).not.toBe(nextCanvasGridState.rootState.canvasProfiles.byId['imageDtl_1'])
    // })

    // describe('Test Action: <CanvasGridActionTypes.SET_IMAGE_HEADER_CONFIG>', ()=>{

    //     test('header configuration store in Canvas.', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare imageDtl stub in canvasGrid.rootState.canvasProfiles.byId
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         let prevCanvasGridRootState = prevCanvasGridState.rootState
    //         //record the reference of imageDtl immutation assertion
    //         let prevImageDtl1State = prevCanvasGridRootState.canvasProfiles.byId['imageDtl_1']
    
    //         //Act
    //         let fontType = 'Times'
    //         let fontSize = 18
    //         let fontColor = 'black'
    //         let bgColor = '#FFFFFF'
    //         let visibility = false
    //         let rtcExamType = 'ESTEST'
    //         let rtcImageCreateDateTime = true
    //         store.dispatch(setImageHeaderConfig(fontType, fontSize, fontColor, bgColor, visibility, rtcExamType, rtcImageCreateDateTime))
    
    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //         expect(prevCanvasGridRootState.rtcExamType).toEqual('ES')
    //         expect(prevCanvasGridRootState.rtcImageCreateDateTime).toEqual(false)
    
    //         expect(nextCanvasGridState.rootState.rtcExamType).toEqual(rtcExamType)
    //         expect(nextCanvasGridState.rootState.rtcImageCreateDateTime).toEqual(rtcImageCreateDateTime)
    
    //         //assert immutation takes place in reducer
    //         expect(prevCanvasGridRootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    //     test('header configuration store in CanvasProfileInitState.', ()=>{
    //         //Arrange
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevImageDtlInitState = {}
    //         prevImageDtlInitState = clone(CanvasProfileInitState)
    //         let prevFontType = prevImageDtlInitState.fontType
    //         let prevFontSize = prevImageDtlInitState.fontSize
    //         let prevFontColor = prevImageDtlInitState.fontColor
    //         let prevBgColor = prevImageDtlInitState.bgColor
    //         let prevVisibility = prevImageDtlInitState.visibility

    //         //Act
    //         let fontType = 'Times'
    //         let fontSize = 18
    //         let fontColor = 'black'
    //         let bgColor = '#FFFFFF'
    //         let visibility = false
    //         let rtcExamType = 'ESTEST'
    //         let rtcImageCreateDateTime = true
    //         store.dispatch(setImageHeaderConfig(fontType, fontSize, fontColor, bgColor, visibility, rtcExamType, rtcImageCreateDateTime))
    
    //         //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //         expect(prevImageDtlInitState.fontType).toEqual(prevFontType)
    //         expect(prevImageDtlInitState.fontSize).toEqual(prevFontSize)
    //         expect(prevImageDtlInitState.fontColor).toEqual(prevFontColor)
    //         expect(prevImageDtlInitState.bgColor).toEqual(prevBgColor)
    //         expect(prevImageDtlInitState.visibility).toEqual(prevVisibility)
            
    //         expect(CanvasProfileInitState.fontType).toEqual(fontType)
    //         expect(CanvasProfileInitState.fontSize).toEqual(fontSize)
    //         expect(CanvasProfileInitState.fontColor).toEqual(fontColor)
    //         expect(CanvasProfileInitState.bgColor).toEqual(bgColor)
    //         expect(CanvasProfileInitState.visibility).toEqual(visibility)

    //         //assert immutation takes place in reducer
    //         expect(prevImageDtlInitState).not.toBe(CanvasProfileInitState)
    //         // expect(prevImageDtl1State).not.toBe(nextCanvasGridState.rootState.canvasProfiles.byId['imageDtl_1'])

    //     })
    // })

    // describe('Test Action: <CanvasGridActionTypes.ON_CANVAS_CLICK>', ()=>{
        
    //     beforeAll(() => {
    //         //Arrange
    //         //prepare 2x2 view mode with 4 images
    //         canvasGridTwoImagesSelectedStub.rootState.showImageIds 
    //             = ["imageDtl_1", "imageDtl_2", "imageDtl_3", "imageDtl_4"]
    //         canvasGridTwoImagesSelectedStub.rootState.viewMode.noOfImage = 4
    //         canvasGridTwoImagesSelectedStub.rootState.viewMode.text = "2x2"
    //     })

    //     afterAll(() => {
    //         //recover to 1x2 view mode with 2 images
    //         canvasGridTwoImagesSelectedStub.rootState.showImageIds 
    //             = ["imageDtl_1", "imageDtl_2"]
    //         canvasGridTwoImagesSelectedStub.rootState.selectedImageIds = ["imageDtl_1"]
    //         canvasGridTwoImagesSelectedStub.rootState.viewMode.noOfImage = 2
    //         canvasGridTwoImagesSelectedStub.rootState.viewMode.text = "1x2"
    //     })

    //     test('onClick: canvas with image', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare selectedImageIds in canvasGrid.rootState
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         let prevCanvasGridRootState = prevCanvasGridState.rootState
    //         //record the reference of selectedImageIds immutation assertion
    //         let prevSelectedImageIds = prevCanvasGridRootState.selectedImageIds

    //         //Act
    //         let imageId = 'imageDtl_3'
    //         let button = ''
    //         store.dispatch(onCanvasClick(imageId, button))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         let nextSelectedImageIds = nextCanvasGridState.rootState.selectedImageIds

    //         expect(prevSelectedImageIds).toEqual(["imageDtl_1"])
    //         expect(nextSelectedImageIds).toEqual(["imageDtl_3"])

    //         expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    //     test('onClick: canvas without image', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare selectedImageIds in canvasGrid.rootState
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         let prevCanvasGridRootState = prevCanvasGridState.rootState
    //         //record the reference of selectedImageIds immutation assertion
    //         let prevSelectedImageIds = prevCanvasGridRootState.selectedImageIds

    //         //Act
    //         let imageId = ''
    //         let button = ''
    //         store.dispatch(onCanvasClick(imageId, button))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         let nextSelectedImageIds = nextCanvasGridState.rootState.selectedImageIds

    //         expect(prevSelectedImageIds).toEqual(["imageDtl_1"])
    //         expect(nextSelectedImageIds).toEqual(["imageDtl_1"])

    //         expect(prevCanvasGridState.rootState).toBe(nextCanvasGridState.rootState)
    //     })

    //     test('Ctrl + onClick: Select', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare selectedImageIds in canvasGrid.rootState
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         //record the reference of selectedImageIds immutation assertion
    //         let prevSelectedImageIds = prevCanvasGridState.rootState.selectedImageIds

    //         //Act
    //         let imageId = 'imageDtl_3'
    //         let button = ImageSelection.CTRL
    //         store.dispatch(onCanvasClick(imageId, button))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         let nextSelectedImageIds = nextCanvasGridState.rootState.selectedImageIds

    //         expect(prevSelectedImageIds).toEqual(["imageDtl_1"])
    //         expect(nextSelectedImageIds).toEqual(["imageDtl_1","imageDtl_3"])

    //         expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    //     test('Ctrl + onClick: DeSelect', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare selectedImageIds in canvasGrid.rootState
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         canvasGridTwoImagesSelectedStub.rootState.selectedImageIds = ["imageDtl_1","imageDtl_3"]
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         //record the reference of selectedImageIds immutation assertion
    //         let prevSelectedImageIds = prevCanvasGridState.rootState.selectedImageIds

    //         //Act
    //         let imageId = 'imageDtl_1'
    //         let button = ImageSelection.CTRL
    //         store.dispatch(onCanvasClick(imageId, button))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         let nextSelectedImageIds = nextCanvasGridState.rootState.selectedImageIds

    //         expect(prevSelectedImageIds).toEqual(["imageDtl_1","imageDtl_3"])
    //         expect(nextSelectedImageIds).toEqual(["imageDtl_3"])

    //         expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    //     test('Shift + onClick: new selected image id > last selected image id', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare selectedImageIds in canvasGrid.rootState
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         canvasGridTwoImagesSelectedStub.rootState.selectedImageIds = ["imageDtl_1"]
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         //record the reference of selectedImageIds immutation assertion
    //         let prevSelectedImageIds = prevCanvasGridState.rootState.selectedImageIds
            
    //         //Act
    //         let imageId = 'imageDtl_3'
    //         let button = ImageSelection.SHIFT
    //         store.dispatch(onCanvasClick(imageId, button))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         let nextSelectedImageIds = nextCanvasGridState.rootState.selectedImageIds

    //         expect(prevSelectedImageIds).toEqual(["imageDtl_1"])
    //         expect(nextSelectedImageIds).toEqual(["imageDtl_1","imageDtl_2","imageDtl_3"])

    //         expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    //     test('Shift + onClick: new selected image id < last selected image id', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
                        
    //         // prepare selectedImageIds in canvasGrid.rootState
    //         let prevImageViewingPanelState = JsonParser.getValue(previousState,"imageViewingPanel")
    //         canvasGridTwoImagesSelectedStub.rootState.selectedImageIds = ["imageDtl_3"]
    //         prevImageViewingPanelState.canvasGrid = clone(canvasGridTwoImagesSelectedStub)
    //         //record the reference of rootState and rootState.viewMode for immutation assertion
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")
    //         //record the reference of selectedImageIds immutation assertion
    //         let prevSelectedImageIds = prevCanvasGridState.rootState.selectedImageIds

    //         //Act
    //         let imageId = 'imageDtl_1'
    //         let button = ImageSelection.SHIFT
    //         store.dispatch(onCanvasClick(imageId, button))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         let nextSelectedImageIds = nextCanvasGridState.rootState.selectedImageIds

    //         expect(prevSelectedImageIds).toEqual(["imageDtl_3"])
    //         expect(nextSelectedImageIds).toEqual(["imageDtl_1","imageDtl_2","imageDtl_3"])

    //         expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
    //     })

    // })

    // describe('Test Action: <CanvasGridActionTypes.ON_IMAGE_ZOOM>', ()=>{
    //     test('Test image zoom in', ()=>{
    //         //Arrange
    //         let previousState = store.getState()
    //         let prevCanvasGridState = JsonParser.getValue(previousState,"canvasGrid")

    //         //arrange mock
    //         dataRespository.findIasWsAuthInfo = jest.fn()
    //         const imageScaleX = 1
    //         const imageScaleY = 2
    //         const imagePannedXCoor = 3
    //         const imagePannedYCoor = 4
    //         ViewingPanelController.mockImplementation(()=>{
    //             return {
    //                 zoomImage: function(seriesId: string, imageId: string, zoomMode: string, zoomX: number, zoomY: number, pannedCoordinateRule: Function){
    //                     store.dispatch(setImageScale(seriesId, imageId, imageScaleX, imageScaleY, imagePannedXCoor, imagePannedYCoor))
    //                 }
    //             }
    //         })
            
    //         //Act
    //         const seriesId = "seriesId_1"
    //         const imageId = "imageId_1"
    //         const imageTool = ImageTool.ZOOM_IN
    //         const zoomX = 5 //absolute coordinate
    //         const zoomY = 6 //absolute coordinate
    //         const pannedCoordinateRule = jest.fn()
    //         store.dispatch(setImageZoom(seriesId, imageId, imageTool, zoomX, zoomY, pannedCoordinateRule))

    //         //Assert
    //         let nextState = store.getState()
    //         let nextCanvasGridState = JsonParser.getValue(nextState,"canvasGrid")
    //         //assert value updated, assert the prev and next state value could assure that value update by Object.assign
    //         expect((nextCanvasGridState.rootState.canvasProfiles.byId[imageId]).imageXCoor).toEqual(imagePannedXCoor)
    //         expect((nextCanvasGridState.rootState.canvasProfiles.byId[imageId]).imageYCoor).toEqual(imagePannedYCoor)
    //         expect((nextCanvasGridState.rootState.canvasProfiles.byId[imageId]).imageScaleX).toEqual(imageScaleX)
    //         expect((nextCanvasGridState.rootState.canvasProfiles.byId[imageId]).imageScaleY).toEqual(imageScaleY)
    //         //assert immutation takes place in reducer
    //         expect(nextCanvasGridState.rootState).not.toBe(prevCanvasGridState.rootState)
    //     })

    //     test('Test image zoom out', ()=>{
    //         //As all branches of data flow are covered by 'Test image zoom in', this test case could be skipped
    //     })
    // })
})

const imageViewingPanelTreeStub = {
    canvasGrid: {
        rootState: {
            width: 200,               
            height: 400,
            viewMode: {
                text: '2x2',
                noOfImage: 4
            },
            selectedSeriesId: '7',
            showImageIds: [
                "imageDtl_3",
                "imageDtl_4"
            ],
            selectedImageIds: [
                "imageDtl_3"
            ],
            slideShowCurrentImgId: 'imageDtl_3',
            slideShowStopImgId: 'imageDtl_4',
            caseNo: '',
            studyDateTime: '20180730142001.123',
            canvasProfiles: {
                byId: {
                    'imageDtl_3': {
                        header: '',
                        imageBase64: '',
                        brightness: 60,
                        contrast: 60,
                        hue: 60,
                        saturation: 60,
                        zoomValue: 200,
                        imageXCoor: 'Center',
                        imageYCoor: 'Center',
                        imageWidth: null,
                        imageHeight: null
                    },
                    'imageDtl_4': {
                        header: '',
                        imageBase64: '',
                        brightness: 40,
                        contrast: 40,
                        hue: 40,
                        saturation: 40,
                        zoomValue: 400,
                        imageXCoor: 'Center',
                        imageYCoor: 'Center',
                        imageWidth: null,
                        imageHeight: null
                    }
                },
                allIds: ['imageDtl_3', 'imageDtl_4']
            },
            imageCreateTimeStamp: [20180730142001.123, 20180730142002.123], 
            rtcExamType: 'ES',
            rtcImageCreateDateTime: false,
            headerTimeText: 'Study Date/Time',
            displayDateTime: [20180730143001.123,20180730143002.123]
        }
    }
}


const studyTreeStub = {
    messageDtl: {
        transactionCode: 'CID2DATA',
        serverHosp: 'VH',
        transactionDtm: '20180605164634.244',
        transactionID: '4B1AC56C-7CE8-4623-9EB7-A6AC00ADD702',
        sendingApplication: 'CID'
    },
    visitDtl: {
        visitHosp: 'VH',
        visitSpec: '',
        visitWardClinic: '',
        payCode: '',
        caseNum: 'HN05000207X',
        adtDtm: ''
    },
    patientDtl: {
        hkid: 'M7673537',
        patKey: '90500231',
        patName: 'TSANG, AH OI',
        patDOB: '19460107000000.000',
        patSex: 'M',
        deathIndicator: ''
    },
    studyDtl: {
        accessionNo: 'R9MLJ5KPEVED1SMQ',
        studyID: '5953c844-8fe6-4587-9ee3-b898586df6f1',
        studyType: 'ERS',
        studyDtm: '20100131123456',
        remark: 'Remark1',
        seriesDtls: {
            seriesDtl:[
                {
                    seriesNo:'f74c17d2-2069-42be-9e20-ab9705ab9189', 
                    examType:'HADRW', 
                    examDtm:'20150318161644', 
                    entityID:'0', 
                    imageDtls:{
                        imageDtl:[
                            {
                                imageFormat:'IM', 
                                imagePath:'/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG', 
                                imageFile:'1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG', 
                                imageID:'ff72b811-6ac6-41fd-a414-8dd87ae8089e', 
                                imageVersion:'1', 
                                imageSequence:'1', 
                                imageKeyword:'Name:Bronchoscopy1|', 
                                imageHandling:'N', 
                                imageType:'JPEG', 
                                imageStatus:'1', 
                                annotationDtls:{
                                    annotationDtl:[
                                        {
                                            annotationID:'annotationID1', 
                                            annotationSeq:'annotationSeq1', 
                                            annotationType:'annotationType1', 
                                            annotationText:'annotationText1', 
                                            annotationCoordinate:'annotationCoordinate1', 
                                            annotationStatus:'annotationStatus1', 
                                            annotationEditable:'annotationEditable1', 
                                            annotationUpdDtm:'annotationUpdDtm1'
                                        },
                                        {
                                            annotationID:'annotationID2', 
                                            annotationSeq:'annotationSeq2', 
                                            annotationType:'annotationType2', 
                                            annotationText:'annotationText2', 
                                            annotationCoordinate:'annotationCoordinate2', 
                                            annotationStatus:'annotationStatus2', 
                                            annotationEditable:'annotationEditable2', 
                                            annotationUpdDtm:'annotationUpdDtm2' 
                                        }
                                    ]
                                }
                            },

                            {
                                imageFormat:'IM2', 
                                imagePath:'/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/12.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG', 
                                imageFile:'1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG', 
                                imageID:'ff72b811-6ac6-41fd-a414-8dd87ae8089e', 
                                imageVersion:'2', 
                                imageSequence:'2', 
                                imageKeyword:'Name:Bronchoscopy2|', 
                                imageHandling:'Y', 
                                imageType:'JPEG2', 
                                imageStatus:'2', 
                                annotationDtls:{
                                    annotationDtl:[
                                        {
                                            annotationID:'annotationID1.2', 
                                            annotationSeq:'annotationSeq1.2', 
                                            annotationType:'annotationType1.2', 
                                            annotationText:'annotationText1.2', 
                                            annotationCoordinate:'annotationCoordinate1.2', 
                                            annotationStatus:'annotationStatus1.2', 
                                            annotationEditable:'annotationEditable1.2', 
                                            annotationUpdDtm:'annotationUpdDtm1.2'
                                        },
                                        {
                                            annotationID:'annotationID2.2', 
                                            annotationSeq:'annotationSeq2.2', 
                                            annotationType:'annotationType2.2', 
                                            annotationText:'annotationText2.2', 
                                            annotationCoordinate:'annotationCoordinate2.2', 
                                            annotationStatus:'annotationStatus2.2', 
                                            annotationEditable:'annotationEditable2.2', 
                                            annotationUpdDtm:'annotationUpdDtm2.2' 
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
    }
}

const canvasGridTwoImagesSelectedStub = {
    "rootState": {
      "canvasProfiles": {
        "allIds": [
            "imageDtl_1",
            "imageDtl_2",
            "imageDtl_3",
            "imageDtl_4",
        ],
        "byId": {
          "imageDtl_1": {
            "brightness": 50,
            "contrast": 50,
            "header": "",
            "hue": 50,
            "imageBase64": "",
            "imageXCoor": 1,
            "imageYCoor": 2,
            "saturation": 50,
            "zoomValue": 100,
            "fontType": "Arial",
            "fontSize": 11,
            "fontColor": "white",
            "bgColor": "#000000",
            "visibility": true
          },
          "imageDtl_2": {
            "brightness": 50,
            "contrast": 50,
            "header": "",
            "hue": 50,
            "imageBase64": "",
            "imageXCoor": 3,
            "imageYCoor": 4,
            "saturation": 50,
            "zoomValue": 100,
            "fontType": "Arial",
            "fontSize": 11,
            "fontColor": "white",
            "bgColor": "#000000",
            "visibility": true
          },
          "imageDtl_3": {
            "brightness": 50,
            "contrast": 50,
            "header": "",
            "hue": 50,
            "imageBase64": "",
            "imageXCoor": 3,
            "imageYCoor": 4,
            "saturation": 50,
            "zoomValue": 100,
            "fontType": "Arial",
            "fontSize": 11,
            "fontColor": "white",
            "bgColor": "#000000",
            "visibility": true
          },
          "imageDtl_4": {
            "brightness": 50,
            "contrast": 50,
            "header": "",
            "hue": 50,
            "imageBase64": "",
            "imageXCoor": 3,
            "imageYCoor": 4,
            "saturation": 50,
            "zoomValue": 100,
            "fontType": "Arial",
            "fontSize": 11,
            "fontColor": "white",
            "bgColor": "#000000",
            "visibility": true
          },
        },
      },
      "caseNo": "",
      "height": 728,
      "imageDlStatus": "",
      "selectedImageIds": [
        "imageDtl_1",
      ],
      "selectedSeriesId": "5",
      "showImageIds": [
        "imageDtl_1",
        "imageDtl_2",
      ],
      "studyDateTime": "",
      "viewMode": {
        "noOfImage": 2,
        "text": "1x2",
      },
      "width": 804,
      "imageCreateTimeStamp": [20180730141001.123,20180730141002.123,20180730141003.123,20180730141004.123], 
      "rtcExamType": 'ES',
      "rtcImageCreateDateTime": false,
      "headerTimeText": 'Study Date/Time',
      "displayDateTime": [20180730142001.123,20180730142002.123,20180730142003.123,20180730142004.123]
    },
}