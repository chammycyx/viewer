import { getStore, stop } from '@test/dataFlow/DataFlowHelper'
import { clone } from '@utils/CloneUtil'
import { FlipDirections } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { flipImage } from '@actions/components/toolBarPanel/transformationTools/Action'

import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'

let store = null

const canvasProfilesTwoImagesStub = {
    byId: {
        'imageId_1': {
            header: '',
            imageBase64: '',
            fabricProfile: {},
            brightness: 50,
            contrast: 50,
            hue: 50,
            saturation: 50,
            zoomValue: 100,
            imageXCoor: null,
            imageYCoor: null,
            imageWidth: null,
            imageHeight: null,
            rotateAngle: 0,
            flipHorizontal: false,
            flipVertical: false
        },
        'imageId_2': {
            header: '',
            imageBase64: '',
            fabricProfile: {},
            brightness: 50,
            contrast: 50,
            hue: 50,
            saturation: 50,
            zoomValue: 100,
            imageXCoor: null,
            imageYCoor: null,
            imageWidth: null,
            imageHeight: null,
            rotateAngle: 0,
            flipHorizontal: false,
            flipVertical: false
        }
    },
    allIds: ['imageId_1', 'imageId_2']
}

beforeEach(() => {
    store = getStore()
    dataRepository.setStore(store)
})

describe('Test Suite: <flipImage>', async ()=>{
    describe('Test Action: <flipImage>', async ()=>{
        test('Test flip selected image vertically', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_2']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageDtl2).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip selected image horizontally', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageDtl2).toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip selected image vertically when image is rotated by 90 degrees', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_2']
            prevCanvasGridRootState.canvasProfiles.byId['imageId_2'].rotateAngle = 90

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            let nextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).toBe(nextImageDtl1)
            expect(prevImageDtl2).not.toBe(nextImageDtl2)

            //Arrange 2
            nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].rotateAngle = -90

            //Act 2
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)

            //Assert
            let nextState2 = store.getState()
            let nextCanvasGridRootState2 = nextState2.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(nextImageDtl1).toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'])
            expect(nextImageDtl2).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip selected image horizontally when image is rotated by 90 degrees', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1']
            prevCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle = 90

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            let nextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextImageDtl1)
            expect(prevImageDtl2).toBe(nextImageDtl2)

            //Arrange 2
            nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle = -90

            //Act 2
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)

            //Assert
            let nextState2 = store.getState()
            let nextCanvasGridRootState2 = nextState2.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(nextImageDtl1).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'])
            expect(nextImageDtl2).toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip selected image vertically/horizontally when image is rotated by 180 degrees', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1']
            prevCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle = 180

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            let nextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextImageDtl1)
            expect(prevImageDtl2).toBe(nextImageDtl2)

            //Act 2
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)

            //Assert
            let nextState2 = store.getState()
            let nextCanvasGridRootState2 = nextState2.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(true)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(nextImageDtl1).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'])
            expect(nextImageDtl2).toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip multiple selected images vertically', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1', 'imageId_2']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            let nextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextImageDtl1)
            expect(prevImageDtl2).not.toBe(nextImageDtl2)

            //Act 2
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)

            //Assert
            let nextState2 = store.getState()
            let nextCanvasGridRootState2 = nextState2.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(nextImageDtl1).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'])
            expect(nextImageDtl2).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip multiple selected images horizontally', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1', 'imageId_2']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            let nextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextImageDtl1)
            expect(prevImageDtl2).not.toBe(nextImageDtl2)

            //Act 2
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)

            //Assert
            let nextState2 = store.getState()
            let nextCanvasGridRootState2 = nextState2.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(nextImageDtl1).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'])
            expect(nextImageDtl2).not.toBe(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'])
        }),
        test('Test flip multiple selected images when one image is rotated', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1', 'imageId_2']
            prevCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle = 90

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            let nextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextImageDtl1)
            expect(prevImageDtl2).not.toBe(nextImageDtl2)

            //Arrange 2
            nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle = 180

            //Act 2
            store.dispatch(flipImage(FlipDirections.HORIZONTAL))
            await stop(2000)

            //Assert
            let nextState2 = store.getState()
            let nextCanvasGridRootState2 = nextState2.components.imageViewingPanel.canvasGrid.rootState

            let nextNextImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let nextNextImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']

            //assert value updated
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(false)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(true)
            expect(nextCanvasGridRootState2.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(nextImageDtl1).not.toBe(nextNextImageDtl1)
            expect(nextImageDtl2).not.toBe(nextNextImageDtl2)

            
            //Arrange 3
            nextCanvasGridRootState2.canvasProfiles.byId['imageId_1'].rotateAngle = 270

            //Act 3
            store.dispatch(flipImage(FlipDirections.VERTICAL))
            await stop(2000)

            //Assert
            let nextState3 = store.getState()
            let nextCanvasGridRootState3 = nextState3.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState3.canvasProfiles.byId['imageId_1'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState3.canvasProfiles.byId['imageId_1'].flipHorizontal).toEqual(true)
            expect(nextCanvasGridRootState3.canvasProfiles.byId['imageId_2'].flipVertical).toEqual(false)
            expect(nextCanvasGridRootState3.canvasProfiles.byId['imageId_2'].flipHorizontal).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(nextNextImageDtl1).not.toBe(nextCanvasGridRootState3.canvasProfiles.byId['imageId_1'])
            expect(nextNextImageDtl2).not.toBe(nextCanvasGridRootState3.canvasProfiles.byId['imageId_2'])
        }, 8000)
    })
})
