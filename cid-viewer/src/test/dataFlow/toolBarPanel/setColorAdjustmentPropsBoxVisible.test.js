import { getStore } from '@test/dataFlow/DataFlowHelper'
import { clone } from '@utils/CloneUtil'
import { setVisible } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'

let store = null

// const colorAdjustmentPropsBoxRootState = {
//     visible: false
// }

beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <setColorAdjustmentPropsBoxVisible>', ()=>{
    describe('Test Action: <setColorAdjustmentPropsBoxVisible>', ()=>{
        test('Test setting color adjustment props box visible true', ()=>{
            //Arrange
            let previousState = store.getState()
            let prevColorAdjustmentPropsBoxRootState = previousState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState
        
            //Act
            store.dispatch(setVisible(true))
        
            //Assert
            let nextState = store.getState()
            let nextColorAdjustmentPropsBoxRootState = nextState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState

            //assert value updated
            expect(nextColorAdjustmentPropsBoxRootState.visible).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(prevColorAdjustmentPropsBoxRootState).not.toBe(nextColorAdjustmentPropsBoxRootState)
        }),
        test('Test setting color adjustment props box visible false', ()=>{
            //Arrange
            let previousState = store.getState()
            let prevColorAdjustmentPropsBoxRootState = previousState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState
        
            //Act
            store.dispatch(setVisible(false))
        
            //Assert
            let nextState = store.getState()
            let nextColorAdjustmentPropsBoxRootState = nextState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState

            //assert value updated
            expect(nextColorAdjustmentPropsBoxRootState.visible).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevColorAdjustmentPropsBoxRootState).not.toBe(nextColorAdjustmentPropsBoxRootState)
        })
    })
})
