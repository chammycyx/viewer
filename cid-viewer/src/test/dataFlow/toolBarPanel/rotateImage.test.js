import { getStore, stop } from '@test/dataFlow/DataFlowHelper'
import { clone } from '@utils/CloneUtil'
import { RotateDirections } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { rotateImage } from '@actions/components/toolBarPanel/transformationTools/Action'

import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'

let store = null

const canvasProfilesTwoImagesStub = {
    byId: {
        'imageId_1': {
            header: '',
            imageBase64: '',
            fabricProfile: {},
            brightness: 50,
            contrast: 50,
            hue: 50,
            saturation: 50,
            zoomValue: 100,
            imageXCoor: null,
            imageYCoor: null,
            imageWidth: null,
            imageHeight: null,
            rotateAngle: 0,
            flipHorizontal: false,
            flipVertical: false
        },
        'imageId_2': {
            header: '',
            imageBase64: '',
            fabricProfile: {},
            brightness: 50,
            contrast: 50,
            hue: 50,
            saturation: 50,
            zoomValue: 100,
            imageXCoor: null,
            imageYCoor: null,
            imageWidth: null,
            imageHeight: null,
            rotateAngle: 0,
            flipHorizontal: false,
            flipVertical: false
        }
    },
    allIds: ['imageId_1', 'imageId_2']
}

beforeEach(() => {
    store = getStore()
    dataRepository.setStore(store)
})

describe('Test Suite: <rotateImage>', async ()=>{
    describe('Test Action: <rotateImage>', async ()=>{
        test('Test rotating selected image clockwise', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_2']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(rotateImage(RotateDirections.RIGHT))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle).toEqual(0)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].rotateAngle).toEqual(90)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageDtl2).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        }),
        test('Test rotating selected image anti-clockwise', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(rotateImage(RotateDirections.LEFT))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle).toEqual(-90)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].rotateAngle).toEqual(0)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageDtl2).toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        }),
        test('Test rotating multiple selected image clockwise', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1', 'imageId_2']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(rotateImage(RotateDirections.RIGHT))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle).toEqual(90)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].rotateAngle).toEqual(90)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageDtl2).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        }),
        test('Test rotating multiple selected image anti-clockwise', async ()=>{
            //Arrange
            let imageId = 'ff72b811-6ac6-41fd-a414-8dd87ae8089e'
            dataRepository.findImageId = jest.fn()
            dataRepository.findImageId.mockReturnValue(imageId)

            let seriesDtl = {
                seriesNo: 'aa72b811-6ac6-41fd-a414-8dd87ae8089z'
            }
            dataRepository.findSeriesDtl = jest.fn()
            dataRepository.findSeriesDtl.mockReturnValue(seriesDtl)

            logger.userActions = jest.fn()

            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_1', 'imageId_2']

            let prevImageDtl1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageDtl2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(rotateImage(RotateDirections.LEFT))
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].rotateAngle).toEqual(-90)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].rotateAngle).toEqual(-90)
            
            //assert immutation takes place in reducer
            expect(prevImageDtl1).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageDtl2).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        })
    })
})
