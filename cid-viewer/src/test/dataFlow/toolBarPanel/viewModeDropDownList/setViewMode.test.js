import {getStore} from '@test/dataFlow/DataFlowHelper'
import {clone} from '@utils/CloneUtil'
import { VIEW_MODES, ViewModeDropDownListActionTypes, setViewMode } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import JsonParser from '@utils/JsonParser'

let store = null

beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <The data flow of viewModeDropDownList>', ()=>{
    describe('Test Action: <ViewModeDropDownListActionTypes.SET_VIEW_MODE>', ()=>{
        test('Test View Mode Change to 2x1', ()=>{
            //Arrange
            let previousState = store.getState()
            let prevRootState = previousState.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState
            let prevViewModeState = previousState.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode
        
            //Act
            store.dispatch(setViewMode(VIEW_MODES.ONE_X_TWO))
        
            //Assert
            let nextState = store.getState()
            let nextViewModeDropDownListState = JsonParser.getValue(nextState,"viewModeDropDownList")

            //assert value updated
            expect(prevViewModeState).not.toEqual(nextViewModeDropDownListState.rootState.viewMode)
            expect(nextViewModeDropDownListState.rootState.viewMode).toEqual(VIEW_MODES.ONE_X_TWO)
            //assert immutation takes place in reducer
            expect(prevRootState).not.toBe(nextViewModeDropDownListState.rootState)
            expect(prevViewModeState).not.toBe(nextViewModeDropDownListState.rootState.viewMode)
        })
    })
})
