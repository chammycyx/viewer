import {getStore, stop} from '@test/dataFlow/DataFlowHelper'

// import utils
import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'
import {clone} from '@utils/CloneUtil'
import { OCTET_STREAM_HEADERS } from '@utils/Base64Util'

// import actions
import { setVisible as setExportImageDialogVisible , setDownload , requestExportImages } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'

// import controllers
import AuditLogController from '@domains/controllers/rootApp/AuditLogController'

// import services
import StudyService from '@domains/services/studyService/StudyService'
import viewerLogController from '@src/main/domains/controllers/ViewerLogController';


jest.mock('@domains/controllers/rootApp/AuditLogController')
jest.mock('@domains/services/studyService/StudyService')


let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <ExportImage>', async () => {

    describe('Test Action: <ExportImageDialogActionTypes#SET_VISIBLE>', async () => {
        test('+++ Verify the update of states when setting export image dialog visible', () => {
            
            // Previous states
            let previousState = store.getState()

            // Previous states of exportImageDialog
            let previousExportImageDialogState = previousState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let previousStateSnapshot = clone(previousState)
                previousStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                previousStateSnapshot.components.toolBarPanel.rootState.btnState = null

            let previousExportImageDialogStateSnapshot = clone(previousExportImageDialogState)
                previousExportImageDialogStateSnapshot.rootState.isVisible = null

            // Assert 0
            expect(previousExportImageDialogState.rootState.isVisible).toEqual(false)

            // Arrange 1
            let isVisible = true
            
            // Act 1
            dataRepository.setStore(store)
            store.dispatch(setExportImageDialogVisible(isVisible))

            // Next states
            let nextState = store.getState()

            // Next states of exportImageDialog
            let nextExportImageDialogState = nextState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let nextStateSnapshot = clone(nextState)
                nextStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                nextStateSnapshot.components.toolBarPanel.rootState.btnState = null

            let nextExportImageDialogStateSnapshot = clone(nextExportImageDialogState)
                nextExportImageDialogStateSnapshot.rootState.isVisible = null

            // Assert 1
            expect(nextExportImageDialogState.rootState.isVisible).toEqual(true)
            expect(JSON.stringify(nextExportImageDialogStateSnapshot)).toEqual(JSON.stringify(previousExportImageDialogStateSnapshot))
            expect(JSON.stringify(nextStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))

            //assert immutation takes place in reducer
            expect(previousExportImageDialogState.rootState).not.toBe(nextExportImageDialogState.rootState)

            // Arrange 2
            isVisible = false

            // Act 2
            store.dispatch(setExportImageDialogVisible(isVisible))
            
            // Current states
            let currentState = store.getState()

            // Current states of exportImageDialog
            let currentExportImageDialogState = currentState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let currentStateSnapshot = clone(currentState)
                currentStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                currentStateSnapshot.components.toolBarPanel.rootState.btnState = null

            let currentExportImageDialogStateSnapshot = clone(currentExportImageDialogState)
                currentExportImageDialogStateSnapshot.rootState.isVisible = null
                
            // Assert 2
            expect(currentExportImageDialogState.rootState.isVisible).toEqual(false)
            expect(JSON.stringify(currentExportImageDialogStateSnapshot)).toEqual(JSON.stringify(nextExportImageDialogStateSnapshot))
            expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(nextStateSnapshot))

            //assert immutation takes place in reducer
            expect(nextExportImageDialogState.rootState).not.toBe(currentExportImageDialogState.rootState)
        })
    })

    describe('Test Action: <ExportImageDialogActionTypes#SET_DOWNLOAD>', async () => {
        test('+++ Verify the update of download states', () => {
            
            // Previous states
            let previousState = store.getState()

            // Previous states of exportImageDialog
            let previousExportImageDialogState = previousState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let previousStateSnapshot = clone(previousState)
                previousStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                previousStateSnapshot.components.toolBarPanel.rootState.btnState = null

            let previousExportImageDialogStateSnapshot = clone(previousExportImageDialogState)
                previousExportImageDialogStateSnapshot.rootState.isRequesting = null
                previousExportImageDialogStateSnapshot.rootState.isDownload = null
                previousExportImageDialogStateSnapshot.rootState.exportImageBase64 = null

            // Assert 0
            expect(previousExportImageDialogState.rootState.isRequesting).toEqual(false)
            expect(previousExportImageDialogState.rootState.isDownload).toEqual(false)
            expect(previousExportImageDialogState.rootState.exportImageBase64).toEqual('')

            // Arrange 1
            let exportImageBase64 = 'base64forexportimage'
            
            // Act 1
            dataRepository.setStore(store)
            store.dispatch(setDownload(true, exportImageBase64))

            // Next states
            let nextState = store.getState()

            // Next states of exportImageDialog
            let nextExportImageDialogState = nextState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let nextStateSnapshot = clone(nextState)
                nextStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                nextStateSnapshot.components.toolBarPanel.rootState.btnState = null

            let nextExportImageDialogStateSnapshot = clone(nextExportImageDialogState)
                nextExportImageDialogStateSnapshot.rootState.isRequesting = null
                nextExportImageDialogStateSnapshot.rootState.isDownload = null
                nextExportImageDialogStateSnapshot.rootState.exportImageBase64 = null

            // Assert 1
            expect(nextExportImageDialogState.rootState.isRequesting).toEqual(false)
            expect(nextExportImageDialogState.rootState.isDownload).toEqual(true)
            expect(nextExportImageDialogState.rootState.exportImageBase64).toEqual(OCTET_STREAM_HEADERS+exportImageBase64)
            expect(JSON.stringify(nextExportImageDialogStateSnapshot)).toEqual(JSON.stringify(previousExportImageDialogStateSnapshot))
            expect(JSON.stringify(nextStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))

            //assert immutation takes place in reducer
            expect(previousExportImageDialogState.rootState).not.toBe(nextExportImageDialogState.rootState)

            // Arrange 2
            exportImageBase64 = ''

            // Act 2
            store.dispatch(setDownload(false, exportImageBase64))
            
            // Current states
            let currentState = store.getState()

            // Current states of exportImageDialog
            let currentExportImageDialogState = currentState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let currentStateSnapshot = clone(currentState)
                currentStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                currentStateSnapshot.components.toolBarPanel.rootState.btnState = null

            let currentExportImageDialogStateSnapshot = clone(currentExportImageDialogState)
                currentExportImageDialogStateSnapshot.rootState.isRequesting = null
                currentExportImageDialogStateSnapshot.rootState.isDownload = null
                currentExportImageDialogStateSnapshot.rootState.exportImageBase64 = null
                
            // Assert 2
            expect(currentExportImageDialogState.rootState.isRequesting).toEqual(false)
            expect(currentExportImageDialogState.rootState.isDownload).toEqual(false)
            expect(currentExportImageDialogState.rootState.exportImageBase64).toEqual(exportImageBase64)
            expect(JSON.stringify(currentExportImageDialogStateSnapshot)).toEqual(JSON.stringify(nextExportImageDialogStateSnapshot))
            expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(nextStateSnapshot))

            //assert immutation takes place in reducer
            expect(nextExportImageDialogState.rootState).not.toBe(currentExportImageDialogState.rootState)
        })
    })

    describe('Test Action: <ExportImageDialogActionTypes#REQUEST_EXPORT_IMAGES>', async () => {

        // Declare testing state
        const imageBase64s = ['1', '2', '3', '4']
        const sampleState = {
            data: {
                study: {
                    studyDtl: {
                        rootState: {
                            seriesDtls: {
                                byId: {
                                    seriesDtl_1: {
                                        imageDtls: {
                                            byId: {
                                                'imageDtl_1': {
                                                    header: '',
                                                    imageBase64: imageBase64s[0],
                                                    brightness: 0,
                                                    contrast: 0,
                                                    hue: 0,
                                                    saturation: 0,
                                                    invert: false,
                                                    imageXCoor: 10,
                                                    imageYCoor: 11,
                                                    imageWidth: 8.1,
                                                    imageHeight: 9.1,
                                                    imageScaleX: 12.1,
                                                    imageScaleY: 13.1,
                                                    imageOriginX: 'center',
                                                    imageOriginY: 'center',
                                                    rotateAngle: 0,
                                                    flipHorizontal: false,
                                                    flipVertical: false,
                                                    fontType: '',
                                                    fontSize: 0,
                                                    fontColor: '',
                                                    bgColor: '',
                                                    visibility: false
                                                },
                                                'imageDtl_2': {
                                                    header: '',
                                                    imageBase64:  imageBase64s[1],
                                                    brightness: 0,
                                                    contrast: 0,
                                                    hue: 0,
                                                    saturation: 0,
                                                    invert: false,
                                                    imageXCoor: 10,
                                                    imageYCoor: 11,
                                                    imageWidth: 8.1,
                                                    imageHeight: 9.1,
                                                    imageScaleX: 12.1,
                                                    imageScaleY: 13.1,
                                                    imageOriginX: 'center',
                                                    imageOriginY: 'center',
                                                    rotateAngle: 0,
                                                    flipHorizontal: false,
                                                    flipVertical: false,
                                                    fontType: '',
                                                    fontSize: 0,
                                                    fontColor: '',
                                                    bgColor: '',
                                                    visibility: false
                                                },
                                                'imageDtl_3': {
                                                    header: '',
                                                    imageBase64:  imageBase64s[2],
                                                    brightness: 0,
                                                    contrast: 0,
                                                    hue: 0,
                                                    saturation: 0,
                                                    invert: false,
                                                    imageXCoor: 10,
                                                    imageYCoor: 11,
                                                    imageWidth: 8.1,
                                                    imageHeight: 9.1,
                                                    imageScaleX: 12.1,
                                                    imageScaleY: 13.1,
                                                    imageOriginX: 'center',
                                                    imageOriginY: 'center',
                                                    rotateAngle: 0,
                                                    flipHorizontal: false,
                                                    flipVertical: false,
                                                    fontType: '',
                                                    fontSize: 0,
                                                    fontColor: '',
                                                    bgColor: '',
                                                    visibility: false
                                                },
                                                'imageDtl_4': {
                                                    header: '',
                                                    imageBase64:  imageBase64s[3],
                                                    brightness: 0,
                                                    contrast: 0,
                                                    hue: 0,
                                                    saturation: 0,
                                                    invert: false,
                                                    imageXCoor: 10,
                                                    imageYCoor: 11,
                                                    imageWidth: 8.1,
                                                    imageHeight: 9.1,
                                                    imageScaleX: 12.1,
                                                    imageScaleY: 13.1,
                                                    imageOriginX: 'center',
                                                    imageOriginY: 'center',
                                                    rotateAngle: 0,
                                                    flipHorizontal: false,
                                                    flipVertical: false,
                                                    fontType: '',
                                                    fontSize: 0,
                                                    fontColor: '',
                                                    bgColor: '',
                                                    visibility: false
                                                }
                                            },
                                            allIds: ['imageDtl_1','imageDtl_2','imageDtl_3','imageDtl_4'],
                                            allHighestVerIds: ['imageDtl_1','imageDtl_2','imageDtl_3','imageDtl_4'],
                                            idsMapping: {
                                                imageDtl_1: 'imageId_1',
                                                imageDtl_2: 'imageId_2',
                                                imageDtl_3: 'imageId_3',
                                                imageDtl_4: 'imageId_4'
                                            }
                                        }
                                    }
                                },
                                allIds: ['seriesDtl_1']
                            }
                        }
                    }
                }
            }
        }

        test('+++ Verify the update of states when requesting export image', () => {
            
            // Previous states
            let previousState = store.getState()

            // Previous states of exportImageDialog
            let previousExportImageDialogState = previousState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let previousStateSnapshot = clone(previousState)
                previousStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                previousStateSnapshot.components.appMask = null
                previousStateSnapshot.components.toolBarPanel.rootState.btnState = null

            // Assert 0
            expect(previousExportImageDialogState.rootState.isVisible).toEqual(false)
            expect(previousExportImageDialogState.rootState.isRequesting).toEqual(false)

            // Arrange 1
            const isVisible = true
            
            // Act 1
            dataRepository.setStore(store)
            store.dispatch(setExportImageDialogVisible(isVisible))

            // Next states
            let nextState = store.getState()

            // Next states of exportImageDialog
            let nextExportImageDialogState = nextState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let nextStateSnapshot = clone(nextState)
                nextStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                nextStateSnapshot.components.appMask = null
                nextStateSnapshot.components.toolBarPanel.rootState.btnState = null

            // Assert 1
            expect(nextExportImageDialogState.rootState.isVisible).toEqual(true)
            expect(nextExportImageDialogState.rootState.isRequesting).toEqual(false)
            expect(JSON.stringify(nextStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))

            //assert immutation takes place in reducer
            expect(previousExportImageDialogState.rootState).not.toBe(nextExportImageDialogState.rootState)

            // Arrange 2
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            // Act 2
            store.dispatch(requestExportImages(selectedSeriesId, selectedImageIds, password))
            
            // Current states
            let currentState = store.getState()

            // Current states of exportImageDialog
            let currentExportImageDialogState = currentState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let currentStateSnapshot = clone(currentState)
                currentStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                currentStateSnapshot.components.appMask = null
                currentStateSnapshot.components.toolBarPanel.rootState.btnState = null

            // Assert 2
            expect(currentExportImageDialogState.rootState.isVisible).toEqual(false)
            expect(currentExportImageDialogState.rootState.isRequesting).toEqual(true)
            expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(nextStateSnapshot))

            //assert immutation takes place in reducer
            expect(nextExportImageDialogState.rootState).not.toBe(currentExportImageDialogState.rootState)
        })

        test('+++ Verify the update of states when successfully requested export image', async () => {

            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'
            const exportImageBase64 = 'base64forexportimage'
            const data = ''
            const seriesDtl = sampleState.data.study.studyDtl.rootState.seriesDtls.byId[selectedSeriesId]

            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockImplementation(()=>{
                let result = []
                selectedImageIds.forEach((element) => {
                    const imageId = seriesDtl.imageDtls.idsMapping[element]
                    result.push(imageId)
                })
                return result
            })
            dataRepository.findExportImageParam = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.error = jest.fn()

            AuditLogController.mockImplementation(()=>{
                return {
                    writeIcwLog: function() {

                    }
                }
            })
            
            StudyService.mockImplementation(()=>{
                return {
                    retrieveImageForExport: function() {
                        let exportImageRsltObj = null
                        exportImageRsltObj = {
                            response: exportImageBase64,
                            data
                        }
                        return exportImageRsltObj
                    }
                }
            })

            viewerLogController.handleLogEx = jest.fn()

            // Previous states
            let previousState = store.getState()

            // Previous states of exportImageDialog
            let previousExportImageDialogState = previousState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let previousStateSnapshot = clone(previousState)
                previousStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                previousStateSnapshot.components.appMask = null
                previousStateSnapshot.components.toolBarPanel.rootState.btnState = null

            // Act 1
            dataRepository.setStore(store)
            store.dispatch(requestExportImages(selectedSeriesId, selectedImageIds, password))
            await stop(3000)

            // current states
            let currentState = store.getState()

            // Current states of exportImageDialog
            let currentExportImageDialogState = currentState.components.toolBarPanel.fileTools.dialogs.exportImageDialog

            // Snapshot with changed values removed
            let currentStateSnapshot = clone(currentState)
                currentStateSnapshot.components.toolBarPanel.fileTools.dialogs.exportImageDialog = null
                currentStateSnapshot.components.appMask = null
                currentStateSnapshot.components.toolBarPanel.rootState.btnState = null

            // Assert
            expect(currentExportImageDialogState.rootState.isRequesting).toEqual(false)
            expect(currentExportImageDialogState.rootState.isDownload).toEqual(true)
            expect(currentExportImageDialogState.rootState.isDownload).not.toEqual(previousExportImageDialogState.rootState.isDownload)
            expect(currentExportImageDialogState.rootState.exportImageBase64).toEqual(OCTET_STREAM_HEADERS+exportImageBase64)
            expect(currentExportImageDialogState.rootState.exportImageBase64).not.toEqual(previousExportImageDialogState.rootState.exportImageBase64)
            expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))

            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(0)
            expect(logger.error).toHaveBeenCalledTimes(0)

            //assert immutation takes place in reducer
            expect(previousExportImageDialogState.rootState).not.toBe(currentExportImageDialogState.rootState)
        })

    })

})