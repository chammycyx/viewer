import { getStore, stop } from '@test/dataFlow/DataFlowHelper'
import { clone } from '@utils/CloneUtil'
import { onColorAdjustmentButtonClick } from '@actions/components/toolBarPanel/Action'
import dataRepository from '@utils/DataRepository'

let store = null

beforeEach(() => {
    store = getStore()
    dataRepository.setStore(store)
})

describe('Test Suite: <onColorAdjustmentButtonClick>', async ()=>{
    describe('Test Action: <onColorAdjustmentButtonClick>', async ()=>{
        test('Test clicking color adjustment button when there is 1 image selected', async ()=>{
            //Arrange
            let previousState = store.getState()
            let prevColorAdjustmentPropsBoxRootState = previousState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.selectedImageIds = ['imageId_1']

            //Act
            store.dispatch(onColorAdjustmentButtonClick())
            await stop(2000)

            //Assert
            let nextState = store.getState()
            let nextColorAdjustmentPropsBoxRootState = nextState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState

            //assert value updated
            expect(nextColorAdjustmentPropsBoxRootState.visible).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(prevColorAdjustmentPropsBoxRootState).not.toBe(nextColorAdjustmentPropsBoxRootState)
        }),
        test('Test clicking color adjustment button when there are 2 images selected', async ()=>{
            //Arrange
            let previousState = store.getState()
            let prevColorAdjustmentPropsBoxRootState = previousState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.selectedImageIds = ['imageId_1', 'imageId_2']
        
            //Act
            store.dispatch(onColorAdjustmentButtonClick())
            await stop(2000)
        
            //Assert
            let nextState = store.getState()
            let nextColorAdjustmentPropsBoxRootState = nextState.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState

            //assert value updated
            expect(nextColorAdjustmentPropsBoxRootState.visible).toEqual(false)
            
            //assert immutation takes place in reducer
            expect(prevColorAdjustmentPropsBoxRootState).toBe(nextColorAdjustmentPropsBoxRootState)
        })
    })
})
