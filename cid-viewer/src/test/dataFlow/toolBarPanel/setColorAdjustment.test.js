import { getStore } from '@test/dataFlow/DataFlowHelper'
import { clone } from '@utils/CloneUtil'
import { setColorAdjustment } from '@actions/components/imageViewingPanel/canvasGrid/Action'

let store = null

const canvasProfilesTwoImagesStub = {
    byId: {
        'imageId_1': {
            header: '',
            imageBase64: '',
            fabricProfile: {},
            brightness: 0,
            contrast:0,
            hue: 0,
            saturation: 0,
            invert: false,
            zoomValue: 100,
            imageXCoor: null,
            imageYCoor: null,
            imageWidth: null,
            imageHeight: null,
            rotateAngle: 0,
            flipHorizontal: false,
            flipVertical: false
        },
        'imageId_2': {
            header: '',
            imageBase64: '',
            fabricProfile: {},
            brightness: 0,
            contrast:0,
            hue: 0,
            saturation: 0,
            invert: false,
            zoomValue: 100,
            imageXCoor: null,
            imageYCoor: null,
            imageWidth: null,
            imageHeight: null,
            rotateAngle: 0,
            flipHorizontal: false,
            flipVertical: false
        }
    },
    allIds: ['imageId_1', 'imageId_2']
}

beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <setColorAdjustment>', ()=>{
    describe('Test Action: <setColorAdjustment>', ()=>{
        test('Test setting color adjustment', ()=>{
            //Arrange
            let previousState = store.getState()
            let prevCanvasGridRootState = previousState.components.imageViewingPanel.canvasGrid.rootState
            prevCanvasGridRootState.canvasProfiles = clone(canvasProfilesTwoImagesStub)
            prevCanvasGridRootState.selectedImageIds = ['imageId_2']

            let prevImageId1 = prevCanvasGridRootState.canvasProfiles.byId['imageId_1']
            let prevImageId2 = prevCanvasGridRootState.canvasProfiles.byId['imageId_2']
        
            //Act
            store.dispatch(setColorAdjustment(10, 20, 30, 40, true))
        
            //Assert
            let nextState = store.getState()
            let nextCanvasGridRootState = nextState.components.imageViewingPanel.canvasGrid.rootState

            //assert value updated
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].brightness).toEqual(0)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].contrast).toEqual(0)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].hue).toEqual(0)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].saturation).toEqual(0)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'].invert).toEqual(false)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].brightness).toEqual(10)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].contrast).toEqual(20)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].hue).toEqual(30)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].saturation).toEqual(40)
            expect(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'].invert).toEqual(true)
            
            //assert immutation takes place in reducer
            expect(prevImageId1).toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_1'])
            expect(prevImageId2).not.toBe(nextCanvasGridRootState.canvasProfiles.byId['imageId_2'])
        })
    })
})
