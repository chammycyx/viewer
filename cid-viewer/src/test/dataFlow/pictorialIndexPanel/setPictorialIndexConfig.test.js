import {getStore} from '@test/dataFlow/DataFlowHelper'
import {clone} from '@utils/CloneUtil'

import {setPictorialIndexConfig} from '../../../main/actions/components/pictorialIndexPanel/thumbnailGrid/Action'

let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <setPictorialIndexConfig>', ()=>{

    test('Testing of setting PictorialIndex config', ()=>{
        
        //Arrange
        let numOfColumn = 1
        let isShowSeriesHeading = false
        let serieName = 'testingSerieName1'

        let previousStateSnapshot = clone(store.getState())
            previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
    
        //Act
        store.dispatch(setPictorialIndexConfig(numOfColumn, isShowSeriesHeading, serieName))
    
        //Assert
        let currentStateSnapshot = clone(store.getState())
            currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        let currentState = store.getState()
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentState.components.pictorialIndexPanel.thumbnailGrid)).toBe(JSON.stringify(
            {
                rootState:{
                    selectedSeriesId: '',
                    selectedImageIds: [],
                    selectedFirstImageId: '',
                    numOfColumn: 1,
                    isShowSeriesHeading: false,
                    serieName: 'testingSerieName1'
                }
            }
        ))
    })
})
