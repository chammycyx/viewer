import {getStore} from '@test/dataFlow/DataFlowHelper'
import {clone} from '../../../main/utils/CloneUtil'

import {setIsHidden} from '../../../main/actions/components/pictorialIndexPanel/Action'

let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <setPictorialIndexIsHidden>', ()=>{

    test('Testing of hiding PictorialIndex', ()=>{
        
        //Arrange
        let previousStateSnapshot = clone(store.getState())
            previousStateSnapshot.components.pictorialIndexPanel = null
    
        //Act
        store.dispatch(setIsHidden(true))
    
        //Assert
        let currentStateSnapshot = clone(store.getState())
            currentStateSnapshot.components.pictorialIndexPanel = null
        let currentState = store.getState()
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentState.components.pictorialIndexPanel)).toBe(JSON.stringify(
            {
                rootState:{
                    isHidden: true
                    },
                thumbnailGrid: {
                    rootState: {
                        selectedSeriesId: '',
                        selectedImageIds: [],
                        selectedFirstImageId: '',
                        numOfColumn: 2,
                        isShowSeriesHeading: true,
                        serieName: ''
                    }
                }
            }
        ))
    })

    test('Testing of showing PictorialIndex', ()=>{
        
        //Arrange
        let previousStateSnapshot = clone(store.getState())
            previousStateSnapshot.components.pictorialIndexPanel = null
    
        //Act
        store.dispatch(setIsHidden(true))
        store.dispatch(setIsHidden(false))
    
        //Assert
        let currentStateSnapshot = clone(store.getState())
            currentStateSnapshot.components.pictorialIndexPanel = null
        let currentState = store.getState()
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentState.components.pictorialIndexPanel)).toBe(JSON.stringify(
            {
                rootState:{
                    isHidden: false
                    },
                thumbnailGrid: {
                    rootState: {
                        selectedSeriesId: '',
                        selectedImageIds: [],
                        selectedFirstImageId: '',
                        numOfColumn: 2,
                        isShowSeriesHeading: true,
                        serieName: ''
                    }
                }
            }
        ))
    })

})
