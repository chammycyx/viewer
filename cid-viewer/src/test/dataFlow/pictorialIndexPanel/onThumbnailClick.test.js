import {getStore, stop} from '@test/dataFlow/DataFlowHelper'
import {clone} from '@utils/CloneUtil'
import dataRepository from '@utils/DataRepository'

// import actions
import {onThumbnailClick } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { setImage as setImageToViewPanel } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { setMode as setSlideShowMode, setPlaying as setSlideShowPlaying } from '@actions/components/toolBarPanel/slideShowTools/Action'

// import controllers
import { initBtnState, normalBtnState } from '@domains/controllers/ButtonStateController'


jest.mock('@domains/controllers/ViewingPanelController')

let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <The data flow of onThumbnailClick>', async ()=>{

    // test('Testing of clicking thumbnail', async ()=>{
        
    //     // Arrange
    //     const seriesIndexId = 'seriesDtl_1'
    //     const imgIndexIds = ['imageDtl_1','imageDtl_2','imageDtl_3','imageDtl_4']
    //     const selectedSeriesId = seriesIndexId
    //     const selectedFirstImageId = imgIndexIds[0]

    //     const caseNo = '1'
    //     const studyDtm = '2'
    //     const viewMode = {
    //         text: '2X2',
    //         noOfImage: 4
    //     }
    //     const imageBase64s = ['1', '2', '3', '4']
    //     const imgsProperties = [
    //         {imageWidth: 8.1, imageHeight: 9.1, imageOriginX: "center", imageOriginY: "center", imageXCoor: 10, imageYCoor: 11, imageScaleX: 12.1, imageScaleY: 13.1},
    //         {imageWidth: 8.1, imageHeight: 9.1, imageOriginX: "center", imageOriginY: "center", imageXCoor: 10, imageYCoor: 11, imageScaleX: 12.1, imageScaleY: 13.1},
    //         {imageWidth: 8.1, imageHeight: 9.1, imageOriginX: "center", imageOriginY: "center", imageXCoor: 10, imageYCoor: 11, imageScaleX: 12.1, imageScaleY: 13.1},
    //         {imageWidth: 8.1, imageHeight: 9.1, imageOriginX: "center", imageOriginY: "center", imageXCoor: 10, imageYCoor: 11, imageScaleX: 12.1, imageScaleY: 13.1}
    //     ]
    //     const imageCreateTimeStamp = ['1', '2', '3', '4']
    //     const examType = '1234'

    //     // previous states
    //     let previousState = store.getState()

    //     // Remove the changed values in snapshot
    //     let previousStateSnapshot = clone(previousState)
    //         previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
    //         previousStateSnapshot.components.imageViewingPanel.canvasGrid = null
    //         previousStateSnapshot.components.toolBarPanel.rootState.btnState = null
            

    //     // mock implementations
    //     dataRepository.findIasWsAuthInfo = jest.fn()
        
    //     ViewingPanelController.mockImplementation(()=>{
    //         return {
    //             showImage: function(){
    //                 store.dispatch(setImageToViewPanel(caseNo, studyDtm, viewMode, seriesIndexId, imgIndexIds, imageBase64s, imgsProperties, imageCreateTimeStamp, examType))
    //             }
    //         }
    //     })
    
    //     // Act
    //     dataRepository.setStore(store)
    //     store.dispatch(onThumbnailClick(selectedSeriesId,selectedFirstImageId))
    //     await stop(5000)
    

    //     // current states
    //     let currentState = store.getState()
    //     let currentCanvasGridState = currentState.components.imageViewingPanel.canvasGrid
    //     let previousCanvasGridState = previousState.components.imageViewingPanel.canvasGrid

    //     // Remove the changed values in snapshot
    //     let currentStateSnapshot = clone(currentState)
    //         currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
    //         currentStateSnapshot.components.imageViewingPanel.canvasGrid = null
    //         currentStateSnapshot.components.toolBarPanel.rootState.btnState = null

    //     // Assert
    //     expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
    //     expect(JSON.stringify(currentState.components.pictorialIndexPanel.thumbnailGrid)).toBe(JSON.stringify(
    //         {
    //             rootState:{
    //                 selectedSeriesId: selectedSeriesId,
    //                 selectedImageIds: imgIndexIds,
    //                 selectedFirstImageId: selectedFirstImageId,
    //                 numOfColumn: 2,
    //                 isShowSeriesHeading: true,
    //                 serieName: ''
    //             }
    //         }
    //     ))
    //     const expectedViewMode = viewMode
    //     const expectedSelectedSeriesId = seriesIndexId
    //     const expectedShowImageIds = imgIndexIds
    //     const expectedCaseNo = caseNo
    //     const expectedStudyDateTime = studyDtm
    //     const expectedSelectedImageIds = [imgIndexIds[0]]
    //     const expectedHeaderTimeText = ' Study Date/Time: '
    //     const expectedDisplayDateTime = [studyDtm, studyDtm, studyDtm, studyDtm]
    //     const expectedCanvasProfiles = {
    //         byId: {
    //             'imageDtl_1': {
    //                 header: '',
    //                 imageBase64: imageBase64s[0],
    //                 brightness: 0,
    //                 contrast: 0,
    //                 hue: 0,
    //                 saturation: 0,
    //                 invert: false,
    //                 imageXCoor: 10,
    //                 imageYCoor: 11,
    //                 imageWidth: 8.1,
    //                 imageHeight: 9.1,
    //                 imageScaleX: 12.1,
    //                 imageScaleY: 13.1,
    //                 imageOriginX: 'center',
    //                 imageOriginY: 'center',
    //                 rotateAngle: 0,
    //                 flipHorizontal: false,
    //                 flipVertical: false,
    //                 fontType: '',
    //                 fontSize: 0,
    //                 fontColor: '',
    //                 bgColor: '',
    //                 visibility: false
    //             },
    //             'imageDtl_2': {
    //                 header: '',
    //                 imageBase64:  imageBase64s[1],
    //                 brightness: 0,
    //                 contrast: 0,
    //                 hue: 0,
    //                 saturation: 0,
    //                 invert: false,
    //                 imageXCoor: 10,
    //                 imageYCoor: 11,
    //                 imageWidth: 8.1,
    //                 imageHeight: 9.1,
    //                 imageScaleX: 12.1,
    //                 imageScaleY: 13.1,
    //                 imageOriginX: 'center',
    //                 imageOriginY: 'center',
    //                 rotateAngle: 0,
    //                 flipHorizontal: false,
    //                 flipVertical: false,
    //                 fontType: '',
    //                 fontSize: 0,
    //                 fontColor: '',
    //                 bgColor: '',
    //                 visibility: false
    //             },
    //             'imageDtl_3': {
    //                 header: '',
    //                 imageBase64:  imageBase64s[2],
    //                 brightness: 0,
    //                 contrast: 0,
    //                 hue: 0,
    //                 saturation: 0,
    //                 invert: false,
    //                 imageXCoor: 10,
    //                 imageYCoor: 11,
    //                 imageWidth: 8.1,
    //                 imageHeight: 9.1,
    //                 imageScaleX: 12.1,
    //                 imageScaleY: 13.1,
    //                 imageOriginX: 'center',
    //                 imageOriginY: 'center',
    //                 rotateAngle: 0,
    //                 flipHorizontal: false,
    //                 flipVertical: false,
    //                 fontType: '',
    //                 fontSize: 0,
    //                 fontColor: '',
    //                 bgColor: '',
    //                 visibility: false
    //             },
    //             'imageDtl_4': {
    //                 header: '',
    //                 imageBase64:  imageBase64s[3],
    //                 brightness: 0,
    //                 contrast: 0,
    //                 hue: 0,
    //                 saturation: 0,
    //                 invert: false,
    //                 imageXCoor: 10,
    //                 imageYCoor: 11,
    //                 imageWidth: 8.1,
    //                 imageHeight: 9.1,
    //                 imageScaleX: 12.1,
    //                 imageScaleY: 13.1,
    //                 imageOriginX: 'center',
    //                 imageOriginY: 'center',
    //                 rotateAngle: 0,
    //                 flipHorizontal: false,
    //                 flipVertical: false,
    //                 fontType: '',
    //                 fontSize: 0,
    //                 fontColor: '',
    //                 bgColor: '',
    //                 visibility: false
    //             }
    //         },
    //         allIds: ['imageDtl_1','imageDtl_2','imageDtl_3','imageDtl_4']
    //     }

    //     expect(currentCanvasGridState.rootState.viewMode).toEqual(expectedViewMode)
    //     expect(currentCanvasGridState.rootState.selectedSeriesId).toEqual(expectedSelectedSeriesId)
    //     expect(currentCanvasGridState.rootState.showImageIds).toEqual(expectedShowImageIds)
    //     expect(currentCanvasGridState.rootState.caseNo).toEqual(expectedCaseNo)
    //     expect(currentCanvasGridState.rootState.studyDateTime).toEqual(expectedStudyDateTime)
    //     expect(currentCanvasGridState.rootState.selectedImageIds).toEqual(expectedSelectedImageIds)
    //     expect(currentCanvasGridState.rootState.headerTimeText).toEqual(expectedHeaderTimeText)
    //     expect(currentCanvasGridState.rootState.displayDateTime).toEqual(expectedDisplayDateTime)
    //     expect(currentCanvasGridState.rootState.canvasProfiles).toEqual(expectedCanvasProfiles)
    //     //assert immutation takes place in reducer
    //     expect(previousCanvasGridState.rootState).not.toBe(currentCanvasGridState.rootState)
    // })

    test('Testing of stopping slide show upon clicking thumbnail', async ()=>{
        
        // Arrange
        const seriesIndexId = 'seriesDtl_1'
        const imgIndexIds = ['imageDtl_1','imageDtl_2','imageDtl_3','imageDtl_4']
        const selectedSeriesId = seriesIndexId
        const selectedFirstImageId = imgIndexIds[0]

        // previous states
        let previousState = store.getState()

        // Get changed values in states for assertions
        let previousSlideShowState = previousState.components.toolBarPanel.slideShowTools.rootState

        // Get snapshot for non-changing states
        let previousStateSnapshot = clone(previousState)
        previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        previousStateSnapshot.components.imageViewingPanel.canvasGrid = null
        previousStateSnapshot.components.toolBarPanel.rootState.btnState = null
        previousStateSnapshot.components.toolBarPanel.slideShowTools.rootState = null
            

        // mock implementations
        dataRepository.findIasWsAuthInfo = jest.fn()
        
        // Arrange 2
        store.dispatch(setSlideShowMode(true))
        store.dispatch(setSlideShowPlaying(true))
        

        // Intermittent states
        let intermittentState = store.getState()

        // Get changed values in states for assertions
        let intermittentSlideShowState = intermittentState.components.toolBarPanel.slideShowTools.rootState

        // Get snapshot for non-changing states
        let intermittentStateSnapshot = clone(intermittentState)
        intermittentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        intermittentStateSnapshot.components.imageViewingPanel.canvasGrid = null
        intermittentStateSnapshot.components.toolBarPanel.rootState.btnState = null
        intermittentStateSnapshot.components.toolBarPanel.slideShowTools.rootState = null
        

        // Act
        dataRepository.setStore(store)
        store.dispatch(onThumbnailClick(selectedSeriesId,selectedFirstImageId))
        await stop(3000)


        // current states
        let currentState = store.getState()

        // Get changed values in states for assertions
        let currentSlideShowState = currentState.components.toolBarPanel.slideShowTools.rootState
        
        // Get snapshot for non-changing states
        let currentStateSnapshot = clone(currentState)
        currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        currentStateSnapshot.components.imageViewingPanel.canvasGrid = null
        currentStateSnapshot.components.toolBarPanel.rootState.btnState = null
        currentStateSnapshot.components.toolBarPanel.slideShowTools.rootState = null

        // Assert
        expect(JSON.stringify(currentSlideShowState)).toBe(JSON.stringify(previousSlideShowState))
        expect(JSON.stringify(currentSlideShowState.isModeOn)).not.toBe(JSON.stringify(intermittentSlideShowState.isModeOn))
        expect(JSON.stringify(currentSlideShowState.isPlaying)).not.toBe(JSON.stringify(intermittentSlideShowState.isPlaying))
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(intermittentStateSnapshot))        
    })


    test('Testing of enabling toolbar buttons upon clicking thumbnail', ()=>{

        // Arrange
        const seriesIndexId = 'seriesDtl_1'
        const imgIndexIds = ['imageDtl_1','imageDtl_2','imageDtl_3','imageDtl_4']
        const selectedSeriesId = seriesIndexId
        const selectedFirstImageId = imgIndexIds[0]

        // previous states
        let previousState = store.getState()
        let previousBtnState = previousState.components.toolBarPanel.rootState.btnState

        // Remove the changed values in snapshot
        let previousStateSnapshot = clone(previousState)
            previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
            previousStateSnapshot.components.imageViewingPanel.canvasGrid = null
            previousStateSnapshot.components.toolBarPanel.rootState.btnState = null
            
        // mock implementations
        dataRepository.findIasWsAuthInfo = jest.fn()

        // Act
        store.dispatch(onThumbnailClick(selectedSeriesId,selectedFirstImageId))

        // current states
        let currentState = store.getState()
        let currentBtnState = currentState.components.toolBarPanel.rootState.btnState

        // Remove the changed values in snapshot
        let currentStateSnapshot = clone(currentState)
            currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
            currentStateSnapshot.components.imageViewingPanel.canvasGrid = null
            currentStateSnapshot.components.toolBarPanel.rootState.btnState = null

        // Assert
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(previousBtnState).toEqual(initBtnState)
        expect(currentBtnState).toEqual(normalBtnState)
    })
})
