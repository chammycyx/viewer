import {getStore} from '@test/dataFlow/DataFlowHelper'
import {clone} from '../../../main/utils/CloneUtil'

import {setSelectedImageIds} from '../../../main/actions/components/pictorialIndexPanel/thumbnailGrid/Action'


let store = null
beforeEach(() => {
    store = getStore()
})


describe('Test Suite: <setSelectedImageIds>', ()=>{

    test('Testing of Selecting 1 Image', ()=>{
        
        //Arrange
        let selectedImageIds = ['ImageId_1']
        let previousStateSnapshot = clone(store.getState())
            previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
    
        //Act
        store.dispatch(setSelectedImageIds(selectedImageIds))
    
        //Assert
        let currentStateSnapshot = clone(store.getState())
            currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        let currentState = store.getState()
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentState.components.pictorialIndexPanel.thumbnailGrid)).toBe(JSON.stringify(
            {
                rootState:{
                    selectedSeriesId: '',
                    selectedImageIds: ['ImageId_1'],
                    selectedFirstImageId: '',
                    numOfColumn: 2,
                    isShowSeriesHeading: true,
                    serieName: ''
                }
            }
        ))
    })

    test('Testing of Selecting 4 Images', ()=>{
        
        //Arrange
        let selectedImageIds = ['ImageId_1','ImageId_2','ImageId_3','ImageId_4']
        let previousStateSnapshot = clone(store.getState())
            previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
    
        //Act
        store.dispatch(setSelectedImageIds(selectedImageIds))
    
        //Assert
        let currentStateSnapshot = clone(store.getState())
            currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        let currentState = store.getState()
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentState.components.pictorialIndexPanel.thumbnailGrid)).toBe(JSON.stringify(
            {
                rootState:{
                    selectedSeriesId: '',
                    selectedImageIds: ['ImageId_1','ImageId_2','ImageId_3','ImageId_4'],
                    selectedFirstImageId: '',
                    numOfColumn: 2,
                    isShowSeriesHeading: true,
                    serieName: ''
                }
            }
        ))
    })

    test('Testing of Selecting 25 Images', ()=>{
        
        //Arrange
        let selectedImageIds = ['ImageId_1','ImageId_2','ImageId_3','ImageId_4'
        ,'ImageId_5','ImageId_6','ImageId_7','ImageId_8','ImageId_9','ImageId_10'
        ,'ImageId_11','ImageId_12','ImageId_13','ImageId_14','ImageId_15','ImageId_16'
        ,'ImageId_17','ImageId_18','ImageId_19','ImageId_20','ImageId_21','ImageId_22'
        ,'ImageId_23','ImageId_24','ImageId_25']
        let previousStateSnapshot = clone(store.getState())
            previousStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
    
        //Act
        store.dispatch(setSelectedImageIds(selectedImageIds))
    
        //Assert
        let currentStateSnapshot = clone(store.getState())
            currentStateSnapshot.components.pictorialIndexPanel.thumbnailGrid = null
        let currentState = store.getState()
        expect(JSON.stringify(currentStateSnapshot)).toBe(JSON.stringify(previousStateSnapshot))
        expect(JSON.stringify(currentState.components.pictorialIndexPanel.thumbnailGrid)).toBe(JSON.stringify(
            {
                rootState:{
                    selectedSeriesId: '',
                    selectedImageIds: ['ImageId_1','ImageId_2','ImageId_3','ImageId_4'
                    ,'ImageId_5','ImageId_6','ImageId_7','ImageId_8','ImageId_9','ImageId_10'
                    ,'ImageId_11','ImageId_12','ImageId_13','ImageId_14','ImageId_15','ImageId_16'
                    ,'ImageId_17','ImageId_18','ImageId_19','ImageId_20','ImageId_21','ImageId_22'
                    ,'ImageId_23','ImageId_24','ImageId_25'],
                    selectedFirstImageId: '',
                    numOfColumn: 2,
                    isShowSeriesHeading: true,
                    serieName: ''
                }
            }
        ))
    })
})
