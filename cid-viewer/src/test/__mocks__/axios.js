let returnObj = jest.fn()

export function setReturnObj(inputObj){
    returnObj.mockReturnValue(inputObj)
}

export function setReturnObjOnce(inputObj){
    returnObj.mockReturnValueOnce(inputObj)
}

export const paths = ['testpath1/path','testpath2/path']

export function errMsg(path){
    return {error: 'url: ' + path + ' not found.'}
}

export default function axios(options) {
    return new Promise((resolve, reject) => {
        const path = options.url
        if(paths.indexOf(path) > -1){
          resolve(returnObj())
        }else{
          reject(errMsg(path))
        }
    })
}