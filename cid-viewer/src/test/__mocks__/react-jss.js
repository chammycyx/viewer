import createDisplayName from '@utils/DisplayNameUtil'

export default function injectSheet() {
    return function(WrappedComp) {
        let WrappedCompWithMockDisplayName = WrappedComp
        WrappedCompWithMockDisplayName.displayName = createDisplayName('MockJss', WrappedCompWithMockDisplayName)
        return WrappedCompWithMockDisplayName
    }
}