import StateTreeUtil from '../../main/utils/StateTreeUtil'
import {SeriesDtlMap, StudyDtlAccessionNoMap, StudyDtlStudyIDMap, StudyDtlStudyTypeMap, StudyDtlStudyDtmMap, StudyDtlRemarkMap} from '../../main/utils/StateTreeMap'

// test('Test StateTreeUtil.merge for data/config', ()=>{
// 	let actualDataTree = {
// 		components:{},
// 		data:{
// 			common:{},
// 			study:{},
// 			config:{
// 				application:{
// 					viewerControl:{},
// 					imageHeader:{},
// 					export:{},
// 					webServiceIcw:{},
// 					webServiceIas:{}
// 				}
// 			}
// 		}
// 	}
// 	let expectedDataTree = {
// 		components:{},
// 		data:{
// 			common:{},
// 			study:{},
// 			config:{
// 				application:{
// 					viewerControl:{
// 						idleTimeout:'20',
// 						EPRTimeoutMethodName: 'acknowledgeEPRTimeout'
// 					},
// 					imageHeader:{},
// 					export:{
// 						ImageExportFileName: 'Images.zip'
// 					},
// 					webServiceIcw:{
// 						cidWsContextPath:'http://cid-corp-d1:11360/cid-image-common_6_0_0/',
// 						suid1: '6hJ3sINlf/TGEiiX0Jzjew=='
// 					},
// 					webServiceIas:{
// 						cidWsContextPath: 'http://dc1cidsd01:8080/axcid-proxy/'
// 					}
// 				}
// 			}
// 		}
// 	}
//     let jsonString =    '{"viewerControl": {'+
//                             '"idleTimeout": "20",'+
//                             '"EPRTimeoutMethodName": "acknowledgeEPRTimeout"'+
//                         '},'+
//                         '"export": {'+
//                             '"ImageExportFileName": "Images.zip"'+
// 						'},'+
// 						'"imageHeader": {},'+
// 						'"webServiceIcw":{' +
// 							'"cidWsContextPath": "http://cid-corp-d1:11360/cid-image-common_6_0_0/",'+
// 							'"suid1":"6hJ3sINlf/TGEiiX0Jzjew=="'+
// 						'},'+
// 						'"webServiceIas":{'+
// 							'"cidWsContextPath":"http://dc1cidsd01:8080/axcid-proxy/"'+
// 						'}}'
						
//     StateTreeUtil.mergeAll(actualDataTree, jsonString)
//     expect(JSON.stringify(actualDataTree)).toBe(JSON.stringify(expectedDataTree))
// })

test('Test StateTreeUtil.merge for data/study/messageDtl', ()=>{
	let actualDataTree = {
		transactionCode:'',
		serverHosp:"",
		transactionDtm:"",
		transactionID:"",
		sendingApplication:""
	}
	let expectedDataTree = {
		transactionCode:"CID2DATA",
		serverHosp:"VH",
		transactionDtm:"20180404150847.794",
		transactionID:"C1AC67E9-BCD2-4B09-9517-22CC65B39511",
		sendingApplication:"CID"
	}
    let jsonString = '{' +
						'"transactionCode":"CID2DATA",' +
						'"serverHosp":"VH",' +
						'"transactionDtm":"20180404150847.794",' +
						'"transactionID":"C1AC67E9-BCD2-4B09-9517-22CC65B39511",' +
						'"sendingApplication":"CID"'+
					'}'
    StateTreeUtil.mergeAll(actualDataTree, jsonString)
    expect(JSON.stringify(actualDataTree)).toBe(JSON.stringify(expectedDataTree))
})

test('Test StateTreeUtil.merge for data/study/patientDtl', ()=>{
	let actualDataTree = {
		hkid:'',
		key:"",
		name:"",
		dob:"",
		sex:"",
		deathIndicator:""
	}
	let expectedDataTree = {
		hkid:"A1234567",
		key:"90500232",
		name:"LEUNG,DINGDONG",
		dob:"19931121000000.000",
		sex:"M",
		deathIndicator:"deathIndicator1"
	}
    let jsonString = '{'+
						'"hkid":"A1234567",' +
						'"patKey":"90500232",' +
						'"patName":"LEUNG,DINGDONG",' +
						'"patDOB":"19931121000000.000",' +
						'"patSex":"M",' +
						'"deathIndicator":"deathIndicator1"'+
					'}'
    StateTreeUtil.mergeAll(actualDataTree, jsonString)
    expect(JSON.stringify(actualDataTree)).toBe(JSON.stringify(expectedDataTree))
})

test('Test StateTreeUtil.merge for data/study/visitDtl', ()=>{
	let actualDataTree = {
		visitHosp:"",
		visitSpec:"",
		visitWardClinic:"",
		payCode:"",
		caseNum:"",
		adtDtm:""
	}
	let expectedDataTree = {
		visitHosp:"VH",
		visitSpec:"VisitSpec1",
		visitWardClinic:"visitWardClinic1",
		payCode:"PayCode1",
		caseNum:"HN05000207X",
		adtDtm:"AdtDtm1"
	}
    let jsonString = '{'+
						'"visitHosp":"VH",' +
						'"visitSpec":"VisitSpec1",' +
						'"visitWardClinic":"visitWardClinic1",' +
						'"payCode":"PayCode1",' +
						'"caseNum":"HN05000207X",' +
						'"adtDtm":"AdtDtm1"'+
					'}'
    StateTreeUtil.mergeAll(actualDataTree, jsonString)
    expect(JSON.stringify(actualDataTree)).toBe(JSON.stringify(expectedDataTree))
})

test('Test StateTreeUtil.merge for data/study/studyDtl', ()=>{
	let actualDataTree = {
		accessionNo:"",
		studyID:"",
		studyType:"",
		studyDtm:"",
		remark:""
	}
	let expectedDataTree = {
		accessionNo:"R9MLJ5KPEVED1SMQ",
		studyID:"5953c844-8fe6-4587-9ee3-b898586df6f1",
		studyType:"ERS",
		studyDtm:"20100131123456",
		remark:"Remark1"
	}
	let jsonString = '{'+
						'"accessionNo":"R9MLJ5KPEVED1SMQ",' +
						'"studyID":"5953c844-8fe6-4587-9ee3-b898586df6f1",' +
						'"studyType":"ERS",' +
						'"studyDtm":"20100131123456",' +
						'"remark":"Remark1"' +
					'}'
    StateTreeUtil.mergeAll(actualDataTree, jsonString)
    expect(JSON.stringify(actualDataTree)).toBe(JSON.stringify(expectedDataTree))
})

test('Test StateTreeUtil.merge for data/study/studyDtl/seriesDtls', ()=>{
	let actualDataTree = {
		accessionNo:"",
		studyID:"",
		studyType:"",
		studyDtm:"",
		remark:"",
		seriesDtls:{}
	}
	let expectedDataTree = {
		accessionNo:"R9MLJ5KPEVED1SMQ",
		studyID:"5953c844-8fe6-4587-9ee3-b898586df6f1",
		studyType:"ERS",
		studyDtm:"20100131123456",
		remark:"Remark1",
		seriesDtls:{
			byId:{
				seriesDtl_1:{
					seriesNo: 'f74c17d2-2069-42be-9e20-ab9705ab9189',
					examType: 'HADRW',
					examDtm:"20150318161644",
					entityID:"0",
					imageDtls:{
						byId:{
							imageDtl_1:{
								imageFormat:"IM",
								imagePath:"/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG",
								imageFile:"1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG",
								imageID:"ff72b811-6ac6-41fd-a414-8dd87ae8089e",
								imageVersion:"1",
								imageSequence:"1",
								imageKeyword:"Name:Bronchoscopy1|",
								imageHandling:"N",
								imageType:"JPEG",
								imageStatus:"1",
								annotationDtls:{
									byId:{
										annotationDtl_1:{
											annotationID:"annotationID1",
											annotationSeq:"annotationSeq1",
											annotationType:"annotationType1",
											annotationText:"annotationText1",
											annotationCoordinate:"annotationCoordinate1",
											annotationStatus:"annotationStatus1",
											annotationEditable:"annotationEditable1",
											annotationUpdDtm:"annotationUpdDtm1"
										},
										annotationDtl_2:{
											annotationID:"annotationID2",
											annotationSeq:"annotationSeq2",
											annotationType:"annotationType2",
											annotationText:"annotationText2",
											annotationCoordinate:"annotationCoordinate2",
											annotationStatus:"annotationStatus2",
											annotationEditable:"annotationEditable2",
											annotationUpdDtm:"annotationUpdDtm2"
										}
									},
									allIds:['annotationDtl_1','annotationDtl_2']
								}
							},
							imageDtl_2:{
								imageFormat:"IM2",
								imagePath:"/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/12.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG",
								imageFile:"1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG",
								imageID:"ff72b811-6ac6-41fd-a414-8dd87ae8089e",
								imageVersion:"2",
								imageSequence:"2",
								imageKeyword:"Name:Bronchoscopy2|",
								imageHandling:"Y",
								imageType:"JPEG2",
								imageStatus:"2",
								annotationDtls:{
									byId:{
										annotationDtl_1:{
											annotationID:"annotationID1.2",
											annotationSeq:"annotationSeq1.2",
											annotationType:"annotationType1.2",
											annotationText:"annotationText1.2",
											annotationCoordinate:"annotationCoordinate1.2",
											annotationStatus:"annotationStatus1.2",
											annotationEditable:"annotationEditable1.2",
											annotationUpdDtm:"annotationUpdDtm1.2"
										},
										annotationDtl_2:{
											annotationID:"annotationID2.2",
											annotationSeq:"annotationSeq2.2",
											annotationType:"annotationType2.2",
											annotationText:"annotationText2.2",
											annotationCoordinate:"annotationCoordinate2.2",
											annotationStatus:"annotationStatus2.2",
											annotationEditable:"annotationEditable2.2",
											annotationUpdDtm:"annotationUpdDtm2.2"
										}
									},
									allIds:['annotationDtl_1','annotationDtl_2']
								}
							}
						},
						allIds:['imageDtl_1','imageDtl_2'],
						allHighestVerIds:['imageDtl_2'],
						idsMapping:{imageDtl_1:"ff72b811-6ac6-41fd-a414-8dd87ae8089e", imageDtl_2:"ff72b811-6ac6-41fd-a414-8dd87ae8089e"}
					}
				}
			},
			allIds:['seriesDtl_1']
		}
	}
	let jsonString = '{'+
						'"accessionNo":"R9MLJ5KPEVED1SMQ",' +
						'"studyID":"5953c844-8fe6-4587-9ee3-b898586df6f1",' +
						'"studyType":"ERS",' +
						'"studyDtm":"20100131123456",' +
						'"remark":"Remark1",' +
						'"seriesDtls":{'+
							'"seriesDtl":['+
								'{'+
									'"seriesNo":"f74c17d2-2069-42be-9e20-ab9705ab9189",' +
									'"examType":"HADRW",' +
									'"examDtm":"20150318161644",' +
									'"entityID":"0",' +
									'"imageDtls":{'+
										'"imageDtl":['+
											'{'+
												'"imageFormat":"IM",' +
												'"imagePath":"/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG",' +
												'"imageFile":"1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG",' +
												'"imageID":"ff72b811-6ac6-41fd-a414-8dd87ae8089e",' +
												'"imageVersion":"1",' +
												'"imageSequence":"1",' +
												'"imageKeyword":"Name:Bronchoscopy1|",' +
												'"imageHandling":"N",' +
												'"imageType":"JPEG",' +
												'"imageStatus":"1",' +
												'"annotationDtls":{'+
													'"annotationDtl":['+
														'{'+
															'"annotationID":"annotationID1",' +
															'"annotationSeq":"annotationSeq1",' +
															'"annotationType":"annotationType1",' +
															'"annotationText":"annotationText1",' +
															'"annotationCoordinate":"annotationCoordinate1",' +
															'"annotationStatus":"annotationStatus1",' +
															'"annotationEditable":"annotationEditable1",' +
															'"annotationUpdDtm":"annotationUpdDtm1"'+
														'},'+
														'{'+
															'"annotationID":"annotationID2",' +
															'"annotationSeq":"annotationSeq2",' +
															'"annotationType":"annotationType2",' +
															'"annotationText":"annotationText2",' +
															'"annotationCoordinate":"annotationCoordinate2",' +
															'"annotationStatus":"annotationStatus2",' +
															'"annotationEditable":"annotationEditable2",' +
															'"annotationUpdDtm":"annotationUpdDtm2"' +
														'}'+
													']'+
												'}'+
											'},'+

											'{'+
												'"imageFormat":"IM2",' +
												'"imagePath":"/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/12.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG",' +
												'"imageFile":"1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG",' +
												'"imageID":"ff72b811-6ac6-41fd-a414-8dd87ae8089e",' +
												'"imageVersion":"2",' +
												'"imageSequence":"2",' +
												'"imageKeyword":"Name:Bronchoscopy2|",' +
												'"imageHandling":"Y",' +
												'"imageType":"JPEG2",' +
												'"imageStatus":"2",' +
												'"annotationDtls":{'+
													'"annotationDtl":['+
														'{'+
															'"annotationID":"annotationID1.2",' +
															'"annotationSeq":"annotationSeq1.2",' +
															'"annotationType":"annotationType1.2",' +
															'"annotationText":"annotationText1.2",' +
															'"annotationCoordinate":"annotationCoordinate1.2",' +
															'"annotationStatus":"annotationStatus1.2",' +
															'"annotationEditable":"annotationEditable1.2",' +
															'"annotationUpdDtm":"annotationUpdDtm1.2"'+
														'},'+
														'{'+
															'"annotationID":"annotationID2.2",' +
															'"annotationSeq":"annotationSeq2.2",' +
															'"annotationType":"annotationType2.2",' +
															'"annotationText":"annotationText2.2",' +
															'"annotationCoordinate":"annotationCoordinate2.2",' +
															'"annotationStatus":"annotationStatus2.2",' +
															'"annotationEditable":"annotationEditable2.2",' +
															'"annotationUpdDtm":"annotationUpdDtm2.2"' +
														'}'+
													']'+
												'}'+
											'}'+
										']'+
									'}'+
								'}'+
							']'+
						'}'+
					'}'
	let clonedActualDataTree = Object.assign({},actualDataTree)
	StateTreeUtil.merge(clonedActualDataTree, jsonString, SeriesDtlMap)
	StateTreeUtil.merge(clonedActualDataTree, jsonString, StudyDtlAccessionNoMap)
	StateTreeUtil.merge(clonedActualDataTree, jsonString, StudyDtlStudyIDMap)
	StateTreeUtil.merge(clonedActualDataTree, jsonString, StudyDtlStudyTypeMap)
	StateTreeUtil.merge(clonedActualDataTree, jsonString, StudyDtlStudyDtmMap)
	StateTreeUtil.merge(clonedActualDataTree, jsonString, StudyDtlRemarkMap)
    expect(JSON.stringify(clonedActualDataTree)).toBe(JSON.stringify(expectedDataTree))

    StateTreeUtil.mergeAll(actualDataTree, jsonString)
    expect(JSON.stringify(actualDataTree)).toBe(JSON.stringify(expectedDataTree))
})