import { sleep } from '@utils/CommonUtil'

jest.useFakeTimers();
describe('Test Suite: <CommonUtil>',()=>{

    test('Test sleep()', () => {
        //Arrange

        //Act
        sleep(1000)

        //Assert
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000)
    })

})