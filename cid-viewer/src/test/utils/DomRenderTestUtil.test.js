import React, { Component } from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import createDisplayName from '@utils/DisplayNameUtil'
import { shallowWrappedComponent } from '@utils/DomRenderTestUtil'

configure({ adapter: new Adapter() })

describe('Test DomRenderTestUtil#shallowWrappedComponent',()=>{
    //Arrange
    class WrappedComp extends Component {
        render() {
            return (
                <span>I am WrappedComp</span>
            )
        }
    }

    class OtherAidComp extends Component {
        render() {
            return (
                <span>I am OtherAidComp</span>
            )
        }
    }

    class Hoc1 extends Component {
        render() {
            return (
                <div>
                    <OtherAidComp />
                    <WrappedComp />
                </div>
            )
        }
    }
    Hoc1.displayName = createDisplayName('Hoc1', WrappedComp)

    class Hoc2 extends Component {
        render() {
            return <Hoc1 />
        }
    }
    Hoc2.displayName = createDisplayName('Hoc2', Hoc1)

    class Hoc3 extends Component {
        render() {
            return (
                <div>
                    <Hoc2 />
                    <OtherAidComp />
                </div>
            )
        }
    }
    Hoc3.displayName = createDisplayName('Hoc3', Hoc2)

    //Act
    const wrapper = shallowWrappedComponent(<Hoc3 />, 'WrappedComp')

    test('Test shallow render a wrapped component without option', ()=>{
        //Assert
        //Verify if the wrapper is rendered successfully
        expect(wrapper).toHaveLength(1)
        //Verify if the wrapper is of WrappedComp
        expect(wrapper.find('span').text()).toEqual('I am WrappedComp')
    })
})