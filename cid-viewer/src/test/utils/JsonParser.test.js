import JsonParser from '../../main/utils/JsonParser'
// import JsonPath from 'advanced-json-path'

test('Test JsonParser.getConfigSections', ()=>{
    let jsonString =    '{"viewerControl": {'+
                            '"idleTimeout": "20",'+
                            '"EPRTimeoutMethodName": "acknowledgeEPRTimeout"'+
                        '},'+
                        '"Export": {'+
                            '"ImageExportFileName": "Images.zip",'+
                            '"EnableEncryption": "true"'+
                        '}}'
    let sections = JsonParser.getConfigSections(jsonString)
    expect(sections).toHaveLength(2)
    expect(sections[0].name).toBe('viewerControl')
    expect(sections[0].entries[0].key).toBe('idleTimeout')
    expect(sections[0].entries[0].value).toBe('20')
    expect(sections[0].entries[1].key).toBe('EPRTimeoutMethodName')
    expect(sections[0].entries[1].value).toBe('acknowledgeEPRTimeout')

    expect(sections[1].name).toBe('Export')
    expect(sections[1].entries[0].key).toBe('ImageExportFileName')
    expect(sections[1].entries[0].value).toBe('Images.zip')
    expect(sections[1].entries[1].key).toBe('EnableEncryption')
    expect(sections[1].entries[1].value).toBe('true')
})

test('Test JsonParser.getAllConfigEntries', ()=>{
    let jsonString =    '{"viewerControl": {'+
                            '"idleTimeout": "20",'+
                            '"EPRTimeoutMethodName": "acknowledgeEPRTimeout"'+
                        '},'+
                        '"Export": {'+
                            '"ImageExportFileName": "Images.zip",'+
                            '"EnableEncryption": "true"'+
                        '}}'
    let entries = JsonParser.getAllConfigEntries(jsonString)
    expect(entries).toHaveLength(4)
    expect(entries[0].key).toBe('idleTimeout')
    expect(entries[0].value).toBe('20')
    expect(entries[1].key).toBe('EPRTimeoutMethodName')
    expect(entries[1].value).toBe('acknowledgeEPRTimeout')
    expect(entries[2].key).toBe('ImageExportFileName')
    expect(entries[2].value).toBe('Images.zip')
    expect(entries[3].key).toBe('EnableEncryption')
    expect(entries[3].value).toBe('true')
})

// test('Test JsonParser.getConfigEntries',()=>{
//     let jsonString =    '{'+
//                             '"idleTimeout": "20",'+
//                             '"EPRTimeoutMethodName": "acknowledgeEPRTimeout",'+
//                             '"ImageExportFileName": "Images.zip"'+
//                         '}'
//     let entries = JsonParser.getConfigEntries(jsonString)
//     expect(entries).toHaveLength(3)
//     expect(entries[0].key).toBe('idleTimeout')
//     expect(entries[0].value).toBe('20')
//     expect(entries[1].key).toBe('EPRTimeoutMethodName')
//     expect(entries[1].value).toBe('acknowledgeEPRTimeout')
//     expect(entries[2].key).toBe('ImageExportFileName')
//     expect(entries[2].value).toBe('Images.zip')
// })

test('Test JsonParser.getValueFromString', ()=>{
    let jsonString =    '{"viewerControl": {'+
                            '"idleTimeout": "20",'+
                            '"EPRTimeoutMethodName": "acknowledgeEPRTimeout1"'+
                        '},'+
                        '"Export": {'+
                            '"ImageExportFileName": "Images.zip",'+
                            '"EnableEncryption": "true"'+
                        '}}'
    let expectedValue1 = JsonParser.getValueFromString(jsonString,'idleTimeout')
    let expectedValue2 = JsonParser.getValueFromString(jsonString,'EPRTimeoutMethodName')
    let expectedValue3 = JsonParser.getValueFromString(jsonString,'ImageExportFileName')
    let expectedValue4 = JsonParser.getValueFromString(jsonString,'EnableEncryption')

    expect(expectedValue1).toBe('20')
    expect(expectedValue2).toBe('acknowledgeEPRTimeout1')
    expect(expectedValue3).toBe('Images.zip')
    expect(expectedValue4).toBe('true')
})
