//Implemention of multiple modules mock 
import * as VersionUtil from '../../main/utils/VersionUtil.js'
    
describe('Test Suite: <VersionUtil>',()=>{
    
    //Reset modules for mocking package.json in each test case.
    beforeEach(() => {
        jest.resetModules()
    })

    test('getVersion - normal flow', ()=>{
        //Arrange

        //First time of Mocking package.json
        jest.mock('../../../package.json', function() {
            return {
                version: '6.0.0.5'
            }
        })
        //Import VersionUtil with mocked package.json
        const MockVersionUtil = require('../../main/utils/VersionUtil.js')

        //Act
        let returnVersion = MockVersionUtil.getVersion()
        
        //Assert
        expect(returnVersion).toEqual('v6.0.0.5')
    })

    test('getVersion - version return null', ()=>{
        //Arrange

        //Second time of Mocking package.json
        jest.mock('../../../package.json', function() {
            return {
                version: null
            }
        })
        //Import VersionUtil with mocked package.json
        const MockVersionUtil = require('../../main/utils/VersionUtil.js')

        //Act
        let returnVersion = MockVersionUtil.getVersion()
        
        //Assert
        expect(returnVersion).toEqual('0.0.0')
    })

    test('getConfigVersion - normal flow', ()=>{
        //Arrange
        let mockVersion = '6.0.0'
        //Act
        let returnVersion = VersionUtil.getConfigVersion(mockVersion)
        //Assert
        expect(returnVersion).toEqual('v'+mockVersion)
    })

    test('getConfigVersion - applicationVersion return null ', ()=>{
        //Arrange
        let mockVersion = null
        //Act
        let returnVersion = VersionUtil.getConfigVersion(mockVersion)
        //Assert
        expect(returnVersion).toEqual('Nil')
    })

    test('getServiceVersion - normal flow (4 digits)', ()=>{
        //Arrange
        let mockContextPath = 'http://cid-corp-d1:11360/cid-image-common_6_0_0_1'
        //Act
        let returnVersion = VersionUtil.getServiceVersion(mockContextPath)
        //Assert
        expect(returnVersion).toEqual('v6.0.0.1')
    })

    test('getServiceVersion - normal flow (3 digits)', ()=>{
        //Arrange
        let mockContextPath = 'http://cid-corp-d1:11360/cid-image-common_5_2_1'
        //Act
        let returnVersion = VersionUtil.getServiceVersion(mockContextPath)
        //Assert
        expect(returnVersion).toEqual('v5.2.1')
    })

    test('getServiceVersion - no version number in cidWsContextPath', ()=>{
        //Arrange
        let mockContextPath = 'http://cid-corp-d1:11360/cid-image-common'
        //Act
        let returnVersion = VersionUtil.getServiceVersion(mockContextPath)
        //Assert
        expect(returnVersion).toEqual('v3.0.0 (or before)')
    })

    test('getServiceVersion - applicationVersion return null ', ()=>{
        //Arrange
        let mockContextPath = null
        //Act
        let returnVersion = VersionUtil.getServiceVersion(mockContextPath)
        //Assert
        expect(returnVersion).toEqual('Nil')
    })

})