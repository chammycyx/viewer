import { getInitColorAdjustmentValues } from '@utils/ColorAdjustmentUtil'
import { CanvasProfileInitState } from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'

test('Test ColorAdjustmentUtil#getInitColorAdjustmentValues', ()=>{
    //Arrange
    const testingVal1 = {
        selectedImageIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3'],
        canvasProfiles: {
            byId: {
                'imageDtl_1': {
                    brightness: 0,
                    contrast: 0,
                    hue: 0,
                    saturation: 0,
                    invert: false
                },
                'imageDtl_2': {
                    brightness: 0,
                    contrast: 0,
                    hue: 0,
                    saturation: 0,
                    invert: false
                },
                'imageDtl_3': {
                    brightness: 0,
                    contrast: 0,
                    hue: 0,
                    saturation: 0,
                    invert: false
                }
            },
            allIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
        }
    }

    const testingVal2 = {
        selectedImageIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3'],
        canvasProfiles: {
            byId: {
                'imageDtl_1': {
                    brightness: 40,
                    contrast: 40,
                    hue: 40,
                    saturation: 40,
                    invert: true
                },
                'imageDtl_2': {
                    brightness: 40,
                    contrast: 40,
                    hue: 40,
                    saturation: 40,
                    invert: true
                },
                'imageDtl_3': {
                    brightness: 40,
                    contrast: 40,
                    hue: 40,
                    saturation: 40,
                    invert: true
                }
            },
            allIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
        }
    }

    const testingVal3 = {
        selectedImageIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3'],
        canvasProfiles: {
            byId: {
                'imageDtl_1': {
                    brightness: 20,
                    contrast: 20,
                    hue: 20,
                    saturation: 20,
                    invert: false
                },
                'imageDtl_2': {
                    brightness: 10,
                    contrast: 10,
                    hue: 30,
                    saturation: 50,
                    invert: true
                },
                'imageDtl_3': {
                    brightness: 0,
                    contrast: 0,
                    hue: 0,
                    saturation: 0,
                    invert: false
                }
            },
            allIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
        }
    }

    const testingVal4 = {
        selectedImageIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3'],
        canvasProfiles: {
            byId: {
                'imageDtl_1': {
                    brightness: 40,
                    contrast: 40,
                    hue: 40,
                    saturation: 40,
                    invert: true
                },
                'imageDtl_2': {
                    brightness: -20,
                    contrast: -30,
                    hue: -30,
                    saturation: -10,
                    invert: false
                },
                'imageDtl_3': {
                    brightness: 40,
                    contrast: 40,
                    hue: 40,
                    saturation: 40,
                    invert: true
                }
            },
            allIds: ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
        }
    }

    const defaultExpectedResult = {
        brightness: CanvasProfileInitState.brightness,
        contrast: CanvasProfileInitState.contrast,
        hue: CanvasProfileInitState.hue,
        saturation: CanvasProfileInitState.saturation,
        invert: CanvasProfileInitState.invert
    }

    //Act
    const result1 = getInitColorAdjustmentValues(testingVal1.selectedImageIds, testingVal1.canvasProfiles)
    const result2 = getInitColorAdjustmentValues(testingVal2.selectedImageIds, testingVal2.canvasProfiles)
    const result3 = getInitColorAdjustmentValues(testingVal3.selectedImageIds, testingVal3.canvasProfiles)
    const result4 = getInitColorAdjustmentValues(testingVal4.selectedImageIds, testingVal4.canvasProfiles)
    
    //Assert
    //Assert shallow clone
    expect(result1).toEqual(defaultExpectedResult)
    expect(result2).toEqual({
        brightness: 40,
        contrast: 40,
        hue: 40,
        saturation: 40,
        invert: true
    })
    expect(result3).toEqual(defaultExpectedResult)
    expect(result4).toEqual(defaultExpectedResult)
})