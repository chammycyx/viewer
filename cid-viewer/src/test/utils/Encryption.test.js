import Encryption from '../../main/utils/CidMagicUtil'

describe('Test Encryption by ECB', ()=>{
    test('Test Encryption#encrypt', ()=>{
        // let plainTest = 'cid'
        // let encryptedText = Encryption.magicByECB(plainTest, 'CID-IAS-ENCRYKEY')
        let plainTest = 'cid'
        let encryptedText = Encryption.magicByECB(plainTest, 'CID-IAS-ENCRYKEY')
        expect(encryptedText).toBe('QI7lxc/dCbggsyE1uOHjgg==')
    })


    test('Test Encryption#decrypt', ()=>{
        let encryptedText = 'QI7lxc/dCbggsyE1uOHjgg=='
        // let encryptedText = 'ynWBT4GBnlmMThavWXLq1A== '
        let decryptedText = Encryption.unmagicByECB(encryptedText, 'CID-IAS-ENCRYKEY')
        expect(decryptedText).toBe('cid')
    })

    test('Test Encryption.encrypt and Encryption.decrypt', ()=>{
        let key = 'CID-IAS-ENCRYKEY'
        let plainText: Array<string> = ['cid', 'abc', 'cid-ias', 'cid-icw', 'cd2-ias', 'sfdhdsiufgswhioqydrhroiqgfsadkjfgwiuefgksjbvksdjgv']
        plainText.forEach((item)=>{
            let encryptedText = Encryption.magicByECB(item, key)
            let decryptedText = Encryption.unmagicByECB(encryptedText, key)
            expect(decryptedText).toBe(item)
        })
    })
})

// describe('Test Encryption by CBC', ()=>{
//     test('Test Encryption#magicByCBC', ()=>{
//         let plainTest = 'cid'
//         let encryptedText = Encryption.magicByCBC(plainTest, 'CID-IAS-ENCRYKEY')
//         expect(encryptedText).toBe('fMq4SYTRvqG3ZHIS5Ug1cg==')
//     })


//     test('Test Encryption#unmagicByCBC', ()=>{
//         let encryptedText = 'fMq4SYTRvqG3ZHIS5Ug1cg=='
//         let decryptedText = Encryption.unmagicByCBC(encryptedText, 'CID-IAS-ENCRYKEY')
//         expect(decryptedText).toBe('cid')
//     })


//     test('Test Encryption.magicByCBC and Encryption.unmagicByCBC', ()=>{
//         // let key = 'ISGKeyAA-AAAAAA-AAAAAAAA'
//         let key = 'CID-IAS-ENCRYKEY'
//         let plainText: Array<string> = ['cid', 'abc', 'cid-ias', 'cid-icw', 'cd2-ias', 'sfdhdsiufgswhioqydrhroiqgfsadkjfgwiuefgksjbvksdjgv']
//         plainText.forEach((item)=>{
//             let encryptedText = Encryption.magicByCBC(item, key)
//             let decryptedText = Encryption.unmagicByCBC(encryptedText, key)
//             expect(decryptedText).toBe(item)
//         })
//     })
// })