
import {getUrlParameter} from '../../main/utils/HttpUtil'

describe('Test HttpUtil#getUrlParameter',()=>{
    test('Test general Parameter', ()=>{
        jsdom.reconfigure({
            url: "http://www.test.com/test?systemID=RIS&accessKey=1FD2B404-662E-48C2-8351-BDF2C40E8711&loginID=ISG&userHospCde=VH&wrkStnID=dc1cidst02&hospCde=VH&patientKey=90500231&specialtyCde=%40%7B%7D%25%24%5B%5D%3F<>%2F%27%3A-*%21~%23%5E%26&caseNo=HN05000207X&accessionNo=VH+ER1810000412U&seriesSeqNo=+&imageSeqNo=2&versionNo=3&appHost=dc1cidst02&appSiteName=CIDImageViewingTester%2F6_0_0%2FcidImageViewer.html&appName=name"
        });

        expect(getUrlParameter('systemID')).toBe('RIS')
        expect(getUrlParameter('accessKey')).toBe('1FD2B404-662E-48C2-8351-BDF2C40E8711')
        expect(getUrlParameter('accessionNo')).toBe('VH ER1810000412U')
        expect(getUrlParameter('appSiteName')).toBe('CIDImageViewingTester/6_0_0/cidImageViewer.html')
        expect(getUrlParameter('specialtyCde')).toBe('@{}%$[]?<>/\':-*!~#^&')
        // expect(getUrlParameter('seriesSeqNo')).toBe('+')
    })

})