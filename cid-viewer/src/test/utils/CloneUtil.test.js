import {clone} from '../../main/utils/CloneUtil'

test('Test CloneUtil#clone', ()=>{
    //Arrange
    let o1 = {a:1,b:'2',c:{c1:3,c2:'4'}}

    //Act
    let o2 = clone(o1)
    
    //Assert
    //Assert shallow clone
    expect(JSON.stringify(o2)).toBe(JSON.stringify(o1))
    //Assert deep clone
    o1.c.c1 = 5
    expect(o2.c.c1).toBe(3)
})