import {callWs, callWsBatch, getJsonFile} from '../../main/utils/WebServiceBackbone'
import {setReturnObj, setReturnObjOnce, paths, errMsg} from 'axios' // '../__mock__/axios.js'
// import CidError from '@components/base/CidError'

const returnData = [
    {
        data: {
            a:11111,
            b:22
        }
    },
    {   
        data: {
            a:222222,
            b:333
        }
    },
    {   
        data: {
            a:3333333,
            b:444
        }
    },
    {   
        data: {
            a:444,
            b:555555
        }
    }
]


describe('Test Suite: <WebServiceBackbone>', ()=>{
    test('Testing of callWs', async ()=>{
        //Arrange
        let username = 'tester'
        let password = 'testerPW!@#'
        let contextPath = paths[0]
        let endPoint = ''
        let data = {a:1,b:'2',c:{c1:3,c2:'4'}}
        setReturnObj(returnData[0])

        //Act
        let responseObj = await callWs(username, password, contextPath, endPoint, data)
        
        //Assert
        expect(responseObj).toEqual({
            response: returnData[0].data,
            data: data
        })
    })

    test('Testing of callWsBatch', async ()=>{
        //Arrange
        let username = 'tester'
        let password = 'testerPW!@#'
        let contextPath = paths[0]
        let endPoint = ''
        let data = [{a:1,b:'2',c:{c1:3,c2:'4'}},
            {a:2,b:'3',c:{c1:4,c2:'5'}},
            {a:3,b:'4',c:{c1:5,c2:'6'}}]
        setReturnObjOnce(returnData[0])
        setReturnObjOnce(returnData[1])
        setReturnObjOnce(returnData[2])

        //Act
        let responseObj = await callWsBatch(username, password, contextPath, endPoint, data)

        //Assert
        expect(responseObj[0]).toEqual({
            response: returnData[0].data,
            data: data[0]
        })
        expect(responseObj[1]).toEqual({
            response: returnData[1].data,
            data: data[1]
        })
        expect(responseObj[2]).toEqual({
            response: returnData[2].data,
            data: data[2]
        })
    })

    test('Testing of getJsonFile - noCallback', async ()=>{
        // Arrange
        let path = paths[0]
        let timeout = 5000
        let successCallback = undefined
        let errorCallback = undefined
        let completeCallback = undefined
        setReturnObj(returnData[0])

        //Act
        let responseObj = await getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)

        //Assert
        expect(responseObj).toEqual(returnData[0].data)
    })

    test('Testing of getJsonFile - noCallback - Error case', async ()=>{
        // Arrange
        let url = 'Wrong/Path'
        let timeout = 5000
        let successCallback = undefined
        let errorCallback = undefined
        let completeCallback = undefined
        let className = 'src.main.utils.WebServiceBackbone'
        let funcName = 'getJsonFile()'
        setReturnObj(returnData[0])

        let data = ''
        let exceptionMsg = ''
        let status = -1
        let statusText = ''

        //Act
        try {
            await getJsonFile(url, timeout, successCallback, errorCallback, completeCallback)
        } catch (cidHttpErr) {
            //Assert
            let mockedAxiosErr = {error: 'url: ' + url + ' not found.'}
            let msg = className+'.'+funcName+'\n'
            + 'Context Path: ' + url + '\n'
            + 'Status & Text: ' + status + ' ' + statusText + '\n'
            + 'Exception message: ' + exceptionMsg + '\n'
            + 'Data: ' + JSON.stringify(data) + '\n'
            + 'Error Content: ' + JSON.stringify(mockedAxiosErr) + mockedAxiosErr
            
            expect(cidHttpErr.message).toEqual(msg)
        }
    })
    
    test('Testing of getJsonFile - successCallback', (done)=>{
        //Arrange
        let path = paths[0]
        let timeout = 5000
        let successCallback = callback
        let errorCallback = undefined
        let completeCallback = undefined
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)
        
        //Assert
        function callback(data) {
            expect(data).toBe(returnData[0].data)
            done()
        }
    })

    test('Testing of getJsonFile - errorCallback', (done)=>{
        //Arrange
        let path = 'testpath123/Wrongpath'
        let timeout = 5000
        let successCallback = undefined
        let errorCallback = callback
        let completeCallback = undefined
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)

        //Assert
        function callback(err) {
            expect(err).toEqual({"error": "url: " + path + " not found."})
            done()
        }
    })
    
    test('Testing of getJsonFile - completeCallback',  (done)=>{
        //Arrange
        let path = paths[0]
        let timeout = 5000
        let successCallback = undefined
        let errorCallback = undefined
        let completeCallback = callback
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)

        //Assert
        function callback() {
            done()
        }
    })

    test('Testing of getJsonFile - successCallback and errorCallback', (done)=>{
        //Arrange
        let path = paths[0]
        let timeout = 5000
        let successCallback = successCallbackFn
        let errorCallback = errorCallbackFn
        let completeCallback = undefined
        let isDoneErrorCallback = false
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)
        
        //Assert
        function successCallbackFn(data) {
            expect(data).toBe(returnData[0].data)
            expect(isDoneErrorCallback).toBe(false)
            done()
        }
        function errorCallbackFn(err) {
             expect(err).toEqual({"error": "url: " + path + " not found."})
            isDoneErrorCallback = true
        } 
    })

    test('Testing of getJsonFile - successCallback and completeCallback', (done)=>{
        //Arrange
        let path = paths[0]
        let timeout = 5000
        let successCallback = successCallbackFn
        let errorCallback = undefined
        let completeCallback = completeCallbackFn
        let isDoneSuccessCallback = false
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)
        
        //Assert
        function successCallbackFn(data) {
            expect(data).toBe(returnData[0].data)
            isDoneSuccessCallback = true
        }
        function completeCallbackFn() {
            expect(isDoneSuccessCallback).toBe(true)
            done()
        }
    })

    test('Testing of getJsonFile - errorCallback and completeCallback', (done)=>{
        //Arrange
        let path = 'Wrong/path/test'
        let timeout = 5000
        let successCallback = undefined
        let errorCallback = errorCallbackFn
        let completeCallback = completeCallbackFn
        let isDoneErrorCallback = false
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)
        
        //Assert
        function errorCallbackFn(err) {
            expect(err).toEqual({"error": "url: " + path + " not found."})
            isDoneErrorCallback = true
         } 
        function completeCallbackFn() {
            expect(isDoneErrorCallback).toBe(true)
            done()
        }
    })

    test('Testing of getJsonFile - all callback with successful response', (done)=>{
        //Arrange
        let path = paths[0]
        let timeout = 5000
        let successCallback = successCallbackFn
        let errorCallback = errorCallbackFn
        let completeCallback = completeCallbackFn
        let isDoneSuccessCallback = false
        let isDoneErrorCallback = false
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)
       
        //Assert
        function successCallbackFn(data) {
            expect(data).toBe(returnData[0].data)
            isDoneSuccessCallback = true
        }
        function errorCallbackFn(err) {
            expect(err).toEqual({"error": "url: " + path + " not found."})
            isDoneErrorCallback = true
         } 
        function completeCallbackFn() {
            expect(isDoneSuccessCallback).toBe(true)
            expect(isDoneErrorCallback).toBe(false)
            done()
        }
    })

    test('Testing of getJsonFile - all callback with error response', (done)=>{
        //Arrange
        let path = 'Wrong/path/test'
        let timeout = 5000
        let successCallback = successCallbackFn
        let errorCallback = errorCallbackFn
        let completeCallback = completeCallbackFn
        let isDoneSuccessCallback = false
        let isDoneErrorCallback = false
        setReturnObjOnce(returnData[0])

        //Act
        getJsonFile(path, timeout, successCallback, errorCallback, completeCallback)
       
        //Assert
        function successCallbackFn(data) {
            expect(data).toBe(returnData[0].data)
            isDoneSuccessCallback = true
        }
        function errorCallbackFn(err) {
            expect(err).toEqual({"error": "url: " + path + " not found."})
            isDoneErrorCallback = true
         } 
        function completeCallbackFn() {
            expect(isDoneSuccessCallback).toBe(false)
            expect(isDoneErrorCallback).toBe(true)
            done()
        }
    })
})
