import {AuthService} from '@domains/services/authService/AuthService'
import { ACCESS_AUTHENTICATION_FAILED, APPLICATION_VERSION_FAILED, EJB_EXCEPTION} from '@src/main/constants/MsgCodeConstants'
import { callWs } from '@utils/WebServiceBackbone'
jest.mock('@utils/WebServiceBackbone')//Note that calling jest.mock(‘./dependency’) will replace all exported functions of dependency.js with mock functions.

describe('Test Class: <AuthService>', ()=>{
	describe('Test Function: <AuthService#validateAccessKeyByHostName>', ()=>{
        test('Test true case for <AuthService#validateAccessKeyByHostName>', async ()=>{
            //Arrange
            callWs.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    resolve({response:"true"})
                })
            })

            //Act
            let authService = new AuthService(jest.fn())
            let actual = await authService.validateAccessKeyByHostName()

            //Assert
            expect(actual).toBeTruthy()
        })

        test('Test false case for <AuthService#validateAccessKeyByHostName>', async ()=>{
            //Arrange
            callWs.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    resolve({response:"false"})
                })
            })
            let mockedDataRepository = jest.fn()
            //     mockedDataRepository.setMsgDialogByCode = jest.fn()
            //     mockedDataRepository.setAppMask = jest.fn()

            //Act
            let authService = new AuthService(jest.fn(), mockedDataRepository)
            let actual = await authService.validateAccessKeyByHostName()

            //Assert
            // expect(mockedDataRepository.setMsgDialogByCode.mock.calls.length).toBe(1)
            // expect(mockedDataRepository.setAppMask.mock.calls.length).toBe(1)
            expect(actual).toBeFalsy()
        })

        test('Test Exception case for <AuthService#validateAccessKeyByHostName>', async ()=>{
            //Arrange
            callWs.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    reject("ErrorABC")
                })
            })
            let mockedDataRepository = jest.fn()
                // mockedDataRepository.setMsgDialogByCode = jest.fn()
                // mockedDataRepository.setAppMask = jest.fn()

            //Act
            let authService = new AuthService(jest.fn(), mockedDataRepository)
            try{
                await authService.validateAccessKeyByHostName()
            }catch(error){
                //Assert
                // expect(mockedDataRepository.setMsgDialogByCode.mock.calls.length).toBe(1)
                // expect(mockedDataRepository.setAppMask.mock.calls.length).toBe(1)
                return
            }
            
            expect(true).toBeFalsy()
        })
    })

    describe('Test Function: <AuthService#validateApplicationStatus>', ()=>{
        test('Test true case for <AuthService#validateApplicationStatus>', async ()=>{
            //Arrange
            callWs.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    resolve({response:"true"})
                })
            })

            //Act
            let authService = new AuthService(jest.fn())
            let actual = await authService.validateApplicationStatus()

            //Assert
            expect(actual).toBeTruthy()
        })

        test('Test false case for <AuthService#validateApplicationStatus>', async ()=>{
            //Arrange
            callWs.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    resolve({response:"false"})
                })
            })
            let mockedDataRepository = jest.fn()
                // mockedDataRepository.setMsgDialogByCode = jest.fn()
                // mockedDataRepository.setAppMask = jest.fn()

            //Act
            let authService = new AuthService(jest.fn(), mockedDataRepository)
            let actual = await authService.validateApplicationStatus()

            //Assert
            // expect(mockedDataRepository.setMsgDialogByCode.mock.calls.length).toBe(1)
            // expect(mockedDataRepository.setAppMask.mock.calls.length).toBe(1)
            expect(actual).toBeFalsy()
        })

        test('Test Exception case for <AuthService#validateApplicationStatus>', async ()=>{
            //Arrange
            callWs.mockImplementationOnce(()=>{
                return new Promise((resolve, reject) => {
                    reject("ErrorABC")
                })
            })
            let mockedDataRepository = jest.fn()
                // mockedDataRepository.setMsgDialogByCode = jest.fn()
                // mockedDataRepository.setAppMask = jest.fn()

            //Act
            let authService = new AuthService(jest.fn(), mockedDataRepository)
            try{
                await authService.validateApplicationStatus()
            }catch(error){
                //Assert
                // expect(mockedDataRepository.setMsgDialogByCode.mock.calls.length).toBe(1)
                // expect(mockedDataRepository.setAppMask.mock.calls.length).toBe(1)
                return
            }
            expect(true).toBeFalsy()
            
        })
    })
})