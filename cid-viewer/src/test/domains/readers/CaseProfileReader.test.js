import {caseProfileReader, CaseProfileType} from '../../../main/domains/readers/CaseProfileReader'

test('Test CaseProfileReader#readCaseProfileFromQueryString', ()=>{
    jsdom.reconfigure({
        url: "http://www.test.com/test?systemID=RIS&accessKey=1FD2B404-662E-48C2-8351-BDF2C40E8711&loginID=ISG&userHospCde=VH&wrkStnID=dc1cidst02&hospCde=VH&patientKey=90500231&specialtyCde=%40%7B%7D%25%24%5B%5D%3F<>%2F%27%3A-*%21~%23%5E%26&caseNo=HN05000207X&accessionNo=VH+ER1810000412U&seriesSeqNo=1&imageSeqNo=2&versionNo=3&appHost=dc1cidst02&appSiteName=CIDImageViewingTester%2F6_0_0%2FcidImageViewer.html&appName=name"
        // url: "http://localhost:7778/cidImageViewer.html#iGq5B3g5n%2B2IHG0n9WTW5X3ncOYmTMuqD6oP6fd2ZZLbYp5GTBy1ExsRsHPz%2BazWGKrT%2B6v8Yz8HlpfIc5AoQTgEICRHQKCVwHhEaEyR12cKXIJuOGjJ6Al8nR2UHEc1YzPwhujaY%2FE44zvt0w%2BUpfQxG%2BNCl6Si0D26F7Fii8mz3MoUkmL4oPDzbwRTlesvPkZnbLYH%2BrLm3kPRaTkzPBpQQjfKQ5gkf2T0a2eQMK1F5sI%2B3blTrGzZg6X5n9jcb%2Fbmw6JexNwJ2aFiJha7YHFf54rWjDQ6d1KtzyFQkjLFQfbjf5uAMNluIgMt4FvkQOgsBvQQwk%2B%2BHoz%2BIY9tBnkOHn8m5dtDXLN3ziLrBII7E93tubUGPmkJeQuqTTe9DTr1l0%2FclCWGVoe6yDyFVLxBZAICMtAzS0coYi2HlQ1cIyL0ac8R8r5xzxJUJNp1kIJ75FCDB6D7b7obWF3kp72UOoHiCYrbyTKuFyS39cV7zOBn43BWOVsK4Olxj8BX8s%2B7T%2BenUb9tGKpGwj2jgjpY7C4mrbP0ouTcBMaZjyb6LSgNhm2XbadxjpbnsQldEQx8A%2BE3ubw0bk%2FWnOYRrQ%3D%3D"
    })

    let caseProfile: CaseProfileType = caseProfileReader.readCaseProfileFromQueryString()
    expect(caseProfile.systemID).toBe('RIS')
    expect(caseProfile.accessKey).toBe('1FD2B404-662E-48C2-8351-BDF2C40E8711')
    expect(caseProfile.loginID).toBe('ISG')
    expect(caseProfile.userHospCde).toBe('VH')
    expect(caseProfile.wrkStnID).toBe('dc1cidst02')
    expect(caseProfile.hospCde).toBe('VH')
    expect(caseProfile.patientKey).toBe('90500231')
    expect(caseProfile.specialtyCde).toBe('@{}%$[]?<>/\':-*!~#^&')
    expect(caseProfile.caseNo).toBe('HN05000207X')
    expect(caseProfile.accessionNo).toBe('VH ER1810000412U')
    expect(caseProfile.seriesSeqNo).toBe('1')
    expect(caseProfile.imageSeqNo).toBe('2')
    expect(caseProfile.versionNo).toBe('3')
    expect(caseProfile.appHost).toBe('dc1cidst02')
    expect(caseProfile.appSiteName).toBe('CIDImageViewingTester/6_0_0/cidImageViewer.html')
    expect(caseProfile.appName).toBe('name')
})