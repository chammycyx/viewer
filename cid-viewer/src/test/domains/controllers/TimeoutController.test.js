import TimeoutController from '@domains/controllers/rootApp/TimeoutController'
import dataRepository from '@utils/DataRepository'
import logger from '@src/main/utils/Logger'
import { RESIZE, MOUSE_DOWN, WHEEL, MOUSE_MOVE, KEY_DOWN, FOCUS } from '@constants/WindowEventConstants'
import { TAB, LEFT, UP, RIGHT, DOWN } from '@constants/KeyCodeConstants'

beforeEach(() => {
    jest.useFakeTimers()
})

describe('TimeoutController', ()=>{
    test('Test addEventListeners of TimeoutController', ()=>{
        //Arrange mock object
        window.addEventListener = jest.fn()
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.addEventListeners()

        //Assert
        expect(window.addEventListener).toHaveBeenCalledTimes(6)
        expect(window.addEventListener).toHaveBeenCalledWith(MOUSE_DOWN, timeoutController.resetTimerHandler, false)
        expect(window.addEventListener).toHaveBeenCalledWith(WHEEL, timeoutController.resetTimerHandler, false)
        expect(window.addEventListener).toHaveBeenCalledWith(MOUSE_MOVE, timeoutController.resetTimer4MouseMoveHandler, false)
        expect(window.addEventListener).toHaveBeenCalledWith(KEY_DOWN, timeoutController.resetTimer4KeyDownHandler, false)
        expect(window.addEventListener).toHaveBeenCalledWith(RESIZE, timeoutController.resetTimerHandler, false)
        expect(window.addEventListener).toHaveBeenCalledWith(FOCUS, timeoutController.resetTimer4FocusHandler, false)
    })

    test('Test removeEventListeners of TimeoutController', ()=>{
        //Arrange mock object
        window.removeEventListener = jest.fn()
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.removeEventListeners()

        //Assert
        expect(window.removeEventListener).toHaveBeenCalledTimes(6)
        expect(window.removeEventListener).toHaveBeenCalledWith(MOUSE_DOWN, timeoutController.resetTimerHandler)
        expect(window.removeEventListener).toHaveBeenCalledWith(WHEEL, timeoutController.resetTimerHandler)
        expect(window.removeEventListener).toHaveBeenCalledWith(MOUSE_MOVE, timeoutController.resetTimer4MouseMoveHandler)
        expect(window.removeEventListener).toHaveBeenCalledWith(KEY_DOWN, timeoutController.resetTimer4KeyDownHandler)
        expect(window.removeEventListener).toHaveBeenCalledWith(RESIZE, timeoutController.resetTimerHandler)
        expect(window.removeEventListener).toHaveBeenCalledWith(FOCUS, timeoutController.resetTimer4FocusHandler)
    })

    const createTestInitTimeout = function(idleTimeout, isInitTimeoutSuccess) {
        //Arrange mock object
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return {idleTimeout}
        })
        let mockResetTimerHandler = jest.fn()
        let mockAddEventListeners = jest.fn()
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.resetTimerHandler = mockResetTimerHandler
        timeoutController.addEventListeners = mockAddEventListeners
        timeoutController.initTimeout()

        //Assert
        if(isInitTimeoutSuccess) {
            expect(mockResetTimerHandler).toHaveBeenCalledTimes(1)
            expect(mockAddEventListeners).toHaveBeenCalledTimes(1)
        } else {
            expect(mockResetTimerHandler).toHaveBeenCalledTimes(0)
            expect(mockAddEventListeners).toHaveBeenCalledTimes(0)
        }
    }

    test('Test initTimeout of TimeoutController when idleTimeout is 20', ()=>{
        createTestInitTimeout(20, true)
    })

    test('Test initTimeout of TimeoutController when idleTimeout is 0', ()=>{
        createTestInitTimeout(0, false)
    })

    test('Test initTimeout of TimeoutController when idleTimeout is <string>', ()=>{
        createTestInitTimeout('test', false)
    })

    test('Test initTimeout of TimeoutController when idleTimeout is -2', ()=>{
        createTestInitTimeout(-2, false)
    })

    const createTestResetTimerHandler = function(timerId, isClearIntervalCalled) {
        //Arrange mock object
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.timerId = timerId
        timeoutController.resetTimerHandler()

        //Assert
        if(isClearIntervalCalled) {
            expect(clearInterval).toHaveBeenCalledTimes(1)
        }
        expect(setInterval).toHaveBeenCalledTimes(1)
        expect(timeoutController.timerId).not.toEqual(timerId)
    }

    test('Test resetTimerHandler of TimeoutController when timer is set before', ()=>{
        const testTimerId = 80
        createTestResetTimerHandler(testTimerId, true)
    })

    test('Test resetTimerHandler of TimeoutController when timer is not set before', ()=>{
        createTestResetTimerHandler(undefined, false)
    })

    test('Test resetTimerHandler of TimeoutController when timer is null', ()=>{
        createTestResetTimerHandler(null, false)
    })

    const createTestResetTimer4MouseMoveHandler = function(lastMouseMoveEvent, event, isResetTimerHandler) {
        //Arrange mock object
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.resetTimerHandler = jest.fn()
        timeoutController.lastMouseMoveEvent = lastMouseMoveEvent
        timeoutController.resetTimer4MouseMoveHandler(event)

        //Assert
        if(isResetTimerHandler)
            expect(timeoutController.resetTimerHandler).toHaveBeenCalledTimes(1)
    }

    test('Test resetTimer4MouseMoveHandler of TimeoutController when last x & y are different from new x & y', ()=>{
        const lastMouseMoveEvent = {
            screenX: 10,
            screenY: 20
        }
        const event = {
            screenX: 6,
            screenY: 511
        }
        createTestResetTimer4MouseMoveHandler(lastMouseMoveEvent, event, true)
    })

    test('Test resetTimer4MouseMoveHandler of TimeoutController when last x is different from new x', ()=>{
        const lastMouseMoveEvent = {
            screenX: 10,
            screenY: 20
        }
        const event = {
            screenX: 676,
            screenY: 20
        }
        createTestResetTimer4MouseMoveHandler(lastMouseMoveEvent, event, true)
    })

    test('Test resetTimer4MouseMoveHandler of TimeoutController when last y is different from new y', ()=>{
        const lastMouseMoveEvent = {
            screenX: 191,
            screenY: 24
        }
        const event = {
            screenX: 191,
            screenY: 587
        }
        createTestResetTimer4MouseMoveHandler(lastMouseMoveEvent, event, true)
    })

    test('Test resetTimer4MouseMoveHandler of TimeoutController when lastMouseMoveEvent is null', ()=>{
        const lastMouseMoveEvent = null
        const event = {
            screenX: 10,
            screenY: 90
        }
        createTestResetTimer4MouseMoveHandler(lastMouseMoveEvent, event, true)
    })

    test('Test resetTimer4MouseMoveHandler of TimeoutController when last x & y are the same with new x & y', ()=>{
        const lastMouseMoveEvent = {
            screenX: 10,
            screenY: 20
        }
        const event = {
            screenX: 10,
            screenY: 20
        }
        createTestResetTimer4MouseMoveHandler(lastMouseMoveEvent, event, false)
    })

    const createTestResetTimer4KeyDownHandler = function(event, isResetTimerHandler) {
        //Arrange mock object
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.resetTimerHandler = jest.fn()
        timeoutController.resetTimer4KeyDownHandler(event)

        //Assert
        if(isResetTimerHandler)
            expect(timeoutController.resetTimerHandler).toHaveBeenCalledTimes(1)
    }

    test('Test resetTimer4KeyDownHandler of TimeoutController when TAB is pressed', ()=>{
        const event = {
            keyCode: TAB
        }
        createTestResetTimer4KeyDownHandler(event, false)
    })

    test('Test resetTimer4KeyDownHandler of TimeoutController when LEFT is pressed', ()=>{
        const event = {
            keyCode: LEFT
        }
        createTestResetTimer4KeyDownHandler(event, false)
    })

    test('Test resetTimer4KeyDownHandler of TimeoutController when UP is pressed', ()=>{
        const event = {
            keyCode: UP
        }
        createTestResetTimer4KeyDownHandler(event, false)
    })

    test('Test resetTimer4KeyDownHandler of TimeoutController when RIGHT is pressed', ()=>{
        const event = {
            keyCode: RIGHT
        }
        createTestResetTimer4KeyDownHandler(event, false)
    })

    test('Test resetTimer4KeyDownHandler of TimeoutController when DOWN is pressed', ()=>{
        const event = {
            keyCode: DOWN
        }
        createTestResetTimer4KeyDownHandler(event, false)
    })

    test('Test timeoutHandler of TimeoutController', ()=>{
        //Arrange mock object
        dataRepository.setSlideShowToolsStop = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.setAppMask = jest.fn()
        logger.system = jest.fn()
        const testTimeId = "40"
        
        //Act
        let timeoutController = new TimeoutController()
        timeoutController.timerId = testTimeId
        timeoutController.removeEventListeners = jest.fn()
        timeoutController.timeoutHandler()

        //Assert
        expect(clearInterval).toHaveBeenCalledTimes(1)
        expect(clearInterval).toHaveBeenCalledWith(testTimeId)
        expect(dataRepository.setSlideShowToolsStop).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
        expect(dataRepository.setAppMask).toHaveBeenCalledTimes(1)
        expect(logger.system).toHaveBeenCalledTimes(1)
    })
})