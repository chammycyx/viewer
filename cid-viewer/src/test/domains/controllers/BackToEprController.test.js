import BackToEprController from '../../../main/domains/controllers/BackToEprController'
describe('BackToEprBtn', ()=>{
    test('Test BackToEprBtn when window.opener is window', ()=>{

        //Arrange mock object
        window.opener = jest.fn()
        window.opener.focus = jest.fn()
        window.blur = jest.fn()
        
        //Act
        BackToEprController.backToEpr()

        //Assert
        expect(window.opener.focus).toHaveBeenCalledTimes(1)
        expect(window.blur).toHaveBeenCalledTimes(1)
        
    })
    
    test('Test BackToEprBtn when window.opener is null', ()=>{

        //Arrange mock object
        window.opener = null
        window.blur = jest.fn()

        //Act
        BackToEprController.backToEpr()

        //Assert
        expect(window.opener).toBeNull()
        expect(window.blur).toHaveBeenCalledTimes(1)

    })

})