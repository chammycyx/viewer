import CidHttpError from '@components/base/CidHttpError'

// import utils
import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'

// import controllers
import AuditLogController from '@domains/controllers/rootApp/AuditLogController'
import ExportImageController from '@domains/controllers/ExportImageController'

// import services
import StudyService from '@domains/services/studyService/StudyService'

// import constants
import { DOWNLOAD_EXPORTED_IMAGE } from '@constants/WordingConstants'
import { EVENT_EXPORT_IMAGE } from '@constants/EventConstants'
import { EJB_EXCEPTION, CID_IAS_WEBSERVICE_TIMEOUT,
    CID_IAS_AUTHEN_UNAVAILABLE } from '@constants/MsgCodeConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'


describe('Test Suite: <ExportImageController>', async () => {

    let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})

    describe('Test Function: <ExportImageController#exportImage>', async () => {
        test('+++ normal export image', async () => {

            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            const seriesDtl = {
                seriesNo: 'testingSeriesNo'
            }
            const imageIds = ['imageId_1', 'imageId_2', 'imageId_3']
            const response = 'base64forexportimage'
            const data = 'testingData'            
            const timeout = 5000
            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockReturnValue(imageIds)
            dataRepository.findExportImageParam = jest.fn().mockReturnValue(data)
            dataRepository.setExportImageDialogForDownload = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()
            dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(timeout)

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.error = jest.fn()

            AuditLogController.prototype.writeIcwLog = jest.fn().mockImplementation(()=>{

            })

            StudyService.prototype.retrieveImageForExport = jest.fn().mockImplementation(()=>{
                return new Promise((resolve, reject) => {
                    resolve({response, data})
                })
            })
            
            // Act
            let exportImageController = new ExportImageController()
            let auditLogController = new AuditLogController()
            let webSInfo = dataRepository.findIasWsAuthInfo()
            let studyService = new StudyService(webSInfo)
            exportImageController.setStudyService(studyService)
            exportImageController.setAuditLogController(auditLogController)
            await exportImageController.exportImage(selectedSeriesId, selectedImageIds, password)

            // Assert
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledWith(selectedSeriesId)
            expect(dataRepository.findImageIds).toHaveBeenCalledTimes(1)
            expect(dataRepository.findImageIds).toHaveBeenCalledWith(selectedSeriesId, selectedImageIds)
            expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)
            expect(dataRepository.setProgressBar.mock.calls[0]).toEqual([true, DOWNLOAD_EXPORTED_IMAGE])
            expect(dataRepository.setProgressBar).toHaveBeenLastCalledWith(false, DOWNLOAD_EXPORTED_IMAGE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(0)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(dataRepository.findExportImageParam).toHaveBeenCalledTimes(1)
            expect(dataRepository.findExportImageParam).toHaveBeenCalledWith(selectedSeriesId, selectedImageIds, password)
            expect(dataRepository.setExportImageDialogForDownload).toHaveBeenCalledTimes(1)
            expect(dataRepository.setExportImageDialogForDownload).toHaveBeenCalledWith(true, response)
            
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledTimes(1)
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledWith(data, timeout)
            expect(AuditLogController.prototype.writeIcwLog).toHaveBeenCalledTimes(1)
            expect(AuditLogController.prototype.writeIcwLog).toHaveBeenCalledWith(EVENT_EXPORT_IMAGE, JSON.stringify(data))
            
            expect(logger.error).toHaveBeenCalledTimes(0)
            expect(logger.userActions).toHaveBeenCalledTimes(1)
            expect(logger.userActionsPerf).toHaveBeenCalledTimes(2)
            expect(logger.servicePerf).toHaveBeenCalledTimes(2)
        })

        test('+++ failed export image with failed study service retrieval', async () => {

            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            const seriesDtl = {
                seriesNo: 'testingSeriesNo'
            }
            const imageIds = ['imageId_1', 'imageId_2', 'imageId_3']
            const data = 'testingData'            
            const timeout = 5000
            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockReturnValue(imageIds)
            dataRepository.findExportImageParam = jest.fn().mockReturnValue(data)
            dataRepository.setExportImageDialogForDownload = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()
            dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(timeout)

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.error = jest.fn()

            AuditLogController.prototype.writeIcwLog = jest.fn().mockImplementation(()=>{

            })

            StudyService.prototype.retrieveImageForExport = jest.fn().mockImplementation(()=>{
                throw mockCidHttpError
            })
            
            // Act
            let exportImageController = new ExportImageController()
            let auditLogController = new AuditLogController()
            let webSInfo = dataRepository.findIasWsAuthInfo()
            let studyService = new StudyService(webSInfo)
            exportImageController.setStudyService(studyService)
            exportImageController.setAuditLogController(auditLogController)

            try {
                await exportImageController.exportImage(selectedSeriesId, selectedImageIds, password)
            } catch(cidHttpErr) {
                expect(cidHttpErr).toEqual(mockCidHttpError)
            }
            

            // Assert
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledWith(selectedSeriesId)
            expect(dataRepository.findImageIds).toHaveBeenCalledTimes(1)
            expect(dataRepository.findImageIds).toHaveBeenCalledWith(selectedSeriesId, selectedImageIds)
            expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)
            expect(dataRepository.setProgressBar.mock.calls[0]).toEqual([true, DOWNLOAD_EXPORTED_IMAGE])
            expect(dataRepository.setProgressBar).toHaveBeenLastCalledWith(false, DOWNLOAD_EXPORTED_IMAGE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(dataRepository.findExportImageParam).toHaveBeenCalledTimes(1)
            expect(dataRepository.findExportImageParam).toHaveBeenCalledWith(selectedSeriesId, selectedImageIds, password)
            expect(dataRepository.setExportImageDialogForDownload).toHaveBeenCalledTimes(0)
            
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledTimes(1)
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledWith(data, timeout)
            expect(StudyService.prototype.retrieveImageForExport).toThrow(mockCidHttpError)
            expect(AuditLogController.prototype.writeIcwLog).toHaveBeenCalledTimes(0)
            
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.userActions).toHaveBeenCalledTimes(1)
            expect(logger.userActionsPerf).toHaveBeenCalledTimes(1)
            expect(logger.servicePerf).toHaveBeenCalledTimes(1)
        })

        test('+++ successful export image with failed ICW write log', async () => {

            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            const seriesDtl = {
                seriesNo: 'testingSeriesNo'
            }
            const imageIds = ['imageId_1', 'imageId_2', 'imageId_3']
            const response = 'base64forexportimage'
            const data = 'testingData'            
            const timeout = 5000
            
            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockReturnValue(imageIds)
            dataRepository.findExportImageParam = jest.fn().mockReturnValue(data)
            dataRepository.setExportImageDialogForDownload = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()
            dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(timeout)

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.error = jest.fn()

            AuditLogController.prototype.writeIcwLog = jest.fn().mockImplementation(()=>{
                throw mockCidHttpError
            })

            StudyService.prototype.retrieveImageForExport = jest.fn().mockImplementation(()=>{
                return new Promise((resolve, reject) => {
                    resolve({response, data})
                })
            })
            
            // Act
            let exportImageController = new ExportImageController()
            let auditLogController = new AuditLogController()
            let webSInfo = dataRepository.findIasWsAuthInfo()
            let studyService = new StudyService(webSInfo)
            exportImageController.setStudyService(studyService)
            exportImageController.setAuditLogController(auditLogController)

            try{
                await exportImageController.exportImage(selectedSeriesId, selectedImageIds, password)
            } catch(cidHttpErr) {
                expect(cidHttpErr).toEqual(mockCidHttpError)
            }

            // Assert
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledWith(selectedSeriesId)
            expect(dataRepository.findImageIds).toHaveBeenCalledTimes(1)
            expect(dataRepository.findImageIds).toHaveBeenCalledWith(selectedSeriesId, selectedImageIds)
            expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)
            expect(dataRepository.setProgressBar.mock.calls[0]).toEqual([true, DOWNLOAD_EXPORTED_IMAGE])
            expect(dataRepository.setProgressBar).toHaveBeenLastCalledWith(false, DOWNLOAD_EXPORTED_IMAGE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(0)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(dataRepository.findExportImageParam).toHaveBeenCalledTimes(1)
            expect(dataRepository.findExportImageParam).toHaveBeenCalledWith(selectedSeriesId, selectedImageIds, password)
            expect(dataRepository.setExportImageDialogForDownload).toHaveBeenCalledTimes(1)
            expect(dataRepository.setExportImageDialogForDownload).toHaveBeenCalledWith(true, response)
            
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledTimes(1)
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledWith(data, timeout)
            expect(AuditLogController.prototype.writeIcwLog).toHaveBeenCalledTimes(1)
            expect(AuditLogController.prototype.writeIcwLog).toHaveBeenCalledWith(EVENT_EXPORT_IMAGE, JSON.stringify(data))
            expect(AuditLogController.prototype.writeIcwLog).toThrow(mockCidHttpError)

            expect(logger.error).toHaveBeenCalledTimes(0)
            expect(logger.userActions).toHaveBeenCalledTimes(1)
            expect(logger.userActionsPerf).toHaveBeenCalledTimes(2)
            expect(logger.servicePerf).toHaveBeenCalledTimes(2)
        })

        test('+++ failed export image - Timeout', async () => {

            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            const seriesDtl = {
                seriesNo: 'testingSeriesNo'
            }
            const imageIds = ['imageId_1', 'imageId_2', 'imageId_3']
            const response = 'base64forexportimage'
            const data = 'testingData'            
            const timeout = 5000
            
            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockReturnValue(imageIds)
            dataRepository.findExportImageParam = jest.fn().mockReturnValue(data)
            dataRepository.setExportImageDialogForDownload = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()
            dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(timeout)

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.errorWithMsgCode = jest.fn()

            AuditLogController.prototype.writeIcwLog = jest.fn().mockImplementation(()=>{
                
            })

            StudyService.prototype.retrieveImageForExport = jest.fn().mockImplementation(()=>{
                mockCidHttpError.isTimeout = true
                throw mockCidHttpError
            })
            
            // Act
            let exportImageController = new ExportImageController()
            let auditLogController = new AuditLogController()
            let webSInfo = dataRepository.findIasWsAuthInfo()
            let studyService = new StudyService(webSInfo)
            exportImageController.setStudyService(studyService)
            exportImageController.setAuditLogController(auditLogController)

            try{
                await exportImageController.exportImage(selectedSeriesId, selectedImageIds, password)
            } catch(cidHttpErr) {
                expect(cidHttpErr).toEqual(mockCidHttpError)
            }

            // Assert
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledTimes(1)
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledWith(data, timeout)

            expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)
            expect(dataRepository.setProgressBar).toHaveBeenLastCalledWith(false, DOWNLOAD_EXPORTED_IMAGE)
            expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenLastCalledWith(CID_IAS_WEBSERVICE_TIMEOUT)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenLastCalledWith(CID_IAS_WEBSERVICE_TIMEOUT)
        })

        test('+++ failed export image - IAS authen fail', async () => {
            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            const seriesDtl = {
                seriesNo: 'testingSeriesNo'
            }
            const imageIds = ['imageId_1', 'imageId_2', 'imageId_3']
            const response = 'base64forexportimage'
            const data = 'testingData'            
            const timeout = 15000
            
            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockReturnValue(imageIds)
            dataRepository.findExportImageParam = jest.fn().mockReturnValue(data)
            dataRepository.setExportImageDialogForDownload = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()
            dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(timeout)

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.errorWithMsgCode = jest.fn()

            AuditLogController.prototype.writeIcwLog = jest.fn().mockImplementation(()=>{
                
            })

            StudyService.prototype.retrieveImageForExport = jest.fn().mockImplementation(()=>{
                let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})
                mockCidHttpError.status = UNAUTHORIZED
                throw mockCidHttpError
            })
            
            // Act
            let exportImageController = new ExportImageController()
            let auditLogController = new AuditLogController()
            let webSInfo = dataRepository.findIasWsAuthInfo()
            let studyService = new StudyService(webSInfo)
            exportImageController.setStudyService(studyService)
            exportImageController.setAuditLogController(auditLogController)

            try{
                await exportImageController.exportImage(selectedSeriesId, selectedImageIds, password)
            } catch(cidHttpErr) {
                expect(cidHttpErr).toEqual(mockCidHttpError)
            }

            // Assert
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledTimes(1)
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledWith(data, timeout)

            expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)
            expect(dataRepository.setProgressBar).toHaveBeenLastCalledWith(false, DOWNLOAD_EXPORTED_IMAGE)
            expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenLastCalledWith(CID_IAS_AUTHEN_UNAVAILABLE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenLastCalledWith(CID_IAS_AUTHEN_UNAVAILABLE)
        })

        test('+++ failed export image - EJBException - IAS user is not is ias authen group', async () => {
            // Arrange
            const selectedSeriesId = 'seriesDtl_1'
            const selectedImageIds = ['imageDtl_1', 'imageDtl_2', 'imageDtl_3']
            const password = 'testingPassword'

            const seriesDtl = {
                seriesNo: 'testingSeriesNo'
            }
            const imageIds = ['imageId_1', 'imageId_2', 'imageId_3']
            const response = 'base64forexportimage'
            const data = 'testingData'            
            const timeout = 15000
            
            // Mock implementations
            dataRepository.findIasWsAuthInfo = jest.fn()
            dataRepository.findSeriesDtl = jest.fn().mockReturnValue(seriesDtl)
            dataRepository.findImageIds = jest.fn().mockReturnValue(imageIds)
            dataRepository.findExportImageParam = jest.fn().mockReturnValue(data)
            dataRepository.setExportImageDialogForDownload = jest.fn()
            dataRepository.setProgressBar = jest.fn()
            dataRepository.setMsgDialogByCode = jest.fn()
            dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(timeout)

            logger.userActions = jest.fn()
            logger.userActionsPerf = jest.fn()
            logger.servicePerf = jest.fn()
            logger.errorWithMsgCode = jest.fn()

            AuditLogController.prototype.writeIcwLog = jest.fn().mockImplementation(()=>{
                
            })
            const exceptionMsg = "exceptionMsgTesting"
            StudyService.prototype.retrieveImageForExport = jest.fn().mockImplementation(()=>{
                let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})
                mockCidHttpError.exceptionMsg = exceptionMsg
                throw mockCidHttpError
            })
            
            // Act
            let exportImageController = new ExportImageController()
            let auditLogController = new AuditLogController()
            let webSInfo = dataRepository.findIasWsAuthInfo()
            let studyService = new StudyService(webSInfo)
            exportImageController.setStudyService(studyService)
            exportImageController.setAuditLogController(auditLogController)

            try{
                await exportImageController.exportImage(selectedSeriesId, selectedImageIds, password)
            } catch(cidHttpErr) {
                expect(cidHttpErr).toEqual(mockCidHttpError)
            }

            // Assert
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledTimes(1)
            expect(StudyService.prototype.retrieveImageForExport).toHaveBeenCalledWith(data, timeout)

            expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)
            expect(dataRepository.setProgressBar).toHaveBeenLastCalledWith(false, DOWNLOAD_EXPORTED_IMAGE)
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledWith(exceptionMsg, EJB_EXCEPTION)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(EJB_EXCEPTION, exceptionMsg)
        })
    })
})