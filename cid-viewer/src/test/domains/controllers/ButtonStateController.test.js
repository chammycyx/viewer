
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { ThumbnailGridActionTypes } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { SlideShowToolsActionTypes } from '@actions/components/toolBarPanel/slideShowTools/Action'
import { BtnStateWatcherActionTypes } from '@actions/components/root/watcher/btnStateWatcher/Action'
import { ButtonStateController, initBtnState, normalBtnState, 
        playSlideshowBtnState, pauseSlideshowBtnState,
        SlideshowMode_multiSelectedImageBtnState, 
        SlideshowMode_deselectedImageBtnState,
        multiSelectedImageBtnState } from '@domains/controllers/ButtonStateController'
import dataRepository from '@utils/DataRepository'

configure({ adapter: new Adapter() })

describe('Test Suite: <ButtonStateController>', () => {
    test('Viewer Deselect All Images', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 0,
            slideShowMode: false,
            isPlayingslideShow: false
        }

        dataRepository.setToolbarBtnState = jest.fn()
        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(initBtnState)
    })

    test('Pressed Play Button', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 1,
            slideShowMode: true,
            isPlayingslideShow: true
        }

        dataRepository.setToolbarBtnState = jest.fn()

        const buttonStateController = new ButtonStateController()

        //Act
        buttonStateController.setToolbarBtnState(SlideShowToolsActionTypes.PLAY, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(playSlideshowBtnState)
    })
    
    test('Pressed Pause Button', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 1,
            slideShowMode: true,
            isPlayingslideShow: false
        }

        dataRepository.setToolbarBtnState = jest.fn()
        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(SlideShowToolsActionTypes.PAUSE, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(pauseSlideshowBtnState)
    })
    
    test('Pressed Image in Pictorial Index Panel', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 1,
            slideShowMode: false,
            isPlayingslideShow: false
        }

        dataRepository.setToolbarBtnState = jest.fn()
        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(ThumbnailGridActionTypes.ON_THUMBNAIL_CLICK, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(normalBtnState)
    })
    
    test('Multiselect Images (No slideshow)', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 2,
            slideShowMode: false,
            isPlayingslideShow: false
        }

        dataRepository.setToolbarBtnState = jest.fn()
        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(multiSelectedImageBtnState)
    })

    test('Deselect image when playing slideshow', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 0,
            slideShowMode: true,
            isPlayingslideShow: true
        }

        dataRepository.setToolbarBtnState = jest.fn()

        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(playSlideshowBtnState)
    })

    test('Select image when playing slideshow', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 1,
            slideShowMode: true,
            isPlayingslideShow: true
        }

        dataRepository.setToolbarBtnState = jest.fn()

        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(playSlideshowBtnState)
    })

    test('Multi-select image when playing slideshow', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 4,
            slideShowMode: true,
            isPlayingslideShow: true
        }

        dataRepository.setToolbarBtnState = jest.fn()
        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(playSlideshowBtnState)
    })

    test('Deselect image when pause slideshow', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 0,
            slideShowMode: true,
            isPlayingslideShow: false
        }
        dataRepository.setToolbarBtnState = jest.fn()

        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(SlideshowMode_deselectedImageBtnState)
    })

    test('Multi-select image when pause slideshow', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 5,
            slideShowMode: true,
            isPlayingslideShow: false
        }
        dataRepository.setToolbarBtnState = jest.fn()
        
        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(SlideshowMode_multiSelectedImageBtnState)
    })

    test('Select 1 image when pause slideshow', () => {
        //Arrange
        const payload = {
            numOfSelectImage: 1,
            slideShowMode: true,
            isPlayingslideShow: false
        }
        dataRepository.setToolbarBtnState = jest.fn()

        //Act
        const buttonStateController = new ButtonStateController()
        buttonStateController.setToolbarBtnState(BtnStateWatcherActionTypes.NUM_OF_SELECTED_IMAGE_CHANGED, payload)

        //Assert
        expect(dataRepository.setToolbarBtnState.mock.calls[0][0]).toBe(pauseSlideshowBtnState)
    })

})