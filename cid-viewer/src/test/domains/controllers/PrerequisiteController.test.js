import PrerequisiteController from '@domains/controllers/rootApp/PrerequisiteController'
import CidError from '@components/base/CidError'
import CidHttpError from '@components/base/CidHttpError'

// import readers
import { caseProfileReader } from '@domains/readers/CaseProfileReader'
import { fileReader } from '@domains/readers/FileReader'

// import services
import { configService } from '@domains/services/configService/ConfigService'
import StudyService from '@domains/services/studyService/StudyService'
import { AuthService } from '@domains/services/authService/AuthService'

// import utils
import JsonParser from '@utils/JsonParser'
import logger from '@utils/Logger'
import dataRepository from '@utils/DataRepository'
import {isNullOrEmptyString} from '@utils/StringUtil'

// import constants
import { ENDPOINT } from '@constants/EndpointConstants'
import { ServiceConstants } from '@constants/ServiceConstants'
import { EVENT_GET_CID_STUDY } from '@constants/EventConstants'
import { EJB_EXCEPTION, CID_APPLICATION_CONFIG_UNAVAILABLE, 
    CID_IAS_AUTHEN_UNAVAILABLE, CID_ICW_AUTHEN_UNAVAILABLE,
    ACCESS_AUTHEN_FAILED, CID_CONFIG_FILE_UNAVAILABLE,
    APP_VER_FAILED } from '@constants/MsgCodeConstants'
import { APP_INIT, MISSING_INPUT_PARAM,
    ACCESS_AUTHEN_FAILED as ACCESS_AUTHEN_FAILED_HC,
    UNABLE_TO_READ_CASE_PROFILE,
    CID_CONFIG_FILE_UNAVAILABLE as CID_CONFIG_FILE_UNAVAILABLE_HC,
    CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG} from '@constants/WordingConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'
import {WINDOW_NAME} from '@constants/ProxyConstants'
import { LOGGER_NAME } from '@constants/LogConstants'
import viewerLogController from '@domains/controllers/ViewerLogController'
import windowPositionController from '@domains/controllers/rootApp/WindowPositionController'

describe('Test Suite: <PrerequisiteController>', ()=>{
    const mockCaseProfile = {
        hospCde: 'QEH',
        caseNo: 'HN05000207X',
        accessionNo: 'VH SJ1354928067Y', // 25 real pics
        seriesSeqNo: 'e27f34a9-66a7-439f-97bc-a913c3b8d37f',
        imageSeqNo : '',
        versionNo: '',
        loginID: 'ISG',
        wrkStnID: 'dc1cidst02',
        systemID: 'RIS',
        userHospCde: 'VH',
        patientKey: '90500231',
        specialtyCde: '',
        appHost: 'http://localhost:7778',
        appSiteName: '',
        appName: 'CID Image Viewer II',
        wrkStnHostname: 'dc1cidst02',
        sessionID: '358c4ca8-c0a8-4915-ab0a-7fabc475dc2a',
        timeToken: '2000'
    }

    let mockCaseProfileState = {
        hospCode: 'QEH',
        caseNo: 'HN05000207X',
        accessionNo: 'VH SJ1354928067Y', // 25 real pics
        seriesNo: 'e27f34a9-66a7-439f-97bc-a913c3b8d37f',
        imageSeqNo: '',
        versionNo: '',
        appHost: 'http://localhost:7778',
        appSiteName: '',
        userId: 'ISG',
        workstationId: 'dc1cidst02',
        workstationHostname: 'dc1cidst02',
        requestSys: 'RIS',
        accessKey: '1FD2B404-662E-48C2-8351-BDF2C40E8711',
        userHospCode: 'VH',
        patientKey: '90500231',
        specialtyCode: '',
        sessionID: '358c4ca8-c0a8-4915-ab0a-7fabc475dc2a',
        caseProfileTimeToken: '2000'
    }

    const mockCidError = new CidError('class_name', 'func_name', 'test_message')

    let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})

    const mockErrMsgCode = {
        errors:{
            3001: {
                title: "CID Service Error",
                content: "EJB Excpetion!"
            },
            3002: {
                "title": "CID Service Error",
                "content": "Failed to invoke the CID WriteLog service\nPlease retry. Please contact system administrator if the sevice is not resume."
            },
            3201: {
                title: "CID Service Error",
                content: "Failed to retrieve configuration files.\nPlease retry. Please contact system administrator if the sevice is not resume."
            },
            4001: {
                title: "CID Service Error",
                content: "Failed to invoke IAS Authentication service.\nPlease retry. Please contact system administrator if the sevice is not resume."
            },
            4002: {
                title: "CID Service Error",
                content: "Failed to invoke ICW Authentication service.\nPlease retry. Please contact system administrator if the sevice is not resume."
            },
            4201: {
                title: "Error: Access Authentication",
                content: "You do not have access right to activate the CID ImageViewer."
            },
            4202: {
                title: "Session Timeout",
                content: "The imageViewer session is terminated."
            },
            4203: {
                title: "Error: Application Version",
                content: "An unmatch version of CID ImageViewer is installed or current version is disabled."
            },
            4204: {
                title: "Error: Missing input parameter",
                content: "Not enough information to process."
            },
            5201: {
                title: "System Warning",
                content: "Window is resized, please stop the slideshow and replay."
            },
            5202: {
                title: "Error: Invalid Case Profile",
                content: "Unable to read case profile."
            },
            5203: {
                title: "System Warning",
                content: "Number of selected images for Window level / contrast adjustment exceeded the limit.\nPlease select less images."
            }
        }
    }

    const mockViewerControlConfig = {
        idleTimeout: "20",
        caseProfileTimeout: "60",
		EPRTimeoutMethodName: "acknowledgeEPRTimeout",
		applicationId: "IV",
		applicationVersion: "6.0.0.3"
    }
    
    const mockAppLocalConfig = {
        viewerControl: {
            idleTimeout: '20',
            caseProfileTimeout: '60',
            EPRTimeoutMethodName: 'acknowledgeEPRTimeout',
            applicationId: "IV",
            applicationVersion: "6.0.0.1"
        },
        export: {
            imageExportFileName: 'Images.zip',
            imageExportDisclaimer: ''
        },
        imageHeader: {
            fontType: "Arial",
            fontSize: 11,
            fontColor: "white",
            bgColor: "#000000",
            visibility: true,
            rtcExamType: "ES",
            rtcImageCreateDateTime: false
        },
        webServiceIcw: {
            cidWsContextPath: 'http://cid-corp-d1:11360/cid-image-common_6_0_0_2',
            suid1: 'di6nu9/XvFXqdO330PbU4w==',
            suid2: 'N3FoxBETpxV/5afL9IcAaQ=='
        },
        pictorialIndex: {
            numOfColumn: 2,
            isShowSeriesHeading: true,
            serieName: 'Series 1'
        },
        colorAdjustment: {
            maxNumOfImage: 2
        }
    }

    const mockLogConfig = {
        "JL": {
            "messageSizeLimit": 500,
            "setOptions": {
                "enabled": true
            }
        },
        "appenders": {
            "ajaxAppender1": {
                "type": "AJAX",
                "setOptions": {
                    "level": 1000,   
                    "batchSize": 20,
                    "batchTimeout": 10000,
                    "maxBatchSize": 1000,
                    "sendTimeout": 5000
                }
            },
            "ajaxAppender2": {
                "type": "AJAX",
                "isDebugLog": true,
                "setOptions": {
                    "level": 1000,
                    "batchSize": 20,
                    "batchTimeout": 10000,
                    "maxBatchSize": 1000,
                    "sendTimeout": 5000
                }
            },
            "consoleAppender1": {
                "type": "CONSOLE",
                "setOptions": {
                    "level": 1000
                }
            }
        },
        "loggers": {
            "CID": {
                "enabled": true,
                "setOptions": {
                    "appenders": ["consoleAppender1"],
                    "level": 1000
                }
            },
            "CID.UserActions": {
                "enabled": true,
                "setOptions": {
                    "appenders": ["consoleAppender1"]
                }
            },
            "CID.Service": {
                "enabled": true,
                "setOptions": {
                    "appenders": ["consoleAppender1"]
                }
            },
            "CID.System": {
                "enabled": true,
                "setOptions": {
                    "appenders": ["consoleAppender1"]
                }
            },
            "CID.API": {
                "enabled": true,
                "setOptions": {
                    "appenders": ["consoleAppender1"]
                }
            },
            "CID.Exception": {
                "enabled": true,
                "setOptions": {
                    "appenders": ["consoleAppender1"]
                }
            }
        },
        "disabledLoggers": [],
        "allowDenyConfig": {
            "allow": {
                "sysId": [],
                "userId": [],
                "wrkId": [],
                "hospcde": []
            },
            "deny": {
                "sysId": [],
                "userId": [],
                "wrkId": [],
                "hospcde": []
            }
        }
    }
    
    const mockStudy = {
        messageDtl: {
            transactionCode: 'CID2DATA',
            serverHosp: 'VH',
            transactionDtm: '20180605164634.244',
            transactionID: '4B1AC56C-7CE8-4623-9EB7-A6AC00ADD702',
            sendingApplication: 'CID'
        },
        visitDtl: {
            visitHosp: 'VH',
            visitSpec: '',
            visitWardClinic: '',
            payCode: '',
            caseNum: 'HN05000207X',
            adtDtm: ''
        },
        patientDtl: {
            hkid: 'M7673537',
            patKey: '90500231',
            patName: 'TSANG, AH OI',
            patDOB: '19460107000000.000',
            patSex: 'M',
            deathIndicator: ''
        },
        studyDtl: {
            accessionNo: 'R9MLJ5KPEVED1SMQ',
            studyID: '5953c844-8fe6-4587-9ee3-b898586df6f1',
            studyType: 'ERS',
            studyDtm: '20100131123456',
            remark: 'Remark1',
            seriesDtls: {
                seriesDtl:[
                    {
                        seriesNo:'f74c17d2-2069-42be-9e20-ab9705ab9189', 
                        examType:'HADRW', 
                        examDtm:'20150318161644', 
                        entityID:'0', 
                        imageDtls:{
                            imageDtl:[
                                {
                                    imageFormat:'IM', 
                                    imagePath:'/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG', 
                                    imageFile:'1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG', 
                                    imageID:'ff72b811-6ac6-41fd-a414-8dd87ae8089e', 
                                    imageVersion:'1', 
                                    imageSequence:'1', 
                                    imageKeyword:'Name:Bronchoscopy1|', 
                                    imageHandling:'N', 
                                    imageType:'JPEG', 
                                    imageStatus:'1', 
                                    annotationDtls:{
                                        annotationDtl:[
                                            {
                                                annotationID:'annotationID1', 
                                                annotationSeq:'annotationSeq1', 
                                                annotationType:'annotationType1', 
                                                annotationText:'annotationText1', 
                                                annotationCoordinate:'annotationCoordinate1', 
                                                annotationStatus:'annotationStatus1', 
                                                annotationEditable:'annotationEditable1', 
                                                annotationUpdDtm:'annotationUpdDtm1'
                                            },
                                            {
                                                annotationID:'annotationID2', 
                                                annotationSeq:'annotationSeq2', 
                                                annotationType:'annotationType2', 
                                                annotationText:'annotationText2', 
                                                annotationCoordinate:'annotationCoordinate2', 
                                                annotationStatus:'annotationStatus2', 
                                                annotationEditable:'annotationEditable2', 
                                                annotationUpdDtm:'annotationUpdDtm2' 
                                            }
                                        ]
                                    }
                                },
    
                                {
                                    imageFormat:'IM2', 
                                    imagePath:'/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/12.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG', 
                                    imageFile:'1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG', 
                                    imageID:'ff72b811-6ac6-41fd-a414-8dd87ae8089e', 
                                    imageVersion:'2', 
                                    imageSequence:'2', 
                                    imageKeyword:'Name:Bronchoscopy2|', 
                                    imageHandling:'Y', 
                                    imageType:'JPEG2', 
                                    imageStatus:'2', 
                                    annotationDtls:{
                                        annotationDtl:[
                                            {
                                                annotationID:'annotationID1.2', 
                                                annotationSeq:'annotationSeq1.2', 
                                                annotationType:'annotationType1.2', 
                                                annotationText:'annotationText1.2', 
                                                annotationCoordinate:'annotationCoordinate1.2', 
                                                annotationStatus:'annotationStatus1.2', 
                                                annotationEditable:'annotationEditable1.2', 
                                                annotationUpdDtm:'annotationUpdDtm1.2'
                                            },
                                            {
                                                annotationID:'annotationID2.2', 
                                                annotationSeq:'annotationSeq2.2', 
                                                annotationType:'annotationType2.2', 
                                                annotationText:'annotationText2.2', 
                                                annotationCoordinate:'annotationCoordinate2.2', 
                                                annotationStatus:'annotationStatus2.2', 
                                                annotationEditable:'annotationEditable2.2', 
                                                annotationUpdDtm:'annotationUpdDtm2.2' 
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }
    }

    test('Test getCaseProfile of PrerequisiteController when env is dev', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'development'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return true
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			return {
                caseProfile: {
                    hospCde: 'QEH',
                    caseNo: 'HN05000207X',
                    accessionNo: 'VH SJ1354928067Y', // 25 real pics
                    seriesSeqNo: 'e27f34a9-66a7-439f-97bc-a913c3b8d37f',
                    imageSeqNo : '',
                    versionNo: '',
                    loginID: 'ISG',
                    wrkStnID: 'dc1cidst02',
                    systemID: 'RIS',
                    userHospCde: 'VH',
                    patientKey: '90500231',
                    specialtyCde: '',
                    appHost: 'http://localhost:7778',
                    appSiteName: '',
                    appName: 'CID Image Viewer II',
                    // wrkStnHostname: 'dc1cidst02',
                    sessionID: '358c4ca8-c0a8-4915-ab0a-7fabc475dc2a',
                    timeToken: '2000'
                }
            }
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return true
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.getCaseProfile()

        //Assert
        const expectedResult = mockCaseProfile
        expect(actualResult).toEqual(expectedResult)
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(0)
    })

    test('Test getCaseProfile of PrerequisiteController when env is dev and validateOpener is false', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'development'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return false
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			return {caseProfile: mockCaseProfile}
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return true
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.getCaseProfile()

        //Assert
        const expectedResult = null
        expect(actualResult).toEqual(expectedResult)
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(0)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(0)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent.mock.calls[0][1]).toEqual(expect.stringContaining('access right'))
    })

    test('Test getCaseProfile of PrerequisiteController when env is dev and validateCaseProfile is false', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'development'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return true
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			return {caseProfile: mockCaseProfile}
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return false
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.getCaseProfile()

        //Assert
        const expectedResult = null
        expect(actualResult).toEqual(expectedResult)
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent.mock.calls[0][1]).toEqual(expect.stringContaining('Not enough information'))
    })

    test('Test getCaseProfile of PrerequisiteController when env is dev and readCaseProfileFromHash throws error', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'development'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return true
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			throw mockCidError
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return false
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            prerequisiteController.getCaseProfile()
        } catch(cidErr) {
            expect(cidErr).toEqual(mockCidError)
        }

        //Assert
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(0)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent.mock.calls[0][1]).toEqual(expect.stringContaining('case profile'))
    })

    test('Test getCaseProfile of PrerequisiteController when env is prod', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'production'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return true
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			return {caseProfile: mockCaseProfile}
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return true
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.getCaseProfile()

        //Assert
        const expectedResult = mockCaseProfile

        expect(actualResult).toEqual(expectedResult)
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(0)
    })

    test('Test getCaseProfile of PrerequisiteController when env is prod and validateOpener is false', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'production'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return false
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			return {caseProfile: mockCaseProfile}
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return true
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.getCaseProfile()

        //Assert
        const expectedResult = null
        expect(actualResult).toEqual(expectedResult)
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(0)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(0)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent.mock.calls[0][1]).toEqual(expect.stringContaining('access right'))
    })

    test('Test getCaseProfile of PrerequisiteController when env is prod and validateCaseProfile is false', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'production'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return true
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			return {caseProfile: mockCaseProfile}
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return false
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.getCaseProfile()

        //Assert
        const expectedResult = null

        expect(actualResult).toEqual(expectedResult)
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent.mock.calls[0][1]).toEqual(expect.stringContaining('Not enough information'))
    })

    test('Test getCaseProfile of PrerequisiteController when env is prod and readCaseProfileFromHash throws error', ()=>{
        //Arrange mock object
        process.env.NODE_ENV = 'production'
        window.opener = {}
        caseProfileReader.validateOpener = jest.fn()
        caseProfileReader.validateOpener.mockImplementation(()=>{
			return true
        })
        caseProfileReader.readCaseProfileFromHash = jest.fn()
        caseProfileReader.readCaseProfileFromHash.mockImplementation(()=>{
			throw mockCidError
        })
        caseProfileReader.validateCaseProfile = jest.fn()
        caseProfileReader.validateCaseProfile.mockImplementation(()=>{
			return false
        })
        dataRepository.setMsgDialogByContent = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            prerequisiteController.getCaseProfile()
        } catch(cidErr) {
            expect(cidErr).toEqual(mockCidError)
        }

        //Assert
        expect(caseProfileReader.validateOpener).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.readCaseProfileFromHash).toHaveBeenCalledTimes(1)
        expect(caseProfileReader.validateCaseProfile).toHaveBeenCalledTimes(0)
        expect(dataRepository.setMsgDialogByContent).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByContent.mock.calls[0][1]).toEqual(expect.stringContaining('case profile'))
    })

    test('Test readCaseProfile of PrerequisiteController when caseProfile is not null', ()=>{
        //Arrange mock object
        dataRepository.saveCaseProfile = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.getCaseProfile = jest.fn()
        prerequisiteController.getCaseProfile.mockImplementation(()=>{
			return mockCaseProfile
        })
        let expectedResult = prerequisiteController.readCaseProfile()

        //Assert
        expect(expectedResult).toEqual(true)
        expect(dataRepository.saveCaseProfile).toHaveBeenCalledTimes(1)
        expect(dataRepository.saveCaseProfile).toHaveBeenCalledWith(mockCaseProfile)
    })

    test('Test readCaseProfile of PrerequisiteController when caseProfile is null', ()=>{
        //Arrange mock object
        dataRepository.saveCaseProfile = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.getCaseProfile = jest.fn()
        prerequisiteController.getCaseProfile.mockImplementation(()=>{
			return null
        })
        let expectedResult = prerequisiteController.readCaseProfile()

        //Assert
        expect(expectedResult).toEqual(false)
    })

    test('Test readCaseProfile of PrerequisiteController getCaseProfile throws error', ()=>{
        //Arrange mock object
        caseProfileReader.getCaseProfile = jest.fn()
        caseProfileReader.getCaseProfile.mockImplementation(()=>{
			throw mockCidError
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            prerequisiteController.readCaseProfile()
        } catch(cidErr) {
            //Assert
            expect(cidErr).toEqual(mockCidError)
        }
    })

    test('Test readErrMsgCode of PrerequisiteController when readFile succeeds', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.saveMsgCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)

        fileReader.readFile = jest.fn()
		fileReader.readFile.mockImplementation((path)=>{
			return new Promise((resolve, reject) => {
				resolve(mockErrMsgCode)
			})
		})
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        await prerequisiteController.readErrMsgCode()

        //Assert
        expect(dataRepository.saveMsgCode).toHaveBeenCalledTimes(1)
        expect(dataRepository.saveMsgCode).toHaveBeenCalledWith(mockErrMsgCode)
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
    })

    test('Test readErrMsgCode of PrerequisiteController when readFile fails', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.saveMsgCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)

        fileReader.readFile = jest.fn()
		fileReader.readFile.mockImplementation((path)=>{
            // let promise = new Promise((resolve, reject) => {
			// 	reject()
			// })
            // return promise.catch(function(httpErr) {
                throw mockCidHttpError
            // })
		})
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.readErrMsgCode()
        } catch(cidHttpErr) {
            //Assert
            expect(cidHttpErr).toEqual(mockCidHttpError)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
        }
    })

    test('Test doAuth of PrerequisiteController when access key is valid and app status is valid', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
			return true
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			return true
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = await prerequisiteController.doAuth()

        //Assert
        expect(actualResult).toEqual(true)
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
    })

    test('Test doAuth of PrerequisiteController when access key is invalid and app status is valid', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        
        logger.errorWithMsgCode = jest.fn()
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
			return false
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			return true
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = await prerequisiteController.doAuth()

        //Assert
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(ACCESS_AUTHEN_FAILED)
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
        expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
        expect(logger.errorWithMsgCode).toHaveBeenCalledWith(ACCESS_AUTHEN_FAILED)
        expect(actualResult).toEqual(false)
    })

    test('Test doAuth of PrerequisiteController when access key is valid and app status is invalid', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        
        logger.errorWithMsgCode = jest.fn()
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
			return true
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			return false
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = await prerequisiteController.doAuth()

        //Assert
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(APP_VER_FAILED)
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
        expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
        expect(logger.errorWithMsgCode).toHaveBeenCalledWith(APP_VER_FAILED)
        expect(actualResult).toEqual(false)
    })

    test('Test doAuth of PrerequisiteController when access key with auth failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        
        logger.errorWithMsgCode = jest.fn()
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
            mockCidHttpError.status = UNAUTHORIZED
            mockCidHttpError.exceptionMsg = null
			throw mockCidHttpError
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			return true
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.doAuth()
        } catch(cidHttpErr) {
            //Assert
            expect(cidHttpErr.status).toEqual(UNAUTHORIZED)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_ICW_AUTHEN_UNAVAILABLE)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledWith(CID_ICW_AUTHEN_UNAVAILABLE)
        }
    })

    test('Test doAuth of PrerequisiteController when access key with other request failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        
        logger.error = jest.fn()
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
            mockCidHttpError.status = 500
            mockCidHttpError.exceptionMsg = 'test_exception_msg'
			throw mockCidHttpError
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			return true
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.doAuth()
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(EJB_EXCEPTION, mockCidHttpError.exceptionMsg)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledWith(mockCidHttpError.exceptionMsg, EJB_EXCEPTION)
        }
    })

    test('Test doAuth of PrerequisiteController when app status with auth failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        
        logger.errorWithMsgCode = jest.fn()
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
            return true
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			mockCidHttpError.status = UNAUTHORIZED
            mockCidHttpError.exceptionMsg = null
			throw mockCidHttpError
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.doAuth()
        } catch(cidHttpErr) {
            //Assert
            expect(cidHttpErr.status).toEqual(UNAUTHORIZED)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_ICW_AUTHEN_UNAVAILABLE)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledWith(CID_ICW_AUTHEN_UNAVAILABLE)
        }
    })

    test('Test doAuth of PrerequisiteController when app status with other request failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.error = jest.fn()
        
        AuthService.prototype.validateAccessKeyByHostName = jest.fn()
        AuthService.prototype.validateAccessKeyByHostName.mockImplementation(()=>{
            return true
        })

        AuthService.prototype.validateApplicationStatus = jest.fn()
        AuthService.prototype.validateApplicationStatus.mockImplementation(()=>{
			mockCidHttpError.status = 500
            mockCidHttpError.exceptionMsg = 'test_exception_msg'
			throw mockCidHttpError
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.doAuth()
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(EJB_EXCEPTION, mockCidHttpError.exceptionMsg)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledWith(mockCidHttpError.exceptionMsg, EJB_EXCEPTION)
        }
    })

    test('Test readLocalAppConfig of PrerequisiteController', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.saveAppConfig = jest.fn()
        dataRepository.savePictorialIndexConfig = jest.fn()
        dataRepository.saveImageHeaderConfig = jest.fn()
        dataRepository.saveColorAdjustmentConfig = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        fileReader.readFile = jest.fn()
		fileReader.readFile.mockImplementation((path)=>{
			return new Promise((resolve, reject) => {
				resolve(mockAppLocalConfig)
			})
		})
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        await prerequisiteController.readLocalAppConfig()

        //Assert
        expect(dataRepository.saveAppConfig).toHaveBeenCalledTimes(1)
        expect(dataRepository.saveAppConfig).toHaveBeenCalledWith(mockAppLocalConfig)
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
    })

    test('Test readLocalAppConfig of PrerequisiteController when readFile fails', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.saveAppConfig = jest.fn()
        dataRepository.savePictorialIndexConfig = jest.fn()
        dataRepository.saveImageHeaderConfig = jest.fn()
        dataRepository.saveColorAdjustmentConfig = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        fileReader.readFile = jest.fn()
		fileReader.readFile.mockImplementation((path)=>{
            // let promise = new Promise((resolve, reject) => {
			// 	reject()
			// })
            // return promise.catch(function(httpErr) {
                throw mockCidHttpError
            // })
		})
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.readLocalAppConfig()
        } catch(cidHttpError) {
            //Assert
            expect(cidHttpError).toEqual(mockCidHttpError)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
        }
    })

    test('Test readLocalLogConfig of PrerequisiteController', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        fileReader.readFile = jest.fn()
		fileReader.readFile.mockImplementation((path)=>{
			return new Promise((resolve, reject) => {
				resolve(mockLogConfig)
			})
        })
        viewerLogController.setConfigWithDataRepository = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        await prerequisiteController.readLocalLogConfig()

        //Assert
        expect(viewerLogController.setConfigWithDataRepository).toHaveBeenCalledTimes(1)
        expect(viewerLogController.setConfigWithDataRepository.mock.calls[0][0]).toEqual(mockLogConfig)
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
    })

    test('Test retrieveAppConfig of PrerequisiteController', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {test: 'test'}
        })
        dataRepository.saveAppConfig = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.servicePerf = jest.fn()
        configService.retrieveAppConfig = jest.fn()
        configService.retrieveAppConfig.mockImplementation((icwEndpoint, icwWsInfo, appCommonConfigData)=>{
			return new Promise((resolve, reject) => {
                const mockAppConfigRsltObj = {
                    response: '{"test_response": "test_response"}',
                    data: {test: 'test'},
                }
                resolve(mockAppConfigRsltObj)
			})
        })
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        await prerequisiteController.retrieveAppConfig()

        //Assert
        expect(dataRepository.saveAppConfig).toHaveBeenCalledTimes(1)
        expect(dataRepository.saveAppConfig).toHaveBeenCalledWith({test_response: "test_response"})
        expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
    })

    test('Test retrieveAppConfig of PrerequisiteController when retrieveAppConfig with auth failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {test: 'test'}
        })
        dataRepository.saveAppConfig = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.servicePerf = jest.fn()
        configService.retrieveAppConfig = jest.fn()
        configService.retrieveAppConfig.mockImplementation((icwEndpoint, icwWsInfo, appCommonConfigData)=>{
			return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = UNAUTHORIZED
                    mockCidHttpError.exceptionMsg = null
                    throw mockCidHttpError
                // })
			})
        })
        logger.errorWithMsgCode = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.retrieveAppConfig()
        } catch(cidHttpErr) {
            //Assert
            expect(cidHttpErr.status).toEqual(UNAUTHORIZED)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_ICW_AUTHEN_UNAVAILABLE)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledWith(CID_ICW_AUTHEN_UNAVAILABLE)
        }
    })

    test('Test retrieveAppConfig of PrerequisiteController when retrieveAppConfig with other request failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {test: 'test'}
        })
        dataRepository.saveAppConfig = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.servicePerf = jest.fn()
        configService.retrieveAppConfig = jest.fn()
        configService.retrieveAppConfig.mockImplementation((icwEndpoint, icwWsInfo, appCommonConfigData)=>{
			return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = 500
                    mockCidHttpError.exceptionMsg = 'test_exception_msg'
                    throw mockCidHttpError
                // })
			})
        })
        logger.error = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.retrieveAppConfig()
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(EJB_EXCEPTION, mockCidHttpError.exceptionMsg)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledWith(mockCidHttpError.exceptionMsg, EJB_EXCEPTION)
        }
    })

    test('Test retrieveAppConfig of PrerequisiteController when retrieveAppConfig return empty response', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIcwWsAuthInfo = jest.fn()
        dataRepository.findIcwWsAuthInfo.mockImplementation(()=>{
			return {test: 'test'}
        })
        dataRepository.saveAppConfig = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.servicePerf = jest.fn()
        configService.retrieveAppConfig = jest.fn()
        configService.retrieveAppConfig.mockImplementation((icwEndpoint, icwWsInfo, appCommonConfigData)=>{
			return new Promise((resolve, reject) => {
                const mockAppConfigRsltObj = {
                    response: '{}',
                    data: {test: 'test'},
                }
                resolve(mockAppConfigRsltObj)
			})
        })
        logger.error = jest.fn()
        
        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.retrieveAppConfig()
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.saveAppConfig).toHaveBeenCalledTimes(0)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_APPLICATION_CONFIG_UNAVAILABLE, CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledWith(CID_APPLICATION_CONFIG_UNAVAILABLE_EXCEPTION_MSG, CID_APPLICATION_CONFIG_UNAVAILABLE)
        }
    })

    test('Test validateCaseProfileTimeToken of PrerequisiteController', ()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        Date.prototype.getTime = jest.fn()
        Date.prototype.getTime.mockImplementation(()=>{
			return 3000
        })

        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.validateCaseProfileTimeToken()

        //Assert
        expect(actualResult).toEqual(true)
    })

    const createTestValidateCaseProfileTimeTokenFalse = function(time, caseProfileTimeToken) {
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
            mockCaseProfileState.caseProfileTimeToken = caseProfileTimeToken
			return mockCaseProfileState
        })
        dataRepository.findViewerControlConfig = jest.fn()
        dataRepository.findViewerControlConfig.mockImplementation(()=>{
			return mockViewerControlConfig
        })
        Date.prototype.getTime = jest.fn()
        Date.prototype.getTime.mockImplementation(()=>{
			return time
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        logger.errorWithMsgCode = jest.fn()

        //Act
        const prerequisiteController = new PrerequisiteController()
        const actualResult = prerequisiteController.validateCaseProfileTimeToken()

        //Assert
        expect(actualResult).toEqual(false)
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(ACCESS_AUTHEN_FAILED)
        expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
        expect(logger.errorWithMsgCode).toHaveBeenCalledWith(ACCESS_AUTHEN_FAILED)
    }

    test('Test validateCaseProfileTimeToken of PrerequisiteController when time token expired', ()=>{
        createTestValidateCaseProfileTimeTokenFalse(63000, '2000')
    })

    test('Test validateCaseProfileTimeToken of PrerequisiteController when caseProfileTimeToken is empty string', ()=>{
        createTestValidateCaseProfileTimeTokenFalse(1000, '')
    })

    test('Test validateCaseProfileTimeToken of PrerequisiteController when caseProfileTimeToken is null', ()=>{
        createTestValidateCaseProfileTimeTokenFalse(1000, null)
    })

    test('Test retrieveStudy of PrerequisiteController', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIasWsAuthInfo = jest.fn()
        dataRepository.findIasWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.saveStudy = jest.fn()
        let instance = jest.fn()
        instance.retrieveStudy = ()=>{
            return new Promise((resolve, reject) => {
                resolve(mockStudy)
            })
        }
        instance.getMetadataStr = jest.fn()
		StudyService.getStudyService = jest.fn()
        StudyService.getStudyService.mockReturnValue(instance)

        //Act
        const prerequisiteController = new PrerequisiteController()
        await prerequisiteController.retrieveStudy()

        //Assert
        expect(dataRepository.saveStudy).toHaveBeenCalledTimes(1)
        expect(dataRepository.saveStudy).toHaveBeenCalledWith(mockStudy)
    })

    test('Test retrieveStudy of PrerequisiteController with auth failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIasWsAuthInfo = jest.fn()
        dataRepository.findIasWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.saveStudy = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.errorWithMsgCode = jest.fn()
        let instance = jest.fn()
        instance.retrieveStudy = ()=>{
            return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = UNAUTHORIZED
                    mockCidHttpError.exceptionMsg = null
                    throw mockCidHttpError
                // })
			})
        }
        instance.getMetadataStr = jest.fn()
		StudyService.getStudyService = jest.fn()
        StudyService.getStudyService.mockReturnValue(instance)

        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.retrieveStudy()
        } catch(cidHttpErr) {
            //Assert
            expect(cidHttpErr.status).toEqual(UNAUTHORIZED)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_IAS_AUTHEN_UNAVAILABLE)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledTimes(1)
            expect(logger.errorWithMsgCode).toHaveBeenCalledWith(CID_IAS_AUTHEN_UNAVAILABLE)
        }
    })

    test('Test retrieveStudy of PrerequisiteController with other web service failure', async()=>{
        //Arrange mock object
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(()=>{
			return mockCaseProfileState
        })
        dataRepository.findIasWsAuthInfo = jest.fn()
        dataRepository.findIasWsAuthInfo.mockImplementation(()=>{
			return {}
        })
        dataRepository.saveStudy = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        dataRepository.findWebServiceTimeoutConfig = jest.fn().mockReturnValue(5000)
        logger.error = jest.fn()
        let instance = jest.fn()
        instance.retrieveStudy = ()=>{
            return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = 500
                    mockCidHttpError.exceptionMsg = 'test_exception_msg'
                    throw mockCidHttpError
                // })
			})
        }
        instance.getMetadataStr = jest.fn()
		StudyService.getStudyService = jest.fn()
        StudyService.getStudyService.mockReturnValue(instance)

        //Act
        const prerequisiteController = new PrerequisiteController()
        try {
            await prerequisiteController.retrieveStudy()
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(EJB_EXCEPTION, mockCidHttpError.exceptionMsg)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledTimes(1)
            expect(logger.error).toHaveBeenCalledWith(mockCidHttpError.exceptionMsg, EJB_EXCEPTION)
        }
    })

    test('Test writeGetStudyIcwLog of PrerequisiteController', async()=>{
        //Arrange mock object
        let auditLogControllerInstance = jest.fn()
        auditLogControllerInstance.writeIcwLog = jest.fn()
        auditLogControllerInstance.writeIcwLog.mockImplementation(()=>{
            return new Promise((resolve, reject) => {
                resolve()
            })
        })
        const mockMetadataStr = 'test_metadataStr'

        //Act
        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.metadataStr = mockMetadataStr
        prerequisiteController.auditLogController = auditLogControllerInstance
        await prerequisiteController.writeGetStudyIcwLog()

        //Assert
        expect(prerequisiteController.auditLogController.writeIcwLog).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.auditLogController.writeIcwLog).toHaveBeenCalledWith(EVENT_GET_CID_STUDY, mockMetadataStr)
    })

    test('Test writeGetStudyIcwLog of PrerequisiteController with web service failure', async()=>{
        //Arrange mock object
        let auditLogControllerInstance = jest.fn()
        auditLogControllerInstance.writeIcwLog = jest.fn()
        auditLogControllerInstance.writeIcwLog.mockImplementation(()=>{
            return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = 500
                    mockCidHttpError.exceptionMsg = 'test_exception_msg'
                    throw mockCidHttpError
                // })
            })
        })
        const mockMetadataStr = 'test_metadataStr'

        //Act
        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.metadataStr = mockMetadataStr
        prerequisiteController.auditLogController = auditLogControllerInstance
        try {
            await prerequisiteController.writeGetStudyIcwLog()
        } catch(cidHttpErr) {
            //Assert
            expect(cidHttpErr).toEqual(mockCidHttpError)
        }
    })

    test('Test initPreliminaryWork of PrerequisiteController', async()=>{
        //Arrange mock object
        dataRepository.setProgressBar = jest.fn()
        dataRepository.setAppMask = jest.fn()
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(() => {
            return mockCaseProfileState
        })
        dataRepository.findAllSeriesDtl = jest.fn()
        dataRepository.findAllSeriesDtl.mockImplementation(() => {
            return {
                byId: {},
                allIds: []
            }
        })

        viewerLogController.disableLogging = jest.fn()

        logger.system = jest.fn()

        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.readCaseProfile = jest.fn()
        prerequisiteController.readCaseProfile.mockImplementation(() => {
            return true
        })
        prerequisiteController.readErrMsgCode = jest.fn()
        prerequisiteController.readLocalAppConfig = jest.fn()
        prerequisiteController.readLocalLogConfig = jest.fn()
        prerequisiteController.validateCaseProfileTimeToken = jest.fn()
        prerequisiteController.validateCaseProfileTimeToken.mockImplementation(() => {
            return true
        })
        prerequisiteController.doAuth = jest.fn()
        prerequisiteController.doAuth.mockImplementation(() => {
            return true
        })
        prerequisiteController.retrieveAppConfig = jest.fn()
        prerequisiteController.retrieveStudy = jest.fn()
        prerequisiteController.writeGetStudyIcwLog = jest.fn()
        windowPositionController.reposition = jest.fn()

        //Act
        await prerequisiteController.initPreliminaryWork()

        //Assert
        expect(prerequisiteController.readCaseProfile).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.readErrMsgCode).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.readLocalAppConfig).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.readLocalLogConfig).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.validateCaseProfileTimeToken).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.doAuth).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.retrieveAppConfig).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.retrieveStudy).toHaveBeenCalledTimes(1)
        expect(prerequisiteController.writeGetStudyIcwLog).toHaveBeenCalledTimes(1)
        expect(windowPositionController.reposition).toHaveBeenCalledTimes(1)
        expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(2)

        // Jest v22.x.x
        expect(dataRepository.setProgressBar.mock.calls[0][0]).toEqual(true)
        expect(dataRepository.setProgressBar.mock.calls[0][1]).toEqual(APP_INIT)
        expect(dataRepository.setProgressBar.mock.calls[1][0]).toEqual(false)
        expect(dataRepository.setProgressBar.mock.calls[1][1]).toEqual(APP_INIT)
        
        // Jest v23.x.x or higher
        // expect(dataRepository.setProgressBar).toHaveBeenNthCalledWith(1,true,APP_INIT)
        // expect(dataRepository.setProgressBar).toHaveBeenNthCalledWith(2,false,APP_INIT)
    })

    const createTestInitApplicationValidationFalse = async function(readCaseProfileRslt, validateCaseProfileTimeTokenRslt, doAuthRslt) {
        //Arrange mock object
        dataRepository.setProgressBar = jest.fn()
        dataRepository.setAppMask = jest.fn()
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(() => {
            return mockCaseProfileState
        })
        dataRepository.findAllSeriesDtl = jest.fn()
        dataRepository.findAllSeriesDtl.mockImplementation(() => {
            return {
                byId: {},
                allIds: []
            }
        })

        viewerLogController.disableLogging = jest.fn()

        logger.system = jest.fn()

        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.readCaseProfile = jest.fn()
        prerequisiteController.readCaseProfile.mockImplementation(() => {
            return readCaseProfileRslt
        })
        prerequisiteController.readErrMsgCode = jest.fn()
        prerequisiteController.readLocalAppConfig = jest.fn()
        prerequisiteController.readLocalLogConfig = jest.fn()
        prerequisiteController.validateCaseProfileTimeToken = jest.fn()
        prerequisiteController.validateCaseProfileTimeToken.mockImplementation(() => {
            return validateCaseProfileTimeTokenRslt
        })
        prerequisiteController.doAuth = jest.fn()
        prerequisiteController.doAuth.mockImplementation(() => {
            return doAuthRslt
        })
        prerequisiteController.retrieveAppConfig = jest.fn()
        prerequisiteController.retrieveStudy = jest.fn()
        prerequisiteController.writeGetStudyIcwLog = jest.fn()

        //Act
        await prerequisiteController.initPreliminaryWork()

        //Assert
        expect(dataRepository.setAppMask).toHaveBeenCalledTimes(1)
        expect(dataRepository.setAppMask).toHaveBeenCalledWith(true)
    }

    test('Test initPreliminaryWork of PrerequisiteController with invalid readCaseProfile', async()=>{
        createTestInitApplicationValidationFalse(false, true, true)
    })

    test('Test initPreliminaryWork of PrerequisiteController with invalid validateCaseProfileTimeToken', async()=>{
        createTestInitApplicationValidationFalse(true, false, true)
    })

    test('Test initPreliminaryWork of PrerequisiteController with invalid doAuth', async()=>{
        createTestInitApplicationValidationFalse(true, true, false)
    })

    test('Test initPreliminaryWork of PrerequisiteController with web service failure', async()=>{
        //Arrange mock object
        dataRepository.setProgressBar = jest.fn()
        dataRepository.setAppMask = jest.fn()
        dataRepository.findCaseProfile = jest.fn()
        dataRepository.findCaseProfile.mockImplementation(() => {
            return mockCaseProfileState
        })
        dataRepository.findAllSeriesDtl = jest.fn()
        dataRepository.findAllSeriesDtl.mockImplementation(() => {
            return {
                byId: {},
                allIds: []
            }
        })

        viewerLogController.disableLogging = jest.fn()

        logger.system = jest.fn()

        const prerequisiteController = new PrerequisiteController()
        prerequisiteController.readCaseProfile = jest.fn()
        prerequisiteController.readCaseProfile.mockImplementation(() => {
            return true
        })
        prerequisiteController.readErrMsgCode = jest.fn()
        prerequisiteController.readLocalAppConfig = jest.fn()
        prerequisiteController.readLocalLogConfig = jest.fn()
        prerequisiteController.validateCaseProfileTimeToken = jest.fn()
        prerequisiteController.validateCaseProfileTimeToken.mockImplementation(() => {
            return true
        })
        prerequisiteController.doAuth = jest.fn()
        prerequisiteController.doAuth.mockImplementation(() => {
            return true
        })
        prerequisiteController.retrieveAppConfig = jest.fn()
        prerequisiteController.retrieveStudy = jest.fn()
        prerequisiteController.retrieveStudy.mockImplementation(() => {
            return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = 500
                    mockCidHttpError.exceptionMsg = 'test_exception_msg'
                    throw mockCidHttpError
                // })
            })
        })
        prerequisiteController.writeGetStudyIcwLog = jest.fn()

        //Act
        try {
            await prerequisiteController.initPreliminaryWork()
        } catch(cidErr) {
            //Assert
            expect(dataRepository.setAppMask).toHaveBeenCalledTimes(1)
            expect(dataRepository.setAppMask).toHaveBeenCalledWith(true)
        }
    })
})