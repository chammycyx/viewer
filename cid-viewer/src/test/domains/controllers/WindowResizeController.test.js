import WindowResizeController from '@domains/controllers/rootApp/WindowResizeController'
import { RESIZE } from '@constants/WindowEventConstants'
import dataRepository from '@utils/DataRepository'
import logger from '@src/main/utils/Logger'
import { RESIZE_APP as RESIZE_APP_LOADING_MSG } from '@constants/WordingConstants'

describe('WindowResizeController', ()=>{
    test('Test initResizeApp of WindowResizeController', ()=>{
        //Arrange mock object
        window.addEventListener = jest.fn()
        
        //Act
        let windowResizeController = new WindowResizeController()
            windowResizeController.initResizeApp()

        //Assert
        expect(window.addEventListener).toHaveBeenCalledTimes(1)
        expect(window.addEventListener).toHaveBeenCalledWith(RESIZE, windowResizeController.bufferResizeApp, false)
    })

    test('Test bufferResizeApp of WindowResizeController', ()=>{
        //Arrange mock object
        jest.useFakeTimers()
        const originalResizeBufferTimerId = -1 // the ID returned will never be negative number, so set -1 as the original value
        dataRepository.setProgressBar = jest.fn()
        setTimeout.mockImplementation
        
        //Act
        let windowResizeController = new WindowResizeController()
            windowResizeController.resizeBufferTimerId = originalResizeBufferTimerId
            windowResizeController.resizeApp = jest.fn()
            windowResizeController.bufferResizeApp()

        //Assert
        expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(1)
        expect(dataRepository.setProgressBar).toHaveBeenCalledWith(true, RESIZE_APP_LOADING_MSG)
        expect(clearTimeout).toHaveBeenCalledTimes(1)
        expect(clearTimeout).toHaveBeenCalledWith(originalResizeBufferTimerId)
        expect(setTimeout).toHaveBeenCalledTimes(1)
        expect(setTimeout.mock.calls[0][0]).toEqual(windowResizeController.checkIfNeedResizeApp)
        expect(windowResizeController.resizeBufferTimerId).not.toEqual(originalResizeBufferTimerId)
    })

    test('Test checkIfNeedResizeApp of WindowResizeController if slide show is off', ()=>{
        //Arrange mock object
        dataRepository.findSlideShowTools = jest.fn()
        dataRepository.findSlideShowTools.mockImplementation(()=>{
			return {isModeOn: false}
        })
        dataRepository.setProgressBar = jest.fn()
        
        //Act
        let windowResizeController = new WindowResizeController()
            windowResizeController.resizeApp = jest.fn()
            windowResizeController.checkIfNeedResizeApp()

        //Assert
        expect(windowResizeController.resizeApp).toHaveBeenCalledTimes(1)
        expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(1)
        expect(dataRepository.setProgressBar).toHaveBeenCalledWith(false, RESIZE_APP_LOADING_MSG)
    })

    test('Test checkIfNeedResizeApp of WindowResizeController if slide show is on', ()=>{
        //Arrange mock object
        dataRepository.findSlideShowTools = jest.fn()
        dataRepository.findSlideShowTools.mockImplementation(()=>{
			return {isModeOn: true}
        })
        dataRepository.setMsgDialogByCode = jest.fn()
        logger.warnWithMsgCode = jest.fn()
        dataRepository.setProgressBar = jest.fn()
        
        //Act
        let windowResizeController = new WindowResizeController()
            windowResizeController.resizeApp = jest.fn()
            windowResizeController.checkIfNeedResizeApp()

        //Assert
        expect(windowResizeController.resizeApp).toHaveBeenCalledTimes(0)
        expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledTimes(1)
        expect(logger.warnWithMsgCode).toHaveBeenCalledTimes(1)
        expect(dataRepository.setProgressBar).toHaveBeenCalledTimes(1)
        expect(dataRepository.setProgressBar).toHaveBeenCalledWith(false, RESIZE_APP_LOADING_MSG)
    })

    const createTestResizeApp = function(innerWidth, innerHeight) {
        //Arrange mock object
        window.innerWidth = innerWidth
        window.innerHeight = innerHeight
        dataRepository.setAppSize = jest.fn()
        let mockViewingPanelControllerInstance = jest.fn()
        mockViewingPanelControllerInstance.resetShownImages = jest.fn()
        mockViewingPanelControllerInstance.resizeImgViewingPanel = jest.fn()
        
        //Act
        let windowResizeController = new WindowResizeController()
            windowResizeController.viewingPanelController = mockViewingPanelControllerInstance
            windowResizeController.resizeApp()

        //Assert
        expect(dataRepository.setAppSize).toHaveBeenCalledTimes(1)
        expect(dataRepository.setAppSize).toHaveBeenCalledWith(innerWidth, innerHeight)
        expect(mockViewingPanelControllerInstance.resizeImgViewingPanel).toHaveBeenCalledTimes(1)
        expect(mockViewingPanelControllerInstance.resetShownImages).toHaveBeenCalledTimes(1)
    }

    test('Test setAppSize of WindowResizeController if window size is 800x600', ()=>{
        createTestResizeApp(800, 600)
    })

    test('Test setAppSize of WindowResizeController if window size is 1024x768', ()=>{
        createTestResizeApp(1024, 768)
    })
})