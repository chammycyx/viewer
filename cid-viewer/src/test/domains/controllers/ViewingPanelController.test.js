
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import {RELOCATE_IMAGE_IN_CANVAS_FACTOR, BRIGHTNESS_ADJUSTMENT_FACTOR} from '@constants/ComponentConstants'
import {ViewingPanelController} from '@domains/controllers//ViewingPanelController'
import StudyService from '@domains/services/studyService/StudyService'
import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'

import { EJB_EXCEPTION, CID_IAS_WEBSERVICE_TIMEOUT,
    CID_IAS_AUTHEN_UNAVAILABLE } from '@constants/MsgCodeConstants'
import { RETRIEVE_IMAGE } from '@constants/WordingConstants'
import { UNAUTHORIZED } from '@constants/HttpStatusConstants'
import CidHttpError from '@components/base/CidHttpError'

configure({ adapter: new Adapter() })

jest.mock('@constants/ComponentConstants', function() {
    return {
        RELOCATE_IMAGE_IN_CANVAS_FACTOR: 0.1,
    }
})

describe('Test Suite: <ViewingPanelController>', () => {
    describe('Verify reset Image function if image is out of canvas', () => {
        // All RELOCATE_IMAGE_IN_CANVAS_FACTOR is mocked as '0.1'
        test('Image out of top side of canvas', () => {
            //Arrange
            const headerHeight = 18
            const imageHeight = 60
            const imageXCoor = 10
            let mockedDataRepository = {}
                mockedDataRepository.setImagePosition = jest.fn()
                mockedDataRepository.findCanvasGrid = jest.fn()
                mockedDataRepository.findCanvasGrid.mockReturnValue({headerHeight:headerHeight, canvasHeight:100, canvasWidth:120})
                mockedDataRepository.findCanvasProfile = jest.fn()
                mockedDataRepository.findCanvasProfile.mockReturnValue({imageHeight:imageHeight, imageWidth:80, imageScaleY:1, imageScaleX:1, rotateAngle: 0})
                mockedDataRepository.findImageId = jest.fn()
                mockedDataRepository.findSeriesDtl = jest.fn()
                mockedDataRepository.findSeriesDtl.mockReturnValue(jest.fn())

            //Act
            let viewingPanelController = new ViewingPanelController(null, mockedDataRepository)
                viewingPanelController.moveImage("seriesId", "imageId", imageXCoor, -(imageHeight/2)+headerHeight)

            //Assert
            const expectedImageXCoor = imageXCoor
            // const expectedImageYCoor = -(imageHeight/2)+(canvasHeight*RELOCATE_IMAGE_IN_CANVAS_FACTOR)+headerHeight
            const expectedImageYCoor = -2
            expect(mockedDataRepository.setImagePosition.mock.calls[0][0]).toBe("seriesId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][1]).toBe("imageId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][2]).toBe(expectedImageXCoor)
            expect(mockedDataRepository.setImagePosition.mock.calls[0][3]).toBe(expectedImageYCoor)
        })

        test('Image out of bottom side of canvas', () => {
            //Arrange
            const headerHeight = 18
            const imageHeight = 60
            const imageXCoor = 10
            const canvasHeight = 100
            let mockedDataRepository = {}
                mockedDataRepository.setImagePosition = jest.fn()
                mockedDataRepository.findCanvasGrid = jest.fn()
                mockedDataRepository.findCanvasGrid.mockReturnValue({headerHeight:headerHeight, canvasHeight:canvasHeight, canvasWidth:120})
                mockedDataRepository.findCanvasProfile = jest.fn()
                mockedDataRepository.findCanvasProfile.mockReturnValue({imageHeight:imageHeight, imageWidth:80, imageScaleY:1, imageScaleX:1, rotateAngle: 0})
                mockedDataRepository.findImageId = jest.fn()
                mockedDataRepository.findSeriesDtl = jest.fn()
                mockedDataRepository.findSeriesDtl.mockReturnValue(jest.fn())

            //Act
            let viewingPanelController = new ViewingPanelController(null, mockedDataRepository)
                viewingPanelController.moveImage("seriesId", "imageId", imageXCoor, canvasHeight + (imageHeight/2))

            //Assert
            const expectedImageXCoor = imageXCoor
            // const expectedImageYCoor = canvasHeight+(imageHeight/2)-(canvasHeight*RELOCATE_IMAGE_IN_CANVAS_FACTOR)
            const expectedImageYCoor = 100+30-10
            expect(mockedDataRepository.setImagePosition.mock.calls[0][0]).toBe("seriesId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][1]).toBe("imageId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][2]).toBe(expectedImageXCoor)
            expect(mockedDataRepository.setImagePosition.mock.calls[0][3]).toBe(expectedImageYCoor)
        })

        test('Image out of left side of canvas', () => {
            //Arrange
            const headerHeight = 18
            const imageWidth = 80
            const imageHeight = 60
            const imageYCoor = 10
            const canvasHeight = 100
            const canvasWidth = 120
            let mockedDataRepository = {}
                mockedDataRepository.setImagePosition = jest.fn()
                mockedDataRepository.findCanvasGrid = jest.fn()
                mockedDataRepository.findCanvasGrid.mockReturnValue({headerHeight:headerHeight, canvasHeight:canvasHeight, canvasWidth:canvasWidth})
                mockedDataRepository.findCanvasProfile = jest.fn()
                mockedDataRepository.findCanvasProfile.mockReturnValue({imageHeight:imageHeight, imageWidth:imageWidth, imageScaleY:1, imageScaleX:1, rotateAngle: 0})
                mockedDataRepository.findImageId = jest.fn()
                mockedDataRepository.findSeriesDtl = jest.fn()
                mockedDataRepository.findSeriesDtl.mockReturnValue(jest.fn())

            //Act
            let viewingPanelController = new ViewingPanelController(null, mockedDataRepository)
                viewingPanelController.moveImage("seriesId", "imageId", -(imageWidth/2), imageYCoor)

            //Assert
            // const expectedImageXCoor = -(imageWidth/2)+(canvasWidth*RELOCATE_IMAGE_IN_CANVAS_FACTOR)
            const expectedImageXCoor = -40+12
            const expectedImageYCoor = imageYCoor
            expect(mockedDataRepository.setImagePosition.mock.calls[0][0]).toBe("seriesId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][1]).toBe("imageId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][2]).toBe(expectedImageXCoor)
            expect(mockedDataRepository.setImagePosition.mock.calls[0][3]).toBe(expectedImageYCoor)
        })

        test('Image out of right side of canvas', () => {
            console.log("BRIGHTNESS_ADJUSTMENT_FACTOR: " + BRIGHTNESS_ADJUSTMENT_FACTOR)
            //Arrange
            const headerHeight = 18
            const imageWidth = 80
            const imageHeight = 60
            const imageYCoor = 10
            const canvasHeight = 100
            const canvasWidth = 120
            let mockedDataRepository = {}
                mockedDataRepository.setImagePosition = jest.fn()
                mockedDataRepository.findCanvasGrid = jest.fn()
                mockedDataRepository.findCanvasGrid.mockReturnValue({headerHeight:headerHeight, canvasHeight:canvasHeight, canvasWidth:canvasWidth})
                mockedDataRepository.findCanvasProfile = jest.fn()
                mockedDataRepository.findCanvasProfile.mockReturnValue({imageHeight:imageHeight, imageWidth:imageWidth, imageScaleY:1, imageScaleX:1, rotateAngle: 0})
                mockedDataRepository.findImageId = jest.fn()
                mockedDataRepository.findSeriesDtl = jest.fn()
                mockedDataRepository.findSeriesDtl.mockReturnValue(jest.fn())

            //Act
            let viewingPanelController = new ViewingPanelController(null, mockedDataRepository)
                viewingPanelController.moveImage("seriesId", "imageId", canvasWidth+(imageWidth/2), imageYCoor)

            //Assert
            // const expectedImageXCoor = canvasWidth+(imageWidth/2)-(canvasWidth*RELOCATE_IMAGE_IN_CANVAS_FACTOR)
            const expectedImageXCoor = 120+40-12
            const expectedImageYCoor = imageYCoor
            expect(mockedDataRepository.setImagePosition.mock.calls[0][0]).toBe("seriesId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][1]).toBe("imageId")
            expect(mockedDataRepository.setImagePosition.mock.calls[0][2]).toBe(expectedImageXCoor)
            expect(mockedDataRepository.setImagePosition.mock.calls[0][3]).toBe(expectedImageYCoor)
        })
    })

    test('retrieveNSaveImages error handling - Remove the highest version image file and click ht thumbnail', async() => {
        //Arrange
        let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})

        let studyService = new StudyService(null)
        studyService.retrieveImages = jest.fn()
        studyService.retrieveImages.mockImplementation(()=>{
			return new Promise((resolve, reject) => {
                // let promise = new Promise((resolve, reject) => {
                //     reject()
                // })
                // return promise.catch(function(httpErr) {
                    mockCidHttpError.status = "Test1"
                    mockCidHttpError.exceptionMsg = "Test2"
                    throw mockCidHttpError
                // })
			})
        })

        const seriesIndexId = 'seriesDtl_1'
        const imgIndexIds = ['imageDtl_3']

        dataRepository.findSeriesDtl = jest.fn()
        dataRepository.findSeriesDtl.mockImplementation(()=>{
			return {
                seriesNo: '12345678-bd08-422c-1234-9686bc33938b'
            }
        })

        dataRepository.isImageExist = jest.fn()
        dataRepository.isImageExist.mockImplementation(()=>{
			return false
        })

        dataRepository.findImageDtl = jest.fn()
        dataRepository.findImageDtl.mockImplementation(()=>{
			return {
                imageID: '12345678-d5e5-4ea3-1234-b0f1dae95b2a',
                imageSequence: '3',
                imageVersion: '3'
            }
        })

        dataRepository.findStudyServiceBaseParm = jest.fn()
        dataRepository.findStudyServiceBaseParm.mockImplementation(()=>{
			return {}
        })

        dataRepository.setProgressBar = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        logger.error = jest.fn()

        dataRepository.findWebServiceTimeoutConfig = jest.fn()
        dataRepository.findWebServiceTimeoutConfig.mockImplementation(()=>{
			return 5000
        })
        //Act
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository,null)
        try {
            await viewingPanelController.retrieveNSaveImages(seriesIndexId, imgIndexIds)
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.isImageExist).toHaveBeenCalledTimes(1)
            expect(dataRepository.findImageDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.findStudyServiceBaseParm).toHaveBeenCalledTimes(1)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(studyService.retrieveImages).toHaveBeenCalledTimes(1)
            
            expect(dataRepository.setProgressBar).toHaveBeenCalledWith(false, RETRIEVE_IMAGE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(EJB_EXCEPTION, mockCidHttpError.exceptionMsg)
            expect(logger.error).toHaveBeenCalledWith(mockCidHttpError.exceptionMsg, EJB_EXCEPTION)
        }
    })

    test('retrieveNSaveImages error handling - IAS authen fail', async() => {
        //Arrange
        let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})

        let studyService = new StudyService(null)
        studyService.retrieveImages = jest.fn()
        studyService.retrieveImages.mockImplementation(()=>{
			return new Promise((resolve, reject) => {
                mockCidHttpError.status = UNAUTHORIZED
                throw mockCidHttpError
			})
        })

        const seriesIndexId = 'seriesDtl_1'
        const imgIndexIds = ['imageDtl_3']

        dataRepository.findSeriesDtl = jest.fn()
        dataRepository.findSeriesDtl.mockImplementation(()=>{
			return {
                seriesNo: '12345678-bd08-422c-1234-9686bc33938b'
            }
        })

        dataRepository.isImageExist = jest.fn()
        dataRepository.isImageExist.mockImplementation(()=>{
			return false
        })

        dataRepository.findImageDtl = jest.fn()
        dataRepository.findImageDtl.mockImplementation(()=>{
			return {
                imageID: '12345678-d5e5-4ea3-1234-b0f1dae95b2a',
                imageSequence: '3',
                imageVersion: '3'
            }
        })

        dataRepository.findStudyServiceBaseParm = jest.fn()
        dataRepository.findStudyServiceBaseParm.mockImplementation(()=>{
			return {}
        })

        dataRepository.setProgressBar = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        logger.errorWithMsgCode = jest.fn()

        dataRepository.findWebServiceTimeoutConfig = jest.fn()
        dataRepository.findWebServiceTimeoutConfig.mockImplementation(()=>{
			return 5000
        })
        //Act
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository,null)
        try {
            await viewingPanelController.retrieveNSaveImages(seriesIndexId, imgIndexIds)
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.isImageExist).toHaveBeenCalledTimes(1)
            expect(dataRepository.findImageDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.findStudyServiceBaseParm).toHaveBeenCalledTimes(1)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(studyService.retrieveImages).toHaveBeenCalledTimes(1)
            
            expect(dataRepository.setProgressBar).toHaveBeenCalledWith(false, RETRIEVE_IMAGE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_IAS_AUTHEN_UNAVAILABLE)
            expect(logger.errorWithMsgCode).toHaveBeenCalledWith(CID_IAS_AUTHEN_UNAVAILABLE)
        }
    })

    test('retrieveNSaveImages error handling - Timeout', async() => {
        //Arrange
        let mockCidHttpError = new CidHttpError('class_name', 'func_name', 'test_url', {})

        let studyService = new StudyService(null)
        studyService.retrieveImages = jest.fn()
        studyService.retrieveImages.mockImplementation(()=>{
			return new Promise((resolve, reject) => {
                mockCidHttpError.isTimeout = true
                throw mockCidHttpError
			})
        })

        const seriesIndexId = 'seriesDtl_1'
        const imgIndexIds = ['imageDtl_3']

        dataRepository.findSeriesDtl = jest.fn()
        dataRepository.findSeriesDtl.mockImplementation(()=>{
			return {
                seriesNo: '12345678-bd08-422c-1234-9686bc33938b'
            }
        })

        dataRepository.isImageExist = jest.fn()
        dataRepository.isImageExist.mockImplementation(()=>{
			return false
        })

        dataRepository.findImageDtl = jest.fn()
        dataRepository.findImageDtl.mockImplementation(()=>{
			return {
                imageID: '12345678-d5e5-4ea3-1234-b0f1dae95b2a',
                imageSequence: '3',
                imageVersion: '3'
            }
        })

        dataRepository.findStudyServiceBaseParm = jest.fn()
        dataRepository.findStudyServiceBaseParm.mockImplementation(()=>{
			return {}
        })

        dataRepository.setProgressBar = jest.fn()
        dataRepository.setMsgDialogByCode = jest.fn()
        logger.errorWithMsgCode = jest.fn()

        dataRepository.findWebServiceTimeoutConfig = jest.fn()
        dataRepository.findWebServiceTimeoutConfig.mockImplementation(()=>{
			return 5000
        })
        //Act
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository,null)
        try {
            await viewingPanelController.retrieveNSaveImages(seriesIndexId, imgIndexIds)
        } catch(cidHttpErr) {
            //Assert
            expect(dataRepository.findSeriesDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.isImageExist).toHaveBeenCalledTimes(1)
            expect(dataRepository.findImageDtl).toHaveBeenCalledTimes(1)
            expect(dataRepository.findStudyServiceBaseParm).toHaveBeenCalledTimes(1)
            expect(dataRepository.findWebServiceTimeoutConfig).toHaveBeenCalledTimes(1)
            expect(studyService.retrieveImages).toHaveBeenCalledTimes(1)
            
            expect(dataRepository.setProgressBar).toHaveBeenCalledWith(false, RETRIEVE_IMAGE)
            expect(dataRepository.setMsgDialogByCode).toHaveBeenCalledWith(CID_IAS_WEBSERVICE_TIMEOUT)
            expect(logger.errorWithMsgCode).toHaveBeenCalledWith(CID_IAS_WEBSERVICE_TIMEOUT)
        }
    })
})