import HotKeyController from '@domains/controllers/rootApp/HotKeyController'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() })

describe('Test Suite: <HotKeyController>', ()=>{
    test('Invalide fps key', ()=>{
        // //Arrange mock object
        let hotKeyController = new HotKeyController()
        const event = {
            ctrlKey: false,
            altKey: false,
            shiftKey: false,
            preventDefault: jest.fn(),
            stopPropagation: jest.fn(),
            which: 65 //a
        }

        //Act
        hotKeyController.removeHotkey()
        window.onkeydown(event)

        //Assert
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
        expect(event.stopPropagation).toHaveBeenCalledTimes(1)
    })
    
    test('Valide fps key', ()=>{
        // //Arrange mock object
        let hotKeyController = new HotKeyController()
        const event = {
            ctrlKey: false,
            altKey: false,
            shiftKey: false,
            preventDefault: jest.fn(),
            stopPropagation: jest.fn(),
            which: 49 //1
        }

        //Act
        hotKeyController.removeHotkey()
        window.onkeydown(event)

        //Assert
        expect(event.preventDefault).toHaveBeenCalledTimes(0)
        expect(event.stopPropagation).toHaveBeenCalledTimes(0)
    })
    
    test('Valide fps key with pressing Ctrl', ()=>{
        // //Arrange mock object
        let hotKeyController = new HotKeyController()
        const event = {
            ctrlKey: true,
            altKey: false,
            shiftKey: false,
            preventDefault: jest.fn(),
            stopPropagation: jest.fn(),
            which: 49 //1
        }

        //Act
        hotKeyController.removeHotkey()
        window.onkeydown(event)

        //Assert
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
        expect(event.stopPropagation).toHaveBeenCalledTimes(1)
    })

    test('Valide fps key with pressing Alt', ()=>{
        // //Arrange mock object
        let hotKeyController = new HotKeyController()
        const event = {
            ctrlKey: false,
            altKey: true,
            shiftKey: false,
            preventDefault: jest.fn(),
            stopPropagation: jest.fn(),
            which: 49 //1
        }

        //Act
        hotKeyController.removeHotkey()
        window.onkeydown(event)

        //Assert
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
        expect(event.stopPropagation).toHaveBeenCalledTimes(1)
    })

    test('Valide fps key with pressing Shift', ()=>{
        // //Arrange mock object
        let hotKeyController = new HotKeyController()
        const event = {
            ctrlKey: false,
            altKey: false,
            shiftKey: true,
            preventDefault: jest.fn(),
            stopPropagation: jest.fn(),
            which: 49 //1
        }

        //Act
        hotKeyController.removeHotkey()
        window.onkeydown(event)

        //Assert
        expect(event.preventDefault).toHaveBeenCalledTimes(1)
        expect(event.stopPropagation).toHaveBeenCalledTimes(1)
    })
})