import dataRepository from '@utils/DataRepository'
import windowPositionController from '@domains/controllers/rootApp/WindowPositionController'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() })

describe('Test Suite: <WindowPositionController>', ()=>{
    test('Test reposition - #1: cmsWindowcenterX < 0 && isMainDisplayPortrait = true', ()=>{
        //Arrange mock object
        Object.defineProperty(screen, 'width', {value: 1024})

        const resolutionWidth = 1024
        const cmsWindowX = 100
        const cmsWindowY = 200
        const cmsWindowcenterX = -50
        const isMainDisplayPortrait = true

        Object.defineProperty(screen, 'width', {value: resolutionWidth})
        dataRepository.findRepositionParm = jest.fn().mockImplementationOnce(() => {
            return {cmsWindowX: cmsWindowX,
                cmsWindowY: cmsWindowY,
                cmsWindowcenterX: cmsWindowcenterX,
                isMainDisplayPortrait: isMainDisplayPortrait
            }})

        window.moveTo = jest.fn()

        //Act
        windowPositionController.reposition()

        //Assert
        expect(window.moveTo).toHaveBeenCalledTimes(1)
        expect(window.moveTo).toHaveBeenCalledWith(cmsWindowX,cmsWindowY)
        
    })

    test('Test reposition - #2: 0 < cmsWindowcenterX < resolutionWidth, && isMainDisplayPortrait = true', ()=>{
        //Arrange mock object
        const resolutionWidth = 1024
        const cmsWindowX = 100
        const cmsWindowY = 200
        const cmsWindowcenterX = 500
        const isMainDisplayPortrait = true

        Object.defineProperty(screen, 'width', {value: resolutionWidth})
        dataRepository.findRepositionParm = jest.fn().mockImplementationOnce(() => {
            return {cmsWindowX: cmsWindowX,
                cmsWindowY: cmsWindowY,
                cmsWindowcenterX: cmsWindowcenterX,
                isMainDisplayPortrait: isMainDisplayPortrait
            }})

        window.moveTo = jest.fn()

        //Act
        windowPositionController.reposition()

        //Assert
        const viewerX = Math.floor(cmsWindowcenterX/resolutionWidth) * resolutionWidth
        const viewerY = 0

        expect(window.moveTo).toHaveBeenCalledTimes(1)
        expect(window.moveTo).toHaveBeenCalledWith(viewerX,viewerY)
    })

    test('Test reposition - #3: cmsWindowcenterX > resolutionWidth, && isMainDisplayPortrait = true', ()=>{
        //Arrange mock object
        const resolutionWidth = 1024
        const cmsWindowX = 100
        const cmsWindowY = 200
        const cmsWindowcenterX = 2048
        const isMainDisplayPortrait = true

        Object.defineProperty(screen, 'width', {value: resolutionWidth})
        dataRepository.findRepositionParm = jest.fn().mockImplementationOnce(() => {
            return {cmsWindowX: cmsWindowX,
                cmsWindowY: cmsWindowY,
                cmsWindowcenterX: cmsWindowcenterX,
                isMainDisplayPortrait: isMainDisplayPortrait
            }})

        window.moveTo = jest.fn()
        
        //Act
        windowPositionController.reposition()

        //Assert
        expect(window.moveTo).toHaveBeenCalledTimes(1)
        expect(window.moveTo).toHaveBeenCalledWith(cmsWindowX,cmsWindowY)
    })

    test('Test reposition - #4: cmsWindowcenterX < 0, && isMainDisplayPortrait = false', ()=>{
        //Arrange mock object
        const resolutionWidth = 1024
        const cmsWindowX = 100
        const cmsWindowY = 200
        const cmsWindowcenterX = -256
        const isMainDisplayPortrait = false

        Object.defineProperty(screen, 'width', {value: resolutionWidth})
        dataRepository.findRepositionParm = jest.fn().mockImplementationOnce(() => {
            return {cmsWindowX: cmsWindowX,
                cmsWindowY: cmsWindowY,
                cmsWindowcenterX: cmsWindowcenterX,
                isMainDisplayPortrait: isMainDisplayPortrait
            }})

        window.moveTo = jest.fn()
        
        //Act
        windowPositionController.reposition()

        //Assert
        const viewerX = Math.floor(cmsWindowcenterX/resolutionWidth) * resolutionWidth
        const viewerY = 0

        expect(window.moveTo).toHaveBeenCalledTimes(1)
        expect(window.moveTo).toHaveBeenCalledWith(viewerX,viewerY)
    })

    test('Test reposition - #5: 0 < cmsWindowcenterX < resolutionWidth, && isMainDisplayPortrait = false', ()=>{
        //Arrange mock object
        const resolutionWidth = 1024
        const cmsWindowX = 100
        const cmsWindowY = 200
        const cmsWindowcenterX = 555
        const isMainDisplayPortrait = false

        Object.defineProperty(screen, 'width', {value: resolutionWidth})
        dataRepository.findRepositionParm = jest.fn().mockImplementationOnce(() => {
            return {cmsWindowX: cmsWindowX,
                cmsWindowY: cmsWindowY,
                cmsWindowcenterX: cmsWindowcenterX,
                isMainDisplayPortrait: isMainDisplayPortrait
            }})

        window.moveTo = jest.fn()
        
        //Act
        windowPositionController.reposition()

        //Assert
        const viewerX = Math.floor(cmsWindowcenterX/resolutionWidth) * resolutionWidth
        const viewerY = 0

        expect(window.moveTo).toHaveBeenCalledTimes(1)
        expect(window.moveTo).toHaveBeenCalledWith(viewerX,viewerY)
    })

    test('Test reposition - #6: cmsWindowcenterX > resolutionWidth, && isMainDisplayPortrait = false', ()=>{
        //Arrange mock object
        const resolutionWidth = 1024
        const cmsWindowX = 100
        const cmsWindowY = 200
        const cmsWindowcenterX = 2345
        const isMainDisplayPortrait = false

        Object.defineProperty(screen, 'width', {value: resolutionWidth})
        dataRepository.findRepositionParm = jest.fn().mockImplementationOnce(() => {
            return {cmsWindowX: cmsWindowX,
                cmsWindowY: cmsWindowY,
                cmsWindowcenterX: cmsWindowcenterX,
                isMainDisplayPortrait: isMainDisplayPortrait
            }})

        window.moveTo = jest.fn()
        
        //Act
        windowPositionController.reposition()

        //Assert
        const viewerX = Math.floor(cmsWindowcenterX/resolutionWidth) * resolutionWidth
        const viewerY = 0

        expect(window.moveTo).toHaveBeenCalledTimes(1)
        expect(window.moveTo).toHaveBeenCalledWith(viewerX,viewerY)
    })
})