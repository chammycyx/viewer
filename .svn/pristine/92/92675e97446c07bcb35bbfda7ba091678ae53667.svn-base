// @flow

import React, {Component, type ComponentType} from 'react'
import { connect } from 'react-redux'
import { log } from '../../actions/data/logs/Action'
import { LOGGER_NAME, LOG_LEVEL } from '../../constants/LogConstants'
import createDisplayName from '../../utils/DisplayNameUtil'

const mapStateToProps = (state: *): * => {
    return {}
}

const mapDispatchToProps = (dispatch: *): * => {
	return {
		dispatchLog: (action: *) => {
			dispatch(action)
		}
        
	}
}

/**
 * Logger HOC for React.Component
 * 
 * @export
 * @param {*} WrappedComponent 
 * @returns {*} 
 */
export function loggerForComponent(WrappedComponent: ComponentType<*>): ComponentType<*> {

    type LoggerOwnPropsType = {
        dispatchLog: Function
    }

    class LoggerForComponent extends Component<LoggerOwnPropsType> {
        onLogging: Function
        logTrace: Function
        logDebug: Function
        logInfo: Function
        logWarn: Function
        logError: Function
        logFatal: Function

        constructor(props: *) {
            super(props)
            this.onLogging = this.onLogging.bind(this)
            this.logTrace = this.logTrace.bind(this)
            this.logDebug = this.logDebug.bind(this)
            this.logInfo = this.logInfo.bind(this)
            this.logWarn = this.logWarn.bind(this)
            this.logError = this.logError.bind(this)
            this.logFatal = this.logFatal.bind(this)
        }

        onLogging(level: number, message: string, msgCode?: string){
            this.props.dispatchLog(log(LOGGER_NAME.CID, level, message, msgCode))
        }

        logTrace(message: string){
            this.onLogging(LOG_LEVEL.TRACE, message)
        }

        logDebug(message: string){
            this.onLogging(LOG_LEVEL.DEBUG, message)
        }

        logInfo(message: string){
            this.onLogging(LOG_LEVEL.INFO, message)
        }

        logWarn(message: string){
            this.onLogging(LOG_LEVEL.WARN, message)
        }

        logError(message: string){
            this.onLogging(LOG_LEVEL.ERROR, message)
        }

        logFatal(message: string){
            this.onLogging(LOG_LEVEL.FATAL, message)
        }


        render(): * {
            return( 
                <div>
                    <WrappedComponent {...this.props} 
                    logTrace={this.logTrace}
                    logDebug={this.logDebug}
                    logInfo={this.logInfo}
                    logWarn={this.logWarn}
                    logError={this.logError}
                    logFatal={this.logFatal}
                    />
                </div>
            )
        }
    }

    LoggerForComponent.displayName = createDisplayName('LoggerForComponent', WrappedComponent)

    return connect(mapStateToProps,mapDispatchToProps)(LoggerForComponent)
}

export type LoggerPropsType = {
    logTrace: Function,
    logDebug: Function,
    logInfo: Function,
    logWarn: Function,
    logError: Function,
    logFatal: Function
}

