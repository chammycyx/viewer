import { initApplication } from '@actions/data/common/Action'
//import { caseProfileReader } from '@domains/readers/CaseProfileReader'
import { fileReader } from '@domains/readers/FileReader'
import {getStore, stop} from '@test/dataFlow/DataFlowHelper'
import dataRepository from '@utils/DataRepository'
import { configService } from '@domains/services/configService/ConfigService'
import logService from '@domains/services/logService/LogService'
import StudyService from '@domains/services/studyService/StudyService'
jest.mock('@domains/services/studyService/StudyService')

import { AuthService }  from '@domains/services/authService/AuthService'
jest.mock('@domains/services/authService/AuthService')

import PrerequisiteController from '@domains/controllers/rootApp/PrerequisiteController'

import { initState as canvasGridInitState, CanvasProfileInitState } from '@reducers/components/imageViewingPanel/canvasGrid/Reducer'
import { initState as thumbnailGridInitState } from '@reducers/components/pictorialIndexPanel/thumbnailGrid/Reducer'
import { initState as colorAdjustmentPropsBoxInitState } from '@reducers/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Reducer'

import { ImageTool } from '@constants/ComponentConstants'
import windowPositionController from '@domains/controllers/rootApp/WindowPositionController'

let store = null
beforeEach(() => {
    store = getStore()
})

describe('Test Suite: <Test data flow for InitService#initApplication>',async ()=>{
	test('+++ Verify the update of Case Profile, Study, App Config, and Message Code', async ()=>{
		//Arrange
		const caseProfileTimeToken = new Date().getTime()
		PrerequisiteController.prototype.getCaseProfile = ()=>{
			return {
				systemID: 'RIS',
				accessKey: '1FD2B404-662E-48C2-8351-BDF2C40E8711',
				loginID: 'ISG',
				userHospCde: 'VH',
				wrkStnID: 'dc1cidst02',
				hospCde: 'VH',
				patientKey: '90500231',
				specialtyCde: '',
				caseNo: 'HN05000207X',
				accessionNo: 'VH TY4023659178M',
				seriesSeqNo: '',
				imageSeqNo: '',
				versionNo: '',
				appHost: 'http://localhost:7778',
				appSiteName: '',
				appName: 'CID Image Viewer II',
				wrkStnHostname: 'dc1cidst02',
				sessionID: '358c4ca8-c0a8-4915-ab0a-7fabc475dc2a',
				timeToken: caseProfileTimeToken
			}
		}
		fileReader.readFile = jest.fn()
		fileReader.readFile
		.mockImplementationOnce((path, timeout)=>{
			return new Promise((resolve, reject) => {
				let appLocalConfig = {
					viewerControl: {
						idleTimeout: '20',
						caseProfileTimeout: '60',
						EPRTimeoutMethodName: 'acknowledgeEPRTimeout',
						applicationId: "IV",
						applicationVersion: "6.0.0.1"
					},
					export: {
						imageExportFileName: 'Images.zip',
						imageExportDisclaimer: ''
					},
					imageHeader: {
						fontType: "Arial",
						fontSize: 11,
						fontColor: "white",
						bgColor: "#000000",
						visibility: true,
						rtcExamType: "ES",
						rtcImageCreateDateTime: false
					},
					webServiceIcw: {
						cidWsContextPath: 'http://cid-corp-d1:11360/cid-image-common_6_0_0_2',
						suid1: 'di6nu9/XvFXqdO330PbU4w==',
						suid2: 'N3FoxBETpxV/5afL9IcAaQ=='
					},
					pictorialIndex: {
						numOfColumn: 2,
						isShowSeriesHeading: true,
						serieName: 'Series 1'
					},
					colorAdjustment: {
						maxNumOfImage: 2
					},
					webService: {
						timeout: 5000
					}	
				}
				resolve(appLocalConfig)
			})
		})
		.mockImplementationOnce((path, timeout)=>{
			return new Promise((resolve, reject) => {
				let errMsgCode = {
					errors:{
						3001: {
							title: "CID Service Error",
							content: "EJB Excpetion!"
						},
						3002: {
							"title": "CID Service Error",
							"content": "Failed to invoke the CID WriteLog service\nPlease retry. Please contact system administrator if the sevice is not resume."
						},
						3201: {
							title: "CID Service Error",
							content: "Failed to retrieve configuration files.\nPlease retry. Please contact system administrator if the sevice is not resume."
						},
						4001: {
							title: "CID Service Error",
							content: "Failed to invoke IAS Authentication service.\nPlease retry. Please contact system administrator if the sevice is not resume."
						},
						4002: {
							title: "CID Service Error",
							content: "Failed to invoke ICW Authentication service.\nPlease retry. Please contact system administrator if the sevice is not resume."
						},
						4201: {
							title: "Error: Access Authentication",
							content: "You do not have access right to activate the CID ImageViewer."
						},
						4202: {
							title: "Session Timeout",
							content: "The imageViewer session is terminated."
						},
						4203: {
							title: "Error: Application Version",
							content: "An unmatch version of CID ImageViewer is installed or current version is disabled."
						},
						4204: {
							title: "Error: Missing input parameter",
							content: "Not enough information to process."
						},
						5201: {
							title: "System Warning",
							content: "Window is resized, please stop the slideshow and replay."
						},
						5202: {
							title: "Error: Invalid Case Profile",
							content: "Unable to read case profile."
						},
						5203: {
							title: "System Warning",
							content: "Number of selected images for Window level / contrast adjustment exceeded the limit.\nPlease select less images."
						}
					}
				}
				resolve(errMsgCode)
			})
		})
		

		AuthService.mockImplementation(()=>{
			return{
				validateAccessKeyByHostName:function(){
					return new Promise((resolve, reject) => {
						resolve(true)
					})
				},
				validateApplicationStatus:function(){
					return new Promise((resolve, reject) => {
						resolve(true)
					})
				}
			}
		})
		
		configService.retrieveAppConfig = jest.fn()
		configService.retrieveAppConfig.mockImplementation(()=>{
			return new Promise((resolve, reject) => {
				resolve(
					{
						response:JSON.stringify(
							{
								ImageServiceRS: {
									RS_Password: 'QI7lxc/dCbggsyE1uOHjgg==',
									RS_URL: 'http://dc1cidsd01:8080/axcid-proxy',
									RS_Username: 'QI7lxc/dCbggsyE1uOHjgg=='
								}
							}
						)
					}
				)
			})
		})

		let retrieveStudy = ()=>{
			return new Promise((resolve, reject) => {
				let returnedStudy = {
					messageDtl: {
						transactionCode: 'CID2DATA',
						serverHosp: 'VH',
						transactionDtm: '20180605164634.244',
						transactionID: '4B1AC56C-7CE8-4623-9EB7-A6AC00ADD702',
						sendingApplication: 'CID'
					},
					visitDtl: {
						visitHosp: 'VH',
						visitSpec: '',
						visitWardClinic: '',
						payCode: '',
						caseNum: 'HN05000207X',
						adtDtm: ''
					},
					patientDtl: {
						hkid: 'M7673537',
						patKey: '90500231',
						patName: 'TSANG, AH OI',
						patDOB: '19460107000000.000',
						patSex: 'M',
						deathIndicator: ''
					},
					studyDtl: {
						accessionNo: 'R9MLJ5KPEVED1SMQ',
						studyID: '5953c844-8fe6-4587-9ee3-b898586df6f1',
						studyType: 'ERS',
						studyDtm: '20100131123456',
						remark: 'Remark1',
						seriesDtls: {
							seriesDtl:[
								{
									seriesNo:'f74c17d2-2069-42be-9e20-ab9705ab9189', 
									examType:'HADRW', 
									examDtm:'20150318161644', 
									entityID:'0', 
									imageDtls:{
										imageDtl:[
											{
												imageFormat:'IM', 
												imagePath:'/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG', 
												imageFile:'1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG', 
												imageID:'ff72b811-6ac6-41fd-a414-8dd87ae8089e', 
												imageVersion:'1', 
												imageSequence:'1', 
												imageKeyword:'Name:Bronchoscopy1|', 
												imageHandling:'N', 
												imageType:'JPEG', 
												imageStatus:'1', 
												annotationDtls:{
													annotationDtl:[
														{
															annotationID:'annotationID1', 
															annotationSeq:'annotationSeq1', 
															annotationType:'annotationType1', 
															annotationText:'annotationText1', 
															annotationCoordinate:'annotationCoordinate1', 
															annotationStatus:'annotationStatus1', 
															annotationEditable:'annotationEditable1', 
															annotationUpdDtm:'annotationUpdDtm1'
														},
														{
															annotationID:'annotationID2', 
															annotationSeq:'annotationSeq2', 
															annotationType:'annotationType2', 
															annotationText:'annotationText2', 
															annotationCoordinate:'annotationCoordinate2', 
															annotationStatus:'annotationStatus2', 
															annotationEditable:'annotationEditable2', 
															annotationUpdDtm:'annotationUpdDtm2' 
														}
													]
												}
											},
				
											{
												imageFormat:'IM2', 
												imagePath:'/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/12.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG', 
												imageFile:'1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG', 
												imageID:'ff72b811-6ac6-41fd-a414-8dd87ae8089e', 
												imageVersion:'2', 
												imageSequence:'2', 
												imageKeyword:'Name:Bronchoscopy2|', 
												imageHandling:'Y', 
												imageType:'JPEG2', 
												imageStatus:'2', 
												annotationDtls:{
													annotationDtl:[
														{
															annotationID:'annotationID1.2', 
															annotationSeq:'annotationSeq1.2', 
															annotationType:'annotationType1.2', 
															annotationText:'annotationText1.2', 
															annotationCoordinate:'annotationCoordinate1.2', 
															annotationStatus:'annotationStatus1.2', 
															annotationEditable:'annotationEditable1.2', 
															annotationUpdDtm:'annotationUpdDtm1.2'
														},
														{
															annotationID:'annotationID2.2', 
															annotationSeq:'annotationSeq2.2', 
															annotationType:'annotationType2.2', 
															annotationText:'annotationText2.2', 
															annotationCoordinate:'annotationCoordinate2.2', 
															annotationStatus:'annotationStatus2.2', 
															annotationEditable:'annotationEditable2.2', 
															annotationUpdDtm:'annotationUpdDtm2.2' 
														}
													]
												}
											}
										]
									}
								}
							]
						}
					}
				}
				resolve(returnedStudy)
			})
		}

		let instance = jest.fn()
			instance.retrieveStudy = retrieveStudy
			instance.getMetadataStr = jest.fn()
		StudyService.getStudyService = jest.fn()
		StudyService.getStudyService.mockReturnValue(instance)

		logService.icwWriteLog = jest.fn()
		logService.icwWriteLog.mockImplementation(()=>{
			return new Promise((resolve, reject) => {
				resolve()
			})
		})

		windowPositionController.reposition = jest.fn()

		//Act
		let prevCommonState = store.getState().data.common
		let prevStudyState = store.getState().data.study
		let prevConfigState = store.getState().data.config
		let prevMsgCodeState = store.getState().data.msgCode
		let prevThumbnailGridState = store.getState().components.pictorialIndexPanel.thumbnailGrid
		let prevCanvasGridState = store.getState().components.imageViewingPanel.canvasGrid
		let prevColorAdjustmentPropsBoxState = store.getState().components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox
		dataRepository.setStore(store)
		store.dispatch(initApplication())
		await stop(2000)

		//Assert
		let nextCommonState = store.getState().data.common
		let nextStudyState = store.getState().data.study
		let nextConfigState = store.getState().data.config
		let nextMsgCodeState = store.getState().data.msgCode
		let nextThumbnailGridState = store.getState().components.pictorialIndexPanel.thumbnailGrid
		let nextCanvasGridState = store.getState().components.imageViewingPanel.canvasGrid
		let nextColorAdjustmentPropsBoxState = store.getState().components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox
		
		//assert value updated, assert the prev and next state value could assure that value update by Object.assign
		let expectedNextCommonState = {
			rootState:{
				requestSys: 'RIS',
				accessKey: '1FD2B404-662E-48C2-8351-BDF2C40E8711',
				userId: 'ISG',
				userHospCode: 'VH',
				workstationId: 'dc1cidst02',
				hospCode: 'VH',
				patientKey: '90500231',
				specialtyCode: '',
				caseNo: 'HN05000207X',
				accessionNo: 'VH TY4023659178M',
				seriesNo: '',
				imageSeqNo: '',
				versionNo: '',
				appHost: 'http://localhost:7778',
				appSiteName: '',
				workstationHostname: 'dc1cidst02',
				sessionID: '358c4ca8-c0a8-4915-ab0a-7fabc475dc2a',
				caseProfileTimeToken: caseProfileTimeToken
			}
		}
		expect(nextCommonState).toEqual(expectedNextCommonState)
		let expectedNextConfigState = {
			application:{
				rootState:{
					viewerControl: {
						idleTimeout: '20',
						caseProfileTimeout: '60',
						EPRTimeoutMethodName: 'acknowledgeEPRTimeout',
						applicationId: "IV",
						applicationVersion: "6.0.0.1"
					},
					export: {
						imageExportFileName: 'Images.zip',
						imageExportDisclaimer: ''
					},
					imageHeader: {
						fontType: "Arial",
						fontSize: 11,
						fontColor: "white",
						bgColor: "#000000",
						visibility: true,
						rtcExamType: "ES",
						rtcImageCreateDateTime: false
					},
					webServiceIas: {
						suid2: 'QI7lxc/dCbggsyE1uOHjgg==',
						cidWsContextPath: 'http://dc1cidsd01:8080/axcid-proxy',
						suid1: 'QI7lxc/dCbggsyE1uOHjgg=='
					},
					webServiceIcw: {
						cidWsContextPath: 'http://cid-corp-d1:11360/cid-image-common_6_0_0_2',
						suid1: 'di6nu9/XvFXqdO330PbU4w==',
						suid2: 'N3FoxBETpxV/5afL9IcAaQ=='
					},
					webService: {
						timeout: 5000
					}
				}
			}
		}
		expect(nextConfigState).toEqual(expectedNextConfigState)

		let expectedNextMsgCodeState = {
			rootState:{
				errors:{
					3001: {
						title: "CID Service Error",
						content: "EJB Excpetion!"
					},
					3002: {
						"title": "CID Service Error",
						"content": "Failed to invoke the CID WriteLog service\nPlease retry. Please contact system administrator if the sevice is not resume."
					},
					3201: {
						title: "CID Service Error",
						content: "Failed to retrieve configuration files.\nPlease retry. Please contact system administrator if the sevice is not resume."
					},
					4001: {
						title: "CID Service Error",
						content: "Failed to invoke IAS Authentication service.\nPlease retry. Please contact system administrator if the sevice is not resume."
					},
					4002: {
						title: "CID Service Error",
						content: "Failed to invoke ICW Authentication service.\nPlease retry. Please contact system administrator if the sevice is not resume."
					},
					4201: {
						title: "Error: Access Authentication",
						content: "You do not have access right to activate the CID ImageViewer."
					},
					4202: {
						title: "Session Timeout",
						content: "The imageViewer session is terminated."
					},
					4203: {
						title: "Error: Application Version",
						content: "An unmatch version of CID ImageViewer is installed or current version is disabled."
					},
					4204: {
						title: "Error: Missing input parameter",
						content: "Not enough information to process."
					},
					5201: {
						title: "System Warning",
						content: "Window is resized, please stop the slideshow and replay."
					},
					5202: {
						title: "Error: Invalid Case Profile",
						content: "Unable to read case profile."
					},
					5203: {
						title: "System Warning",
						content: "Number of selected images for Window level / contrast adjustment exceeded the limit.\nPlease select less images."
					}
				}
			}
		}
		expect(nextMsgCodeState).toEqual(expectedNextMsgCodeState)

		let expectedNextStudyState = {
			messageDtl: {
				rootState: {
					transactionCode: 'CID2DATA',
					serverHosp: 'VH',
					transactionDtm: '20180605164634.244',
					transactionID: '4B1AC56C-7CE8-4623-9EB7-A6AC00ADD702',
					sendingApplication: 'CID'
				}
			},
			visitDtl: {
				rootState: {
					visitHosp: 'VH',
					visitSpec: '',
					visitWardClinic: '',
					payCode: '',
					caseNum: 'HN05000207X',
					adtDtm: ''
				}
			},
			patientDtl: {
				rootState: {
					hkid: 'M7673537',
					key: '90500231',
					name: 'TSANG, AH OI',
					dob: '19460107000000.000',
					sex: 'M',
					deathIndicator: ''
				}
			},
			studyDtl:{
				rootState: {
					accessionNo: 'R9MLJ5KPEVED1SMQ',
					studyID: '5953c844-8fe6-4587-9ee3-b898586df6f1',
					studyType: 'ERS',
					studyDtm: '20100131123456',
					remark: 'Remark1',
					seriesDtls: {
						byId: {
							seriesDtl_1: {
								seriesNo: 'f74c17d2-2069-42be-9e20-ab9705ab9189',
								examType: 'HADRW',
								examDtm: '20150318161644',
								entityID: '0',
								imageDtls: {
									byId: {
										imageDtl_1: {
											imageBase64: undefined,
											imageThumbnailBase64: undefined,
											imageFormat: 'IM',
											imagePath: '/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG',
											imageFile: '1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v1.JPEG',
											imageID: 'ff72b811-6ac6-41fd-a414-8dd87ae8089e',
											imageVersion: '1',
											imageSequence: '1',
											imageKeyword: 'Name:Bronchoscopy1|',
											imageHandling: 'N',
											imageType: 'JPEG',
											imageStatus: '1',
											annotationDtls: {
												byId: {
													annotationDtl_1: {
														annotationID: 'annotationID1',
														annotationSeq: 'annotationSeq1',
														annotationType: 'annotationType1',
														annotationText: 'annotationText1',
														annotationCoordinate: 'annotationCoordinate1',
														annotationStatus: 'annotationStatus1',
														annotationEditable: 'annotationEditable1',
														annotationUpdDtm: 'annotationUpdDtm1'
													},
													annotationDtl_2: {
														annotationID: 'annotationID2',
														annotationSeq: 'annotationSeq2',
														annotationType: 'annotationType2',
														annotationText: 'annotationText2',
														annotationCoordinate: 'annotationCoordinate2',
														annotationStatus: 'annotationStatus2',
														annotationEditable: 'annotationEditable2',
														annotationUpdDtm: 'annotationUpdDtm2'
													}
												},
												allIds: ['annotationDtl_1', 'annotationDtl_2']
											}
										},
										imageDtl_2: {
											imageBase64: undefined,
											imageThumbnailBase64: undefined,
											imageFormat: 'IM2',
											imagePath: '/data/data02/cid/936/a5a/a0e/2aa/R9MLJ5KPEVED1SMQ/f74c17d2-2069-42be-9e20-ab9705ab9189/12.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG',
											imageFile: '1.ff72b811-6ac6-41fd-a414-8dd87ae8089e_v2.JPEG',
											imageID: 'ff72b811-6ac6-41fd-a414-8dd87ae8089e',
											imageVersion: '2',
											imageSequence: '2',
											imageKeyword: 'Name:Bronchoscopy2|',
											imageHandling: 'Y',
											imageType: 'JPEG2',
											imageStatus: '2',
											annotationDtls: {
												byId: {
													annotationDtl_1: {
														annotationID: 'annotationID1.2',
														annotationSeq: 'annotationSeq1.2',
														annotationType: 'annotationType1.2',
														annotationText: 'annotationText1.2',
														annotationCoordinate: 'annotationCoordinate1.2',
														annotationStatus: 'annotationStatus1.2',
														annotationEditable: 'annotationEditable1.2',
														annotationUpdDtm: 'annotationUpdDtm1.2'
													},
													annotationDtl_2: {
														annotationID: 'annotationID2.2',
														annotationSeq: 'annotationSeq2.2',
														annotationType: 'annotationType2.2',
														annotationText: 'annotationText2.2',
														annotationCoordinate: 'annotationCoordinate2.2',
														annotationStatus: 'annotationStatus2.2',
														annotationEditable: 'annotationEditable2.2',
														annotationUpdDtm: 'annotationUpdDtm2.2'
													}
												},
												allIds: ['annotationDtl_1', 'annotationDtl_2']
											}
										}
									},
									allIds: ['imageDtl_1', 'imageDtl_2'],
									allHighestVerIds: ['imageDtl_2'],
									idsMapping:{imageDtl_1:"ff72b811-6ac6-41fd-a414-8dd87ae8089e", imageDtl_2:"ff72b811-6ac6-41fd-a414-8dd87ae8089e"}
								}
							}
						},
						allIds: ['seriesDtl_1']
					}
				}
			}
		}
		expect(nextStudyState).toEqual(expectedNextStudyState)

		// rootSaga -> initApplication -> initController -> readLocalAppConfig -> savePictorialIndexConfig
		let expectedThumbnailGridState = {
			rootState: thumbnailGridInitState
		}
		expectedThumbnailGridState.rootState.numOfColumn = 2
		expectedThumbnailGridState.rootState.isShowSeriesHeading = true
		expectedThumbnailGridState.rootState.serieName = 'Series 1'
		expect(nextThumbnailGridState).toEqual(expectedThumbnailGridState)

		// rootSaga -> initApplication -> initController -> readLocalAppConfig -> saveImageHeaderConfig
		let expectedCanvasGridState = {
			rootState: canvasGridInitState
		}
		expect(CanvasProfileInitState.fontType).toEqual("Arial")
		expect(CanvasProfileInitState.fontSize).toEqual(11)
		expect(CanvasProfileInitState.fontColor).toEqual("white")
    	expect(CanvasProfileInitState.bgColor).toEqual("#000000")
		expect(CanvasProfileInitState.headerVisibility).toEqual(true)

		// rootSaga -> initApplication -> initController -> readLocalAppConfig -> saveColorAdjustmentConfig
		let expectedColorAdjustmentPropsBoxState = {
			rootState: colorAdjustmentPropsBoxInitState
		}
		expectedColorAdjustmentPropsBoxState.rootState.maxNumOfImage = 2
		expect(nextColorAdjustmentPropsBoxState).toEqual(expectedColorAdjustmentPropsBoxState)

		// rootSaga -> initApplication -> setImageTool
		expectedCanvasGridState.rootState.imageTool = ImageTool.SELECT
		expect(nextCanvasGridState.rootState.imageTool).toEqual(expectedCanvasGridState.rootState.imageTool)

		//assert immutation takes place in reducer
		expect(prevCommonState.rootState).not.toBe(nextCommonState.rootState)
		expect(prevConfigState.application.rootState).not.toBe(nextConfigState.application.rootState)
		expect(prevMsgCodeState.rootState).not.toBe(nextMsgCodeState.rootState)
		expect(prevStudyState.messageDtl.rootState).not.toBe(nextStudyState.messageDtl.rootState)
		expect(prevStudyState.patientDtl.rootState).not.toBe(nextStudyState.patientDtl.rootState)
		expect(prevStudyState.visitDtl.rootState).not.toBe(nextStudyState.visitDtl.rootState)
		expect(prevStudyState.studyDtl.rootState).not.toBe(nextStudyState.studyDtl.rootState)
		expect(prevCanvasGridState.rootState).not.toBe(nextCanvasGridState.rootState)
		expect(prevThumbnailGridState.rootState).not.toBe(nextThumbnailGridState.rootState)
		expect(prevColorAdjustmentPropsBoxState.rootState).not.toBe(nextColorAdjustmentPropsBoxState.rootState)

		expect(windowPositionController.reposition).toHaveBeenCalledTimes(1)
		
	})
})