import { call, takeEvery, all, take, actionChannel, takeLatest } from 'redux-saga/effects'
import { LOGGER_NAME, LOG_LEVEL } from '@constants/LogConstants'
import { buffers, delay } from 'redux-saga'

// import actions
import { BACK_TO_EPR, ToolBarPanelActionTypes, PRINT_IMAGE } from '@actions/components/toolBarPanel/Action'
import { LOG, LOG_SNAPSHOT } from '@actions/data/logs/Action'
import { CommonActionTypes } from '@actions/data/common/Action'
import { ThumbnailGridActionTypes } from '@actions/components/pictorialIndexPanel/thumbnailGrid/Action'
import { CanvasGridActionTypes } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { ExportImageDialogActionTypes } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'
import { SlideShowToolsActionTypes } from '@actions/components/toolBarPanel/slideShowTools/Action'
import { TransformationToolsActionTypes } from '@actions/components/toolBarPanel/transformationTools/Action'
import { ColorAdjustmentPropsBoxActionTypes } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'
import { PictorialIndexPanelActionTypes } from '@actions/components/pictorialIndexPanel/Action'
import { ViewModeDropDownListActionTypes } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'

// import services/handler
import viewerLogController from '@domains/controllers/ViewerLogController'
//import viewerLogController from '@domains/controllers/rootApp/CommonLogController'
import StudyService from '@domains/services/studyService/StudyService'
import {ViewingPanelController} from '@domains/controllers/ViewingPanelController'
import backToEprController from '@domains/controllers/BackToEprController'
import { SlideShowController } from '@domains/controllers/SlideShowController'
import { PictorialIndexPanelController } from '@domains/controllers/PictorialIndexPanelController'
import { ImagePrintingController } from '@domains/controllers/ImagePrintingController'
import { ButtonStateController } from '@domains/controllers/ButtonStateController'
import AuditLogController from '@domains/controllers/rootApp/AuditLogController'
import WindowResizeController from '@domains/controllers/rootApp/WindowResizeController'
import ExportImageController from '@domains/controllers/ExportImageController'
import ColorAdjustmentController from '@domains/controllers/ColorAdjustmentController'
import {ViewModeController} from '@domains/controllers/ViewModeController'
import {AppInitController} from '@domains/controllers/rootApp/AppInitController'


// import utils
import dataRepository from '@utils/DataRepository'
import logger from '@utils/Logger'

export default function* rootSaga(dispatch) {
    try {
        // Reminder:
        // Please use takeEvery() in "yield all"
        // Do not use take() in "yield all" since suspension of take() will block the following other takeEvery()
        // Ref: https://redux-saga.js.org/docs/api/#alleffects---parallel-effects
        yield all([
            watchLogActions(), 
            watchInitActions() ,
            watchPanelButtonActions(), 
            watchPictorialIndexPanelActions(),
            watchViewingPanelUnorderedActions(),
            watchActionForButtonState(),
            watchPrevButtonActions(),
            watchNextButtonActions()
        ]);
    } catch(ex) {
        logger.error(ex)
    }
}



//Initialization

function* watchInitActions() {
    try{
        yield takeEvery(CommonActionTypes.INIT_APPLICATION, initApplication)
    }catch(ex){
        logger.error(ex)
    }
}

function* initApplication() {
    try {
        let appInitController = new AppInitController()
        yield call(appInitController.initApp)
    } catch(ex) {
        logger.error(ex)
    }
}

//Initialization




//Viewing Panel

function* watchViewingPanelUnorderedActions(){
    try {
        yield takeEvery(CanvasGridActionTypes.ON_IMAGE_ZOOM, handleOnImageZoomAction)
        yield takeEvery(CanvasGridActionTypes.ON_CANVAS_CLICK, handleOnCanvasClick)
		yield takeEvery(CanvasGridActionTypes.ON_IMAGE_MOVED, handleOnImageMoved) 
    } catch(ex) {
        logger.error(ex)
    }
}

function* handleOnImageZoomAction(action){
    try {
        let viewingPanelController = new ViewingPanelController(null, dataRepository)
        yield call(viewingPanelController.zoomImage, action.payload.seriesId, action.payload.imageId, action.payload.zoomMode, action.payload.zoomX, action.payload.zoomY, action.payload.pannedCoordinateRule)
    } catch(ex) {
        logger.error(ex)
    }
}


function* handleOnCanvasClick(action){
    try{
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)

        yield call(viewingPanelController.selectCanvas, action.payload.imageId, action.payload.button)
    } catch(ex) {
        logger.error(ex)
    }
}

function* handleOnImageMoved(action){
    try{
        // let webSInfo = dataRepository.findIasWsAuthInfo()
        // const studyService = new StudyService(webSInfo)
        
        // let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let viewingPanelController = new ViewingPanelController(null, dataRepository)

        yield call(viewingPanelController.moveImage, action.payload.seriesId, action.payload.imageId, action.payload.xCoor, action.payload.yCoor)
    } catch(ex) {
        logger.error(ex)
    }
}

//Viewing Panel


//Logging

function* watchLogActions() {
    try{
        logger.info('testing log')
        yield takeEvery('*', interceptActionForLogging)
        yield takeEvery(LOG, handleLog)
		yield takeLatest(LOG_SNAPSHOT, handleLog)
    }catch(ex){
        logger.error(ex)
    }
}

function* interceptActionForLogging(action) {
    try{
        yield call(viewerLogController.handleLogEx, LOGGER_NAME.CID, LOG_LEVEL.TRACE, JSON.stringify(action))
    }catch(ex){
        logger.error(ex)
    }

}


function handleLog(action){
    try{
        viewerLogController.handleLogEx(action.loggerName, action.level, action.logMsg, action.msgCode)
    }catch(ex){
        logger.error(ex)
    }
}

//Logging


//Tool Bar

function* watchPanelButtonActions(){
    try{
        yield takeEvery(BACK_TO_EPR,backToEprController.backToEpr)
        yield takeEvery(ExportImageDialogActionTypes.REQUEST_EXPORT_IMAGES, exportImage)
        yield takeEvery(SlideShowToolsActionTypes.PLAY, playSlideShow)
        yield takeEvery(SlideShowToolsActionTypes.PAUSE, pauseSlideShow)
        yield takeEvery(SlideShowToolsActionTypes.STOP, stopSlideShow)
        yield takeEvery(ToolBarPanelActionTypes.RESET_IMAGES, handleResetImages)
        yield takeEvery(ToolBarPanelActionTypes.ON_COLOR_ADJUSTMENT_BUTTON_CLICK, handleOnColorAdjustmentButtonClick)
        yield takeEvery(TransformationToolsActionTypes.ROTATE_IMAGE, handleRotateImage)
        yield takeEvery(TransformationToolsActionTypes.FLIP_IMAGE, handleFlipImage)
		yield takeEvery(ColorAdjustmentPropsBoxActionTypes.CHANGE_COLOR_ADJUSTMENT, handleColorAdjustment)
        yield takeEvery(PRINT_IMAGE, printImage)
        yield takeEvery(ViewModeDropDownListActionTypes.ON_VIEW_MODE_CHANGE, handleOnViewModeChange)
        
    }catch(ex){
        logger.error(ex)
    }
}

function* watchPrevButtonActions(){
    try{
        const channel = yield actionChannel(SlideShowToolsActionTypes.SET_PREV, buffers.expanding(10))
        while(true){
            const {type, payload} = yield take(channel)
            yield call(setPrevSlideShow, payload)
            //Make UI responsive
            yield delay(0)
        }
    }catch(ex){
        logger.error(ex)
    }
}

function* watchNextButtonActions(){
    try{
        const channel = yield actionChannel(SlideShowToolsActionTypes.SET_NEXT, buffers.expanding(10))
        while(true){
            const {type, payload} = yield take(channel)
            yield call(setNextSlideShow, payload)
            //Make UI responsive
            yield delay(0)
        }
    }catch(ex){
        logger.error(ex)
    }
}

function* handleRotateImage(action) {
    try{
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        yield call(viewingPanelController.rotateImage, action.payload.rotateDirection)
    }catch(ex){
        logger.error(ex)
    }
}

function* handleFlipImage(action) {
    try{
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        yield call(viewingPanelController.flipImage, action.payload.flipDirection)
    }catch(ex){
        logger.error(ex)
    }
}

function* handleColorAdjustment(action) {
    try{
        let viewingPanelController = new ViewingPanelController(null, dataRepository)
        yield call(viewingPanelController.setColorAdjustment, action.payload.brightness, action.payload.contrast, action.payload.hue, action.payload.saturation, action.payload.invert, action.payload.contrastToolIdx)
    }catch(ex){
        logger.error(ex)
    }
}

function* printImage(action) {
    try{
        let imagePrintingController = new ImagePrintingController()
        yield call(imagePrintingController.printImage, action.payload.printMode)
    }catch(ex){
        logger.error(ex)
    }
}

async function exportImage(action) {
    try {
        let exportImageController = new ExportImageController()
        let auditLogController = new AuditLogController()
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        exportImageController.setStudyService(studyService)
        exportImageController.setAuditLogController(auditLogController)

        await exportImageController.exportImage(action.payload.selectedSeriesId, action.payload.selectedImageIds, action.payload.password)
    }catch(ex){
        logger.error(ex)
    }
}

function* playSlideShow(action) {
    try {
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        let slideShowController = new SlideShowController(pictorialIndexPanelController, viewingPanelController)
        
        yield call(slideShowController.playSlideShow, action.payload.fps)
    } catch(ex) {
        logger.error(ex)
    }
}

function* pauseSlideShow(action) {
    try {
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        let slideShowController = new SlideShowController(pictorialIndexPanelController, viewingPanelController)
        
        yield call(slideShowController.pauseSlideShow)
    } catch(ex) {
        logger.error(ex)
    }
}

function* stopSlideShow(action) {
    try {
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        let slideShowController = new SlideShowController(pictorialIndexPanelController, viewingPanelController)
        let windowResizeController = new WindowResizeController()

        windowResizeController.setViewingPanelController(viewingPanelController)
        slideShowController.setWindowResizeController(windowResizeController)

        yield call(slideShowController.stopSlideShowAndUpdateShowImages)
    } catch(ex) {
        logger.error(ex)
    }
}

function* setPrevSlideShow(action) {
    try {
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        let slideShowController = new SlideShowController(pictorialIndexPanelController, viewingPanelController)
        
        yield call(slideShowController.switchSlide, -1)
    } catch(ex) {
        logger.error(ex)
    }
}

function* setNextSlideShow(action) {
    try {
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        let slideShowController = new SlideShowController(pictorialIndexPanelController, viewingPanelController)
        
        yield call(slideShowController.switchSlide, +1)
    } catch(ex) {
        logger.error(ex)
    }
}

//Tool Bar
// Reset Actions
function* handleResetImages(action) {
    try {
        let viewingPanelController = new ViewingPanelController(null, dataRepository)
        yield call(viewingPanelController.resetSelectedImages, action.payload.selectedSeriesId, action.payload.selectedImageIds)
    } catch(ex) {
        logger.error(ex)
    }
}

function* handleOnColorAdjustmentButtonClick(action) {
    try {
        let colorAdjustmentController = new ColorAdjustmentController()
        yield call(colorAdjustmentController.onColorAdjustmentButtonClick)
    } catch(ex) {
        logger.error(ex)
    }
}

// Pictorial
function* watchPictorialIndexPanelActions(dispatch) {
    try{
        yield takeEvery(ThumbnailGridActionTypes.ON_THUMBNAIL_CLICK, handleThumbnailClick)
		yield takeEvery(PictorialIndexPanelActionTypes.ON_TOGGLE_PANEL_BUTTON_CLICK, handleTogglePictIndexPanel)
    }catch(ex){
        logger.error(ex)
    }
}

function* handleTogglePictIndexPanel(action) {
    try {
        let viewingPanelController = new ViewingPanelController(null, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        yield call(pictorialIndexPanelController.showPanel, action.payload.isHidden)
    } catch(ex) {
        logger.error(ex)
    }
}

function* handleThumbnailClick(action) {
    try {
        let webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        let viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        let pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        let slideShowController = new SlideShowController(pictorialIndexPanelController, viewingPanelController)
        let windowResizeController = new WindowResizeController(dataRepository)

        pictorialIndexPanelController.setSlideShowController(slideShowController)
        pictorialIndexPanelController.setWindowResizeController(windowResizeController)
        windowResizeController.setViewingPanelController(viewingPanelController)
        // slideShowController.setWindowResizeController(windowResizeController)

        yield call(pictorialIndexPanelController.handleThumbnailClick, action.payload.selectedSeriesId, action.payload.selectedFirstImageId)
    } catch(ex) {
        logger.error(ex)
    }
}

// Pictorial

// ButtonControl
function* watchActionForButtonState() {
    try{
        yield takeEvery('*', controlButtonState)
    }catch(ex){
        logger.error(ex)
    }
}

function* controlButtonState(action) {
    try{
        const buttonStateController = new ButtonStateController(action.type, action.payload)
        yield call(buttonStateController.setToolbarBtnState, action.type, action.payload)
    }catch(ex){
        logger.error(ex)
    }
}
// ButtonControl


function* handleOnViewModeChange(action) {
    try{
        const webSInfo = dataRepository.findIasWsAuthInfo()
        const studyService = new StudyService(webSInfo)
        
        const viewingPanelController = new ViewingPanelController(studyService, dataRepository)
        const pictorialIndexPanelController = new PictorialIndexPanelController(viewingPanelController)
        const viewModeController = new ViewModeController(viewingPanelController, pictorialIndexPanelController)
        yield call(viewModeController.changeViewMode, action.payload.viewMode)
    }catch(ex){
        logger.error(ex)
    }
}