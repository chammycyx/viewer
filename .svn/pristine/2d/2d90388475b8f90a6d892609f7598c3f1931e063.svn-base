---
globals:
  process: true
env:
  browser: true
  commonjs: true
  es6: true
  jest: true
  jest/globals: true
extends:
  - eslint:recommended
  - plugin:flowtype/recommended
  - plugin:jest/recommended
  - plugin:react/recommended  
  - plugin:redux-saga/recommended
parser: babel-eslint
parserOptions:
  ecmaFeatures:
    experimentalObjectRestSpread: true
    jsx: true
  sourceType: module
plugins:
- flowtype
- react
- jest
- redux-saga
rules:
  no-unused-vars:
    - 1
    - vars: all
      args: all
  linebreak-style: 0
  flowtype/boolean-style:
    - 2
    - boolean
  flowtype/define-flow-type: 1
  flowtype/delimiter-dangle:
    - 2
    - never
  flowtype/generic-spacing:
    - 2
    - never
  flowtype/no-primitive-constructor-types: 2
  flowtype/no-types-missing-file-annotation: 2

  # Ignore 'any' and 'function' for weak type checking
  flowtype/no-weak-types: [2,{
    "any": false,
    "Function": false
  }]
  
  flowtype/object-type-delimiter:
    - 2
    - comma
  flowtype/require-parameter-type: 2
  flowtype/require-return-type:
    - 2
    - always
    - annotateUndefined: never
  flowtype/require-valid-file-annotation: 2
  flowtype/semi:
    - 2
    - never
  flowtype/space-after-type-colon:
    - 2
    - always
  flowtype/space-before-generic-bracket:
    - 2
    - never
  flowtype/space-before-type-colon:
    - 2
    - never
  flowtype/type-id-match:
    - 2
    - "^([A-Z][a-z0-9]+)+Type$"
  flowtype/union-intersection-spacing:
    - 2
    - always
  flowtype/use-flow-type: 1
  flowtype/valid-syntax: 1
  # Suppose it works for ignore the specific propsType but it doesn't
  # react/prop-types: 0 
  # react/prop-types: ["disabled", {ignore:['logInfo']}]
  jest/no-disabled-tests: warn
  jest/no-focused-tests: error
  jest/no-identical-title: error
  jest/prefer-to-have-length: warn
  jest/valid-expect: error
settings:
  flowtype:
    onlyFilesWithFlowAnnotation: true