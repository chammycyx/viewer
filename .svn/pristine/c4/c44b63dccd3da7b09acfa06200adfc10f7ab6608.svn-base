// @flow
import React, { Component } from 'react'
import type { Node } from 'react'
import cidComponent, { type CidComponentPropsType } from '@components/base/CidComponent'
import LinearProgress from '@material-ui/core/LinearProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'

type AppMaskPropsType = CidComponentPropsType & {
    appMaskData: *
}

export const styles: Function = function(theme: *): * {
	const DIALOG_WIDTH = '600px'
	const DIALOG_HEIGHT = '110px'

	return {
        div: {
            position: 'fixed', 
            display: function(props: *): string {
                return props.appMaskData.isMasked ? 'block' : 'none'
            },
            width: '100%', 
            height: '100%', 
            top: 0, 
            left: 0, 
            right: 0, 
            bottom: 0, 
            backgroundColor: 'rgba(40, 40, 40, 0.9)', 
            zIndex: 999
        },
        dialog: {
            cursor: function(props: *): string {
                return props.appMaskData.isProgressing ? 'wait' : 'default'
            },
            fontSize: 16
        },
        content: {
            // width: "500px"
        },
        paper: {
            width: DIALOG_WIDTH,
			minWidth: DIALOG_WIDTH,
			maxWidth: DIALOG_WIDTH,
			height: DIALOG_HEIGHT,
			minHeight: DIALOG_HEIGHT,
			maxHeight: DIALOG_HEIGHT
        }
    }
}

class AppMask extends Component<AppMaskPropsType> {

    constructor(props: *) {
        super(props)
    }

    render(): Node {
        const classes = this.props.classes
        return (
            <div id="overlay" className={classes.div}>
                <Dialog
                    open={this.props.appMaskData.isProgressing}
                    classes={{root: classes.dialog, paper: classes.paper}}>
                    <DialogTitle>
						<span>{this.props.appMaskData.description}</span>
					</DialogTitle>
                    <DialogContent className={classes.content}>
                        <div>
                            <LinearProgress  />
                        </div>
                    </DialogContent>
                </Dialog>
            </div>
        )
    }
}

export default cidComponent(styles)(AppMask)