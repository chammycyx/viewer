import 'babel-polyfill' // absent -> Uncaught ReferenceError: regeneratorRuntime is not defined

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'

import createSagaMiddleware from 'redux-saga'

import RootAppContainer from '@components/RootAppContainer'
import RootReducer from '@reducers/RootReducer'

import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import createGenerateClassName from '@material-ui/core/styles/createGenerateClassName'
import cidTheme from '@styles/CidTheme'

import JssProvider from 'react-jss/lib/JssProvider'

// import the Fabric.js library
import { fabric } from 'fabric'

import logger from '@utils/Logger'
import dataRespository from '@utils/DataRepository'

import rootSaga from './src/main/sagas/rootSaga'

// import actions
import { initApplication } from '@actions/data/common/Action'

// import version number
import { getVersion } from '@utils/VersionUtil'

// fixed codes for Saga
const sagaMiddleware = createSagaMiddleware()
let store = createStore(RootReducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga, store.dispatch)

// JSS
const generateClassName = createGenerateClassName()

//init
logger.setDispatcher(store.dispatch)
dataRespository.setStore(store)
store.dispatch(initApplication())
fabric.filterBackend = fabric.initFilterBackend()

//update window title
window.document.title+=" ("+getVersion()+")"

let rootElement = document.getElementById('root-element')

ReactDOM.render(
    <Provider store={store}>
        <JssProvider generateClassName={generateClassName}>
            <MuiThemeProvider theme={createMuiTheme(cidTheme)}>
                <RootAppContainer />
            </MuiThemeProvider>
        </JssProvider>
    </Provider>
    ,
    rootElement
)
