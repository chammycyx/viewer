import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import ShallowRenderer from 'react-test-renderer/shallow'
import configureStore from 'redux-mock-store'
import { createMuiTheme } from '@material-ui/core/styles'
import cidTheme from '@styles/CidTheme'

import { VIEW_MODES } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action'
import { RotateDirections, FlipDirections } from '@actions/components/imageViewingPanel/canvasGrid/Action'
import { PRINT_MODE } from '@actions/components/toolBarPanel/Action'
import ToolBarPanel, { styles } from '@components/toolBarPanel/ToolBarPanel'
import ToolBarPanelContainer from '@components/toolBarPanel/ToolBarPanelContainer'

import { onViewModeChange } from '@actions/components/toolBarPanel/viewModeTools/viewModeDropDownList/Action.js'
import { changeColorAdjustment } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'
import { rotateImage, flipImage } from '@actions/components/toolBarPanel/transformationTools/Action'
import { setVisible as setColorAdjustmentPropsBoxVisible } from '@actions/components/toolBarPanel/transformationTools/propsBox/colorAdjustmentPropsBox/Action'
import { setVisible as setExportImageDialogVisible , setDownload , requestExportImages } from '@actions/components/toolBarPanel/fileTools/dialogs/exportImageDialog/Action'
import { play as playSlideShow, pause as pauseSlideShow, stop as stopSlideShow, setPrev as setSlideShowPrev, setNext as setSlideShowNext } from '@actions/components/toolBarPanel/slideShowTools/Action'
import { setFps } from '@actions/components/toolBarPanel/slideShowTools/fpsSelector/Action'
import { backToEpr, resetImages, setTool, printImage, onColorAdjustmentButtonClick } from '@actions/components/toolBarPanel/Action'

import {ImageTool} from '@constants/ComponentConstants'

configure({ adapter: new Adapter() })

describe('Test Suite: <ToolBarPanelContainer>', () => {
    // Declare testing state
    const initialState1 = {
        components: {
            rootState: {
                appWidth: 640
            },
            toolBarPanel: {
                rootState:{
                    btnState: {
                        deselectBtnState: true,
                        zoomInBtnState: true,
                        zoomOutBtnState: true,
                        viewModeBtnState: true,
                        rotateLeftBtnState: true,
                        rotateRightBtnState: true,
                        flipVerticalBtnState: true,
                        flipHorizonalBtnState: true,
                        contrastBtnState: true,
                        previousBtnState: false,
                        nextBtnState: false,
                        pauseBtnState: false,
                        stopBtnState: false,
                        playBtnState: true,
                        fpsBtnState: true,
                        resetBtnState: true,
                        exportBtnState: true,
                        printBtnState: true,
                        backToEPRBtnState: true
                    }
                },
                viewModeTools: {
                    viewModeDropDownList: {
                        rootState: {
                            viewMode: VIEW_MODES.ONE_X_ONE
                        }
                    }
                },
                transformationTools: {
                    propsBox: {
                        colorAdjustmentPropsBox: {
                            rootState: {
                                visible: false
                            }
                        }
                    }
                },
                fileTools: {
                    dialogs: {
                        exportImageDialog: {
                            rootState: {
                                isVisible: false,
                                isRequesting: false,
                                isDownload: false,
                                title: "DISCLAIMER",
                                agreementLabel: "I have read and agreed to the above terms and condition.",
                                passwordQueryLabel: "Please input a security password to encrypt and decrypt the exported image(s)",
                                passwordLabel: "Password",
                                password2Label: "Confirm Password",
                                requestingMsg: "Please wait while requesting for image download",
                                exportImageBase64: ""
                            }
                        }
                    }
                },
                slideShowTools: {
                    rootState: {
                        isModeOn: false,
                        isPlaying: false,
                        timer: null
                    },
                    fpsSelector: {
                        rootState: {
                            fps: 1
                        }
                    }
                }
            },
            imageViewingPanel: {
                canvasGrid: {
                    rootState: {
                        width: 20,
                        height: 20,
                        viewMode: VIEW_MODES.ONE_X_ONE,
                        selectedSeriesId: '',
                        showImageIds: [],
                        selectedImageIds: [],
                        imageDlStatus: '',
                        caseNo: '',
                        studyDateTime: '',
                        canvasProfiles: {
                            byId: {},
                            allIds: []
                        }
                    }
                }
            }
        },
        data: {
            config: {
                application: {
                    rootState: {
                        export: {
                            imageExportFileName: 'Images.zip',
                            imageExportDisclaimer: "<b>Exported image(s) may contain sensitive patient information.</b><br><ul><li>Exported images should only be used for justified clinical purposes.</li><li>Please remove patient information from the image(s) and store in encrypted media before any use</li><li>Please use security password to encrypt and decrypt the exported image(s). \"7-Zip\" should be used to open the encrypted file <a target=\"_blank\" rel=\"noopener noreferrer\" href=\"http://infosec.home/SecurityTopics/DataEncryption/7ZipUserGuide.aspx?Menu=CommonEncryptionSoftware&Lang=en-US\">7-Zip User Guide</a></li></ul>"
                        }
                    }
                }
            }
        }
    }

    const initialState2 = {
        components: {
            rootState: {
                appWidth: 100
            },
            toolBarPanel: {
                rootState:{
                    btnState: {
                        deselectBtnState: false,
                        zoomInBtnState: false,
                        zoomOutBtnState: false,
                        viewModeBtnState: false,
                        rotateLeftBtnState: false,
                        rotateRightBtnState: false,
                        flipVerticalBtnState: false,
                        flipHorizonalBtnState: false,
                        contrastBtnState: false,
                        previousBtnState: true,
                        nextBtnState: true,
                        pauseBtnState: false,
                        stopBtnState: true,
                        playBtnState: true,
                        fpsBtnState: true,
                        resetBtnState: false,
                        exportBtnState: false,
                        printBtnState: false,
                        backToEPRBtnState: true
                    }
                },
                viewModeTools: {
                    viewModeDropDownList: {
                        rootState: {
                            viewMode: VIEW_MODES.TWO_X_THREE
                        }
                    }
                },
                transformationTools: {
                    propsBox: {
                        colorAdjustmentPropsBox: {
                            rootState: {
                                visible: false
                            }
                        }
                    }
                },
                fileTools: {
                    dialogs: {
                        exportImageDialog: {
                            rootState: {
                                isVisible: true,
                                isRequesting: true,
                                isDownload: true,
                                title: "DISCLAIMER2",
                                agreementLabel: "XXXI have read and agreed to the above terms and condition.",
                                passwordQueryLabel: "XXXPlease input a security password to encrypt and decrypt the exported image(s)",
                                passwordLabel: "XXXPassword",
                                password2Label: "XXXConfirm Password",
                                requestingMsg: "XXXPlease wait while requesting for image download",
                                exportImageBase64: ""
                            }
                        }
                    }
                },
                slideShowTools: {
                    rootState: {
                        isModeOn: false,
                        isPlaying: false,
                        timer: null
                    },
                    fpsSelector: {
                        rootState: {
                            fps: 1
                        }
                    }
                }
            },
            imageViewingPanel: {
                canvasGrid: {
                    rootState: {
                        width: 30,
                        height: 30,
                        viewMode: VIEW_MODES.TWO_X_THREE,
                        selectedSeriesId: '',
                        showImageIds: ['ImageId_1', 'ImageId_2'],
                        selectedImageIds: ['ImageId_1', 'ImageId_2'],
                        imageDlStatus: '',
                        caseNo: '',
                        studyDateTime: '',
                        canvasProfiles: {
                            byId: {
                                'imageId_1': {
                                    header: '',
                                    imageBase64: '',
                                    fabricProfile: {},
                                    brightness: 0,
                                    contrast: 0,
                                    hue: 0,
                                    saturation: 0,
                                    invert: false,
                                    zoomValue: 100,
                                    imageXCoor: null,
                                    imageYCoor: null,
                                    imageWidth: null,
                                    imageHeight: null,
                                    rotateAngle: 0,
                                    flipHorizontal: false,
                                    flipVertical: false,
                                    fontType: '',
                                    fontSize: 0,
                                    fontColor: '',
                                    bgColor: '',
                                    headerVisibility: false
                                },
                                'imageId_2': {
                                    header: '',
                                    imageBase64: '',
                                    fabricProfile: {},
                                    brightness: 20,
                                    contrast: 30,
                                    hue: 10,
                                    saturation: 0,
                                    invert: true,
                                    zoomValue: 100,
                                    imageXCoor: null,
                                    imageYCoor: null,
                                    imageWidth: null,
                                    imageHeight: null,
                                    rotateAngle: 0,
                                    flipHorizontal: false,
                                    flipVertical: false,
                                    fontType: '',
                                    fontSize: 0,
                                    fontColor: '',
                                    bgColor: '',
                                    headerVisibility: false
                                }
                            },
                            allIds: ['ImageId_1', 'ImageId_2']
                        }
                    }
                }
            }
        },
        data: {
            config: {
                application: {
                    rootState: {
                        export: {
                            imageExportFileName: 'Exports.zip',
                            imageExportDisclaimer: "<b>Exported image(s) may not contain sensitive patient information.</b><br><ul><li>Exported images should not only be used for justified clinical purposes.</li><li>Please do not remove patient information from the image(s) and not store in encrypted media before any use</li><li>Please do not use security password to encrypt and decrypt the exported image(s). \"7-Zip\" should not be used to open the encrypted file <a target=\"_blank\" rel=\"noopener noreferrer\" href=\"http://infosec.home/SecurityTopics/DataEncryption/7ZipUserGuide.aspx?Menu=CommonEncryptionSoftware&Lang=en-US\">7-Zip User Guide</a></li></ul>"
                        }
                    }
                }
            }
        }
    }
    
    //For action dispatchers testing
    

    // Mock store
    const mockStore = configureStore()
    const store1 = mockStore(initialState1)
    const store2 = mockStore(initialState2)

    // Create Material UI theme
    const theme = createMuiTheme(cidTheme)

    // Create JSS classes
    const classes = styles(theme)

    // The node to be tested
    const createNode = function(store){
        return <ToolBarPanelContainer
            store={store}
            theme={theme}
            classes={classes}
        />
    }
    const node1 = createNode(store1)
    const node2 = createNode(store2)

    // Shallow render the wrapped component
    const wrapper1 = shallow(node1)
    const wrapper2 = shallow(node2)
    

    test('+++ Verify the container component rendered', () => {
        expect(wrapper1).toHaveLength(1)
    })

    const createTest = function (wrapper, store, viewMode, rotateDirection, flipDirection, 
            colorAdjustmentPropsBoxVisible, brightness, contrast, hue, saturation, invert, 
            exportImageDialogVisible, isDownload, downloadString, selectedSeriesId, 
            selectedImageIds, password, imageTool, fps) {

                let toolBarPanelContainer = wrapper.find(ToolBarPanel)
                toolBarPanelContainer.props().onViewModeChange(viewMode)
                toolBarPanelContainer.props().backToEpr()
                toolBarPanelContainer.props().onRotateButtonClick(rotateDirection)
                toolBarPanelContainer.props().onFlipButtonClick(flipDirection)
                toolBarPanelContainer.props().setColorAdjustmentPropsBoxVisible(colorAdjustmentPropsBoxVisible)
                toolBarPanelContainer.props().onColorAdjustmentChange(brightness, contrast, hue, saturation, invert)
                toolBarPanelContainer.props().setExportImageDialogVisible(exportImageDialogVisible)
                toolBarPanelContainer.props().setDownload(isDownload, downloadString)
                toolBarPanelContainer.props().onExport(selectedSeriesId, selectedImageIds, password)
                toolBarPanelContainer.props().onReset(selectedSeriesId, selectedImageIds)
                toolBarPanelContainer.props().setTool(imageTool)
                toolBarPanelContainer.props().setFps(fps)
                toolBarPanelContainer.props().playSlideShow(fps)
                toolBarPanelContainer.props().pauseSlideShow()
                toolBarPanelContainer.props().setSlideShowPrev()
                toolBarPanelContainer.props().setSlideShowNext()
                toolBarPanelContainer.props().stopSlideShow()
                toolBarPanelContainer.props().onPrintButtonClick(PRINT_MODE.PRINT_SELECTED_IMAGES)
                
                const actions = store.getActions()
                expect(actions).toEqual([
                    onViewModeChange(viewMode), 
                    backToEpr(),
                    rotateImage(rotateDirection),
                    flipImage(flipDirection),
                    setColorAdjustmentPropsBoxVisible(colorAdjustmentPropsBoxVisible),
                    changeColorAdjustment(brightness, contrast, hue, saturation, invert),
                    setExportImageDialogVisible(exportImageDialogVisible),
                    setDownload(isDownload, downloadString),
                    requestExportImages(selectedSeriesId, selectedImageIds, password),
                    resetImages(selectedSeriesId, selectedImageIds),
                    setTool(imageTool),
                    setFps(fps),
                    playSlideShow(fps),
                    pauseSlideShow(),
                    setSlideShowPrev(),
                    setSlideShowNext(),
                    stopSlideShow(),
                    printImage(PRINT_MODE.PRINT_SELECTED_IMAGES)
                ])
    }

    test('+++ Verify if there is any hard-code output of the action dispatchers - Case 1', () => {
        createTest(wrapper1, store1, VIEW_MODES.TWO_X_TWO, RotateDirections.LEFT, FlipDirections.HORIZONTAL, true, 0, 0, 0, 0, false, false, false, "abcde", "1234567", ["image_id_1", "image_id_2"], "abc123", ImageTool.ZOOM_IN, 4)
    })

    test('+++ Verify if there is any hard-code output of the action dispatchers - Case 2', () => {
        createTest(wrapper2, store2, VIEW_MODES.THREE_X_THREE, RotateDirections.RIGHT, FlipDirections.VERTICAL, false, 10, 20, 30, 40, true,  true, true, "12345", "abcdefg", ["image_id_5", "image_id_7", "image_id_8"], "1234abc", ImageTool.ZOOM_OUT, 6.2)
    })
    

    test('+++ Verify if there is any hard-code state - Case 1', () => {
        let toolBarPanelContainer = wrapper1.find(ToolBarPanel)
        expect(toolBarPanelContainer.props().viewMode).toEqual(initialState1.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode)
        expect(toolBarPanelContainer.props().canvasGrid).toEqual(initialState1.components.imageViewingPanel.canvasGrid.rootState)
        expect(toolBarPanelContainer.props().exportImageDialogData).toEqual(initialState1.components.toolBarPanel.fileTools.dialogs.exportImageDialog.rootState)
        expect(toolBarPanelContainer.props().exportConfig).toEqual(initialState1.data.config.application.rootState.export)
        expect(toolBarPanelContainer.props().colorAdjustmentPropsBox).toEqual(initialState1.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState)
        expect(toolBarPanelContainer.props().slideShow).toEqual(initialState1.components.toolBarPanel.slideShowTools.rootState)
        expect(toolBarPanelContainer.props().fps).toEqual(initialState1.components.toolBarPanel.slideShowTools.fpsSelector.rootState.fps)
        expect(toolBarPanelContainer.props().btnState).toEqual(initialState1.components.toolBarPanel.rootState.btnState)
    })

    test('+++ Verify if there is any hard-code state - Case 2', () => {
        let toolBarPanelContainer = wrapper2.find(ToolBarPanel)
        expect(toolBarPanelContainer.props().viewMode).toEqual(initialState2.components.toolBarPanel.viewModeTools.viewModeDropDownList.rootState.viewMode)
        expect(toolBarPanelContainer.props().canvasGrid).toEqual(initialState2.components.imageViewingPanel.canvasGrid.rootState)
        expect(toolBarPanelContainer.props().exportImageDialogData).toEqual(initialState2.components.toolBarPanel.fileTools.dialogs.exportImageDialog.rootState)
        expect(toolBarPanelContainer.props().exportConfig).toEqual(initialState2.data.config.application.rootState.export)
        expect(toolBarPanelContainer.props().colorAdjustmentPropsBox).toEqual(initialState2.components.toolBarPanel.transformationTools.propsBox.colorAdjustmentPropsBox.rootState)
        expect(toolBarPanelContainer.props().slideShow).toEqual(initialState2.components.toolBarPanel.slideShowTools.rootState)
        expect(toolBarPanelContainer.props().fps).toEqual(initialState2.components.toolBarPanel.slideShowTools.fpsSelector.rootState.fps)
        expect(toolBarPanelContainer.props().btnState).toEqual(initialState2.components.toolBarPanel.rootState.btnState)
    })
    
    //Temporary comment to bypass the validation 
    test('+++ Verify the snapshot', () => {
        const renderer = new ShallowRenderer()
        renderer.render(node1)
        const result = renderer.getRenderOutput()
        expect(result).toMatchSnapshot()
    })
})